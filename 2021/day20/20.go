package main

import (
	_ "embed"
	"fmt"
	"math"
	"strings"
)

type Image struct {
	pixels        [][]bool
	externalPixel bool
}

func NewImage(width, height int) *Image {
	var im Image
	for i := 0; i < height; i++ {
		row := make([]bool, width)
		im.pixels = append(im.pixels, row)
	}
	return &im
}

func (im *Image) Extend() {
	newImage := NewImage(len(im.pixels[0])+4, len(im.pixels)+4)
	newImage.Fill(im.externalPixel)
	for i := 0; i < len(im.pixels); i++ {
		for j := 0; j < len(im.pixels[i]); j++ {
			newImage.pixels[i+2][j+2] = im.pixels[i][j]
		}
	}
	im.pixels = newImage.pixels
}

func (im *Image) Fill(value bool) {
	for i := 0; i < len(im.pixels); i++ {
		for j := 0; j < len(im.pixels[i]); j++ {
			im.pixels[i][j] = value
		}
	}
}

func boolToInt(boolArray [9]bool) int {

	index := 0
	for k := 8; k >= 0; k-- {
		if boolArray[k] {
			index += int(math.Pow(2, float64(8-k)))
		}
	}
	return index
}

func (im *Image) Enhance(iea string) {
	im.Extend()
	targetImage := NewImage(len(im.pixels[0]), len(im.pixels))
	for i := 0; i < len(im.pixels); i++ {
		for j := 0; j < len(im.pixels[i]); j++ {
			window := [9]bool{
				im.Pixel(i-1, j-1), im.Pixel(i-1, j), im.Pixel(i-1, j+1),
				im.Pixel(i, j-1), im.Pixel(i, j), im.Pixel(i, j+1),
				im.Pixel(i+1, j-1), im.Pixel(i+1, j), im.Pixel(i+1, j+1),
			}
			index := boolToInt(window)
			if iea[index] == '#' {
				targetImage.pixels[i][j] = true
			}
		}
	}
	im.pixels = targetImage.pixels
	infiniteWindow := [9]bool{im.externalPixel, im.externalPixel, im.externalPixel, im.externalPixel, im.externalPixel, im.externalPixel, im.externalPixel, im.externalPixel, im.externalPixel}
	infiniteIndex := boolToInt(infiniteWindow)
	im.externalPixel = iea[infiniteIndex] == '#'
}

func (im *Image) CountLitPixels() int {
	sum := 0
	for i := 0; i < len(im.pixels); i++ {
		for j := 0; j < len(im.pixels[i]); j++ {
			if im.pixels[i][j] {
				sum++
			}
		}
	}
	return sum
}

func (im *Image) Pixel(x, y int) bool {
	if x < 0 || x >= len(im.pixels[0]) {
		return im.externalPixel
	}
	if y < 0 || y >= len(im.pixels) {
		return im.externalPixel
	}
	return im.pixels[x][y]
}

func (im *Image) SetPixel(x, y int, value bool) {
	im.pixels[x][y] = value
}

func (im *Image) String() string {
	output := ""
	for i := 0; i < len(im.pixels); i++ {
		for j := 0; j < len(im.pixels[i]); j++ {
			pixel := im.pixels[i][j]
			if pixel {
				output += "#"
				continue
			}

			output += "."
		}
		output += "\n"
	}
	return output
}

func loadImage(input string) (im *Image, iea string) {
	lines := strings.Split(strings.TrimSpace(input), "\n")
	iea = lines[0]
	imageData := lines[2:]
	height := len(imageData)
	width := len(imageData[0])
	im = NewImage(width, height)
	for i := 0; i < height; i++ {
		for j := 0; j < width; j++ {
			im.SetPixel(i, j, imageData[i][j] == '#')
		}
	}
	return
}

//go:embed input_20.txt
var input string

func main() {
	im, iea := loadImage(input)
	im.Enhance(iea)
	im.Enhance(iea)
	fmt.Println("Part 1:", im.CountLitPixels())

	im, iea = loadImage(input)
	for i := 0; i < 50; i++ {
		im.Enhance(iea)
	}
	fmt.Println("Part 2:", im.CountLitPixels())
}
