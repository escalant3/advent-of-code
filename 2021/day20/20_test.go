package main

import (
	_ "embed"
	"testing"
)

//go:embed input_20_test.txt
var testInput string

func TestSample(t *testing.T) {
	im, iea := loadImage(testInput)
	im.Enhance(iea)
	im.Enhance(iea)
	if r := im.CountLitPixels(); r != 35 {
		t.Errorf("expected 35; got %d", r)
	}

	im, iea = loadImage(testInput)
	for i := 0; i < 50; i++ {
		im.Enhance(iea)
	}

	if r := im.CountLitPixels(); r != 3351 {
		t.Errorf("expected 3351; got %d", r)
	}
}
