package main

import (
	_ "embed"
	"fmt"
	"strconv"
	"strings"
)

//go:embed input_01.txt
var input string

func part1(measurements []int) int {
	increases := 0
	for i := 1; i < len(measurements); i++ {
		if measurements[i] > measurements[i-1] {
			increases++
		}
	}

	return increases
}

func part2(measurements []int) int {
	var windows []int
	for i := 0; i < len(measurements)-2; i++ {
		windows = append(windows, (measurements[i] + measurements[i+1] + measurements[i+2]))
	}

	return part1(windows)
}

func main() {
	measurements_str := strings.Fields(input)
	measurements := make([]int, len(measurements_str))
	for i, m := range measurements_str {
		measurements[i], _ = strconv.Atoi(m)
	}

	increasesPart1 := part1(measurements)
	fmt.Printf("There are %v increases\n", increasesPart1)

	increasesPart2 := part2(measurements)
	fmt.Printf("There are %v increases\n", increasesPart2)
}
