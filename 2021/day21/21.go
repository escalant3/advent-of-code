package main

import (
	"fmt"
	"strconv"
	"strings"
)

func part1(p1position, p2position int) int {
	var p1Pos, p2Pos, p1Score, p2Score int
	p1Pos = p1position - 1
	p2Pos = p2position - 1
	deterministicDie := 1

	for {
		step := deterministicDie + deterministicDie + 1 + deterministicDie + 2
		p1Pos = (p1Pos + step) % 10
		p1Score += p1Pos + 1

		deterministicDie += 3

		if p1Score >= 1000 {
			return p2Score * (deterministicDie - 1)
		}

		step = deterministicDie + deterministicDie + 1 + deterministicDie + 2
		p2Pos = (p2Pos + step) % 10
		p2Score += p2Pos + 1

		deterministicDie += 3

		if p2Score >= 1000 {
			return p1Score * (deterministicDie - 1)
		}
	}
}

func getDataFromKey(s string) (pos1 int, pos2 int, score1 int, score2 int) {
	keys := strings.Split(s, "_")
	pos1, _ = strconv.Atoi(keys[0])
	pos2, _ = strconv.Atoi(keys[1])
	score1, _ = strconv.Atoi(keys[2])
	score2, _ = strconv.Atoi(keys[3])
	return
}

func getKeyFromData(pos1, pos2, score1, score2 int) string {
	return fmt.Sprintf("%d_%d_%d_%d", pos1, pos2, score1, score2)
}

func part2(p1position, p2position int) int {
	p1Wins := 0
	p2Wins := 0
	tracker := make(map[string]int)

	tracker[getKeyFromData(p1position-1, p2position-1, 0, 0)] = 1

	perTurnUniverses := make(map[int]int)
	for i := 1; i <= 3; i++ {
		for j := 1; j <= 3; j++ {
			for k := 1; k <= 3; k++ {
				perTurnUniverses[i+j+k]++
			}
		}
	}

	for i := 0; i < 10; i++ {
		newScores := make(map[string]int)
		for value, universes := range perTurnUniverses {
			for key, count := range tracker {
				pos1, pos2, score1, score2 := getDataFromKey(key)
				pos1 = (pos1 + value) % 10
				score1 = score1 + pos1 + 1
				count *= universes
				if score1 >= 21 {
					p1Wins += count
					continue
				}
				newScores[getKeyFromData(pos1, pos2, score1, score2)] += count
			}
		}
		tracker = newScores

		newScores = make(map[string]int)
		for value, universes := range perTurnUniverses {
			for key, count := range tracker {
				pos1, pos2, score1, score2 := getDataFromKey(key)
				pos2 = (pos2 + value) % 10
				score2 = score2 + pos2 + 1
				count *= universes
				if score2 >= 21 {
					p2Wins += count
					continue
				}
				newScores[getKeyFromData(pos1, pos2, score1, score2)] += count
			}
		}
		tracker = newScores

		if len(tracker) == 0 {
			break
		}
	}

	fmt.Println(p1Wins, p2Wins)
	if p1Wins > p2Wins {
		return p1Wins
	}
	return p2Wins
}

func main() {
	fmt.Println("Part 1:", part1(3, 5))
	fmt.Println("Part 2:", part2(3, 5))
}
