package main

import "testing"

func TestSample(t *testing.T) {
	r := part1(4, 8)
	expected := 739785
	if r != expected {
		t.Errorf("expected: %d; got: %d", expected, r)
	}
}

func TestSample2(t *testing.T) {
	r := part2(4, 8)
	expected := 444356092776315
	if r != expected {
		t.Errorf("expected: %d; got: %d", expected, r)
	}
}
