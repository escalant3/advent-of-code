package main

import (
	_ "embed"
	"testing"
)

func TestSplit(t *testing.T) {
	cases := map[int]Pair{
		10: {5, 5},
		11: {5, 6},
	}
	for input, expectation := range cases {
		r := split(input)
		if r != expectation {
			t.Errorf("expected %v; got %v", expectation, r)
		}
	}
}

func TestGetNextFourthNestedPair(t *testing.T) {
	cases := map[string][]int{
		"[[[[[9,8],1],2],3],4]":                 {0, 1},
		"[7,[6,[5,[4,[3,2]]]]]":                 {4, 5},
		"[[6,[5,[4,[3,2]]]],1]":                 {3, 4},
		"[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]": {3, 4},
	}
	for input, expectation := range cases {
		r := getNextFourthNestedPairPosition(NewSnailNumbers(input), 0)
		if r[0] != expectation[0] || r[1] != expectation[1] {
			t.Errorf("expected %v; got %v", expectation, r)
		}
	}
	r := getNextFourthNestedPairPosition(NewSnailNumbers("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"), 5)
	e := []int{7, 8}
	if r[0] != e[0] || r[1] != e[1] {
		t.Errorf("expected %v; got %v", e, r)
	}

	r = getNextFourthNestedPairPosition(NewSnailNumbers("[[1,2],[3,4]]"), 0)
	if r != nil {
		t.Errorf("expected nil; got %v", r)
	}
}

func isSameSnailNumber(a, b []SnailNumber) bool {
	for i := range a {
		if a[i].value != b[i].value || a[i].depth != b[i].depth {
			return false
		}
	}
	return true
}

func TestParseSnailNumber(t *testing.T) {
	cases := map[string][]SnailNumber{
		"[[[[[9,8],1],2],3],4]":                 {{9, 5}, {8, 5}, {1, 4}, {2, 3}, {3, 2}, {4, 1}},
		"[7,[6,[5,[4,[3,2]]]]]":                 {{7, 1}, {6, 2}, {5, 3}, {4, 4}, {3, 5}, {2, 5}},
		"[[6,[5,[4,[3,2]]]],1]":                 {{6, 2}, {5, 3}, {4, 4}, {3, 5}, {2, 5}, {1, 1}},
		"[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]": {{3, 2}, {2, 3}, {1, 4}, {7, 5}, {3, 5}, {6, 2}, {5, 3}, {4, 4}, {3, 5}, {2, 5}},
	}
	for input, expectation := range cases {
		a := NewSnailNumbers(input)
		if !isSameSnailNumber(a, expectation) {
			t.Errorf("expected %v; got %v", expectation, a)
		}
	}
}

func TestExplode(t *testing.T) {
	expectation := []SnailNumber{{0, 4}, {9, 4}, {2, 3}, {3, 2}, {4, 1}}
	a := NewSnailNumbers("[[[[[9,8],1],2],3],4]")
	a = explode(a, []int{0, 1})
	if !isSameSnailNumber(a, expectation) {
		t.Errorf("expected %v; got %v", expectation, a)
	}

	expectation = []SnailNumber{{7, 1}, {6, 2}, {5, 3}, {7, 4}, {0, 4}}
	a = NewSnailNumbers("[7,[6,[5,[4,[3,2]]]]]")
	a = explode(a, []int{4, 5})
	if !isSameSnailNumber(a, expectation) {
		t.Errorf("expected %v; got %v", expectation, a)
	}

	expectation = []SnailNumber{{6, 2}, {5, 3}, {7, 4}, {0, 4}, {3, 1}}
	a = NewSnailNumbers("[[6,[5,[4,[3,2]]]],1]")
	a = explode(a, []int{3, 4})
	if !isSameSnailNumber(a, expectation) {
		t.Errorf("expected %v; got %v", expectation, a)
	}

	expectation = []SnailNumber{{3, 2}, {2, 3}, {8, 4}, {0, 4}, {9, 2}, {5, 3}, {4, 4}, {3, 5}, {2, 5}}
	a = NewSnailNumbers("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]")
	a = explode(a, []int{3, 4})
	if !isSameSnailNumber(a, expectation) {
		t.Errorf("expected %v; got %v", expectation, a)
	}

	a = []SnailNumber{{3, 2}, {2, 3}, {8, 4}, {0, 4}, {9, 2}, {5, 3}, {4, 4}, {3, 5}, {2, 5}}
	a = explode(a, []int{7, 8})
	expectation = []SnailNumber{{3, 2}, {2, 3}, {8, 4}, {0, 4}, {9, 2}, {5, 3}, {7, 4}, {0, 4}}
	if !isSameSnailNumber(a, expectation) {
		t.Errorf("expected %v; got %v", expectation, a)
	}

	a = []SnailNumber{{0, 4}, {7, 6}, {7, 6}, {0, 4}}
	a = explode(a, []int{1, 2})
	expectation = []SnailNumber{{7, 4}, {0, 5}, {7, 4}}
	if !isSameSnailNumber(a, expectation) {
		t.Errorf("expected %v; got %v", expectation, a)
	}
}

func TestAdd(t *testing.T) {
	a := NewSnailNumbers("[1,2]")
	b := NewSnailNumbers("[[3,4],5]")
	expectation := []SnailNumber{{1, 2}, {2, 2}, {3, 3}, {4, 3}, {5, 2}}
	r := add(a, b)
	if !isSameSnailNumber(r, expectation) {
		t.Errorf("expected %v; got %v", expectation, a)
	}
}

func TestAddReduce(t *testing.T) {
	var a, b, r, expectation []SnailNumber

	cases := [][]string{
		{
			"[[[[4,3],4],4],[7,[[8,4],9]]]",
			"[1,1]",
			"[[[[0,7],4],[[7,8],[6,0]]],[8,1]]",
		},
		{
			"[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]",
			"[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]",
			"[[[[4,0],[5,4]],[[7,7],[6,0]]],[[8,[7,7]],[[7,9],[5,0]]]]",
		},
	}

	for _, item := range cases {
		a = NewSnailNumbers(item[0])
		b = NewSnailNumbers(item[1])
		expectation = NewSnailNumbers(item[2])
		r = addReduce(a, b)
		if !isSameSnailNumber(r, expectation) {
			t.Errorf("\nexpected %v;\n got %v", expectation, r)
		}
	}
}

func TestSumList(t *testing.T) {
	expectations := map[string]string{
		"[1,1]\n[2,2]\n[3,3]\n[4,4]":               "[[[[1,1],[2,2]],[3,3]],[4,4]]",
		"[1,1]\n[2,2]\n[3,3]\n[4,4]\n[5,5]":        "[[[[3,0],[5,3]],[4,4]],[5,5]]",
		"[1,1]\n[2,2]\n[3,3]\n[4,4]\n[5,5]\n[6,6]": "[[[[5,0],[7,4]],[5,5]],[6,6]]",
	}

	for input, expectationStr := range expectations {
		r := solve(input)
		expectation := NewSnailNumbers(expectationStr)
		if !isSameSnailNumber(r, expectation) {
			t.Errorf("expected %v; got %v", expectation, r)
		}
	}
}

//go:embed input_18_test.txt
var testInput string

func TestSample(t *testing.T) {
	r := solve(testInput)
	expectation := NewSnailNumbers("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]")
	if !isSameSnailNumber(r, expectation) {
		t.Errorf("expected %v; got %v", expectation, r)
	}
}

func TestMagnitude(t *testing.T) {
	cases := map[string]int{
		"[9]":                               9,
		"[9,1]":                             29,
		"[1,9]":                             21,
		"[[9,1],[1,9]]":                     129,
		"[[1,2],[[3,4],5]]":                 143,
		"[[[[0,7],4],[[7,8],[6,0]]],[8,1]]": 1384,
		"[[[[1,1],[2,2]],[3,3]],[4,4]]":     445,
		"[[[[3,0],[5,3]],[4,4]],[5,5]]":     791,
		"[[[[5,0],[7,4]],[5,5]],[6,6]]":     1137,
		"[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]":         3488,
		"[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]": 4140,
	}
	for input, expectation := range cases {
		r := magnitude(NewSnailNumbers(input))
		if r != expectation {
			t.Errorf("expected: %d; got %d", expectation, r)
		}
	}
}

//go:embed input_18_test2.txt
var testInput2 string

func TestSampleFinal(t *testing.T) {
	sum := solve(testInput2)
	mag := magnitude(sum)
	if mag != 4140 {
		t.Errorf("expected: 4140; got %d", mag)
	}
}

func TestBiggestMagnitude(t *testing.T) {
	r := biggestMagnitude(testInput2)
	if r != 3993 {
		t.Errorf("expected: 3993; got %d", r)
	}
}
