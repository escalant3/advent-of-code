package main

import (
	_ "embed"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

type Pair struct {
	x, y int
}

type SnailNumber struct {
	value, depth int
}

func NewSnailNumbers(s string) (list []SnailNumber) {
	depth := 0
	position := 0
	numberRegexp := regexp.MustCompile(`(\d+)`)
	for position < len(s) {
		if s[position] == '[' {
			depth++
			position++
			continue
		} else if s[position] == ']' {
			depth--
			position++
			continue
		} else if s[position] == ',' {
			position++
			continue
		} else {
			numberPosition := numberRegexp.FindIndex([]byte(s[position:]))
			if numberPosition == nil {
				panic("parsing error")
			}
			value := parseInt(s[position+numberPosition[0] : position+numberPosition[1]])
			list = append(list, SnailNumber{value, depth})
			position += numberPosition[1]
		}
	}
	return
}

func split(n int) Pair {
	c := n / 2
	r := n % 2
	if r == 0 {
		return Pair{c, c}
	}
	return Pair{c, c + 1}
}

func parseInt(s string) int {
	r, err := strconv.Atoi(s)
	if err != nil {
		panic(fmt.Sprintf("cannot parse number: %s", s))
	}
	return r
}

func getNextFourthNestedPairPosition(snailNumber []SnailNumber, initialPosition int) []int {
	i := initialPosition
	for ; i < len(snailNumber)-1; i++ {
		n := snailNumber[i]
		if n.depth <= 4 {
			continue
		}
		if n.depth == snailNumber[i+1].depth {
			return []int{i, i + 1}
		}

	}
	return nil
}

func explode(sn []SnailNumber, positions []int) []SnailNumber {
	before := positions[0] - 1
	if before >= 0 {
		sn[before].value += sn[positions[0]].value
	}

	after := positions[1] + 1
	if after < len(sn) {
		sn[after].value += sn[positions[1]].value
	}

	new := append(sn[:positions[0]], SnailNumber{0, sn[positions[0]].depth - 1})
	sn = append(new, sn[positions[1]+1:]...)

	return sn
}

func add(a, b []SnailNumber) []SnailNumber {
	r := append(a, b...)
	for i := 0; i < len(r); i++ {
		r[i].depth++
	}
	return r
}

func addReduce(a, b []SnailNumber) []SnailNumber {
	r := add(a, b)
	fmt.Println("Addition", r)
	r = reduce(r)
	return r
}

func reduce(sn []SnailNumber) []SnailNumber {
	change := true
	for change {
		position := 0
		change = false

		// Explode
		for {
			positions := getNextFourthNestedPairPosition(sn, position)
			if positions == nil {
				break
			}
			if positions != nil {
				sn = explode(sn, positions)
				change = true
				position = positions[1] + 1
				fmt.Println("Explode", sn, position)
				break
			}
		}

		// Split
		var new []SnailNumber
		for i, n := range sn {
			if !change && n.value > 9 {
				p := split(n.value)
				new = append(new, SnailNumber{p.x, n.depth + 1}, SnailNumber{p.y, n.depth + 1})
				change = true
				fmt.Println("Split", new)
				new = append(new, sn[i+1:]...)
				break
			}
			new = append(new, n)
		}
		sn = new
	}
	fmt.Println("Final", sn)
	return sn
}

func solve(input string) []SnailNumber {
	numbers := strings.Fields(input)
	r := NewSnailNumbers(numbers[0])
	for i := 1; i < len(numbers); i++ {
		r = addReduce(r, NewSnailNumbers(numbers[i]))
		fmt.Println("Partial result", r)
	}
	return r
}

func biggestMagnitude(input string) int {
	numbersStr := strings.Fields(input)
	var numbers [][]SnailNumber
	for i := 0; i < len(numbersStr); i++ {
		numbers = append(numbers, NewSnailNumbers(numbersStr[i]))
	}
	max := 0
	for i := 0; i < len(numbers); i++ {
		for j := 0; j < len(numbers); j++ {
			if i == j {
				continue
			}

			a := make([]SnailNumber, len(numbers[i]))
			b := make([]SnailNumber, len(numbers[j]))
			copy(a, numbers[i])
			copy(b, numbers[j])
			mag := magnitude(addReduce(a, b))
			if mag > max {
				max = mag
			}
		}
	}
	return max
}

func magnitude(sn []SnailNumber) int {
	if len(sn) == 0 {
		panic("no magnitude for empty snail number")
	}
	if len(sn) == 1 {
		return sn[0].value
	}
	if len(sn) == 2 {
		return 3*sn[0].value + 2*sn[1].value
	}

	maxDepth := 0
	for _, n := range sn {
		if n.depth > maxDepth {
			maxDepth = n.depth
		}
	}

	for i := range sn {
		if sn[i].depth == maxDepth {
			if i < len(sn)-1 && sn[i+1].depth == maxDepth {
				n := SnailNumber{
					3*sn[i].value + 2*sn[i+1].value,
					sn[i].depth - 1,
				}
				new := append(sn[:i], n)
				return magnitude(append(new, sn[i+2:]...))
			} else {
				panic("wat")
			}
		}
	}

	panic("not able to continue with magnitude calculation")
}

//go:embed input_18.txt
var input string

func main() {
	sum := solve(input)
	mag := magnitude(sum)
	fmt.Println("Part 1:", mag)

	max := biggestMagnitude(input)
	fmt.Println("Part 2:", max)
}
