package main

import (
	_ "embed"
	"fmt"
	"math"
	"strings"
)

//go:embed input_14.txt
var input string

func step(s string, ruleset map[string]string) (result string) {
	for i := 0; i < len(s)-1; i++ {
		result += s[i : i+1]
		if expansion, ok := ruleset[s[i:i+2]]; ok {
			result += expansion
		}
	}
	result += s[len(s)-1:]
	return
}

func maxMinInString(s string) (max, min int) {
	h := make(map[byte]int)
	for _, c := range []byte(s) {
		if _, ok := h[c]; !ok {
			h[c] = 0
		}
		h[c]++
	}

	min = math.MaxInt
	max = 0
	for _, value := range h {
		if value > max {
			max = value
		}
		if value < min {
			min = value
		}
	}

	return
}

func polymerize(input string, steps int) int {
	lines := strings.Split(input, "\n")
	ruleset := make(map[string]string)
	polymer := strings.TrimSpace(lines[0])
	for _, rule := range lines[2:] {
		if strings.TrimSpace(rule) == "" {
			continue
		}
		ruleset[rule[:2]] = rule[len(rule)-1:]
	}

	for i := 0; i < steps; i++ {
		polymer = step(polymer, ruleset)
	}

	max, min := maxMinInString(polymer)
	return max - min
}

func getHistogram(pair string, ruleset map[string]string, steps int) map[string]int64 {
	h := make(map[string]int64)
	h[pair]++
	for i := 0; i < steps; i++ {
		hStep := make(map[string]int64)
		for key, value := range h {

			expansion := ruleset[key]
			if expansion == "" {
				panic(fmt.Sprintf("rule not found %s", key))
			}
			key1 := key[:1] + expansion
			key2 := expansion + key[1:]
			hStep[key1] += value
			hStep[key2] += value

		}
		h = hStep
	}
	return h
}

func polymerize3(input string, steps int) int64 {
	lines := strings.Split(input, "\n")
	ruleset := make(map[string]string)
	result := make(map[string]int64)
	polymer := strings.TrimSpace(lines[0])
	for _, rule := range lines[2:] {
		if strings.TrimSpace(rule) == "" {
			continue
		}
		ruleset[rule[:2]] = rule[len(rule)-1:]
	}
	for i := 0; i < len(polymer)-1; i++ {
		h := getHistogram(polymer[i:i+2], ruleset, steps)
		for key, value := range h {
			result[key] += value
		}
	}

	h := make(map[string]int64)
	for key, value := range result {
		h[key[:1]] += value / 2
		h[key[1:]] += value / 2
	}

	var max int64
	min := int64(math.MaxInt64)
	for _, value := range h {
		if value < min {
			min = value
		}
		if value > max {
			max = value
		}
	}

	return max - min
}

func main() {
	r := polymerize(input, 10)
	fmt.Println("Part 1:", r)
	r1 := polymerize3(input, 40)
	fmt.Println("Part 3:", r1)
}
