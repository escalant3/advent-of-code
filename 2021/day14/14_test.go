package main

import (
	"testing"

	_ "embed"
)

//go:embed input_14_test.txt
var testInput string

func TestStep(t *testing.T) {
	ruleset := map[string]string{"NN": "C", "NC": "B", "CB": "H"}
	expected := "NCNBCHB"
	r := step("NNCB", ruleset)
	if r != "NCNBCHB" {
		t.Errorf("excepted: %s, got: %s", expected, r)
	}
}

func TestPolymerize(t *testing.T) {
	expected := 1588
	r := polymerize(testInput, 10)
	if r != expected {
		t.Errorf("excepted: %d, got: %d", expected, r)
	}
}

func TestPolymerize3(t *testing.T) {
	expected := int64(2188189693529)
	r := polymerize3(testInput, 40)
	if r != expected {
		t.Errorf("excepted: %d, got: %d", expected, r)
	}
}
