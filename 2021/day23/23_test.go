package main

import (
	"testing"
)

func TestPart1Steps(t *testing.T) {
	c := NewBurrow(
		Bronze,
		Amber,
		Copper,
		Desert,
		Bronze,
		Copper,
		Desert,
		Amber,
	)

	energy := 0

	e, err := c.moveFromBedroomToCorridor(2, 0, 3)
	if err != nil {
		t.Errorf("unexpected error %e", err)
	}
	if e != 40 {
		t.Errorf("expected energy 40, got %d", e)
	}
	energy += e

	e, err = c.moveFromBedroomToBedroom(1, 0, 2, 0)
	if err != nil {
		t.Errorf("unexpected error %e", err)
	}
	if e != 400 {
		t.Errorf("expected energy 400, got %d", e)
	}
	energy += e

	e, err = c.moveFromBedroomToCorridor(1, 1, 5)
	if err != nil {
		t.Errorf("unexpected error %e", err)
	}
	if e != 3000 {
		t.Errorf("expected energy 3000, got %d", e)
	}
	energy += e

	e, err = c.moveFromCorridorToBedroom(3, 1, 1)
	if err != nil {
		t.Errorf("unexpected error %e", err)
	}
	if e != 30 {
		t.Errorf("expected energy 30, got %d", e)
	}
	energy += e

	e, err = c.moveFromBedroomToBedroom(0, 0, 1, 0)
	if err != nil {
		t.Errorf("unexpected error %e", err)
	}
	if e != 40 {
		t.Errorf("expected energy 40, got %d", e)
	}
	energy += e

	e, err = c.moveFromBedroomToCorridor(3, 0, 7)
	if err != nil {
		t.Errorf("unexpected error %e", err)
	}
	energy += e
	e, err = c.moveFromBedroomToCorridor(3, 1, 9)
	if err != nil {
		t.Errorf("unexpected error %e", err)
	}
	energy += e

	if energy != 5513 {
		t.Errorf("expected energy to be 5513 (%d)", energy)
	}

	e, err = c.moveFromCorridorToBedroom(7, 3, 1)
	if err != nil {
		t.Errorf("unexpected error %e", err)
	}
	energy += e

	e, err = c.moveFromCorridorToBedroom(5, 3, 0)
	if err != nil {
		t.Errorf("unexpected error %e", err)
	}
	energy += e

	e, err = c.moveFromCorridorToBedroom(9, 0, 0)
	if err != nil {
		t.Errorf("unexpected error %e", err)
	}
	energy += e

	if energy != 12521 {
		t.Errorf("expected energy to be 12521 (%d)", energy)
	}
}

func TestPart1(t *testing.T) {
	c := NewBurrow(
		Copper,
		Amber,
		Copper,
		Desert,
		Bronze,
		Copper,
		Desert,
		Amber,
	)

	r := solve(c, true)

	if r != 12521 {
		t.Errorf("expected 12521, got %d", r)
	}
}
