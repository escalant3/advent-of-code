package main

import (
	"errors"
	"fmt"
	"math"
)

type Amphipod byte

const (
	Amber  Amphipod = 'A'
	Bronze Amphipod = 'B'
	Copper Amphipod = 'C'
	Desert Amphipod = 'D'
	None   Amphipod = 'N'
)

type Corridor [11]Amphipod

type Burrow struct {
	rooms    [4][2]Amphipod
	corridor Corridor
}

func NewBurrow(a1, a2, b1, b2, c1, c2, d1, d2 Amphipod) Burrow {
	var b Burrow
	for i := range b.corridor {
		b.corridor[i] = None
	}
	b.rooms[0][0] = a1
	b.rooms[0][1] = a2
	b.rooms[1][0] = b1
	b.rooms[1][1] = b2
	b.rooms[2][0] = c1
	b.rooms[2][1] = c2
	b.rooms[3][0] = d1
	b.rooms[3][1] = d2

	return b
}

func (b *Burrow) Clone() Burrow {
	newB := Burrow{}
	newB.corridor = b.corridor
	newB.rooms = b.rooms
	return newB
}

func (b *Burrow) moveFromBedroomToCorridor(roomId, roomPosition, corridorPosition int) (energy int, err error) {
	if b.rooms[roomId][roomPosition] == None {
		return 0, errors.New("trying to move an empty tile")
	}
	if roomPosition == 1 && b.rooms[roomId][0] != None {
		return 0, errors.New("trying to move from inner room with external tile occupied")
	}
	if corridorPosition < 0 || corridorPosition > 10 {
		return 0, errors.New("invalid corridor position")
	}

	if roomPosition == 1 {
		if b.rooms[roomId][1] == Amber && roomId == 0 {
			return 0, errors.New("moving a properly placed amphipod")
		}
		if b.rooms[roomId][1] == Bronze && roomId == 1 {
			return 0, errors.New("moving a properly placed amphipod")
		}
		if b.rooms[roomId][1] == Copper && roomId == 2 {
			return 0, errors.New("moving a properly placed amphipod")
		}
		if b.rooms[roomId][1] == Desert && roomId == 3 {
			return 0, errors.New("moving a properly placed amphipod")
		}
	}
	if roomPosition == 0 {
		if b.rooms[roomId][1] == Amber && b.rooms[roomId][0] == Amber && roomId == 0 {
			return 0, errors.New("moving a properly placed amphipod")
		}
		if b.rooms[roomId][1] == Bronze && b.rooms[roomId][0] == Bronze && roomId == 1 {
			return 0, errors.New("moving a properly placed amphipod")
		}
		if b.rooms[roomId][1] == Copper && b.rooms[roomId][0] == Copper && roomId == 2 {
			return 0, errors.New("moving a properly placed amphipod")
		}
		if b.rooms[roomId][1] == Desert && b.rooms[roomId][0] == Desert && roomId == 3 {
			return 0, errors.New("moving a properly placed amphipod")
		}
	}
	corridorStart := 2*roomId + 2

	// Check room entrances
	if b.corridor[corridorStart] != None {
		return 0, errors.New("cannot get out of the room")
	}
	start := corridorStart
	end := corridorPosition
	if end < start {
		start, end = end, start
	}
	for i := start; i <= end; i++ {
		if b.corridor[i] != None {
			return 0, errors.New("cannot reach destination")
		}
	}
	b.corridor[corridorPosition] = b.rooms[roomId][roomPosition]
	b.rooms[roomId][roomPosition] = None

	distance := end - start + roomPosition + 1

	switch b.corridor[corridorPosition] {
	case Amber:
		return distance, nil
	case Bronze:
		return 10 * distance, nil
	case Copper:
		return 100 * distance, nil
	case Desert:
		return 1000 * distance, nil
	}

	return 0, errors.New("invalid tile")
}

func (b *Burrow) moveFromBedroomToBedroom(originId, originPosition, destinationId, destinationPosition int) (energy int, err error) {
	if originId == destinationId {
		return 0, errors.New("cannot move to same room")
	}
	if originPosition == 1 && b.rooms[originId][0] != None {
		return 0, errors.New("trying to move from inner room with external tile occupied")
	}
	if b.rooms[destinationId][destinationPosition] != None {
		return 0, errors.New("moving to occupied room")
	}
	if b.rooms[destinationId][1] != None && b.rooms[destinationId][1] != b.rooms[originId][originPosition] {
		return 0, errors.New("moving to unordered room")
	}

	if b.rooms[originId][originPosition] == Amber && destinationId != 0 {
		return 0, errors.New("bad room for amphipod")
	}
	if b.rooms[originId][originPosition] == Bronze && destinationId != 1 {
		return 0, errors.New("bad room for amphipod")
	}
	if b.rooms[originId][originPosition] == Copper && destinationId != 2 {
		return 0, errors.New("bad room for amphipod")
	}
	if b.rooms[originId][originPosition] == Desert && destinationId != 3 {
		return 0, errors.New("bad room for amphipod")
	}
	if originPosition == 1 {
		if b.rooms[originId][1] == Amber && originId == 0 {
			return 0, errors.New("moving a properly placed amphipod")
		}
		if b.rooms[originId][1] == Bronze && originId == 1 {
			return 0, errors.New("moving a properly placed amphipod")
		}
		if b.rooms[originId][1] == Copper && originId == 2 {
			return 0, errors.New("moving a properly placed amphipod")
		}
		if b.rooms[originId][1] == Desert && originId == 3 {
			return 0, errors.New("moving a properly placed amphipod")
		}
	}
	if originPosition == 0 {
		if b.rooms[originId][1] == Amber && b.rooms[originId][0] == Amber && originId == 0 {
			return 0, errors.New("moving a properly placed amphipod")
		}
		if b.rooms[originId][1] == Bronze && b.rooms[originId][0] == Bronze && originId == 1 {
			return 0, errors.New("moving a properly placed amphipod")
		}
		if b.rooms[originId][1] == Copper && b.rooms[originId][0] == Copper && originId == 2 {
			return 0, errors.New("moving a properly placed amphipod")
		}
		if b.rooms[originId][1] == Desert && b.rooms[originId][0] == Desert && originId == 3 {
			return 0, errors.New("moving a properly placed amphipod")
		}
	}

	start := originId*2 + 2
	end := destinationId*2 + 2
	if end < start {
		start, end = end, start
	}
	for i := start; i <= end; i++ {
		if b.corridor[i] != None {
			return 0, errors.New("cannot reach destination")
		}
	}

	distance := originPosition + destinationPosition + end - start + 2

	b.rooms[destinationId][destinationPosition] = b.rooms[originId][originPosition]
	b.rooms[originId][originPosition] = None

	switch b.rooms[destinationId][destinationPosition] {
	case Amber:
		return distance, nil
	case Bronze:
		return 10 * distance, nil
	case Copper:
		return 100 * distance, nil
	case Desert:
		return 1000 * distance, nil
	}

	return 0, errors.New("invalid tile")
}

func (b *Burrow) moveFromCorridorToBedroom(corridorPosition, roomId, roomPosition int) (energy int, err error) {
	if corridorPosition < 0 || corridorPosition > 10 {
		return 0, errors.New("invalid corridor position")
	}
	if b.corridor[corridorPosition] == None {
		return 0, errors.New("trying to move empty tile")
	}
	if b.rooms[roomId][roomPosition] != None {
		return 0, errors.New("cannot move to non-empty tile")
	}
	if roomPosition == 0 && b.rooms[roomId][1] == None {
		return 0, errors.New("trying to move to outer room with inner room empty")
	}
	if b.rooms[roomId][1] != None && b.rooms[roomId][1] != b.corridor[corridorPosition] {
		return 0, errors.New("moving to unordered room")
	}

	if b.corridor[corridorPosition] == Amber && roomId != 0 {
		return 0, errors.New("bad room for amphipod")
	}
	if b.corridor[corridorPosition] == Bronze && roomId != 1 {
		return 0, errors.New("bad room for amphipod")
	}
	if b.corridor[corridorPosition] == Copper && roomId != 2 {
		return 0, errors.New("bad room for amphipod")
	}
	if b.corridor[corridorPosition] == Desert && roomId != 3 {
		return 0, errors.New("bad room for amphipod")
	}

	start := corridorPosition
	end := roomId*2 + 2
	if end < start {
		start, end = end, start
		end -= 1
	} else {
		start += 1
	}
	for i := start; i <= end; i++ {
		if b.corridor[i] != None {
			return 0, errors.New("cannot reach destination")
		}
	}
	b.rooms[roomId][roomPosition] = b.corridor[corridorPosition]
	b.corridor[corridorPosition] = None

	distance := end - start + roomPosition + 2

	switch b.rooms[roomId][roomPosition] {
	case Amber:
		return distance, nil
	case Bronze:
		return 10 * distance, nil
	case Copper:
		return 100 * distance, nil
	case Desert:
		return 1000 * distance, nil
	}

	return 0, errors.New("invalid tile")
}

func solve(b Burrow, isTop bool) int {
	//fmt.Println("SOLVE!!", a)
	if b.rooms[0][0] == Amber && b.rooms[0][1] == Amber &&
		b.rooms[1][0] == Bronze && b.rooms[1][1] == Bronze &&
		b.rooms[2][0] == Copper && b.rooms[2][1] == Copper &&
		b.rooms[3][0] == Desert && b.rooms[3][1] == Desert {
		return 0
	}

	roomIds := [4]int{0, 1, 2, 3}
	roomPositions := [2]int{0, 1}
	corridorPositions := [7]int{0, 1, 3, 5, 7, 9, 10}
	actions := [3]int{0, 1, 2}
	var energies []int
	for _, roomId := range roomIds {
		for _, position := range roomPositions {
			for _, corridorPosition := range corridorPositions {
				for _, action := range actions {
					b1 := b.Clone()
					switch action {
					case 0:
						{
							energy, err := b1.moveFromBedroomToCorridor(roomId, position, corridorPosition)
							if err != nil {
								continue
							}
							foo := solve(b1, false)
							if foo < 0 || foo == math.MaxInt {
								continue
							}
							energies = append(energies, energy+foo)
							if isTop {
								fmt.Println(energies)
							}
						}
					case 2:
						{
							energy, err := b1.moveFromCorridorToBedroom(corridorPosition, roomId, position)
							if err != nil {
								continue
							}
							foo := solve(b1, false)
							if foo < 0 || foo == math.MaxInt {
								continue
							}
							energies = append(energies, energy+foo)
							if isTop {
								fmt.Println(energies)
							}
						}
					}
				}
			}

			/*
				b1 := b.Clone()
				for _, r := range roomIds {
					for _, p := range roomPositions {

						energy, err := b1.moveFromBedroomToBedroom(roomId, position, r, p)
						if err != nil {
							continue
						}
						energies = append(energies, energy+solve(b1))
					}
				}
			*/
		}
	}

	min := math.MaxInt
	for _, e := range energies {
		if e < min && e > 0 {
			min = e
		}
	}
	return min
}

func main() {
	c := NewBurrow(
		Copper,
		Bronze,
		Desert,
		Amber,
		Desert,
		Bronze,
		Amber,
		Copper,
	)

	r := solve(c, true)
	fmt.Println(r)
}
