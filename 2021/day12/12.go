package main

import (
	_ "embed"
	"fmt"
	"strings"
	"unicode"
)

//go:embed input_12.txt
var input string

type CaveSize int8

const (
	Small CaveSize = iota
	Big
)

type cave struct {
	name        string
	size        CaveSize
	connections []*cave
}

func isUpper(s string) bool {
	for _, c := range s {
		if !unicode.IsUpper(c) {
			return false
		}
	}
	return true
}

func NewCave(name string) *cave {
	size := Small
	if isUpper(name) {
		size = Big
	}
	return &cave{name, size, nil}
}

func findPath(input string, traverseFunc func([]string, *cave) int) int {
	connections := strings.Fields(input)
	caveMap := make(map[string]*cave)
	for _, c := range connections {
		sides := strings.Split(c, "-")
		var caveA, caveB *cave
		var ok bool
		if caveA, ok = caveMap[sides[0]]; !ok {
			caveA = NewCave(sides[0])
			caveMap[sides[0]] = caveA
		}
		if caveB, ok = caveMap[sides[1]]; !ok {
			caveB = NewCave(sides[1])
			caveMap[sides[1]] = caveB
		}
		caveA.connections = append(caveA.connections, caveB)
		caveB.connections = append(caveB.connections, caveA)
	}

	var visited []string

	totalPaths := traverseFunc(visited, caveMap["start"])

	return totalPaths
}

func contains(s []string, needle string) bool {
	for _, item := range s {
		if item == needle {
			return true
		}
	}
	return false
}

func containsTwice(s []string, needle string) bool {
	if needle == "start" || needle == "end" {
		return contains(s, needle)
	}

	s = append(s, needle)
	count := make(map[string]int)
	presentTwice := false

	for _, item := range s {
		if isUpper(item) {
			continue
		}
		count[item]++
		if count[item] > 1 {
			if presentTwice {
				return true
			}
			presentTwice = true
		}
	}
	return false
}

func traverse(visited []string, node *cave) int {
	visited = append(visited, node.name)

	if node.name == "end" {
		return 1
	}

	var validConnections []*cave
	for _, connection := range node.connections {
		if connection.size == Big || !contains(visited, connection.name) {

			validConnections = append(validConnections, connection)
		}
	}

	if len(validConnections) == 0 {
		return 0
	}

	sum := 0
	for _, connection := range validConnections {
		sum += traverse(visited, connection)
	}
	return sum
}

func traverse2(visited []string, node *cave) int {
	visited = append(visited, node.name)

	if node.name == "end" {
		return 1
	}

	var validConnections []*cave
	for _, connection := range node.connections {
		if connection.size == Big || !containsTwice(visited, connection.name) {

			validConnections = append(validConnections, connection)
		}
	}

	if len(validConnections) == 0 {
		return 0
	}

	sum := 0
	for _, connection := range validConnections {
		sum += traverse2(visited, connection)
	}
	return sum
}

func main() {
	r := findPath(input, traverse)
	fmt.Println("Part 1:", r)
	r = findPath(input, traverse2)
	fmt.Println("Part 2:", r)
}
