package main

import (
	_ "embed"
	"testing"
)

//go:embed input_12_test1.txt
var testInput1 string

//go:embed input_12_test2.txt
var testInput2 string

//go:embed input_12_test3.txt
var testInput3 string

func TestPaths1(t *testing.T) {
	r := findPath(testInput1, traverse)
	if r != 10 {
		t.Errorf("expected: 10, got: %d", r)
	}
}

func TestPaths2(t *testing.T) {
	r := findPath(testInput2, traverse)
	if r != 19 {
		t.Errorf("expected: 19, got: %d", r)
	}
}

func TestPaths3(t *testing.T) {
	r := findPath(testInput3, traverse)
	if r != 226 {
		t.Errorf("expected: 226, got: %d", r)
	}
}

func TestPath1Part2(t *testing.T) {
	r := findPath(testInput1, traverse2)
	if r != 36 {
		t.Errorf("expected: 36, got: %d", r)
	}
}

func TestPaths2Part2(t *testing.T) {
	r := findPath(testInput2, traverse2)
	if r != 103 {
		t.Errorf("expected: 103, got: %d", r)
	}
}

func TestPaths3Part2(t *testing.T) {
	r := findPath(testInput3, traverse2)
	if r != 3509 {
		t.Errorf("expected: 3509, got: %d", r)
	}
}
