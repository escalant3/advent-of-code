package main

import (
	_ "embed"
	"fmt"
	"strconv"
	"strings"
)

//go:embed input_11.txt
var input string

func simulate(population [][]uint8, steps int) int {
	flashes := 0
	for i := 0; i < steps; i++ {
		for j := 0; j < len(population); j++ {
			for k := 0; k < len(population[j]); k++ {
				// Add 1 to every item
				population[j][k] += 1
			}
		}
		flashing := true
		for flashing {
			flashing = false
			for j := 0; j < len(population); j++ {
				for k := 0; k < len(population[j]); k++ {
					if population[j][k] > 9 {
						flashes++
						population[j][k] = 0
						for _, n := range validNeighbors(population, j, k) {
							if population[n.x][n.y] != 0 {
								flashing = true
								population[n.x][n.y]++
							}
						}
					}
				}
			}
		}
	}
	return flashes
}
func simulate2(population [][]uint8) int {
	goal := len(population) * len(population[0])
	steps := 0
	for {
		steps++
		for j := 0; j < len(population); j++ {
			for k := 0; k < len(population[j]); k++ {
				// Add 1 to every item
				population[j][k] += 1
			}
		}
		flashing := true
		flashesInIteration := 0
		for flashing {
			flashing = false
			for j := 0; j < len(population); j++ {
				for k := 0; k < len(population[j]); k++ {
					if population[j][k] > 9 {
						population[j][k] = 0
						flashesInIteration++
						for _, n := range validNeighbors(population, j, k) {
							if population[n.x][n.y] != 0 {
								flashing = true
								population[n.x][n.y]++
							}
						}
					}
				}
			}
			if flashesInIteration == goal {
				return steps
			}
		}
	}
}

type point struct {
	x, y int
}

func validNeighbors(p [][]uint8, x, y int) []point {
	var result []point
	for i := x - 1; i <= x+1; i++ {
		for j := y - 1; j <= y+1; j++ {
			if i >= 0 && j >= 0 && i < len(p) && j < len(p[0]) {
				result = append(result, point{i, j})
			}
		}
	}
	return result
}

func parseInput(input string) (population [][]uint8) {
	lines := strings.Fields(input)
	for _, line := range lines {
		var l []uint8
		for _, b := range strings.Split(line, "") {
			v, _ := strconv.Atoi(b)
			l = append(l, uint8(v))
		}
		population = append(population, l)
	}
	return
}

func main() {
	r := simulate(parseInput(input), 100)
	fmt.Printf("Part 1: %d\n", r)
	r = simulate2(parseInput(input))
	fmt.Printf("Part 2: %d\n", r)
}
