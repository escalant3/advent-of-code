package main

import (
	_ "embed"
	"testing"
)

//go:embed input_11_test.txt
var testInput string

func TestSimulate(t *testing.T) {
	p := parseInput(testInput)
	r := simulate(p, 100)
	if r != 1656 {
		t.Errorf("expected: 1656, got: %d", r)
	}
}

func TestSimulate2(t *testing.T) {
	p := parseInput(testInput)
	r := simulate2(p)
	if r != 195 {
		t.Errorf("expected: 195; got:%d", r)
	}
}
