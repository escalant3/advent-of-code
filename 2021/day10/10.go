package main

import (
	_ "embed"
	"fmt"
	"sort"
	"strings"
)

//go:embed input_10.txt
var input string

func matchesOpening(opening, close byte) bool {
	switch {
	case opening == '{' && close == '}',
		opening == '[' && close == ']',
		opening == '(' && close == ')',
		opening == '<' && close == '>':
		return true
	default:
		return false
	}
}

func byteToScore(a byte) int {
	switch a {
	case ')':
		return 3
	case ']':
		return 57
	case '}':
		return 1197
	case '>':
		return 25137
	default:
		panic(fmt.Sprintf("invalid closing character %v", a))
	}
}
func corruptedSyntaxScore(lines []string) int {
	var stack []byte
	sum := 0
	for _, line := range lines {
		for _, c := range []byte(line) {
			if c == '{' || c == '[' || c == '(' || c == '<' {
				stack = append(stack, c)
			} else if !matchesOpening(stack[len(stack)-1], c) {
				sum += byteToScore(c)
				break
			} else {
				stack = stack[:len(stack)-1]
			}
		}
	}
	return sum
}

func fixLinesScore(lines []string) int {
	var stack []byte
	var scores []int
outer:
	for _, line := range lines {
		stack = stack[:0]
		for _, c := range []byte(line) {
			if c == '{' || c == '[' || c == '(' || c == '<' {
				stack = append(stack, c)
			} else if !matchesOpening(stack[len(stack)-1], c) {
				continue outer
			} else {
				stack = stack[:len(stack)-1]
			}
		}
		scores = append(scores, 0)
		for len(stack) > 0 {
			scores[len(scores)-1] *= 5
			switch stack[len(stack)-1] {
			case '(':
				scores[len(scores)-1] += 1
			case '[':
				scores[len(scores)-1] += 2
			case '{':
				scores[len(scores)-1] += 3
			case '<':
				scores[len(scores)-1] += 4
			}
			stack = stack[:len(stack)-1]
		}
	}
	sort.Ints(scores)
	return scores[len(scores)/2]
}

func main() {
	lines := strings.Fields(input)
	result := corruptedSyntaxScore(lines)
	fmt.Printf("Part 1: %d\n", result)
	result = fixLinesScore(lines)
	fmt.Printf("Part 2: %d\n", result)

}
