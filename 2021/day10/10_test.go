package main

import (
	_ "embed"
	"strings"
	"testing"
)

//go:embed input_10_test.txt
var testInput string

func TestCorruptedSyntaxScore(t *testing.T) {
	lines := strings.Fields(testInput)
	result := corruptedSyntaxScore(lines)
	if result != 26397 {
		t.Errorf("expected: 26397; got %d", result)
	}
}

func TestFixLinesScore(t *testing.T) {
	lines := strings.Fields(testInput)
	result := fixLinesScore(lines)
	if result != 288957 {
		t.Errorf("expected: 288957, got: %d", result)
	}
}
