package main

import (
	_ "embed"
	"fmt"
	"testing"
)

func TestBeaconPair(t *testing.T) {
	cases := [][]*Vector3{
		{
			NewVector3(1, 1, 1),
			NewVector3(3, 3, 3),
			NewVector3(2, 2, 2),
		},
		{
			NewVector3(0, 0, 1),
			NewVector3(0, 0, 2),
			NewVector3(0, 0, 1),
		},
		{
			NewVector3(0, 0, 1),
			NewVector3(0, 0, 2),
			NewVector3(1, 0, 0),
		},
		{
			NewVector3(1, 0, 0),
			NewVector3(2, 0, 0),
			NewVector3(0, 1, 0),
		},
	}
	for i := range cases {
		v1 := cases[i][0]
		v2 := cases[i][1]
		expectation := cases[i][2]
		bp := NewBeaconPair(v1, v2)
		if !bp.magnitude.isSameMagnitude(expectation) {
			t.Errorf("expected %v, got %v", expectation, bp.magnitude)
		}
	}
}

//go:embed input_19_test.txt
var testInput string

func TestSample(t *testing.T) {
	scanners := parseInput(testInput)
	pairsByScanner := getBeaconPairsByScanner(scanners)
	commonPairs := getCommonPairs(pairsByScanner[0], pairsByScanner[1])
	fmt.Println("Common pairs", len(commonPairs))
	fmt.Println("Totals pairs", len(pairsByScanner[0]), len(pairsByScanner[1]))

	var uniques []*Vector3
	for _, pair := range commonPairs {
		v1 := pair.pair1.v1
		v2 := pair.pair1.v2
		if !contains(uniques, v1) {
			uniques = append(uniques, v1)
		}
		if !contains(uniques, v2) {
			uniques = append(uniques, v2)
		}
	}
	t.Errorf("fail %v", len(uniques))
}
