package main

import (
	"fmt"
	"math"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

type Vector3 struct {
	x, y, z int
}

func NewVector3(x, y, z int) *Vector3 {
	return &Vector3{x, y, z}
}

func NewVector3FromString(s string) *Vector3 {
	values := strings.Split(strings.TrimSpace(s), ",")
	x, err := strconv.Atoi(values[0])
	if err != nil {
		panic(err)
	}
	y, err := strconv.Atoi(values[1])
	if err != nil {
		panic(err)
	}
	z, err := strconv.Atoi(values[2])
	if err != nil {
		panic(err)
	}
	return &Vector3{x, y, z}
}

func (v *Vector3) isSameMagnitude(w *Vector3) bool {
	a := []int{v.x, v.y, v.z}
	b := []int{w.x, w.y, w.z}
	sort.Ints(a)
	sort.Ints(b)
	return a[0] == b[0] && a[1] == b[1] && a[2] == b[2]
}

type BeaconPair struct {
	v1, v2    *Vector3
	magnitude *Vector3
}

type BeaconPairMatch struct {
	pair1, pair2 *BeaconPair
}

func NewBeaconPair(v1, v2 *Vector3) *BeaconPair {
	bp := BeaconPair{v1, v2, nil}
	bp.magnitude = NewVector3(int(math.Abs(float64(v1.x-v2.x))), int(math.Abs(float64(v1.y-v2.y))), int(math.Abs(float64(v1.z-v2.z))))
	return &bp
}

func NewBeaconPairFromString(s1, s2 string) *BeaconPair {
	v1 := NewVector3FromString(s1)
	v2 := NewVector3FromString(s2)
	return NewBeaconPair(v1, v2)
}

func parseInput(s string) [][]*Vector3 {
	scannerRegexp := regexp.MustCompile(`scanner \d+`)
	beaconRegexp := regexp.MustCompile(`-?\d+,-?\d+,-?\d+`)
	var scanners [][]*Vector3
	for _, line := range strings.Split(strings.TrimSpace(s), "\n") {
		if r := scannerRegexp.FindString(line); r != "" {
			var beacons []*Vector3
			scanners = append(scanners, beacons)
			continue
		}
		if r := beaconRegexp.FindString(line); r != "" {
			scanners[len(scanners)-1] = append(scanners[len(scanners)-1], NewVector3FromString(line))
			continue
		}
	}
	return scanners
}

func getBeaconPairsByScanner(scanners [][]*Vector3) [][]*BeaconPair {
	var pairsByScanner [][]*BeaconPair
	for i := 0; i < len(scanners); i++ {
		var pairs []*BeaconPair
		pairsByScanner = append(pairsByScanner, pairs)
		for j := 0; j < len(scanners[i]); j++ {
			for k := j + 1; k < len(scanners[i]); k++ {
				bp := NewBeaconPair(scanners[i][j], scanners[i][k])
				pairs = append(pairs, bp)
			}
		}
		pairsByScanner[i] = pairs
	}
	return pairsByScanner
}

func contains(slice []*Vector3, v *Vector3) bool {
	for _, w := range slice {
		if v == w {
			return true
		}
	}
	return false
}

func getCommonPairs(s1, s2 []*BeaconPair) (result []*BeaconPairMatch) {
	for _, bp1 := range s1 {
		for _, bp2 := range s2 {
			if bp1.magnitude.isSameMagnitude(bp2.magnitude) {
				result = append(result, &BeaconPairMatch{bp1, bp2})
				fmt.Println("Same pair")
			}
		}
	}
	return
}
