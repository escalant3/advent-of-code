#!/usr/bin/env python3

import math

class Vect3D:
    def __init__(self, tup):
        self.x = tup[0]
        self.y = tup[1]
        self.z = tup[2]

    def distance(self, v):
        return math.sqrt(math.pow(self.x - v.x, 2) + math.pow(self.y - v.y, 2) + math.pow(self.z - v.z, 2))

    def __str__(self):
        return f'({self.x},{self.y},{self.z})'

    __repr__ = __str__

    def __eq__(self, item):
        if isinstance(item, Vect3D):
            return self.x == item.x and self.y == item.y and self.z == item.z
        return False

    def __hash__(self):
        return hash((self.x, self.y, self.z))

class Scanner:
    def __init__(self):
        self.beacons = []
        self.distances = []
        self.relative_position = {}

    def add_beacon(self, tup):
        beacon = Vect3D(tup)
        self.beacons.append(beacon)

    def empty(self):
        return len(self.beacons) == 0

    def compute_distances(self):
        self.distances = {}
        self.distances2 = {}
        for x in range(len(self.beacons)):
            for y in range(x+1, len(self.beacons)):
                a = self.beacons[x]
                b = self.beacons[y]
                d = a.distance(b)
                self.distances[(a,b)] = d
                if a not in self.distances2:
                    self.distances2[a] = []
                if b not in self.distances2:
                    self.distances2[b] = []
                self.distances2[a].append(d)
                self.distances2[b].append(d)

    def overlaps(self, scanner):
        matches = 0
        for (v1, v2), dv in scanner.distances.items():
            for (u1, u2), du in self.distances.items():
                if dv == du:
                    matches += 1
        if matches < 12:
            return None, None, None

        same_points = []
        for u1, u_distances in self.distances2.items():
            max_intersect = 0
            for v1, distances in scanner.distances2.items():
                intersect = 0
                for d in u_distances:
                    if d in distances:
                        intersect += 1
                if intersect > max_intersect:
                    max_intersect = intersect
                    same_point = v1
            if max_intersect > 1:
                same_points.append((u1, same_point))

        sp2 = [x[1] for x in same_points]
        orientations = {
            'xyz': sp2,
            'xzy': [Vect3D((b.x, b.z, b.y)) for b in sp2],
            'yxz': [Vect3D((b.y, b.x, b.z)) for b in sp2],
            'yzx': [Vect3D((b.y, b.z, b.x)) for b in sp2],
            'zxy': [Vect3D((b.z, b.x, b.y)) for b in sp2],
            'zyx': [Vect3D((b.z, b.y, b.x)) for b in sp2],
        }
        for x in range(-1,2,2):
            for y in range(-1,2,2):
                for z in range(-1,2,2):
                    for orientation_id, orientation in orientations.items():
                        for j in range(1, len(orientation)):
                            for i in range(1, len(same_points)):
                                u0 = same_points[i-1][0]
                                u1 = same_points[i][0]
                                v0 = orientation[j-1]
                                v1 = orientation[j]
                                if u0.x + x * v0.x != u1.x + x * v1.x or u0.y + y * v0.y != u1.y + y * v1.y  or u0.z + z * v0.z != u1.z + z * v1.z:
                                    break
                                else:
                                    return Vect3D((u1.x+x*v1.x, u1.y+y*v1.y, u1.z+z*v1.z)), Vect3D((x, y, z)), orientation_id
                    

        raise ValueError('scanner position not found', same_points)

    def clean(self):
        self.beacons = []
        self.distances = {}
        self.distances2 = {}


def main(file_name):
    scanners = []
    current_scanner = None
    with open(file_name) as f:
        for line in f.readlines():
            line = line.strip()
            if not line:
                continue

            if line.startswith('--- scanner'):
                current_scanner = Scanner()
                scanners.append(current_scanner)
                continue

            coords = tuple(map(int, line.split(',')))
            current_scanner.add_beacon(coords)


    for s in scanners:
        s.compute_distances()

    scanner_positions = {}
    scanner_positions[0] = {'relative_to': 0, 'position': Vect3D((0,0,0))}


    while True:
        scanners_with_beacons = len([x for x in scanners if len(x.beacons)])
        if scanners_with_beacons == 1:
            break
        print(scanners_with_beacons)
        for x in range(len(scanners)):
            for y in range(x+1, len(scanners)):
                if scanners[x].empty() or scanners[y].empty():
                    continue
                scanner_position, scanner_orientation, orientation_id = scanners[x].overlaps(scanners[y])
                if scanner_position is not None:
                    if y not in scanner_positions:
                        scanner_positions[y] = {'relative_to': x, 'position': scanner_position,
                                                'orientation': scanner_orientation, 'axis': orientation_id }
                    for beacon in scanners[y].beacons:
                        beacon_in_0 = Vect3D((
                            scanner_position.x - scanner_orientation.x * beacon.__getattribute__(orientation_id[0]),
                            scanner_position.y - scanner_orientation.y * beacon.__getattribute__(orientation_id[1]),
                            scanner_position.z - scanner_orientation.z * beacon.__getattribute__(orientation_id[2]),
                        ))
                        if beacon_in_0 not in scanners[x].beacons:
                            scanners[x].beacons.append(beacon_in_0)
                    scanners[x].compute_distances()
                    scanners[y].clean()

    # Fix scanners without reference to (0,0,0)
    while True:
        changes = False
        for key, value in scanner_positions.items():
            if value['relative_to'] != 0:
                base = scanner_positions[value['relative_to']]
                if base['relative_to'] != 0:
                    continue
                changes = True
                value['relative_to'] = 0
                value['position'] = Vect3D((base['position'].__getattribute__('x') - base['orientation'].x * value['position'].__getattribute__(value['axis'][0]),
                                            base['position'].__getattribute__('y') - base['orientation'].y * value['position'].__getattribute__(value['axis'][1]),
                                            base['position'].__getattribute__('z') - base['orientation'].z * value['position'].__getattribute__(value['axis'][2])
                                            ,))

        if not changes:
            break

    max_distance = 0
    for s1p in scanner_positions.values():
        for s2p in scanner_positions.values():
            s1 = s1p['position']
            s2 = s2p['position']
            d = abs(s1.x - s2.x) + abs(s1.y - s2.y) + abs(s1.z - s2.z)
            max_distance = max(d, max_distance)
    return len(scanners[0].beacons), max_distance

if __name__ == '__main__':
    r1, r2 = main('19_input.txt')
    print(f'Part 1: {r1}\nPart 2: {r2}')

