package main

import (
	_ "embed"
	"testing"
)

//go:embed input_15_test.txt
var testInput string

func TestShortestPath(t *testing.T) {
	r := shortestPath(testInput, 1)
	expected := 40
	if r != expected {
		t.Errorf("expected %d, got %d", expected, r)
	}
}

func TestShortestPath2(t *testing.T) {
	r := shortestPath(testInput, 5)
	expected := 315
	if r != expected {
		t.Errorf("expected %d, got %d", expected, r)
	}
}
