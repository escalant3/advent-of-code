package main

import (
	_ "embed"
	"fmt"
	"math"
	"strconv"
	"strings"
)

//go:embed input_15.txt
var input string

type point struct {
	x, y int
}

func shortestPath(caveStr string, expandLevel int) int {
	var cave [][]int
	var unvisited []point

	for i, line := range strings.Split(caveStr, "\n") {
		cave = append(cave, make([]int, len(line)))
		for j := range line {
			value, _ := strconv.Atoi(line[j : j+1])
			cave[i][j] = value
		}
	}

	// Expand cave
	var expandedCave [][]int
	for i := 0; i < expandLevel*len(cave); i++ {
		expandedCave = append(expandedCave, make([]int, len(cave)*expandLevel))
	}
	for x := 0; x < expandLevel*len(cave); x++ {
		for y := 0; y < expandLevel*len(cave); y++ {
			unvisited = append(unvisited, point{x, y})
			risk := cave[x%len(cave)][y%len(cave)] + x/len(cave) + y/len(cave)
			if risk > 9 {
				risk = risk - 9
				if risk < 1 || risk > 9 {
					panic("risk")
				}
			}
			expandedCave[x][y] = risk
		}
	}

	cave = expandedCave

	var distances [][]int
	size := len(cave)
	for i := 0; i < len(cave); i++ {
		distances = append(distances, make([]int, size))
		for j := 0; j < len(cave); j++ {
			distances[i][j] = math.MaxInt
		}
	}

	distances[0][0] = 0

	for len(unvisited) > 0 {
		currentIndex := getNextIndex(distances, unvisited)
		current := unvisited[currentIndex]
		neighbors := []point{
			{current.x, current.y - 1},
			{current.x - 1, current.y},
			{current.x + 1, current.y},
			{current.x, current.y + 1},
		}
		for _, n := range neighbors {
			if contains(unvisited, n) {
				distance := distances[current.x][current.y] + cave[n.x][n.y]
				if distance < distances[n.x][n.y] {
					distances[n.x][n.y] = distance
				}
			}
		}
		unvisited = append(unvisited[0:currentIndex], unvisited[currentIndex+1:]...)
	}

	return distances[size-1][size-1]
}

func contains(s []point, p point) bool {
	for _, item := range s {
		if p == item {
			return true
		}
	}
	return false
}

func getNextIndex(distances [][]int, unvisited []point) int {
	min := math.MaxInt
	nextIndex := 0
	for i, p := range unvisited {
		if distances[p.x][p.y] < min {
			min = distances[p.x][p.y]
			nextIndex = i
		}
	}
	return nextIndex
}

func main() {
	r := shortestPath(input, 1)
	fmt.Println("Part 1:", r)
	r = shortestPath(input, 5)
	fmt.Println("Part 2:", r)
}
