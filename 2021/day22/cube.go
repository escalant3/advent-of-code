package main

import "fmt"

type Cube struct {
	xStart, xEnd, yStart, yEnd, zStart, zEnd int
	state                                    bool
}

func (c *Cube) isValid() bool {
	return c.xStart <= c.xEnd && c.yStart <= c.yEnd && c.zStart <= c.zEnd
}

func abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}

func (c *Cube) Volume() int {
	return abs(c.xEnd-c.xStart+1) * abs(c.yEnd-c.yStart+1) * abs(c.zEnd-c.zStart+1)
}

func (c *Cube) Intersects(x Cube) bool {
	return max(c.xStart, x.xStart) <= min(c.xEnd, x.xEnd) && max(c.yStart, x.yStart) <= min(c.yEnd, x.yEnd) && max(c.zStart, x.zStart) <= min(c.zEnd, x.zEnd)
}

func (c *Cube) SolveIntersection(newCube Cube) []Cube {
	var cs []Cube
	// -X expansion
	cs = append(cs, Cube{
		c.xStart,
		newCube.xStart - 1,
		c.yStart,
		c.yEnd,
		c.zStart,
		c.zEnd,
		c.state})
	// X expansion
	cs = append(cs, Cube{
		newCube.xEnd + 1,
		c.xEnd,
		c.yStart,
		c.yEnd,
		c.zStart,
		c.zEnd,
		c.state})
	// -Y expansion
	cs = append(cs, Cube{
		max(c.xStart, newCube.xStart),
		min(newCube.xEnd, c.xEnd),
		c.yStart,
		newCube.yStart - 1,
		c.zStart,
		c.zEnd,
		c.state})
	// Y expansion
	cs = append(cs, Cube{
		max(c.xStart, newCube.xStart),
		min(newCube.xEnd, c.xEnd),
		newCube.yEnd + 1,
		c.yEnd,
		c.zStart,
		c.zEnd,
		c.state})
	// -Z expansion
	cs = append(cs, Cube{
		max(c.xStart, newCube.xStart),
		min(newCube.xEnd, c.xEnd),
		max(c.yStart, newCube.yStart),
		min(c.yEnd, newCube.yEnd),
		newCube.zEnd + 1,
		c.zEnd,
		c.state})
	// Z expansion
	cs = append(cs, Cube{
		max(c.xStart, newCube.xStart),
		min(newCube.xEnd, c.xEnd),
		max(c.yStart, newCube.yStart),
		min(c.yEnd, newCube.yEnd),
		c.zStart,
		newCube.zStart - 1,
		c.state})

	var cubes []Cube
	for _, cube := range cs {
		if cube.isValid() {
			cubes = append(cubes, cube)
		}
	}

	for i := 0; i < len(cubes); i++ {
		for j := i + 1; j < len(cubes); j++ {
			if cubes[i].Intersects(cubes[j]) {
				panic("new cubes intersect")
			}
		}
	}

	return cubes
}

func (c Cube) String() string {
	s := fmt.Sprintf("%d,%d,%d,%d,%d,%d %t\n", c.xStart, c.xEnd, c.yStart, c.yEnd, c.zStart, c.zEnd, c.state)
	for x := c.xStart; x <= c.xEnd; x++ {
		for y := c.yStart; y <= c.yEnd; y++ {
			for z := c.zStart; z <= c.zEnd; z++ {
				s += fmt.Sprintf("%d,%d,%d\n", x, y, z)
			}
		}
	}
	return s
}
