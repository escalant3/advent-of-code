package main

import (
	_ "embed"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

func parseInt(s string) int {
	r, _ := strconv.Atoi(s)
	return r
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func part1(input string) int {
	re := regexp.MustCompile(`(on|off) x=(-?\d+)..(-?\d+),y=(-?\d+)..(-?\d+),z=(-?\d+)..(-?\d+)`)
	world := make(map[string]bool)
	for _, line := range strings.Split(input, "\n") {
		r := re.FindStringSubmatch(line)
		instruction := r[1]
		xStart := max(parseInt(r[2]), -50)
		xEnd := min(parseInt(r[3]), 50)
		yStart := max(parseInt(r[4]), -50)
		yEnd := min(parseInt(r[5]), 50)
		zStart := max(parseInt(r[6]), -50)
		zEnd := min(parseInt(r[7]), 50)
		for x := xStart; x <= xEnd; x++ {
			for y := yStart; y <= yEnd; y++ {
				for z := zStart; z <= zEnd; z++ {
					key := fmt.Sprintf("%d_%d_%d", x, y, z)
					world[key] = instruction == "on"
				}
			}
		}
	}

	sum := 0
	for _, v := range world {
		if v {
			sum++
		}
	}
	return sum
}

func part2(input string) int {
	re := regexp.MustCompile(`(on|off) x=(-?\d+)..(-?\d+),y=(-?\d+)..(-?\d+),z=(-?\d+)..(-?\d+)`)
	var cubes []Cube
	for _, line := range strings.Split(input, "\n") {
		r := re.FindStringSubmatch(line)
		instruction := r[1]
		xStart := parseInt(r[2])
		xEnd := parseInt(r[3])
		yStart := parseInt(r[4])
		yEnd := parseInt(r[5])
		zStart := parseInt(r[6])
		zEnd := parseInt(r[7])
		cube := Cube{xStart, xEnd, yStart, yEnd, zStart, zEnd, instruction == "on"}
		var newCubes []Cube
		for _, c := range cubes {
			if c.Intersects(cube) {
				newCubes = append(newCubes, c.SolveIntersection(cube)...)
				continue
			}
			newCubes = append(newCubes, c)
		}
		cubes = newCubes
		cubes = append(cubes, cube)
	}

	sum := 0
	for _, c := range cubes {
		if c.state {
			sum += c.Volume()
		}
	}
	return sum
}

//go:embed input_22.txt
var input string

func main() {
	fmt.Println("Part 1:", part1(input))
	fmt.Println("Part 2:", part2(input))
}
