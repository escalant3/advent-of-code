package main

import (
	_ "embed"
	"testing"
)

//go:embed input_22_test1.txt
var testInput1 string

//go:embed input_22_test2.txt
var testInput2 string

//go:embed input_22_test3.txt
var testInput3 string

func TestSample(t *testing.T) {
	r := part1(testInput1)
	if r != 39 {
		t.Errorf("expected: %d; got: %d", 39, r)
	}
}
func TestSample2Part1(t *testing.T) {
	r := part1(testInput2)
	if r != 590784 {
		t.Errorf("expected: %d; got: %d", 590784, r)
	}
}

func TestSamplePart2(t *testing.T) {
	r := part2(testInput1)
	if r != 39 {
		t.Errorf("expected: %d; got: %d", 39, r)
	}
}

func TestSample3Part2(t *testing.T) {
	r := part2(testInput3)
	expectation := 2758514936282235
	if r != expectation {
		t.Errorf("expected %d, got %d", expectation, r)
	}
}
