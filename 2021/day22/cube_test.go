package main

import (
	"fmt"
	"testing"
)

func TestIntersection(t *testing.T) {
	c1 := Cube{10, 12, 10, 12, 10, 12, true}
	c2 := Cube{11, 13, 11, 13, 11, 13, true}
	if !c1.Intersects(c2) {
		t.Errorf("expected to interect")
	}
}

func TestSplit(t *testing.T) {
	c1 := Cube{10, 12, 10, 12, 10, 12, true}
	c2 := Cube{11, 13, 11, 13, 11, 13, true}
	cubes := c1.SolveIntersection(c2)
	cubes = append(cubes, c2)

	sum := 0
	for _, c := range cubes {
		fmt.Printf("%v\n", c)
		if c.state {
			fmt.Println(c.Volume())
			sum += c.Volume()
		}
	}
	if sum != 46 {
		t.Errorf("expected %d, got %d", 46, sum)
	}

	fmt.Println("-----------------------------")
	fmt.Println(cubes)

	c3 := Cube{9, 11, 9, 11, 9, 11, false}
	var newCubes []Cube
	for _, c := range cubes {
		if c.Intersects(c3) {
			newCubes = append(newCubes, c.SolveIntersection(c3)...)
			continue
		}
		newCubes = append(newCubes, c)
	}
	cubes = newCubes
	cubes = append(cubes, c3)
	fmt.Println(cubes)

	sum = 0
	for _, c := range cubes {
		fmt.Printf("%v\n", c)
		if c.state {
			fmt.Println(c.Volume())
			sum += c.Volume()
		}
	}

	fmt.Println("***************")
	fmt.Println(cubes)
	if sum != 38 {
		t.Errorf("expected %d, got %d", 38, sum)
	}
}
