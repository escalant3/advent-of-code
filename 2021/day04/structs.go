package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

type Game struct {
	numberList    []int
	boards        []board
	currentNumber int
}

func (g *Game) initFromInput(input string) {
	data := strings.Split(input, "\n")
	numbers := data[0]
	boardsStr := data[1:]

	g.numberList = atoiSlice(strings.Split(numbers, ","))
	b := board{}

	for _, line := range boardsStr {
		line = strings.Trim(line, " ")
		if line == "" {
			continue
		}

		boardComplete := b.addRow(line)
		if boardComplete {
			g.addBoard(b)
			b = board{}
		}
	}
}

func (g *Game) addBoard(b board) {
	g.boards = append(g.boards, b)
}

func (g *Game) drawNumber() {
	n := g.numberList[g.currentNumber]
	for i := range g.boards {
		b := &g.boards[i]
		b.markNumber(n)
	}
	g.currentNumber++
}

func (g *Game) boardWithBingo() int {
	for i := range g.boards {
		b := &g.boards[i]
		if b.hasBingo() {
			return i
		}
	}
	return -1
}

func (g *Game) boardScore(boardIndex int) int {
	return g.boards[boardIndex].score() * g.numberList[g.currentNumber-1]
}

func (g *Game) deleteBoardAtIndex(idx int) {
	g.boards = append(g.boards[:idx], g.boards[idx+1:]...)
}

func (g *Game) isLastBoard() bool {
	return len(g.boards) == 1
}

type cell struct {
	value int
	drawn bool
}

type board struct {
	rows     [5][5]cell
	rowCount int
}

func atoiSlice(slice []string) []int {
	var r []int
	for _, value := range slice {
		intValue, err := strconv.Atoi(value)
		if err != nil {
			panic(fmt.Sprintf("cannot convert to []int %v", slice))
		}
		r = append(r, intValue)
	}
	return r
}

func (b *board) addRow(line string) (complete bool) {
	if b.rowCount >= 5 {
		panic("board overflow")
	}

	space := regexp.MustCompile(`\s+`)
	lineNumbersStr := strings.Split(space.ReplaceAllString(line, " "), " ")
	for i, numberStr := range lineNumbersStr {
		number, err := strconv.Atoi(numberStr)
		if err != nil {
			panic(fmt.Sprintf("cannot convert %s into int", numberStr))
		}
		b.rows[b.rowCount][i].value = number
	}
	b.rowCount++

	return b.rowCount == 5
}

func (b *board) markNumber(n int) {
	for i := range b.rows {
		for j := 0; j < 5; j++ {
			if b.rows[i][j].value == n {
				b.rows[i][j].drawn = true
				return
			}
		}
	}
}

func (b *board) hasBingo() bool {
OUTER_ROWS:
	for _, row := range b.rows {
		for _, cell := range row {
			if !cell.drawn {
				continue OUTER_ROWS
			}
		}
		return true
	}

OUTER_COLUMNS:
	for i := 0; i < 5; i++ {
		for _, row := range b.rows {
			if !row[i].drawn {
				continue OUTER_COLUMNS
			}
		}
		return true
	}

	return false
}

func (b *board) score() int {
	var sum int
	for _, row := range b.rows {
		for _, cell := range row {
			if !cell.drawn {
				sum += cell.value
			}
		}
	}
	return sum
}
