package main

import (
	_ "embed"
	"fmt"
)

//go:embed input_04.txt
var input string

func part1(g Game) int {
	for {
		g.drawNumber()
		if boardIdx := g.boardWithBingo(); boardIdx != -1 {
			return g.boardScore(boardIdx)
		}
	}
}

func part2(g Game) int {
	for {
		g.drawNumber()
		for boardIdx := g.boardWithBingo(); boardIdx != -1; {
			if g.isLastBoard() {
				return g.boardScore(boardIdx)
			}
			g.deleteBoardAtIndex(boardIdx)
			boardIdx = g.boardWithBingo()
		}
	}
}

func main() {
	game := Game{}
	game.initFromInput(input)
	result := part1(game)
	fmt.Printf("Part 1: %d\n", result)

	result = part2(game)
	fmt.Printf("Part 2: %d\n", result)
}
