package main

import (
	_ "embed"
	"testing"
)

//go:embed input_04_test.txt
var testInput string

func TestPart1(t *testing.T) {
	game := Game{}
	game.initFromInput(testInput)
	result := part1(game)
	if result != 4512 {
		t.Errorf("Got %v; Expected 4512", result)
	}
}

func TestPart2(t *testing.T) {
	game := Game{}
	game.initFromInput(testInput)
	result := part2(game)
	if result != 1924 {
		t.Errorf("Got %v; Expected 1924", result)
	}
}
