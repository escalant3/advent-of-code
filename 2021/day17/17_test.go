package main

import "testing"

func TestSample(t *testing.T) {
	targetAreaStr := "target area: x=20..30, y=-10..-5"
	highest, validSpeeds := solve(targetAreaStr)
	if highest != 45 {
		t.Errorf("expected: 45; got: %d", highest)
	}
	if validSpeeds != 112 {
		t.Errorf("expected: 112, got: %d", validSpeeds)
	}
}

func TestSimulateValidVelocity(t *testing.T) {
	velocities := []Pair{{7, 2}, {6, 3}, {9, 0}}
	ta := TargetArea{20, -10, 30, -5}
	for _, v := range velocities {
		r := simulate(ta, v)
		if !r.valid {
			t.Errorf("Velocity %v expected to be valid", v)
		}
	}
}

func TestSimulateInvalidVelocity(t *testing.T) {
	velocities := []Pair{{17, -4}}
	ta := TargetArea{20, -10, 30, -5}
	for _, v := range velocities {
		r := simulate(ta, v)
		if r.valid {
			t.Errorf("Velocity %v expected to be invalid", v)
		}
	}
}
