package main

import (
	"fmt"
	"regexp"
	"strconv"
)

type TargetArea struct {
	x1, y1, x2, y2 int
}

type Pair struct {
	x, y int
}

type Trajectory struct {
	rightMostPoint int
	highestPoint   int
	valid          bool
}

func NewTargetArea(s string) TargetArea {
	tare := regexp.MustCompile(`target area: x=(-?\d+)..(-?\d+), y=(-?\d+)..(-?\d+)`)
	r := tare.FindStringSubmatch(s)
	return TargetArea{
		parseInt(r[1]),
		parseInt(r[3]),
		parseInt(r[2]),
		parseInt(r[4]),
	}
}

func parseInt(s string) int {
	r, err := strconv.Atoi(s)
	if err != nil {
		panic("error parsing input")
	}
	return r
}

func simulate(ta TargetArea, velocity Pair) Trajectory {
	var position Pair
	var t Trajectory
	for position.x <= ta.x2 && position.y >= ta.y1 {
		if position.x >= ta.x1 && position.y <= ta.y2 {
			t.valid = true
			return t
		}
		position.x += velocity.x
		position.y += velocity.y

		if velocity.x > 0 {
			velocity.x--
		} else if velocity.x < 0 {
			velocity.x++
		}
		velocity.y--

		if position.x > t.rightMostPoint {
			t.rightMostPoint = position.x
		}
		if position.y > t.highestPoint {
			t.highestPoint = position.y
		}
	}
	return t
}

func solve(targetAreaStr string) (int, int) {
	highest := 0
	validSpeeds := 0
	ta := NewTargetArea(targetAreaStr)
	for x := 0; x < 2000; x++ {
		for y := ta.y1; y < 2000; y++ {
			trajectory := simulate(ta, Pair{x, y})
			if trajectory.valid {
				validSpeeds++
				if trajectory.highestPoint > highest {
					highest = trajectory.highestPoint
				}
			}
			if trajectory.rightMostPoint > ta.x2 {
				break
			}
		}
	}
	return highest, validSpeeds
}

func main() {
	r, v := solve("target area: x=241..273, y=-97..-63")
	fmt.Println("Part 1:", r)
	fmt.Println("Part 2:", v)
}
