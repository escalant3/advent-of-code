package main

import (
	_ "embed"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

//go:embed input_05.txt
var input string

type coordinate struct {
	x int
	y int
}

type lineOfVent struct {
	start coordinate
	end   coordinate
}

func (lv *lineOfVent) getPoints(includeDiagonals bool) []coordinate {
	var result []coordinate
	if lv.start.x == lv.end.x {
		start := lv.start.y
		end := lv.end.y
		if start > end {
			start, end = end, start
		}
		for i := start; i <= end; i++ {
			result = append(result, coordinate{x: lv.start.x, y: i})
		}
		return result

	} else if lv.start.y == lv.end.y {
		start := lv.start.x
		end := lv.end.x
		if start > end {
			start, end = end, start
		}
		for i := start; i <= end; i++ {
			result = append(result, coordinate{x: i, y: lv.start.y})
		}
		return result

	} else if includeDiagonals {
		start := lv.start
		end := lv.end

		if lv.start.x > lv.end.x {
			start, end = end, start
		}

		j := start.y
		yStep := 1
		if start.y > end.y {
			yStep = -1
		}

		for i := start.x; i <= end.x; i++ {
			result = append(result, coordinate{x: i, y: j})
			j += yStep
		}
		return result

	} else {
		return result
	}
}

func main() {
	result := part1(input)
	fmt.Printf("Part 1: %d\n", result)
	result = part2(input)
	fmt.Printf("Part 2: %d\n", result)
}

func parseInput(input string) (linesOfVents []lineOfVent) {
	lines := strings.Split(input, "\n")
	regex := regexp.MustCompile(`(\d+),(\d+) -> (\d+),(\d+)`)
	for _, line := range lines {
		if line == "" {
			continue
		}
		result := regex.FindStringSubmatch(line)
		if len(result) != 5 {
			panic("error parsing line")
		}

		startX, _ := strconv.Atoi(result[1])
		startY, _ := strconv.Atoi(result[2])
		endX, _ := strconv.Atoi(result[3])
		endY, _ := strconv.Atoi(result[4])

		start := coordinate{x: startX, y: startY}
		end := coordinate{x: endX, y: endY}

		linesOfVents = append(linesOfVents, lineOfVent{start, end})
	}
	return
}

func solveProblem(linesOfVents []lineOfVent, includeDiagonals bool) int {
	diagram := make(map[coordinate]int)

	for _, lineOfVent := range linesOfVents {
		for _, point := range lineOfVent.getPoints(includeDiagonals) {
			diagram[point] += 1
		}
	}

	sum := 0
	for _, pointValue := range diagram {
		if pointValue > 1 {
			sum += 1
		}
	}

	return sum
}

func part1(input string) int {
	linesOfVents := parseInput(input)
	return solveProblem(linesOfVents, false)
}

func part2(input string) int {
	linesOfVents := parseInput(input)
	return solveProblem(linesOfVents, true)
}
