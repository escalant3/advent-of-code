package main

import (
	_ "embed"
	"testing"
)

//go:embed input_05_test.txt
var testInput string

func TestPart1(t *testing.T) {
	result := part1(testInput)
	if result != 5 {
		t.Errorf("expected 5, got %v", result)
	}
}

func TestPart2(t *testing.T) {
	result := part2(testInput)
	if result != 12 {
		t.Errorf("expected 12, got %v", result)
	}
}
