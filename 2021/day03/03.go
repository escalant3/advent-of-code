package main

import (
	_ "embed"
	"fmt"
	"strconv"
	"strings"
)

//go:embed input_03.txt
var input string

func bitSliceToDecimal(slice []int) int64 {
	r, err := strconv.ParseInt(strings.Trim(strings.Replace(fmt.Sprint(slice), " ", "", -1), "[]"), 2, 64)
	if err != nil {
		panic("wrong binary number")
	}

	return r
}

func part1(diagnostics []string) int64 {
	size := len(diagnostics[0])
	gamma := make([]int, size)
	epsilon := make([]int, size)
	for bitIdx := 0; bitIdx < size; bitIdx++ {
		gammaCounter := 0
		epsilonCounter := 0
		for _, diagnostic := range diagnostics {
			if diagnostic[bitIdx] == '1' {
				gammaCounter++
			} else {
				epsilonCounter++
			}
		}
		if gammaCounter > epsilonCounter {
			gamma[bitIdx] = 1
		} else {
			epsilon[bitIdx] = 1
		}
	}

	gammaValue := bitSliceToDecimal(gamma)
	epsilonValue := bitSliceToDecimal(epsilon)

	return gammaValue * epsilonValue
}

func filterDiagnostics(diagnostics []string, position int, value byte) []string {
	var result []string
	for _, d := range diagnostics {
		if d[position] == value {
			result = append(result, d)
		}
	}
	return result
}

func countPositionalBits(s []string, position int) (int, int) {
	numberOfOnes := 0
	numberOfZeroes := 0
	for _, diagnostic := range s {
		if diagnostic[position] == '1' {
			numberOfOnes++
		} else {
			numberOfZeroes++
		}
	}
	return numberOfOnes, numberOfZeroes
}

func getOxygenRating(diagnostics []string, bitIdx int) int64 {
	size := len(diagnostics)
	if size == 0 {
		panic("no diagnostics left")
	}
	if size == 1 {
		rating, err := strconv.ParseInt(diagnostics[0], 2, 64)
		if err != nil {
			panic("error converting diagnostic to decimal")
		}
		return rating
	}

	numberOfOnes, numberOfZeroes := countPositionalBits(diagnostics, bitIdx)

	if numberOfOnes >= numberOfZeroes {
		return getOxygenRating(filterDiagnostics(diagnostics, bitIdx, '1'), bitIdx+1)
	} else {
		return getOxygenRating(filterDiagnostics(diagnostics, bitIdx, '0'), bitIdx+1)
	}
}

func getCO2Rating(diagnostics []string, bitIdx int) int64 {
	size := len(diagnostics)
	if size == 0 {
		panic("no diagnostics left")
	}
	if size == 1 {
		rating, err := strconv.ParseInt(diagnostics[0], 2, 64)
		if err != nil {
			panic("error converting diagnostic to decimal")
		}
		return rating
	}

	numberOfOnes, numberOfZeroes := countPositionalBits(diagnostics, bitIdx)

	if numberOfZeroes <= numberOfOnes {
		return getCO2Rating(filterDiagnostics(diagnostics, bitIdx, '0'), bitIdx+1)
	} else {
		return getCO2Rating(filterDiagnostics(diagnostics, bitIdx, '1'), bitIdx+1)
	}
}

func part2(diagnostics []string) int64 {
	oxigen := getOxygenRating(diagnostics, 0)
	co2 := getCO2Rating(diagnostics, 0)
	return oxigen * co2
}

func main() {
	fmt.Printf("Part 1 result is %d\n", part1(strings.Fields(input)))
	fmt.Printf("Part 2 result is %d\n", part2(strings.Fields(input)))
}
