package main

import (
	_ "embed"
	"strings"
	"testing"
)

//go:embed input_03_test.txt
var testInput string

func TestPart1(t *testing.T) {
	result := part1(strings.Fields(testInput))
	if result != 198 {
		t.Errorf("Got %d, expected 198", result)
	}
}

func TestPart2(t *testing.T) {
	result := part2(strings.Fields(testInput))
	if result != 230 {
		t.Errorf("Got %d, expected 230", result)
	}
}
