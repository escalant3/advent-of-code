package main

import (
	_ "embed"
	"fmt"
	"strconv"
	"strings"
)

//go:embed input_06.txt
var input string

func simulate(input string, days int) uint64 {
	var p [9]uint64
	populationStr := strings.Split(input, ",")

	for _, fstr := range populationStr {
		fishAge, _ := strconv.Atoi(fstr)
		p[fishAge]++
	}

	for i := 0; i < days; i++ {
		aux := p[0]
		for j := 1; j <= 8; j++ {
			p[j-1] = p[j]
		}
		p[8] = aux
		p[6] += aux
	}

	sum := uint64(0)
	for _, value := range p {
		sum += value
	}
	return sum
}

func main() {
	result := simulate(input, 80)
	fmt.Printf("Part 1: %d\n", result)
	result = simulate(input, 256)
	fmt.Printf("Part 2: %d\n", result)
}
