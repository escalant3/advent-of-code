package main

import (
	_ "embed"
	"fmt"
	"testing"
)

//go:embed input_06_test.txt
var testInput string

func TestSimulate(t *testing.T) {
	const cases int = 3
	expectations := [cases]uint64{26, 5934, 26984457539}
	days := [cases]int{18, 80, 256}
	for i := 0; i < cases; i++ {
		fmt.Printf("%v, %v\n", expectations[i], days[i])
		result := simulate(testInput, days[i])
		if result != expectations[i] {
			t.Errorf("expected: 	%v. got: %v", expectations[i], result)
		}
	}
}
