package main

import (
	_ "embed"
	"fmt"
	"math"
	"strconv"
	"strings"
)

func parseBinary(s string) int {
	r, err := strconv.ParseInt(s, 2, 64)
	if err != nil {
		panic("could not parse binary")
	}
	return int(r)
}

type Node struct {
	version  int
	typeId   int
	value    int // Type 4 only
	operands []Node
}

func readBinaryPackage(bitString string) (node Node, position int) {
	node.version = parseBinary(bitString[position:3])
	position += 3
	node.typeId = parseBinary(bitString[position : position+3])
	position += 3

	// Literal operator
	if node.typeId == 4 {
		numberBits := ""
		for {
			prefix := bitString[position]
			numberBits += bitString[position+1 : position+5]
			position += 5
			if prefix == '0' {
				node.value = parseBinary(numberBits)
				break
			}
		}
	} else {
		lengthTypeId := bitString[position]
		position++
		if lengthTypeId == '0' {
			length := parseBinary(bitString[position : position+15])
			position += 15
			end := position + length
			for position < end {
				n, nRead := readBinaryPackage(bitString[position:])
				node.operands = append(node.operands, n)
				position += nRead
			}
		} else {
			numberOfSubpackets := parseBinary(bitString[position : position+11])
			position += 11
			for i := 0; i < numberOfSubpackets; i++ {
				n, nRead := readBinaryPackage(bitString[position:])
				node.operands = append(node.operands, n)
				position += nRead
			}
		}
	}
	return node, position
}

func evaluateNode(node Node) int {
	switch node.typeId {

	// Sum
	case 0:
		{
			sum := 0
			for _, n := range node.operands {

				sum += evaluateNode(n)
			}
			return sum
		}

	// Product
	case 1:
		{
			prod := 1
			for _, n := range node.operands {
				prod *= evaluateNode(n)
			}
			return prod
		}

	// Minimum
	case 2:
		{
			min := math.MaxInt
			for _, n := range node.operands {
				v := evaluateNode(n)
				if v < min {
					min = v
				}
			}
			return min
		}

	// Maximum
	case 3:
		{
			max := math.MinInt
			for _, n := range node.operands {
				v := evaluateNode(n)
				if v > max {
					max = v
				}
			}
			return max
		}

	// Literal
	case 4:
		return node.value

	//Greater than
	case 5:
		{
			if evaluateNode(node.operands[0]) > evaluateNode(node.operands[1]) {
				return 1
			}
			return 0
		}

	//Less than
	case 6:
		{
			if evaluateNode(node.operands[0]) < evaluateNode(node.operands[1]) {
				return 1
			}
			return 0
		}

	//Equal to
	case 7:
		{
			if evaluateNode(node.operands[0]) == evaluateNode(node.operands[1]) {

				return 1
			}
			return 0
		}
	}

	return -1
}

func evaluateHexadecimalPackage(s string) int {
	node, _ := readBinaryPackage(binToHex(s))
	return evaluateNode(node)
}

//go:embed input_16.txt
var input string

func versionSumFromList(l []Node) int {
	sum := 0
	for _, subNode := range l {
		sum += subNode.version
		if len(subNode.operands) > 0 {
			sum += versionSumFromList(subNode.operands)
		}
	}
	return sum
}

func sumVersionsFromBinary(s string) int {
	node, _ := readBinaryPackage(s)
	sum := node.version
	sum += versionSumFromList(node.operands)
	return sum
}

func binToHex(s string) string {
	s = strings.ReplaceAll(s, "0", "0000")
	s = strings.ReplaceAll(s, "1", "0001")
	s = strings.ReplaceAll(s, "2", "0010")
	s = strings.ReplaceAll(s, "3", "0011")
	s = strings.ReplaceAll(s, "4", "0100")
	s = strings.ReplaceAll(s, "5", "0101")
	s = strings.ReplaceAll(s, "6", "0110")
	s = strings.ReplaceAll(s, "7", "0111")
	s = strings.ReplaceAll(s, "8", "1000")
	s = strings.ReplaceAll(s, "9", "1001")
	s = strings.ReplaceAll(s, "A", "1010")
	s = strings.ReplaceAll(s, "B", "1011")
	s = strings.ReplaceAll(s, "C", "1100")
	s = strings.ReplaceAll(s, "D", "1101")
	s = strings.ReplaceAll(s, "E", "1110")
	s = strings.ReplaceAll(s, "F", "1111")
	return s
}

func sumVersionsFromHex(s string) int {
	return sumVersionsFromBinary(binToHex(s))
}

func main() {
	r := sumVersionsFromHex(strings.TrimSpace(input))
	fmt.Println("Part 1", r)
	r = evaluateHexadecimalPackage(strings.TrimSpace(input))
	fmt.Println("Part 2", r)
}
