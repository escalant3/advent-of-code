package main

import "testing"

func TestSamples(t *testing.T) {
	expectations := map[string]int{
		"110100101111111000101000":                                 6,
		"00111000000000000110111101000101001010010001001000000000": 9,
		"11101110000000001101010000001100100000100011000001100000": 14,
	}
	for input, expectation := range expectations {
		r := sumVersionsFromBinary(input)
		if r != expectation {
			t.Errorf("expected %d, got %d", expectation, r)
		}
	}
}

func TestHexSamples(t *testing.T) {
	expectations := map[string]int{
		"8A004A801A8002F478":             16,
		"620080001611562C8802118E34":     12,
		"C0015000016115A2E0802F182340":   23,
		"A0016C880162017C3686B18A3D4780": 31,
	}
	for input, expectation := range expectations {
		r := sumVersionsFromHex(input)
		if r != expectation {
			t.Errorf("expected %d, got %d", expectation, r)
		}
	}
}

func TestEvaluateSamples(t *testing.T) {
	expectations := map[string]int{
		"C200B40A82":                 3,
		"04005AC33890":               54,
		"880086C3E88112":             7,
		"CE00C43D881120":             9,
		"D8005AC2A8F0":               1,
		"F600BC2D8F":                 0,
		"9C005AC2F8F0":               0,
		"9C0141080250320F1802104A08": 1,
	}
	for input, expectation := range expectations {
		r := evaluateHexadecimalPackage(input)
		if r != expectation {
			t.Errorf("expected %d, got %d", expectation, r)
		}
	}
}
