package main

import (
	_ "embed"
	"testing"
)

//go:embed input_07_test.txt
var testInput string

func TestPart1(t *testing.T) {
	result := calculateCost(testInput, linearCost)
	if result != 37 {
		t.Errorf("got %d. expected 37", result)
	}
}

func TestPart2(t *testing.T) {
	result := calculateCost(testInput, complexCost)
	if result != 168 {
		t.Errorf("got %d. expected 168", result)
	}
}
