package main

import (
	_ "embed"
	"fmt"
	"math"
	"strconv"
	"strings"
)

func calculateCost(testInput string, cost func(int, int) int) int {
	var crabs []int

	min := math.MaxInt
	max := math.MinInt

	for _, crabStr := range strings.Split(strings.Trim(testInput, "\n"), ",") {
		crab, _ := strconv.Atoi(crabStr)
		if crab < min {
			min = crab
		}
		if crab > max {
			max = crab
		}
		crabs = append(crabs, crab)
	}

	cheapestFuel := math.MaxInt
	for i := min; i <= max; i++ {
		fuelCost := 0
		for _, crab := range crabs {
			fuelCost += cost(crab, i)
		}
		if fuelCost < cheapestFuel {
			cheapestFuel = fuelCost
		}
	}

	return cheapestFuel
}

//go:embed input_07.txt
var input string

func linearCost(x, y int) int {
	return int(math.Abs(float64(x - y)))
}

func complexCost(x, y int) int {
	diff := linearCost(x, y)
	cost := 0
	for i := 0; i <= diff; i++ {
		cost += i
	}
	return cost
}

func main() {
	result := calculateCost(input, linearCost)
	fmt.Printf("Part 1: %d\n", result)
	result = calculateCost(input, complexCost)
	fmt.Printf("Part 2: %d\n", result)
}
