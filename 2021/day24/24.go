package main

import (
	_ "embed"
	"fmt"
	"strconv"
	"strings"
)

type Alu struct {
	w, x, y, z   int
	instructions []string
}

func (a *Alu) getValue(register string) int {
	switch register {
	case "w":
		return a.w
	case "x":
		return a.x
	case "y":
		return a.y
	case "z":
		return a.z
	default:
		v, err := strconv.Atoi(register)
		if err != nil {
			panic(fmt.Sprintf("invalid register %s", register))
		}
		return v
	}
}

func (a *Alu) setValue(value int, destination string) {
	switch destination {
	case "w":
		a.w = value
	case "x":
		a.x = value
	case "y":
		a.y = value
	case "z":
		a.z = value
	default:
		panic(fmt.Sprintf("invalid register %s", destination))
	}
}

func (alu *Alu) input(value int, destination string) {
	alu.setValue(value, destination)
}

func (alu *Alu) add(destination string, origin string) {
	a := alu.getValue(destination)
	b := alu.getValue(origin)
	alu.setValue(a+b, destination)
}

func (alu *Alu) multiply(destination string, origin string) {
	a := alu.getValue(destination)
	b := alu.getValue(origin)
	alu.setValue(a*b, destination)
}

func (alu *Alu) divide(destination string, origin string) {
	a := alu.getValue(destination)
	b := alu.getValue(origin)
	alu.setValue(a/b, destination)
}

func (alu *Alu) modulo(destination string, origin string) {
	a := alu.getValue(destination)
	b := alu.getValue(origin)
	alu.setValue(a%b, destination)
}

func (alu *Alu) equal(destination, origin string) {
	a := alu.getValue(destination)
	b := alu.getValue(origin)
	if a == b {
		alu.setValue(1, destination)
	} else {
		alu.setValue(0, destination)
	}
}

func (a *Alu) loadProgram(input string) {
	a.instructions = strings.Split(strings.TrimSpace(input), "\n")
}

//go:embed input_24.txt
var input string

func main() {
	a := Alu{}
	a.loadProgram(input)
	logger := 0
outer:
	for mn := 99998766699996; mn > 11111111111111; mn-- {
		logger++
		modelNumber := strconv.Itoa(mn)
		if strings.Contains(modelNumber, "0") {
			continue
		}
		if logger > 100000 {
			fmt.Printf("Program input %s\n", modelNumber)
			logger = 0
		}
		i := 0
		for i < len(modelNumber) {
			for _, ins := range a.instructions {
				//fmt.Println(ins)
				op := ins[0:3]
				switch op {
				case "inp":
					{
						c, err := strconv.Atoi(modelNumber[i : i+1])
						if err != nil {
							panic("model number error")
						}
						i++
						a.input(c, ins[4:])
					}

				case "add":
					a.add(ins[4:5], ins[6:])
				case "mul":
					a.multiply(ins[4:5], ins[6:])
				case "div":
					a.divide(ins[4:5], ins[6:])
				case "mod":
					a.modulo(ins[4:5], ins[6:])
				case "eql":
					a.equal(ins[4:5], ins[6:])
				}
			}
			result := a.getValue("z")
			if result == 0 {
				fmt.Println("z is 0 for ", modelNumber)
				break outer
			}
		}
	}
}
