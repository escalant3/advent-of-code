package main

import (
	_ "embed"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

type Paper struct {
	dots [][]bool
}

func (p *Paper) Print() {
	sum := 0
	for i := 0; i < len(p.dots); i++ {
		for j := 0; j < len(p.dots[0]); j++ {
			if p.dots[i][j] {
				sum++
				fmt.Print("#")
			} else {
				fmt.Print(".")
			}
		}
		fmt.Println()
	}
	fmt.Println("Activated points: ", sum)
	fmt.Println()
}

func (p Paper) EnableDot(x, y int) {
	p.dots[y][x] = true
}

func (p *Paper) Fold(ins instruction) {
	if ins.direction == Horizontal {
		for i := 1; i < len(p.dots)-ins.amount; i++ {
			for j := 0; j < len(p.dots[0]); j++ {
				p.dots[ins.amount-i][j] = p.dots[ins.amount-i][j] || p.dots[ins.amount+i][j]
			}
		}
		p.dots = p.dots[:ins.amount]
	} else if ins.direction == Vertical {
		for i := 0; i < len(p.dots); i++ {
			for j := 1; j < len(p.dots[0])-ins.amount; j++ {
				p.dots[i][ins.amount-j] = p.dots[i][ins.amount-j] || p.dots[i][ins.amount+j]
			}
		}
		for i := 0; i < len(p.dots); i++ {
			p.dots[i] = p.dots[i][:ins.amount]
		}
	}
}

func NewPaper(width, height int) Paper {
	var p [][]bool
	for i := 0; i <= height; i++ {
		row := make([]bool, width+1)
		p = append(p, row)
	}
	return Paper{p}
}

type point struct {
	x, y int
}

type Direction int8

const (
	Horizontal Direction = iota
	Vertical
)

type instruction struct {
	direction Direction
	amount    int
}

//go:embed input_13.txt
var input string

func origami() {
	pointRegexp := regexp.MustCompile(`(\d+),(\d+)`)
	foldRegexp := regexp.MustCompile(`fold along (x|y)=(\d+)`)
	var points []point
	var instructions []instruction
	var maxX, maxY int
	for _, line := range strings.Split(input, "\n") {
		fmt.Println(line)
		if r := pointRegexp.FindStringSubmatch(line); len(r) > 0 {
			x, _ := strconv.Atoi(r[1])
			y, _ := strconv.Atoi(r[2])
			if x > maxX {
				maxX = x
			}
			if y > maxY {
				maxY = y
			}
			points = append(points, point{x, y})
		} else if r := foldRegexp.FindStringSubmatch(line); len(r) > 0 {
			d := Horizontal
			if r[1] == "x" {
				d = Vertical
			}
			amount, _ := strconv.Atoi(r[2])
			instructions = append(instructions, instruction{d, amount})
		}
	}

	paper := NewPaper(maxX, maxY)
	for _, c := range points {
		paper.EnableDot(c.x, c.y)
	}
	paper.Print()
	for _, ins := range instructions {
		paper.Fold(ins)
		paper.Print()
		// break // Part 1
	}
}

func main() {
	origami()
}
