package main

import (
	_ "embed"
	"fmt"
	"strconv"
	"strings"
)

//go:embed input_02.txt
var input string

type Direction int8

const (
	Forward Direction = iota
	Up
	Down
)

func directionFromString(s string) Direction {
	switch s {
	case "forward":
		return Forward
	case "up":
		return Up
	case "down":
		return Down
	default:
		panic("invalid direction")
	}
}

type command struct {
	direction Direction
	units     int
}

func parseInput(input string) []command {
	str := strings.Trim(input, "\n\b")
	str_slice := strings.Split(str, "\n")
	var commands []command
	for _, command_str := range str_slice {
		command_split := strings.Split(command_str, " ")
		d := directionFromString(command_split[0])
		u, _ := strconv.Atoi(command_split[1])
		commands = append(commands, command{direction: d, units: u})
	}

	return commands
}

func part1(input string) int {
	commands := parseInput(input)
	horizontal := 0
	depth := 0
	for _, c := range commands {
		if c.direction == Forward {
			horizontal += c.units
		} else if c.direction == Up {
			depth -= c.units
		} else if c.direction == Down {
			depth += c.units
		} else {
			panic(fmt.Errorf("invalid operation %v", c))
		}
	}

	return horizontal * depth
}

func part2(input string) int {
	commands := parseInput(input)
	horizontal := 0
	depth := 0
	aim := 0
	for _, c := range commands {
		if c.direction == Forward {
			horizontal += c.units
			depth += aim * c.units
		} else if c.direction == Up {
			aim -= c.units
		} else if c.direction == Down {
			aim += c.units
		} else {
			panic(fmt.Errorf("invalid operation %v", c))
		}
	}

	return horizontal * depth
}

func main() {
	fmt.Printf("Part 1 result: %d\n", part1(input))
	fmt.Printf("Part 2 result: %d\n", part2(input))
}
