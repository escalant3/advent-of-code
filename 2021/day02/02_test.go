package main

import (
	_ "embed"
	"testing"
)

//go:embed input_02_test.txt
var testInput string

func TestPart1(t *testing.T) {
	result := part1(testInput)
	if result != 150 {
		t.Errorf("Got %d; want 150", result)
	}
}

func TestPart2(t *testing.T) {
	result := part2(testInput)
	if result != 900 {
		t.Errorf("Got %d; want 900", result)
	}
}
