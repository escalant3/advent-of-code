package main

import (
	_ "embed"
	"fmt"
	"strconv"
	"strings"
)

//go:embed input_08.txt
var input string

type configuration [10]string

func containsAll(input string, chars string) bool {
	for _, char := range strings.Split(chars, "") {
		if !strings.Contains(input, char) {
			return false
		}
	}
	return true
}

func solve(input string) (int, int) {
	lines := strings.Split(input, "\n")
	sumPart1 := 0
	sumPart2 := 0
	for _, line := range lines {
		segments := strings.Split(line, "|")
		patterns, outputs := strings.Fields(segments[0]), strings.Fields(segments[1])
		var c configuration

		for _, pattern := range patterns {
			switch len(pattern) {
			case 2:
				c[1] = pattern
			case 3:
				c[7] = pattern
			case 4:
				c[4] = pattern
			case 7:
				c[8] = pattern
			}
		}

		for _, pattern := range patterns {
			// Number 6 has length 6 and the same segments as 8,
			// except for a segment present in all 1, 7 and 4
			if len(pattern) == 6 {
				if !containsAll(pattern, c[1]) && !containsAll(pattern, c[7]) && !containsAll(pattern, c[4]) {
					c[6] = pattern
					continue
				}
			}
			// Number 9 has length 6 and the same segments as 8,
			// including a segment present in all 1, 7, and 4
			if len(pattern) == 6 {
				if containsAll(pattern, c[1]) && containsAll(pattern, c[7]) && containsAll(pattern, c[4]) {
					c[9] = pattern
					continue
				}
			}
			// Number 0 has length 6 and the same segments as 8,
			// except for a segment present in 4 but missing in 1 and 7
			if len(pattern) == 6 {
				if containsAll(pattern, c[1]) && containsAll(pattern, c[7]) && !containsAll(pattern, c[4]) {
					c[0] = pattern
					continue
				}
			}
			// Number 3 has length 5 and all segments present in 1 and 7
			// but not in 4
			if len(pattern) == 5 {
				if containsAll(pattern, c[1]) && containsAll(pattern, c[7]) && !containsAll(pattern, c[4]) {
					c[3] = pattern
					continue
				}
			}
		}

		for _, pattern := range patterns {
			if len(pattern) == 5 && containsAll(c[6], pattern) {
				c[5] = pattern
				continue
			}
			if len(pattern) == 5 && !containsAll(c[6], pattern) && !containsAll(pattern, c[1]) {
				c[2] = pattern
				continue
			}
		}

		outputValue := ""
		for _, output := range outputs {
			length := len(output)
			if length == 2 || length == 4 || length == 3 || length == 7 {
				sumPart1 += 1
			}

			// Part 2
			for index, value := range c {
				if len(value) == len(output) && containsAll(value, output) {
					outputValue += strconv.Itoa(index)
				}
			}
		}
		value, _ := strconv.Atoi(outputValue)
		sumPart2 += value
	}
	return sumPart1, sumPart2
}

func main() {
	part1, part2 := solve(input)
	fmt.Printf("Part 1: %d\n", part1)
	fmt.Printf("Part 2: %d\n", part2)
}
