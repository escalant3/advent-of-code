package main

import (
	_ "embed"
	"testing"
)

//go:embed input_08_test.txt
var testInput string

func TestProblem(t *testing.T) {
	part1, part2 := solve(testInput)
	if part1 != 26 {
		t.Errorf("got %d, expected 26", part1)
	}
	if part2 != 61229 {
		t.Errorf("got %d, expected 61229", part2)
	}
}
