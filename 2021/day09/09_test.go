package main

import (
	_ "embed"
	"testing"
)

//go:embed input_09_test.txt
var testInput string

func TestFindRisk(t *testing.T) {
	result := findRisk(testInput)
	if result != 15 {
		t.Errorf("expected: 15; got %d", result)
	}
}

func TestFindBasins(t *testing.T) {
	result := findBasins(testInput)
	if result != 1134 {
		t.Errorf("expected: 1134; got %d", result)
	}
}
