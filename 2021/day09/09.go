package main

import (
	_ "embed"
	"fmt"
	"sort"
	"strconv"
	"strings"
)

//go:embed input_09.txt
var input string

type point struct {
	x int
	y int
}

func getHeightMap(input string) [][]byte {
	var heightMap [][]byte
	lines := strings.Fields(input)
	for _, line := range lines {
		heightMap = append(heightMap, []byte(line))
	}
	return heightMap
}

func getValidNeighbors(heightMap [][]byte, p point) []point {
	var result []point
	lineSize := len(heightMap[0])
	if p.x > 0 {
		result = append(result, point{p.x - 1, p.y})
	}
	if p.x < len(heightMap)-1 {
		result = append(result, point{p.x + 1, p.y})
	}
	if p.y > 0 {
		result = append(result, point{p.x, p.y - 1})
	}
	if p.y < lineSize-1 {
		result = append(result, point{p.x, p.y + 1})
	}
	return result
}

func findLowPoints(heightMap [][]byte) []point {
	var lowPoints []point
	lineSize := len(heightMap[0])
	for i := 0; i < len(heightMap); i++ {
	outer:
		for j := 0; j < lineSize; j++ {
			for _, validNeighbor := range getValidNeighbors(heightMap, point{i, j}) {
				if heightMap[i][j] > heightMap[validNeighbor.x][validNeighbor.y] {
					continue outer
				}
			}
			lowPoints = append(lowPoints, point{i, j})
		}
	}
	return lowPoints
}

func findRisk(input string) int {
	heightMap := getHeightMap(input)
	lowPoints := findLowPoints(heightMap)
	risk := 0
	for _, lowPoint := range lowPoints {
		lp, _ := strconv.Atoi(string(heightMap[lowPoint.x][lowPoint.y]))
		risk += lp + 1
	}
	return risk
}

func contains(stack []point, needle point) bool {
	for _, p := range stack {
		if needle == p {
			return true
		}
	}
	return false
}

func findBasins(input string) int {
	var basinSizes []int
	heightMap := getHeightMap(input)
	lowPoints := findLowPoints(heightMap)

	for _, lowPoint := range lowPoints {
		pointsToExpand := []point{lowPoint}
		var pointsExpanded []point
		for len(pointsToExpand) > 0 {
			var newPoints []point
			for _, p := range pointsToExpand {
				if heightMap[p.x][p.y] == '9' {
					continue
				}
				if !contains(pointsExpanded, p) {
					pointsExpanded = append(pointsExpanded, p)
				}

				potentialPointsToExpand := getValidNeighbors(heightMap, p)
				for _, ppe := range potentialPointsToExpand {
					if !contains(pointsExpanded, ppe) {
						newPoints = append(newPoints, ppe)
					}
				}
			}
			pointsToExpand = newPoints
		}
		basinSizes = append(basinSizes, len(pointsExpanded))
	}

	sort.Sort(sort.Reverse(sort.IntSlice(basinSizes)))
	size := 1
	for _, v := range basinSizes[:3] {
		size *= v
	}
	return size
}

func main() {
	result := findRisk(input)
	fmt.Printf("Part 1: %d\n", result)
	result = findBasins(input)
	fmt.Printf("Part 2: %d\n", result)
}
