'use strict';

const Fs = require('fs');

const data = Fs.readFileSync('input03.txt', 'utf-8').trim().split('\n');

class Slope {

    constructor(treemap, speed={x:3,y:1}) {
        this.treemap = treemap;
        this.width = treemap[0].length;
        this.height = treemap.length;
        this.position = { x: 0, y: 0 };
        this.speed = speed;;
        this.finished = false;
        this.treesEncountered = 0;
    }

    move() {
        this.position.x += this.speed.x;
        this.position.y += this.speed.y;

        if (this.position.y >= this.height) {
            this.finished = true;
            return;
        }

        this.position.x %= this.width;
        if (this.hasTree(this.position.x, this.position.y)) {
            this.treesEncountered++;
        }
    }

    hasTree(posX, posY) {
        return this.treemap[posY][posX] === '#';
    }
}

const slope = new Slope(data);
while (!slope.finished) {
    slope.move();
}
console.log(`Trees encountered: ${slope.treesEncountered}`);

// Part 2
const result = [
        { x: 1, y: 1 },
        { x: 3, y: 1 },
        { x: 5, y: 1 },
        { x: 7, y: 1 },
        { x: 1, y: 2}
    ]
    .map(speed => {
        const slope = new Slope(data, speed);
        while (!slope.finished) {
            slope.move();
        }
        return slope.treesEncountered;
    })
    .reduce((acc, value) => acc * value, 1);

console.log(`Multiplication of all variants: ${result}`);
