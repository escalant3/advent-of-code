'use strict';

const Fs = require('fs')

const data =
    Fs.readFileSync('input01.txt', 'utf-8')
    .trim()
    .split('\n')
    .map(Number)

outer:
for(let i=0; i<data.length-1; i++) {
    for(let j=i+1; j<data.length; j++) {
        const [a, b] = [data[i], data[j]];
        if (a+b === 2020) {
            console.log(`${a} * ${b} = ${a*b}`)
            break outer;
        }
    }
}

outer2:
for(let i=0; i<data.length-2; i++) {
    for(let j=i+1; j<data.length-1; j++) {
        for(let k=j+1; k<data.length; k++) {
            const [a, b, c] = [data[i], data[j], data[k]];
            if (a+b+c === 2020) {
                console.log(`${a} * ${b} * ${c} = ${a*b*c}`)
                break outer2;
            }
        }
    }
}
