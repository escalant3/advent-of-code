'use strict';

const Fs = require('fs');

const validatePasswordPart1 = passwordStr => {
    const [_, minPolicy, maxPolicy, letterPolicy, password] =
        passwordStr.match(/(\d+)-(\d+) (\w+): (\w+)/);

    const matches = password.match(new RegExp(letterPolicy, 'g'));
    const appearances = matches ? matches.length : 0;
    return appearances >= Number(minPolicy) && appearances <= Number(maxPolicy);
};

const validatePasswordPart2 = passwordStr => {
    const [_, idx1, idx2, letterPolicy, password] =
        passwordStr.match(/(\d+)-(\d+) (\w+): (\w+)/);

    const firstMatches = password[Number(idx1)-1] === letterPolicy;
    const secondMatches = password[Number(idx2)-1] === letterPolicy;

    if (firstMatches && secondMatches) return false;

    return firstMatches || secondMatches;
};

const data = Fs.readFileSync('input02.txt', 'utf-8')
    .trim()
    .split('\n');

console.log(data.filter(validatePasswordPart1).length);
console.log(data.filter(validatePasswordPart2).length);
