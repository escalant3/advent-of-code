'use strict';

const Fs = require('fs');


class Rule {
    constructor(ruleString) {
        const words = ruleString.split(' ');
        this.id = words.slice(0, 2).join(' ');

        const parseContentItem = str => {
            const contentItem = str.trim().split(' ');
            return { quantity: Number(contentItem[0]), id: contentItem.slice(1, 3).join(' ') };
        };

        const nestRule = words.slice(4).join(' ').split(',');
        if (nestRule.length > 1) {
            this.contents = nestRule.map(parseContentItem);
        }
        else if (nestRule[0] === 'no other bags.') {
            this.contents = [];
        }
        else {
            this.contents = [parseContentItem(nestRule[0])];
        }
    }
}

class LuggageProcessingSystem {
    constructor(fileName) {
        const data = Fs.readFileSync(fileName, 'utf-8').trim().split('\n');
        this.rules = data.map(x => new Rule(x));
    }

    optionsForBag(bagId) {
        const result = [];
        for(let rule of this.rules) {
            if (rule.contents.some(x => x.id === bagId)) {
                result.push(rule.id);
            }
        }

        if (result.length) {
            const nested = [...new Set(result.map((x) => this.optionsForBag(x)).flat())];
            if (nested.length) {
                return [...new Set(result.concat(nested))];
            }
        }

        return result;
    }

    numberOfBags(bagId, multiplier=1) {
        return this.rules.reduce((acc, rule) => {
            if (rule.id === bagId) {
                const sum = rule.contents.reduce((acc, nestedBag) => {
                    return acc + (nestedBag.quantity) * (1 + this.numberOfBags(nestedBag.id));
                }, 0);
                return acc + sum;
            }

            return acc;
        }, 0);
    }
}

const lps = new LuggageProcessingSystem('input07.txt');
const options = lps.optionsForBag('shiny gold');
console.log(`Part 1 result: ${options.length}`);
console.log(`Part 2 result: ${lps.numberOfBags('shiny gold')}`);


/*
const Assert = require('assert');
const lpst = new LuggageProcessingSystem('test07.txt');
console.log(lpst.optionsForBag('shiny gold'));
Assert.strictEqual(lpst.optionsForBag('shiny gold').length, 4);
console.log(lpst.numberOfBags('shiny gold'));
const lpstb = new LuggageProcessingSystem('test07b.txt');
console.log(lpstb.numberOfBags('shiny gold'));
*/

