'use strict';

const Fs = require('fs');

const part1 = filename => {
    const data = Fs.readFileSync(filename, 'utf-8').trim().split('\n');
    const earliestDeparture = data[0];
    const availableBuses = data[1].split(',').filter(x => x!== 'x').map(x => Number(x));
    let departureTime = earliestDeparture;

    while (true) {
        const bus = availableBuses.find(x => departureTime % x === 0);
        if (bus) {
            return (departureTime - earliestDeparture) * bus;
        }
        departureTime++;
    }
};

const part2 = filename => {
    const data = Fs.readFileSync(filename, 'utf-8').trim().split('\n');
    return processPart2(data[1]);
};

const processPart2 = busesString => {
    const buses = busesString.split(',').map((busId, index) => busId === 'x' ? null : ({ busId: Number(busId), index })).filter(x => x);
    buses.sort((a,b) => b.busId - a.busId);

    const steps = buses.map(x => x.busId);
    let counters = buses.map(x => 0);

    // Puzzle gives a hint to start at 100 billion
    // (comment out for tests)
    counters[0] = 99999999999507;

    outer:
    while (true) {
        counters[0] += steps[0];
        for(let i=1; i<counters.length; i++) {
            const goal = counters[0] - buses[0].index + buses[i].index;
            if (goal % steps[i] !== 0) {
                continue outer;
            }
            counters[i] = goal;
        }
        return counters[0] - buses[0].index;
    }
};

console.log(`Part 1: ${part1('input13.txt')}`);
console.log(`Part 2: ${part2('input13.txt')}`);

/*
const Assert = require('assert');
Assert.strictEqual(part1('test13.txt'), 295);
Assert.strictEqual(part2('test13.txt'), 1068781);
Assert.strictEqual(processPart2('17,x,13,19'), 3417);
Assert.strictEqual(processPart2('1789,37,47,1889'), 1202161486);
Assert.strictEqual(processPart2('67,7,59,61'), 754018);
Assert.strictEqual(processPart2('67,x,7,59,61'), 779210);
Assert.strictEqual(processPart2('67,7,x,59,61'), 1261476);
Assert.strictEqual(processPart2('1789,37,47,1889'), 1202161486);
*/
