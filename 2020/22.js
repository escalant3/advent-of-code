'use strict';

const Fs = require('fs');

const deckScore = deck => deck.reverse().reduce((acc, item, index) => acc + item * (index + 1), 0);

const part1 = filename => {
    const getCards = str => str.split(':\n')[1].split('\n').map(Number);
    let [player1, player2]  = Fs.readFileSync(filename, 'utf-8').trim().split('\n\n');
    player1 = getCards(player1);
    player2 = getCards(player2);

    while (player1.length && player2.length) {
        const cardP1 = player1.splice(0, 1)[0];
        const cardP2 = player2.splice(0, 1)[0];

        if (cardP1 > cardP2) {
            player1 = player1.concat([cardP1, cardP2]);
        } else {
            player2 = player2.concat([cardP2, cardP1]);
        }
    }

    const winner = player1.length ? player1 : player2;
    return deckScore(winner);
};

const recursiveCombat = (player1, player2) => {
    const historyP1 = [];
    const historyP2 = [];
    while (player1.length && player2.length) {
        const sc1 = player1.join(',');
        const sc2 = player2.join(',');

        if (historyP1.includes(sc1) || historyP2.includes(sc2)) {
            return { winner: 'player1', deck: player1 };
        }
        historyP1.push(sc1);
        historyP2.push(sc2);

        const cardP1 = player1.splice(0, 1)[0];
        const cardP2 = player2.splice(0, 1)[0];

        if (player1.length >= cardP1 && player2.length >= cardP2) {
            const { winner } = recursiveCombat(player1.slice(0, cardP1), player2.slice(0, cardP2));  
            if (winner === 'player1') {
                player1 = player1.concat([cardP1, cardP2]);
            } else {
                player2 = player2.concat([cardP2, cardP1]);
            }
        }
        else {
            if (cardP1 > cardP2) {
                player1 = player1.concat([cardP1, cardP2]);
            } else {
                player2 = player2.concat([cardP2, cardP1]);
            }
        }
    }

    return player1.length ? { winner: 'player1', deck: player1 } : { winner: 'player2', deck: player2 };
};

const part2 = filename => {
    const getCards = str => str.split(':\n')[1].split('\n').map(Number);
    let [player1, player2]  = Fs.readFileSync(filename, 'utf-8').trim().split('\n\n');
    player1 = getCards(player1);
    player2 = getCards(player2);

    const { deck } = recursiveCombat(player1, player2);
    return deckScore(deck);
};

console.log(`Part 1: ${part1('input22.txt')}`);
console.log(`Part 2: ${part2('input22.txt')}`);
