'use strict';

const run = (input, times) => {
    const tracker = {};
    const lastSpokenIndex = Array(times).fill(null);
    const timesSpoken = Array(times).fill(0);

    input = input.split(',').map(Number);

    input.forEach((number, index) => {
        lastSpokenIndex[number] = index;
        timesSpoken[number] = 1;
    });

    let lastNumber = input[input.length-1];

    for (let i=input.length; i<times; i++) {
        const secondLastNumber = lastNumber;
        const lastSpoken = lastSpokenIndex[lastNumber];
        lastNumber = lastSpoken !== null ? (i-1) - lastSpokenIndex[lastNumber] : 0;
        lastSpokenIndex[secondLastNumber] = i-1;
        timesSpoken[lastNumber]++
    }

    return lastNumber;
};

const input = '20,9,11,0,1,2';
console.log(`Part 1: ${run(input, 2020)}`);
console.log(`Part 2: ${run(input, 30000000)}`);

const Assert = require('assert');
Assert.strictEqual(run('0,3,6', 2020), 436);
Assert.strictEqual(run('1,3,2', 2020), 1);
Assert.strictEqual(run('2,1,3', 2020), 10);
Assert.strictEqual(run('1,2,3', 2020), 27);
Assert.strictEqual(run('2,3,1', 2020), 78);
Assert.strictEqual(run('3,2,1', 2020), 438);
Assert.strictEqual(run('3,1,2', 2020), 1836);
Assert.strictEqual(run('0,3,6', 30000000), 175594);
Assert.strictEqual(run('1,3,2', 30000000), 2578);
Assert.strictEqual(run('2,1,3', 30000000), 3544142);
Assert.strictEqual(run('1,2,3', 30000000), 261214);
Assert.strictEqual(run('2,3,1', 30000000), 6895259);
Assert.strictEqual(run('3,2,1', 30000000), 18);
Assert.strictEqual(run('3,1,2', 30000000), 362);
