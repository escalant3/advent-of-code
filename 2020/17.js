'use strict';

const Fs = require('fs');

let cubeCoords = [];
for(let x=-1; x<=1; x++)
    for(let y=-1; y<=1; y++)
        for(let z=-1; z<=1; z++)
            if (x || y || z) cubeCoords.push([x,y,z])

let hypercubeCoords = [];
for(let x=-1; x<=1; x++)
    for(let y=-1; y<=1; y++)
        for(let z=-1; z<=1; z++)
            for(let w=-1; w<=1; w++)
                if (x || y || z || w) hypercubeCoords.push([x,y,z,w])

const isActive = x => x === 1;

const getNextValue = (value, surroundingActive )=> {
    if (value === 1 && (surroundingActive === 2 || surroundingActive === 3)) return 1;
    if (value === 0 && surroundingActive === 3) return 1;
    return 0;
};

const key = (x, y, z, w) => (w === undefined) ? `${x}_${y}_${z}` : `${x}_${y}_${z}_${w}`;

const getEdges = currentGeneration => {
    let minX = Infinity;
    let maxX = -Infinity;
    let minY = Infinity;
    let maxY = -Infinity;
    let minZ = Infinity;
    let maxZ = -Infinity;
    Object.keys(currentGeneration).forEach(key => {
        const [x, y, z] = key.split('_').map(Number);
        minX = Math.min(minX, x);
        minY = Math.min(minY, y);
        minZ = Math.min(minZ, z);
        maxX = Math.max(maxX, x);
        maxY = Math.max(maxY, y);
        maxZ = Math.max(maxZ, z);
    });
    return { minX, maxX, minY, maxY, minZ, maxZ };
};

const findActiveSurrounding = (generation, z, y, x) => {
    return cubeCoords.reduce((acc, coord) => {
        const dx = x + coord[2];
        const dy = y + coord[1];
        const dz = z + coord[0];
        return acc + (generation[key(dx,dy,dz)] || 0);
    }, 0);
};

const tick = currentGeneration => {
    const nextGeneration = {};
    const { minX, maxX, minY, maxY, minZ, maxZ } = getEdges(currentGeneration);
    for(let z=minZ-1; z<=maxZ+1; z++) {
        for(let y=minY-1; y<=maxY+1; y++) {
            for(let x=minX-1; x<=maxX+1; x++) {
                const activeSurrounding = findActiveSurrounding(currentGeneration, z, y, x);
                const cell = currentGeneration[key(x,y,z)] || 0;
                nextGeneration[key(x,y,z)] = getNextValue(cell, activeSurrounding);
            }
        }
    }
    return nextGeneration;
};

const part1 = (filename, cycles) => {
    const data = Fs.readFileSync(filename, 'utf-8')
        .trim()
        .split('\n')
        .map(x => x.split('').map(x => x === '#' ? 1 : 0));

    let currentGeneration = {};
    let nextGeneration;
    for(let x=0; x<data[0].length; x++) {
        for(let y=0; y<data.length; y++) {
            currentGeneration[key(x, y, 0)] = data[y][x];
        }
    }

    for(let i=0; i<cycles; i++) {
        nextGeneration = tick(currentGeneration);
        currentGeneration = nextGeneration;
    }

    return Object.values(currentGeneration).reduce((acc, item) => acc + item, 0);
};

const getEdges2 = currentGeneration => {
    let minX = Infinity;
    let maxX = -Infinity;
    let minY = Infinity;
    let maxY = -Infinity;
    let minZ = Infinity;
    let maxZ = -Infinity;
    let minW = Infinity;
    let maxW = -Infinity;
    Object.keys(currentGeneration).forEach(key => {
        const [x, y, z, w] = key.split('_').map(Number);
        minX = Math.min(minX, x);
        minY = Math.min(minY, y);
        minZ = Math.min(minZ, z);
        minW = Math.min(minW, w);
        maxX = Math.max(maxX, x);
        maxY = Math.max(maxY, y);
        maxZ = Math.max(maxZ, z);
        maxW = Math.max(maxW, w);
    });
    return { minX, maxX, minY, maxY, minZ, maxZ, minW, maxW };
};

const findActiveSurrounding2 = (generation, w, z, y, x) => {
    return hypercubeCoords.reduce((acc, coord) => {
        const dx = x + coord[3];
        const dy = y + coord[2];
        const dz = z + coord[1];
        const dw = w + coord[0];
        return acc + (generation[key(dx,dy,dz,dw)] || 0);
    }, 0);
};

const tick2 = currentGeneration => {
    const nextGeneration = {};
    const { minX, maxX, minY, maxY, minZ, maxZ, minW, maxW } = getEdges2(currentGeneration);
    for(let w=minW-1; w<=maxW+1; w++) {
        for(let z=minZ-1; z<=maxZ+1; z++) {
            for(let y=minY-1; y<=maxY+1; y++) {
                for(let x=minX-1; x<=maxX+1; x++) {
                    const activeSurrounding = findActiveSurrounding2(currentGeneration, w, z, y, x);
                    const cell = currentGeneration[key(x,y,z,w)] || 0;
                    nextGeneration[key(x,y,z,w)] = getNextValue(cell, activeSurrounding);
                }
            }
        }
    }
    return nextGeneration;
};

const part2 = (filename, cycles) => {
    const data = Fs.readFileSync(filename, 'utf-8')
        .trim()
        .split('\n')
        .map(x => x.split('').map(x => x === '#' ? 1 : 0));

    let currentGeneration = {};
    let nextGeneration;
    for(let x=0; x<data[0].length; x++) {
        for(let y=0; y<data.length; y++) {
            currentGeneration[key(x, y, 0, 0)] = data[y][x];
        }
    }

    for(let i=0; i<cycles; i++) {
        nextGeneration = tick2(currentGeneration);
        currentGeneration = nextGeneration;
    }

    return Object.values(currentGeneration).reduce((acc, item) => acc + item, 0);
};


console.log(`Part 1: ${part1('input17.txt', 6)}`);
console.log(`Part 2: ${part2('input17.txt', 6)}`);
