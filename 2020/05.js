const Fs = require('fs');

const decodePass = boardingPass => {
    const row = parseInt(boardingPass.slice(0,7).replace(/F/g, 0).replace(/B/g, 1), 2);
    const column = parseInt(boardingPass.slice(-3).replace(/L/g, 0).replace(/R/g, 1), 2);
    return { row, column, id: row * 8 +  column };
};

const data = Fs.readFileSync('input05.txt', 'utf-8').trim().split('\n');

const max = data.map(decodePass).reduce((max, item) => Math.max(max, item.id), 0);
console.log(`Highest seat ID ${max}`);

const sortedSeatIds = data.map(decodePass).map(pass => pass.id).sort((a,b) => a-b);
for (let i=0; i<sortedSeatIds.length-1; i++) {
   if (sortedSeatIds[i+1] - sortedSeatIds[i] > 1) {
       console.log(`Your seat ID is : ${sortedSeatIds[i]+1}`);
       break;
   }
}

// Tests
const Assert = require('assert');

const test = () => {
    [
        { pass: 'BFFFBBFRRR', row: 70, column: 7, id: 567 },
        { pass: 'FFFBBBFRRR', row: 14, column: 7, id: 119 },
        { pass: 'BBFFBBFRLL', row: 102, column: 4, id: 820 }
    ]
    .forEach(testCase => {
        const { row, column, id } = decodePass(testCase.pass);
        Assert.strictEqual(row, testCase.row);
        Assert.strictEqual(column, testCase.column);
        Assert.strictEqual(id, testCase.id);
    });
};

test();


