'use strict';

const Fs = require('fs');

class Tile {
    constructor(block) {
        const calculateBinaryValue = (tileRow) => {
            tileRow = tileRow.replace(/#/g, '1').replace(/\./g, '0');
            return parseInt(tileRow, 2);
        };

        const reverse = str => str.split('').reverse().join('');

        let lines = block.split('\n');
        this.id = lines[0].split(' ')[1].replace(':', '');
        this.contents = lines.slice(1).map(x => x.split(''));
        this.contents_of = lines.slice(1).map(x => x.split(''));

        this.transformations = [];
        this.transformations.push(this.borders);
        this.rotate();
        this.transformations.push(this.borders);
        this.rotate();
        this.transformations.push(this.borders);
        this.rotate();
        this.transformations.push(this.borders);

        this.rotate();
        this.flip();

        this.transformations.push(this.borders);
        this.rotate();
        this.transformations.push(this.borders);
        this.rotate();
        this.transformations.push(this.borders);
        this.rotate();
        this.transformations.push(this.borders);

        this.contents = lines.slice(1).map(x => x.split(''));
        this.connections = JSON.parse(JSON.stringify([...new Set(this.transformations.flat())]));
    }

    getTopLeftTransformation() {
        return this.transformations.findIndex(x => x[0] === null && x[1] === null && x[2] !== null && x[3] !== null);
    }

    edgeValues() { return this.sides.concat(this.flippedSides); }
    isCorner() { return this.transformations.every(t => t.filter(x => x).length === 2); }
    isSide() { return this.transformations.every(t => t.filter(x => x).length === 3); }
    getActiveTransformation() { return this.transformations[this.tf]; };
    matchInPosition(bottomConstraint, rightConstraint) {
        for(let i=0; i<this.transformations.length; i++) {
            const transformation = this.transformations[i];
            if (bottomConstraint === transformation[0] && rightConstraint === transformation[1]) {
                this.tf = i;
                if (i >= 4) this.flip();
                for (let j=0; j<i%4; j++) this.rotate();
                return true;
            }
        }
        return false;
    }

    get borders() {
        const calculateBinaryValue = (tileRow) => {
            tileRow = tileRow.replace(/#/g, '1').replace(/\./g, '0');
            return parseInt(tileRow, 2);
        };
        const borders = [
            this.contents[0].join(''),
            this.contents.map(x => x[0]).join(''),
            this.contents[this.contents.length-1].join(''),
            this.contents.map(x => x[x.length-1]).join('')
        ];
        return borders.map(x => calculateBinaryValue(x));
    }

    rotate() {
        this.contents = this.contents[0].map((x, idx) => this.contents.map(row => row[idx]).reverse());
    }

    flip() {
        this.contents = this.contents.map((x, idx) => this.contents[this.contents.length-1-idx]);
    }
}

class TileSet {
    constructor(filename) {
        this.tiles = Fs.readFileSync(filename, 'utf-8').trim().split('\n\n').map(block => new Tile(block));
    }

    nullifyEdges() {
        const edges = {};
        this.tiles.forEach(tile => {
            tile.transformations.forEach(transformation => {
                transformation.forEach(edge => {
                    if (!edges[edge]) { edges[edge] = 0; }
                    edges[edge]++;
                });
            });
        });

        this.tiles = this.tiles.map(tile => {
            tile.transformations = tile.transformations.map(t => {
                t = t.map(x => edges[x] === 8 ? x : null);
                return t;
            });
            return tile;
        });
    }

    get(idx) { return this.tiles[idx]; }
    getById(tileId) { return this.tiles.find(x => x.id == tileId); }
    get size() { return this.tiles.length; }
    get corners() { return this.tiles.filter(x => x.isCorner()); }
    get sides() { return this.tiles.filter(x => x.isSide()); }
}

const tileSet = new TileSet('input20.txt');
tileSet.nullifyEdges();

const corners = tileSet.corners;
if (corners.length !== 4) throw new Error(`wrong number of corners: ${corners.length}`);
const result = corners.reduce((acc, corner) => acc * Number(corner.id), 1);
console.log(`Part 1: ${result}`);

const sides = tileSet.sides;

const side = Math.sqrt(tileSet.size);
const results = [];

const grid = new Array(tileSet.size);
const corner = corners[0];
if (!corner.matchInPosition(null, null)) throw new Error(`Using a no corner unit as top-left initial corner`);
grid[0] = corner;
for(let y=0; y<side; y++) {
    for(let x=0; x<side; x++) {
        if (x === 0 && y === 0) continue;
        const idx = x + y * side;

        for(let tile of tileSet.tiles) {
            if (grid.filter(x => x).map(x => x.id).includes(tile.id)) continue;

            let bottomConstraint = null;
            let rightConstraint = null;
            
            let topNeighbour = grid[(y-1)*side+x];
            if (topNeighbour) {
                const activeTranformation = topNeighbour.getActiveTransformation();
                bottomConstraint = activeTranformation[2];
            }

            let leftNeighbour = grid[y*side+(x-1)];
            if (leftNeighbour) {
                const activeTranformation = leftNeighbour.getActiveTransformation();
                rightConstraint = activeTranformation[3];
            }

            if (tile.matchInPosition(bottomConstraint, rightConstraint)) {
                grid[idx] = tile;
                break;
            }
        }
    }
}

const gridSize = grid.length;
const gridWidth = Math.sqrt(gridSize);

const drawWithBorders = () => {
    const stringifiedTiles = grid.map(tile => tile.contents.map(row => row.join('')));

    const image = [];
    for(let i=0; i<gridSize; i+=gridWidth) {
        const section = stringifiedTiles.slice(i, i+gridWidth);
        for(let j=0; j<10; j++) {
            const line = section.map(x => x[j]).reduce((acc, item) => acc + item, '');
            image.push(line);
        }
    }
    return image;
};

const drawWithoutBorders = () => {
    const stringifiedTiles = grid.map(tile => tile.contents.map(row => row.slice(1,-1).join('')));

    const image = [];
    for(let i=0; i<gridSize; i+=gridWidth) {
        const section = stringifiedTiles.slice(i, i+gridWidth);
        for(let j=1; j<9; j++) {
            const line = section.map(x => x[j]).reduce((acc, item) => acc + item, '');
            image.push(line);
        }
    }
    return image;
};



let image = drawWithoutBorders();
const dragonHeadRegexp = new RegExp('(\\S){18}#\\S', 'g');
const dragonBodyRegexp = new RegExp('#\\S\\S\\S\\S##\\S\\S\\S\\S##\\S\\S\\S\\S###', 'g');
const dragonFeetRegexp = new RegExp('#\\S{2}#\\S{2}#\\S{2}#\\S{2}#\\S{2}#');
const placeholder = 'xxxxxxxxxxxxxxxxxxxx';

const rotate = image => {
    image = image.map(x => x.split(''));
    image = image[0].map((x, idx) => image.map(row => row[idx]).reverse());
    return image.map(x => x.join(''));
};

const flip = image => {
    image = image.map(x => x.split(''));
    image  = image.map((x, idx) => image[image.length-1-idx]);
    return image.map(x => x.join(''));
};

let dragonCount;
dragonSearch:
for(let k=0; k<2; k++) {
    if (k === 1) {
        image = rotate(image);
        image = flip(image);
    }

    for(let i=0; i<4; i++) {
        let dragonsFound = false;
        dragonCount = 0;
        for(let j=0; j<image.length-2; j++) {
            const result = [...image[j+1].matchAll(dragonBodyRegexp)].map(x => x.index)
                .filter(x => image[j][x+18] === '#')
                .filter(x => image[j+2].slice(x).search(dragonFeetRegexp) === 1);

            if (result.length) {
                for(let position of result) {
                    dragonsFound = true;
                    dragonCount++;
//                    image[j] = image[j].slice(0, position) + placeholder + image[j].slice(position + 20);
//                    image[j+1] = image[j+1].slice(0, position) + placeholder + image[j+1].slice(position + 20);
//                    image[j+2] = image[j+2].slice(0, position) + placeholder + image[j+2].slice(position + 20);
//                    console.log(image[j]);
//                    console.log(image[j+1]);
//                    console.log(image[j+2]);
                }
            }

        }
        if (dragonsFound) break dragonSearch;
        image = rotate(image);
    }
};

//image.forEach(line => console.log(line));
const result2 = image.reduce((acc, line) => acc + line.match(/#/g).length, 0) - dragonCount * 15;
console.log(`Part 2: ${result2}`);

