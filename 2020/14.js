'use strict';

const Fs = require('fs');

String.prototype.reverse = function() {
    return this.split('').reverse().join('');
};

const part1 = (filename) => {
    const data = Fs.readFileSync(filename, 'utf-8').trim().split('\n');

    const memory = {};
    let maskAsString;
    let address;
    let value;

    for(let line of data) {
        if (line.startsWith('mask')) {
            maskAsString = line.split(' = ')[1];
            continue;;
        }

        const parsedInstruction = line.match(/mem\[(\d+)\] = (\d+)/);
        address = Number(parsedInstruction[1]);
        value = Number(parsedInstruction[2]);

        const mask = maskAsString.split('').reverse();
        value = value.toString(2).split('').reverse();
        const result = mask.map((bit, index) => {
            if (bit === 'X') return value[index] || 0;
            return bit;
        }).reverse().join('');
        memory[address] = parseInt(result, 2);
    }

    return Object.values(memory).reduce((acc, item) => acc + item, 0);
};

const part2 = (filename) => {
    const data = Fs.readFileSync(filename, 'utf-8').trim().split('\n');

    let memory = {};
    let maskAsString;
    let address;
    let value;

    for(let line of data) {
        if (line.startsWith('mask')) {
            maskAsString = line.split(' = ')[1];
            continue;;
        }

        const parsedInstruction = line.match(/mem\[(\d+)\] = (\d+)/);
        address = Number(parsedInstruction[1]);
        value = Number(parsedInstruction[2]);

        const mask = maskAsString.split('').reverse();
        const addressValue = address.toString(2).split('').reverse();
        const result = mask.map((bit, index) => {
            if (bit === '0') return Number(addressValue[index]) || 0;
            else if (bit === '1') return 1;
            return 'X';;
        }).join('');

        let addresses = [result];
        while (addresses.some(address => address.includes('X'))) {
            const newAddresses = [];
            addresses.forEach(address => {
                const floatingIndex = address.indexOf('X');
                newAddresses.push(address.slice(0, floatingIndex) + '0' + address.slice(floatingIndex+1));
                newAddresses.push(address.slice(0, floatingIndex) + '1' + address.slice(floatingIndex+1));
            });
            addresses = newAddresses;
        }
        addresses.map(address => {
            const memoryAddress = parseInt(address.reverse(), 2);
            memory[memoryAddress] = value;
        })
    }

    return Object.values(memory).reduce((acc, item) => acc + item, 0);
};

console.log(`Part 1: ${part1('input14.txt')}`);
console.log(`Part 2: ${part2('input14.txt')}`);
