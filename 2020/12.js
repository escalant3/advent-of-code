'use strict';

const Fs = require('fs');

const part1 = filename => {
    const data = Fs.readFileSync(filename, 'utf-8').trim().split('\n');
    let x = 0;
    let y = 0;
    let direction = 0;;
    data.forEach(instruction => {
        const action = instruction[0];
        const value = Number(instruction.slice(1));
        if (action === 'N') y += value;
        if (action === 'S') y -= value;
        if (action === 'W') x -= value;
        if (action === 'E') x += value;
        if (action === 'L') direction += value;
        if (action === 'R') direction -= value;
        if (action === 'F') {
            x += Math.round(value * Math.cos(direction*Math.PI/180));
            y += Math.round(value * Math.sin(direction*Math.PI/180));
        }
    });

    return Math.abs(x) + Math.abs(y);
};

const part2 = filename => {
    const waypoint = { x: 10, y: 1, direction: 0,

        move(x, y) {
            this.x += x;
            this.y += y;
        },

        rotateAroundOrigin(degrees) {
            // angle = atan2(y2 - y1, x2 - x1)
            const angle = Math.atan2(this.y, this.x);
            const radius = this.y / Math.sin(angle);
            const radians = degrees * Math.PI / 180;
            const newAngle = angle + radians;
            this.x = Math.cos(newAngle) * radius;
            this.y = Math.sin(newAngle) * radius;
        }
    };

    const ship = { x: 0, y: 0 };

    const data = Fs.readFileSync(filename, 'utf-8').trim().split('\n');
    data.forEach(instruction => {
        const action = instruction[0];
        const value = Number(instruction.slice(1));
        if (action === 'N') waypoint.move(0, value, 0);
        if (action === 'S') waypoint.move(0, -value, 0);
        if (action === 'W') waypoint.move(-value, 0, 0);
        if (action === 'E') waypoint.move(value, 0, 0);
        if (action === 'L') waypoint.rotateAroundOrigin(value);
        if (action === 'R') waypoint.rotateAroundOrigin(-value);
        if (action === 'F') {
            ship.x += waypoint.x * value;
            ship.y += waypoint.y * value;
        }
    });

    return Math.round(Math.abs(ship.x) + Math.abs(ship.y));
};

console.log(`Part 1: ${part1('input12.txt')}`);
console.log(`Part 2: ${part2('input12.txt')}`);
