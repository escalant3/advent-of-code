'use strict';

const Fs = require('fs');

const isValid = (list, preambleSize, offset) => {
    const value = list[offset];
    for(let i=offset-preambleSize; i<offset-1; i++) {
        for(let j=i+1; j<offset; j++) {
            if (list[i]+list[j] === value) return true;
        }
    }
    return false;
};

const getInvalidNumber = (data, preambleSize) => {
    let position = preambleSize;
    for(;;) {
        if (!isValid(data, preambleSize, position)) {
            return data[position];
        }
        position++;
    }
};

const checkEncoding = (fileName, preambleSize) => {
    const data = Fs.readFileSync(fileName, 'utf-8').trim().split('\n').map(Number);
    return getInvalidNumber(data, preambleSize);
};

const findWeakness = (fileName, preambleSize) => {
    const data = Fs.readFileSync(fileName, 'utf-8').trim().split('\n').map(Number);
    const invalidNumber = getInvalidNumber(data, preambleSize);
    const index = data.indexOf(invalidNumber);
    for(let i=index-1; i>0; i--) {
        let sum = data[i];
        for(let j=i-1; j>=0; j--) {
            sum += data[j];
            if (sum === invalidNumber) {
                return data.slice(j, i+1).reduce((acc, item) => Math.min(acc, item), Infinity) +
                        data.slice(j, i+1).reduce((acc, item) => Math.max(acc, item), 0);
            }

            if (sum > invalidNumber) {
                break;
            }

        }
    }
};

console.log(`Part 1: ${checkEncoding('input09.txt', 25)} is not valid`);
console.log(`Part 2: ${findWeakness('input09.txt', 25)}`);

const Assert = require('assert');
Assert.strictEqual(checkEncoding('test09.txt', 5), 127);
Assert.strictEqual(findWeakness('test09.txt', 5), 62);
