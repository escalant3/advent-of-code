'use strict';

const Fs = require('fs');

const isBlack = value => {
    if (value === undefined) return false;
    return value % 2 !== 0;
};
const isWhite = value => !isBlack(value);

const part1 = filename => {
    let  data = Fs.readFileSync(filename, 'utf-8').trim().split('\n');
    data = data.map(line => {
        const steps = [];
        while (line) {
            const next2 = line.slice(0, 2);
            if (next2 === 'ne' || next2 === 'nw' || next2 === 'se' || next2 === 'sw') {
                steps.push(next2);
                line = line.slice(2);
                continue;
            }
            steps.push(line[0]);
            line = line.slice(1);
        }
        return steps;
    });

    // Representing an hexagonal grid as a cube
    // https://www.redblobgames.com/grids/hexagons/#coordinates-cube
    const grid = {};
    data.forEach(line => {
        let x = 0;
        let y = 0;
        let z = 0;
        line.forEach(step => {
            if (step === 'nw') { y+= 1; z-= 1; }
            if (step === 'sw') { z+= 1; x-= 1; }
            if (step === 'ne') { x+= 1; z-= 1; }
            if (step === 'se') { z+= 1; y-= 1; }
            if (step === 'w') { y+= 1; x-= 1; }
            if (step === 'e') { x+= 1; y-= 1; }
        });
        const key = `${x}_${y}_${z}`;
        if (!grid[key]) grid[key] = 0;
        grid[key]++;
    });

    return {
        result: Object.values(grid).filter(isBlack).length,
        grid
    };
};

const blackNeighbors = (grid, x, y, z) => {
    return [[0,1,-1],[1,0,-1],[1,-1,0],[0,-1,1],[-1,0,1],[-1,1,0]].filter(coords => {
        const [dx,dy,dz] = coords;
        const oldkey = `${x}_${y}_${z}`;
        const key = `${x+dx}_${y+dy}_${z+dz}`;
        return isBlack(grid[key]);
    }).length;
};

const getLimits = grid => Object.keys(grid).reduce((acc, item) => {
    const [itemX, itemY, itemZ] = item.split('_').map(Number);
    return {
        minX: Math.min(acc.minX, itemX),
        maxX: Math.max(acc.maxX, itemX),
        minY: Math.min(acc.minY, itemY),
        maxY: Math.max(acc.maxY, itemY),
        minZ: Math.min(acc.minZ, itemZ),
        maxZ: Math.max(acc.maxZ, itemZ)
    };
}, { minX: Infinity, maxX: -Infinity, minY: Infinity, maxY: -Infinity, minZ: Infinity, maxZ: -Infinity });

const part2 = (grid, gens) => {
    for(let i=0; i<gens; i++) {
        const newGrid = {};
        const l = getLimits(grid);
        for(let x=l.minX-1; x<=l.maxX+1; x++) {
            for(let y=l.minY-1; y<=l.maxY+1; y++) {
                for(let z=l.minZ-1; z<=l.maxZ+1; z++) {
                    if (x + y + z !== 0) { continue; }
                    const n = blackNeighbors(grid, x, y, z);
                    const key = `${x}_${y}_${z}`;
                    if (isBlack(grid[key]) && (n === 0 || n > 2)) newGrid[key] = 0;
                    else if (isWhite(grid[key]) && n === 2) newGrid[key] = 1;
                    else newGrid[key] = grid[key] || 0;
                }
            }
        }
        grid = newGrid;
    }
    return Object.values(grid).filter(isBlack).length;
};

const { result, grid } = part1('input24.txt');
console.log(`Part 1: ${result}`);

const result2 = part2(grid, 100);
console.log(`Part 2: ${result2}`);
