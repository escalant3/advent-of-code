'use strict';

const Fs = require('fs');

const surroundings = [ [-1, -1], [-1, 0], [-1, 1], [0, -1], [0, 1], [1, -1], [1, 0], [1,1] ];

const iteratePart1 = (currentGeneration) => {
    const width = currentGeneration[0].length;
    const height = currentGeneration.length;

    return currentGeneration.map((row, rowIdx) => {
        return row.map((cell, columnIdx) => {
            if (cell === '.') return '.';

            const sum = surroundings
                .map(direction => ({ x: columnIdx + direction[1], y: rowIdx + direction[0] }))
                .filter(({ x, y }) => x >= 0 && x < width && y >=0 && y < height)
                .reduce((acc, {x, y}) => acc + (currentGeneration[y][x] === '#' ? 1 : 0), 0);

            if (cell === 'L' && sum === 0) { return '#'; }
            else if (cell === '#' && sum >= 4) { return 'L'; }
            else return cell;
        });
    });
};

const iteratePart2 = (currentGeneration) => {
    const width = currentGeneration[0].length;
    const height = currentGeneration.length;

    const checkUntilSeatOrWall = (y, x, direction) => {
        y += direction[0];
        x += direction[1];
        if (x < 0 || x >= width || y < 0 || y >= height) return 0;
        if (currentGeneration[y][x] === '.') return checkUntilSeatOrWall(y, x, direction);
        return currentGeneration[y][x] === '#' ? 1 : 0;
    };

    return currentGeneration.map((row, rowIdx) => {
        return row.map((cell, columnIdx) => {
            if (cell === '.') return '.';

            const sum = surroundings
                .map(direction => checkUntilSeatOrWall(rowIdx, columnIdx, direction))
                .reduce((acc, directionResult) => acc + directionResult, 0)

            if (cell === 'L' && sum === 0) { return '#'; }
            else if (cell === '#' && sum >= 5) { return 'L'; }
            else return cell;
        });
    });
};

const run = (filename, iterationFn) => {
    const data = Fs.readFileSync(filename, 'utf-8').trim().split('\n');
    let currentGeneration = data.map(x => x.split(''));
    let nextGeneration;

    while(true) {
        nextGeneration = iterationFn(currentGeneration);
        if (JSON.stringify(currentGeneration) === JSON.stringify(nextGeneration)) { break; }
        currentGeneration = nextGeneration;
    }

    return nextGeneration.flat().filter(x => x === '#').length;
};

console.log(`Part 1: ${run('input11.txt', iteratePart1)}`);
console.log(`Part 2: ${run('input11.txt', iteratePart2)}`);

