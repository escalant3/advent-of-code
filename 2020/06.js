'use strict';

const Fs = require('fs');

const readFile = fileName => Fs.readFileSync(fileName, 'utf-8')
    .trim()
    .split('\n\n');

const countUniques = group => {
    const set = new Set();
    for(let answer of group) {
        set.add(answer);
    }
    return set.size;
};

const part1 = inputFile => {
    const data = readFile(inputFile);
    const groups = data.map(group => group.replace(/\n/g, ''));
    return groups.reduce((acc, group) => acc + countUniques(group), 0);
};

const part2 = inputFile => {
    const data = readFile(inputFile);
    return data.reduce((total, group) => {
        const peopleAnswers = group.split('\n');
        let commonAnswers = peopleAnswers[0].split('');
        peopleAnswers.forEach(personAnswer => {
            commonAnswers = commonAnswers.filter(answer => personAnswer.includes(answer));
        });
        return total + commonAnswers.length;
    }, 0);
};

console.log(`Part 1 answer: ${part1('input06.txt')}`);
console.log(`Part 2 answer: ${part2('input06.txt')}`);


// Test
const Assert = require('assert');
Assert.strictEqual(part1('test06.txt'), 11);
Assert.strictEqual(part2('test06.txt'), 6);

