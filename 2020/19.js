'use strict';

const Fs = require('fs');

const parse = filename => {
    const ruleSet = {};
    let [rules, messages] = Fs.readFileSync(filename, 'utf-8').trim().split('\n\n');
    for(let rule of rules.split('\n')) {
        let [ruleId, ruleDefinition] = rule.split(':');
        if (ruleDefinition.includes('a')) {
            ruleSet[ruleId] = 'a';
            continue;
        }
        if (ruleDefinition.includes('b')) {
            ruleSet[ruleId] = 'b';
            continue;
        }

        let ruleDefinitions = ruleDefinition.split('|').map(subRule => subRule.split(' ').filter(x => x));
        ruleSet[ruleId] = ruleDefinitions;
    };

    messages = messages.split('\n');
    return { ruleSet, messages };
};

const unresolved = x => x !== 'a' && x !== 'b';

const hasUnresolvedRules = combinations => {
    return combinations.some(combination => {
        return Array.isArray(combination) && combination.some(unresolved);
    });
};

const evolve = combination => {
    const idx = combination.findIndex(unresolved);
    if (idx === -1) return [combination];

    const translation = ruleSet[combination[idx]];

    if (typeof translation === 'string') {
        return [combination.slice(0, idx)
            .concat([translation])
            .concat(combination.slice(idx+1))];
    }

    return translation.map(x => combination.slice(0, idx)
        .concat(x)
        .concat(combination.slice(idx+1))
    );
};

const isValidSoFar = (combination, message) => {
    const prefix = combination.filter(x => !unresolved(x)).join('');
    return message.startsWith(prefix);
};

const isValid = (rule, message) => {
    let combinations = [];
    combinations.push(rule[0]);
    while (hasUnresolvedRules(combinations)) {
        let newCombinations = [];
        for(let combination of combinations) {
            newCombinations = newCombinations.concat(evolve(combination));
        }
        combinations = newCombinations.filter(c => isValidSoFar(c, message));
    }

    return combinations.length ? combinations[0].join('') === message: false;
};


const { ruleSet, messages } = parse('input19.txt');

let validMessages = messages.filter(message => isValid(ruleSet['0'], message));
console.log(`Part 1: ${validMessages.length}`);

// Part 2 modifications
// 8: 42 | 42 8
// 11: 42 31 | 42 11 31
ruleSet['8'] = [['42'], ['42', '8']];
ruleSet['11'] = [['42', '31'], ['42', '11', '31']];

validMessages = messages.filter(message => isValid(ruleSet['0'], message));
console.log(`Part 2: ${validMessages.length}`);
