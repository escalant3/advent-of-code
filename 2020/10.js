'use strict';

const Fs = require('fs');

const part1 = filename => {
    const data = Fs.readFileSync(filename, 'utf-8').trim().split('\n').map(Number).sort((a,b) => a-b);

    const differences = [];
    for(let i=0; i<data.length-1; i++) {
        differences.push(data[i+1]-data[i]);
    }

    // Seat adapter
    differences.splice(1, 0, data[0]);
    // Device adapter
    differences.push(3);

    return differences.filter(x => x===3).length * differences.filter(x => x===1).length;
};

const part2 = filename => {
    const data = Fs.readFileSync(filename, 'utf-8').trim().split('\n').map(Number).sort((a,b) => b-a);

    data.splice(0, 0, data[0]+3);
    data.splice(data.length, 0, 0);

    const possiblePathsToNode = {};
    for(let i=0; i<data.length; i++) {
        const value = data[i];
        possiblePathsToNode[value] = {
            possibleExpansions: data.slice(i+1).filter(x => value - x < 4),
            computedExpansion: null
        };
    }

    const expandPath = (value) => {
        if (value === 0) {
            return 1;
        }
        const { possibleExpansions, computedExpansion } = possiblePathsToNode[value];
        if (computedExpansion) { return computedExpansion; }

        const result = possibleExpansions.reduce((acc, possiblePreviousNode) => {
            return acc + expandPath(possiblePreviousNode);
        }, 0);

        possiblePathsToNode[value].computedExpansion = result;
        return result;
    };

    return expandPath(data[0]);
};

console.log(`Part 1: ${part1('input10.txt')}`);
console.log(`Part 2: ${part2('input10.txt')}`);

/*
const Assert = require('assert');
Assert.strictEqual(part1('test10.txt'), 35);
Assert.strictEqual(part1('test10b.txt'), 220);
*/

/*
const part2Debug = filename => {
    const data = Fs.readFileSync(filename, 'utf-8').trim().split('\n').map(Number).sort((a,b) => a-b);

    console.log(JSON.stringify(data));
    data.splice(0, 0, 0);
    data.splice(data.length, 0, data[data.length-1]+3);
    console.log(JSON.stringify(data));

    const possiblePathsPerNode = {};
    for(let i=0; i<data.length; i++) {
        const value = data[i];
        possiblePathsPerNode[value] = data.slice(i+1).filter(x => x - value < 4);
    }

    console.log(possiblePathsPerNode);

    const potentialCombinations = [];
    const endValue = data[data.length-1];

    const expandPathDebug = (currentPath, value) => {
        const currentPathCopy = currentPath.concat([value]);
        if (value === endValue) {
            numberOfPotentialCombinations++;
            potentialCombinations.push(JSON.stringify(currentPathCopy));
            return;
        }
        possiblePathsPerNode[value].forEach(possibleNextNode => {
            expandPath(currentPathCopy, possibleNextNode);
        });
    };

    console.log(potentialCombinations);
};

part2Debug('test10.txt');
*/
