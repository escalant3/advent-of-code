'use strict';

const Fs = require('fs');

const findMatching = (expression, opening) => {
    let innerOpenings = 0;
    for(let i=opening+1;i<expression.length;i++) {
        if (expression[i] === '(') innerOpenings++;
        if (expression[i] === ')' && innerOpenings) {
            innerOpenings--;
            continue;
        }

        if (expression[i] === ')') {
            return i;
        }
    }
    throw new Error(`Matching paren for opening at ${opening} not found`);
};

const solve = expression => {
    if (typeof(expression) === 'string') expression = expression.split(' ').join('').split('');

    while (expression.includes('(')) {
        const opening = expression.indexOf('(');
        const matching = findMatching(expression, opening);
        return solve(
            expression.slice(0, opening)
                .concat([solve(expression.slice(opening+1, matching))])
                .concat(expression.slice(matching+1))
        );
    }

    if (expression.length === 3) {
        if (expression[1] === '+') return Number(expression[0]) + Number(expression[2]);
        else if (expression[1] === '*') return Number(expression[0]) * Number(expression[2]);
        else throw new Error(`Invalid operation ${expression[1]}`);
    }

    return solve([solve(expression.slice(0, 3))].concat(expression.slice(3)));
};

const solve2 = e => {
    if (typeof(e) === 'string') e = e.split(' ').join('').split('');

    while (e.includes('(')) {
        const opening = e.indexOf('(');
        const matching = findMatching(e, opening);
        return solve2(
            e.slice(0, opening)
                .concat([solve2(e.slice(opening+1, matching))])
                .concat(e.slice(matching+1))
        );
    }

    while (e.includes('+')) {
        const sumIndex = e.indexOf('+');
        return solve2(
            e.slice(0, sumIndex-1)
            .concat([Number(e[sumIndex-1]) + Number(e[sumIndex+1])])
            .concat(e.slice(sumIndex+2))
        );
    }

    if (e.length === 3) {
        if (e[1] === '+') return Number(e[0]) + Number(e[2]);
        else if (e[1] === '*') return Number(e[0]) * Number(e[2]);
        else throw new Error(`Invalid operation ${e[1]}`);
    }

    if (e.length === 1) return e[0];

    return solve2([solve2(e.slice(0, 3))].concat(e.slice(3)));
};


const data = Fs.readFileSync('input18.txt', 'utf-8').trim().split('\n')

const result = data.reduce((acc, expression) => acc + solve(expression), 0);
console.log(`Part 1: ${result}`);

const result2 = data.reduce((acc, expression) => acc + solve2(expression), 0);
console.log(`Part 2: ${result2}`);

const Assert = require('assert');
Assert.strictEqual(solve('1 + 2 * 3 + 4 * 5 + 6'), 71);
Assert.strictEqual(solve('1 + (2 * 3) + (4 * (5 + 6))'), 51);
Assert.strictEqual(solve('5 + (8 * 3 + 9 + 3 * 4 * 3)'), 437);
Assert.strictEqual(solve('5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))'), 12240);
Assert.strictEqual(solve('((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2'), 13632);
Assert.strictEqual(solve2('1 + 2 * 3 + 4 * 5 + 6'), 231);
Assert.strictEqual(solve2('1 + (2 * 3) + (4 * (5 + 6))'), 51);
Assert.strictEqual(solve2('5 + (8 * 3 + 9 + 3 * 4 * 3)'), 1445);
Assert.strictEqual(solve2('5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))'), 669060);
Assert.strictEqual(solve2('((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2'), 23340);

