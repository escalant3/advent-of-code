'use strict';

const Fs = require('fs');

class InfiniteLoopError extends Error {}

class Machine {
    #code;
    #accumulator;
    #programCounter;
    #poke;

    loadCode(filename) {
        this.#code = Fs.readFileSync(filename, 'utf-8').trim().split('\n').map(line => {
            const [ instruction, operand ] = line.split(' ');
            return { instruction, operand: Number(operand) };
        });
        this.#poke = {};
    }

    execute() {
        const executedInstructions = [];
        this.#accumulator = 0;
        this.#programCounter = 0;

        for(;;) {
            if (this.#programCounter === this.#code.length) break;

            let { instruction, operand } = this.#code[this.#programCounter];
            if (executedInstructions.includes(this.#programCounter)) {
                throw new InfiniteLoopError(
                    `Instruction ${this.#programCounter} already run. Aborting! Accumulator is ${this.#accumulator}`
                );
            }

            executedInstructions.push(this.#programCounter);

            if (this.#poke.instructionNumber === this.#programCounter) {
                instruction = this.#poke.newInstruction;
            }

            switch (instruction) {
                case 'acc':
                    this.#accumulator += operand;
                case 'nop':
                    this.#programCounter++;
                    continue;
                case 'jmp':
                    this.#programCounter += operand;
                    continue;
                default:
                    throw new Error(`Unexpected instruction ${instruction} in line ${this.#programCounter}`);
            }
        }
    }

    patchProgram() {
        let fixerCounter = 0;
        for (;;) {
            try {
                this.execute();
                console.log(`Part 2: Program finished. Accumulator: ${this.#accumulator}`);
                break;
            }
            catch(err) {
                if (err instanceof InfiniteLoopError) {
                    while (fixerCounter < this.#code.length && this.#code[fixerCounter].instruction === 'acc') fixerCounter++;
                    this.#poke = {
                        instructionNumber: fixerCounter,
                        newInstruction: this.#code[fixerCounter].instruction === 'nop' ? 'jmp' : 'nop'
                    };
                    fixerCounter++;
                }
                else {
                    throw err;
                }
            }
        }
    }
}

const machine = new Machine();
machine.loadCode('input08.txt');
try { machine.execute(); } catch(err) { console.log(`Part 1: ${err.message}`); }
machine.patchProgram();
