'use strict';

const isNextThree = (current, x) =>
    x === current.next.value ||
    x === current.next.next.value ||
    x === current.next.next.next.value;

const iterate = (nodes, current, max=9) => {
    let d = current.value - 1;
    while (d < 1 || isNextThree(current, d)) {
        d--;
        if (d < 1) d = max;
    }

    const destinationNode = nodes[d];
    const aux = destinationNode.next;
    destinationNode.next = current.next;
    current.next = destinationNode.next.next.next.next;
    destinationNode.next.next.next.next = aux;
};

const part1 = (labeling, games) => {

    const nodes = {};
    for(let i=0; i<labeling.length; i++) {
        const v = labeling[i];
        const node = { value: Number(v) };
        if (i > 0) nodes[labeling[i-1]].next = node;
        if (i === labeling.length - 1) node.next = nodes[labeling[0]];
        nodes[v] = node;
    }
    let current = nodes[labeling[0]];

    for(let i=0; i<games; i++) {
        iterate(nodes, current);
        current = current.next;
    }

    let n = nodes[1];
    let result = '';
    for(let i=0; i<8; i++) {
        n = n.next;
        result += n.value;
    }
    return result;
};

const part2 = (labeling, games) => {
    const nodes = {};
    for(let i=0; i<labeling.length; i++) {
        const v = labeling[i];
        const node = { value: Number(v) };
        if (i > 0) nodes[labeling[i-1]].next = node;
        if (i === labeling.length - 1) node.next = nodes[labeling[0]];
        nodes[v] = node;
    }

    const size = 1000000;
    for(let i=10; i<=size; i++) {
        const node = { value: i};
        nodes[i] = node;
        if (i === 10) {
            nodes[labeling[labeling.length-1]].next = node;
            continue;
        }
        if (i === size) node.next = nodes[labeling[0]];
        nodes[i-1].next = node;
    }

    let current = nodes[labeling[0]];

    for(let i=0; i<games; i++) {
        iterate(nodes, current, 1000000);
        current = current.next;
    }

    let n = nodes[1];
    return n.next.value * n.next.next.value;
};

const result = part1('186524973', 100);
console.log(`Part 1: ${result}`);
const result2 = part2('186524973', 10000000);
console.log(result2);

