'use strict';

const Fs = require('fs');

const data = Fs.readFileSync('input21.txt', 'utf-8').trim().split('\n').map(line => {
    let [ingredients, candidates] = line.split(' (contains ');
    ingredients = ingredients.split(' ');
    candidates = candidates.replace(')', '').split(', ');
    return { ingredients,  candidates };
});

const candidates = {};
data.forEach(foodItem => {
    foodItem.candidates.forEach(allergen => {
        if (!candidates[allergen]) {
            candidates[allergen] = [...foodItem.ingredients]; 
            return;
        }
    candidates[allergen] = candidates[allergen].filter(x => foodItem.ingredients.includes(x));
    });
});

const ingredients = data.map(x => x.ingredients).flat();
const candidateIngredients = Object.values(candidates).flat();

console.log(`Part 1: ${ingredients.filter(x => !candidateIngredients.includes(x)).length}`);

let candidateList = [];
for(let candidate in candidates) {
    candidateList.push({ allergen: candidate, candidates: candidates[candidate] });
}

const hasUnresolvedFoodItems = () => candidateList.find(x => !x.ingredient);

while (hasUnresolvedFoodItems()) {
    const singleCandidate = candidateList.find(x => x.candidates.length === 1);
    const allergen = singleCandidate.candidates[0];
    singleCandidate.ingredient = allergen;
    candidateList = candidateList.map(item => {
        item.candidates = item.candidates.filter(x => x !== allergen);
        return item;
    });
}

candidateList.sort((a,b) => a.allergen < b.allergen ? -1 : 1);
const result = candidateList.map(x => x.ingredient).join(',');
console.log(`Part 2: ${result}`);
