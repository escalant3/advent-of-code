'use strict';

const Fs = require('fs');

const parse = filename => {
    let [rules, myTicket, nearbyTickets] = Fs.readFileSync(filename, 'utf-8').trim().split('\n\n');

    rules = rules.split('\n');
    myTicket = myTicket.split('\n')[1].split(',').map(Number);
    nearbyTickets = nearbyTickets.split('\n').slice(1).map(ticketString => ticketString.split(',').map(Number));

    rules = rules.map(ruleString => {
        const [_, ruleName, minA, maxA, minB, maxB] = ruleString.match(/(.+): (\d+)-(\d+) or (\d+)-(\d+)/);
        return { ruleName, minA: Number(minA), maxA: Number(maxA), minB: Number(minB), maxB: Number(maxB) };
    });

    const tickets = nearbyTickets;
    tickets.push(myTicket);
    return { tickets, rules, myTicket };
};

const ruleMatches = number => rule =>(number >= rule.minA && number <= rule.maxA) || (number >= rule.minB && number <= rule.maxB);

const part1 = filename => {
    const { tickets, rules } = parse(filename);

    let error = 0;
    tickets.forEach(ticket => {
        ticket.forEach(number => {
            if (!rules.some(ruleMatches(number))) {
                error += number;
            }
        });
    });
    return error;
};

const part2 = filename => {
    let { tickets, rules, myTicket } = parse(filename);

    let combinations = Array(rules.length);
    let processedTickets = Array(rules.length);
    const solution = Array(rules.length);

    tickets = tickets
        .filter(ticket => ticket.every(number => rules.some(ruleMatches(number))))
        .map(ticket => ticket.map((number, index) => rules.filter(ruleMatches(number)).map(rule => rule.ruleName)));

    processedTickets = processedTickets.map(x => []);
    for(let k=0; k<rules.length; k++) {
        processedTickets[k] = tickets[0][k].slice();
        for(let i=1; i<tickets.length; i++) {
            processedTickets[k] = processedTickets[k].filter(x => tickets[i][k].includes(x));
        }
    }

    while (!processedTickets.every(x => x.length === 0)) {
        const onlyOneFieldIndex = processedTickets.findIndex(x => x.length === 1);
        const solvedField = processedTickets[onlyOneFieldIndex][0];
        solution[onlyOneFieldIndex] = solvedField;
        processedTickets = processedTickets.map(sortedRule => sortedRule.filter(x => x !== solvedField));
    }

    let mul = 1;
    for(let i=0; i<solution.length; i++) {
        if (solution[i].startsWith('departure')) {
            mul *= myTicket[i]
        }
    }

    return mul;
};

console.log('Part 1:', part1('input16.txt'));
console.log('Part 2:', part2('input16.txt'));

