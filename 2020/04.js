'use strict';

const Fs = require('fs');

const isValidPassport = (passport) => {
    return passport.includes('ecl:') &&
        passport.includes('pid:') &&
        passport.includes('eyr:') &&
        passport.includes('hcl:') &&
        passport.includes('byr:') &&
        passport.includes('iyr') &&
        passport.includes('hgt');
};

const hasValidYear = (passport, key, minYear, maxYear) => {
    const regex = new RegExp(`^${key}:(\\d+)$`);
    const matches = passport.match(regex);
    if (!matches) return false;
    const year = Number(matches[1]);
    return year >= minYear && year <= maxYear;
};

const hasValidBirthYear = (passport) => hasValidYear(passport, 'byr', 1920, 2002);
const hasValidIssueYear = (passport) => hasValidYear(passport, 'iyr', 2010, 2020);
const hasValidExpirationYear = (passport) => hasValidYear(passport, 'eyr', 2020, 2030);

const hasValidHeight = (passport) => {
    const matches = passport.match(/^hgt:(?:(\d{3})cm|(\d{2})in)$/);
    if (!matches) return false;
    const [_, cms, inches] = matches;

    if (cms) {
        const value = Number(cms);
        return value >= 150 && value <= 193;
    }

    const value = Number(inches);
    return value >= 59 && value <= 76;
};

const hasValidHairColor = (passport) => !!passport.match(/^hcl:#[0-9a-f]{6}$/);
const hasValidEyeColor = (passport) => !!passport.match(/^ecl:(amb|blu|brn|gry|grn|hzl|oth)$/);
const hasValidPassportId = passport => !!passport.match(/^pid:[0-9]{9}$/);

const isValidPassportPart2 = (passport) => {
    const passportComponents = passport.split(' ');
    return isValidPassport(passport) &&
        passportComponents.some(hasValidBirthYear) &&
        passportComponents.some(hasValidIssueYear) &&
        passportComponents.some(hasValidExpirationYear) &&
        passportComponents.some(hasValidHeight) &&
        passportComponents.some(hasValidHairColor) &&
        passportComponents.some(hasValidEyeColor) &&
        passportComponents.some(hasValidPassportId);
};


const data = Fs.readFileSync('input04.txt', 'utf-8')
    .trimRight()
    .split('\n');


const passports = [];
let last = '';
for(let line of data) {
    if (line === '') {
        last = '';
        continue;
    }

    if (last === '') {
        passports.push(line);
    }
    else {
        passports[passports.length-1] += ` ${line}`;
    }
    last = line;
}

const validPassports = passports.filter(isValidPassport);
console.log(`Valid passports: ${validPassports.length}`);

const validPassports2 = passports.filter(isValidPassportPart2);
console.log(`Valid passports: ${validPassports2.length}`);

