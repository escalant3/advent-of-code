'use strict';

let cache = {};

const transform = (subjectN, loops) => {
    let value = 1;

    const p = loops-1;

    if (cache[p]) {
        value = cache[p];
        value *= subjectN;
        value %= 20201227;
        cache[loops] = value;
        return value;
    }

    for(let i=1; i<=loops; i++) {
        value *= subjectN;
        value %= 20201227;
        cache[i] = value;
    }
    return value;
};

const findLoop = value => {
    let loop = 1;
    while (true) {
        const v = transform(7, loop);
        if (value === v) return loop; 
        loop++;
    }
};

// From input
const cardKey = 5099500;
const doorKey = 7648211;
const cardKeyLoop = findLoop(cardKey);
const doorLoop = findLoop(doorKey);
cache = {};
const t1 = transform(cardKey, doorLoop);
cache = {};
const t2 = transform(doorKey, cardKeyLoop);
console.log(`Part 1: Hack for both loops give ${t1} and ${t2}`);


const Assert = require('assert');
cache = {};
Assert.strictEqual(transform(7, 8), 5764801);
cache = {};
Assert.strictEqual(transform(7, 11), 17807724);
cache = {};
Assert.strictEqual(transform(5764801, 11), 14897079);
cache = {};
Assert.strictEqual(transform(17807724, 8), 14897079);
cache = {};
Assert.strictEqual(findLoop(5764801), 8);
cache = {};
Assert.strictEqual(findLoop(17807724), 11);
