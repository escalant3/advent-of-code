#include <assert.h>
#include <fstream>
#include <iostream>
#include <sstream>

using namespace std;


struct solution_t {
	int final_floor;
	int basement_entrance;
};


struct solution_t get_floor_from_string(const string& path) {
	int current_floor = 0;
	int step_num = 0;

	struct solution_t solution;

	solution.basement_entrance = -1;

	for(char step: path) {
		step_num++;
		if (step == '(') current_floor++;
		if (step == ')') current_floor--;

		if (current_floor < 0 && solution.basement_entrance == -1) {
			solution.basement_entrance = step_num;
		}
	}

	solution.final_floor = current_floor;
	return solution;
}

void test_1() {
	assert(get_floor_from_string("(())").final_floor == 0);
	assert(get_floor_from_string("()()").final_floor == 0);
	assert(get_floor_from_string("(((").final_floor == 3);
	assert(get_floor_from_string("(()(()(").final_floor == 3);
	assert(get_floor_from_string("))(((((").final_floor == 3);
	assert(get_floor_from_string("())").final_floor == -1);
	assert(get_floor_from_string("))(").final_floor== -1);
	assert(get_floor_from_string(")))").final_floor == -3);
	assert(get_floor_from_string(")())())").final_floor == -3);
	cout << "Test for first part pass!" << endl;
}

string read_input() {
	ifstream f("01_input.txt");
	stringstream buffer;
	buffer << f.rdbuf();
	return buffer.str();
}

int main() {
	test_1();
	string input = read_input();
	solution_t solution = get_floor_from_string(input); 
	cout << "Final floor: " << solution.final_floor << endl;
	cout << "Step when he gets in the basement: " << solution.basement_entrance << endl;
	return 0;
}
