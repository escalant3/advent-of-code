#include <iostream>
#include <vector>

#include "utils.h"

#define ON '#'
#define OFF '.'

using namespace std;

class LightsGrid {
	private:
		vector<string> grid;
		int get_surrounding_on(int i, int j) {
			int above = i-1, below = i+1, left=j-1, right=j+1;
			int sum = 0;
			auto row_size = grid[0].size();
			auto number_of_rows = grid.size();

			sum += (above >= 0 && left >= 0 && grid[above][left] == '#') ? 1 : 0;
			sum += (above >= 0 && grid[above][j] == '#') ? 1 : 0;
			sum += (above >= 0 && right < row_size && grid[above][right] == '#') ? 1 : 0;
			sum += (right < row_size && grid[i][right] == '#') ? 1 : 0;
			sum += (below < number_of_rows && right < row_size && grid[below][right] == '#') ? 1 : 0;
			sum += (below < number_of_rows && grid[below][j] == '#') ? 1 : 0;
			sum += (below < number_of_rows && left >= 0 && grid[below][left] == '#') ? 1 : 0;
			sum += (left >= 0 && grid[i][left] == '#') ? 1 : 0;

//			cout << "(" << i << "," << j << ")" << ": " << sum << endl;
			return sum;
		}

		void lit_corner() {
			grid[0][0] = ON;
			grid[0][grid[0].size()-1] = ON;
			grid[grid.size()-1][0] = ON;
			grid[grid.size()-1][grid[0].size()-1] = ON;
		}
	public:
		LightsGrid(vector<string> input) {
			for(auto row: input) {
				grid.push_back(row);
			}
		}

		void update() {
			vector<string> new_grid(grid);

			for(size_t i=0; i<grid.size(); ++i) {
				for(size_t j=0; j<grid[0].size(); ++j) {
					int son = get_surrounding_on(i, j);
					if (grid[i][j] == ON) {
						new_grid[i][j] = (son >= 2 && son <= 3) ? ON : OFF;
					} else if (son == 3) {
						new_grid[i][j] = ON;
					}
				}
			}

			grid = new_grid;
		}

		void update2() {
			lit_corner();
			update();
			lit_corner();
		}

		int get_lights_on() {
			int sum = 0;
			for(auto r: grid) {
				for(auto c: r) {
					if (c == ON) ++sum;
				}
			}
			return sum;
		}

		void print() {
			cout << endl;
			for(auto row: grid) {
				cout << row << endl;
			}
		}
};

void test() {
	vector<string> input = split_str_by_char(read_file("18_test.txt"), '\n');

	LightsGrid grid = LightsGrid(input);
	grid.print();
	grid.update();
	grid.update();
	grid.update();
	grid.update();
	grid.print();
	cout << "There are " << grid.get_lights_on() << " lights on" << endl;
}

void test2() {
	vector<string> input = split_str_by_char(read_file("18_test.txt"), '\n');

	LightsGrid grid = LightsGrid(input);
	grid.print();
	grid.update2();
	grid.update2();
	grid.update2();
	grid.update2();
	grid.print();
	cout << "There are " << grid.get_lights_on() << " lights on" << endl;
}
int main() {
	//test();
	//test2();

	vector<string> input = split_str_by_char(read_file("18_input.txt"), '\n');
	LightsGrid grid = LightsGrid(input);
	for(int i=0; i<100; ++i) {
		grid.update();
	}
	cout << "There are " << grid.get_lights_on() << " lights on in part 1" << endl;

	LightsGrid grid2 = LightsGrid(input);
	for(int i=0; i<100; ++i) {
		grid2.update2();
	}
	cout << "There are " << grid2.get_lights_on() << " lights on in part 2" << endl;

	return 0;
}
