#include <assert.h>
#include <iostream>

#include "utils.h"

using namespace std;

bool is_vowel(char c) {
	return (c=='a' || c=='e' || c=='i' || c=='o' || c=='u');
}

bool contains_three_vowels(const string& s) {
	int counter = 0;
	for(char c: s) {
		if (is_vowel(c)) {
			counter++;
		}

		if (counter > 2) {
			return true;
		}
	}

	return false;
}

bool contains_one_letter_that_appears_twice_in_a_row(const string& s) {

	for(int i=0; i<s.size()-1; i++) {
		if (s[i] == s[i+1]) return true;
	}

	return false;
}

bool contains_blacklisted_string(const string& s) {

	for(int i=0; i<s.size()-1; i++) {
		if (s[i] == 'a' && s[i+1] == 'b') return true;
		else if (s[i] == 'c' && s[i+1] == 'd') return true;
		else if (s[i] == 'p' && s[i+1] == 'q') return true;
		else if (s[i] == 'x' && s[i+1] == 'y') return true;
	}

	return false;
}

bool contains_pair_twice(const string& s) {
	for(int i=0; i<s.size()-1; i++) {
		for(int j=i+2; j<s.size()-1; j++) {
			if (s[i] == s[j] && s[i+1] == s[j+1]) {
				return true;
			}
		}
	}

	return false;
}

bool contains_letter_that_repeats_with_one_in_between(const string& s) {

	for(int i=0; i<s.size()-2; i++) {
		if (s[i] == s[i+2]) return true;
	}

	return false;
}

bool is_good_string(const string& s) {
	return contains_three_vowels(s) &&
		contains_one_letter_that_appears_twice_in_a_row(s) &&
		!contains_blacklisted_string(s);
}

bool is_good_string_2(const string& s) {
	return contains_pair_twice(s) &&
		contains_letter_that_repeats_with_one_in_between(s);
}

void test_1() {
	assert(is_good_string("ugknbfddgicrmopn"));
	assert(is_good_string("aaa"));
	assert(!is_good_string("jchzalrnumimnmhp"));
	assert(!is_good_string("haegwjzuvuyypxyu"));
	assert(!is_good_string("dvszwmarrgswjxmb"));
	cout << "All tests for part 1 passed!" << endl;
}

void test_2() {
	assert(contains_pair_twice("xyxy"));
	assert(contains_pair_twice("aabcefgaa"));
	assert(!contains_pair_twice("aaa"));
	assert(contains_letter_that_repeats_with_one_in_between("xyx"));
	assert(contains_letter_that_repeats_with_one_in_between("abcdefeghi"));
	assert(contains_letter_that_repeats_with_one_in_between("aaa"));
	assert(is_good_string_2("qjhvhtzxzqqjkmpb"));
	assert(is_good_string_2("xxyxx"));
	assert(!is_good_string_2("uurcxstgmygtbstg"));
	assert(!is_good_string_2("ieodomkazucvgmuy"));
	cout << "All tests for part 2 passed!" << endl;
}

int main() {
	test_1();
	test_2();
	string input = read_file("05_input.txt");
	vector<string> strings = split_str_by_char(input, '\n');
	int part_1_counter = 0;
	int part_2_counter = 0;
	for(auto s: strings) {
		if (is_good_string(s)) part_1_counter++;
		if (is_good_string_2(s)) part_2_counter++;
	}
	cout << "Part 1: " <<  part_1_counter << " of " << strings.size() << " strings were good" << endl;
	cout << "Part 2: " << part_2_counter << " of " << strings.size() << " strings were good" << endl;
	return 0;
}
