#!/usr/bin/env python

# Cheating with python (no JSON in C++ STL)

import json

def sum_values_from_list(data):
    sum = 0

    for item in data:
        if type(item) == int:
            sum += item
        elif type(item) == dict:
            sum += sum_values_from_dict(item)
        elif type(item) == list:
            sum += sum_values_from_list(item)

    return sum

def sum_values_from_dict(data):

    sum = 0

    if 'red' in data.values():
        return 0

    for value in data.values():
        if type(value) == int:
            sum += value

        elif type(value) == list:
            sum += sum_values_from_list(value)

        elif type(value) == dict:
            sum += sum_values_from_dict(value)

    return sum


def process(data):
    if type(data) == dict:
        return sum_values_from_dict(data)
    elif type(data) == list:
        return sum_values_from_list(data)
    else:
        return 0

def test():
    assert(process([1,2,3]) == 6)
    assert(process({"a":2,"b":4}) == 6)
    assert(process([[[3]]]) == 3)
    assert(process({"a":{"b":4},"c":-1}) == 3)
    assert(process({"a":[-1,1]}) == 0)
    assert(process([-1,{"a":1}]) == 0)
    assert(process([]) == 0)
    assert(process({}) == 0)
    assert(process([1,{"c":"red","b":2},3]) == 4)

with open('12_input.txt') as f:
    data = f.read()
    data = json.loads(data)

    test()
    total = process(data)
    print('The total sum is %d' % total)
