'use strict';

// From input
const goalRow = 3010;
const goalColumn = 3019;

// Calculating 3010x3019 index using autoincremental model
// Indexes grow by +n horizontally
let value = 0;
for(let i=1; i<= goalColumn; i++) { value += i; }
// Indexes grow by +n vertically starting with the column number
for(let i=0; i<goalRow-1; i++) { value += goalColumn + i; }

const coordinatesIndex = value;

let previous = 20151125;
for(let i=1; i<coordinatesIndex; i++) {
    let value = previous * 252533;
    value %= 33554393;
    previous = value; 
}
console.log(`Part 1: Value for (${goalRow}, ${goalColumn}) is at index ${coordinatesIndex} with value ${previous}`);
