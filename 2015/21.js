'use strict';

const weapons = [
    { name: 'Dagger', cost: 8, damage: 4, armor: 0 },
    { name: 'Shortsword', cost: 10, damage: 5, armor: 0 },
    { name: 'Warhammer',cost: 25, damage: 6, armor: 0 },
    { name: 'Longsword',cost: 40, damage: 7, armor: 0 },
    { name: 'Greataxe',cost: 74, damage: 8, armor: 0 }
];

const armor = [
    { name: 'Empty 1', cost: 0, damage: 0, armor: 0 },
    { name: 'Leather', cost: 13, damage: 0, armor: 1 },
    { name: 'Chainmail', cost: 31, damage: 0, armor: 2 },
    { name: 'Splintmail',cost: 53, damage: 0, armor: 3 },
    { name: 'Bandedmail',cost: 75, damage: 0, armor: 4 },
    { name: 'Platemail',cost: 102, damage: 0, armor: 5 }
];

const rings = [
    { name: 'Empty 1', cost: 0, damage: 0, armor: 0 },
    { name: 'Empty 2', cost: 0, damage: 0, armor: 0 },
    { name: 'Damage +1', cost: 25, damage: 1, armor: 0 },
    { name: 'Damage +2', cost: 50, damage: 2, armor: 0 },
    { name: 'Damage +3', cost: 100, damage: 3, armor: 0 },
    { name: 'Defense +1', cost: 20, damage: 0, armor: 1 },
    { name: 'Defense +2', cost: 40, damage: 0, armor: 2 },
    { name: 'Defense +3', cost: 80, damage: 0, armor: 3 }
];

let combinations = [];
for(let i=0; i<weapons.length; i++) {
    for(let j=0; j<armor.length; j++) {
        for(let k=0; k<rings.length-1; k++) {
            for(let l=k; l<rings.length; l++) {
                combinations.push([
                    weapons[i],
                    armor[j],
                    rings[k],
                    rings[l]
                ]);
            }
        }
    }
}

// From puzzle input
const boss = { damage: 8, armor: 2, hitpoints: 100 };

const doesPlayerWin = (player, boss) => {
    let playerHp = player.hitpoints;
    let bossHp = boss.hitpoints;
    while (playerHp > 0 && bossHp > 0) {
        bossHp -= Math.max(player.damage - boss.armor, 1);
        if (bossHp <= 0) break;
        playerHp -= Math.max(boss.damage - player.armor, 1);
    }

    return playerHp > 0;
};

const result = combinations.reduce((result, playerSet) => {
    const playerStats = playerSet.reduce((stats, item) => {
        const damage = item.damage + stats.damage;
        const armor = item.armor + stats.armor;
        const cost = item.cost + stats.cost;
        return { damage, armor, cost };
    }, { damage: 0, armor: 0, cost: 0 });

    playerStats.hitpoints = 100;

    const win = doesPlayerWin(playerStats, boss);

    if (playerStats.cost < result.cheapestWin && win) {
        result.cheapestWin = playerStats.cost;
    }

    if (playerStats.cost > result.priciestLose && !win) {
        result.priciestLose = playerStats.cost;
    }

    return result;
}, { cheapestWin: Infinity, priciestLose: -Infinity });


console.log(`Part 1: Minimum gold to win is ${result.cheapestWin}`);
console.log(`Part 2: Maximum gold and losing is ${result.priciestLose}`);
