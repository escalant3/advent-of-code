#include <assert.h>

#include <functional>
#include <iostream>
#include <regex>

#include "utils.h"

#define SIZE 1000

using namespace std;

enum LightOperation { TurnOn, TurnOff, Toggle };

typedef struct {
	int x1;
	int y1;
	int x2;
	int y2;
	LightOperation operation;
} light_instruction_t;

int grid[SIZE][SIZE];

void turn(int x1, int y1, int x2, int y2, std::function<int (int)> f) {
	for(int i=x1; i<x2+1; i++) {
		for(int j=y1; j<y2+1; j++) {
			grid[i][j] = f(grid[i][j]);
		}
	}
}

void turn_off(int x1, int y1, int x2, int y2) {
	std::function<int (int)> f = [](int current) { return 0; };
	turn(x1, y1, x2, y2, f);
}

void turn_on(int x1, int y1, int x2, int y2) {
	std::function<int (int)> f = [](int current) { return 1; };
	turn(x1, y1, x2, y2, f);
}

void toggle(int x1, int y1, int x2, int y2) {
	std::function<int (int)> f = [](int current) {
		if (current == 0) return 1;
		else return 0;
	};
	turn(x1, y1, x2, y2, f);
}

void turn_off_2(int x1, int y1, int x2, int y2) {
	std::function<int (int)> f = [](int current) { return max(0, current-1); };
	turn(x1, y1, x2, y2, f);
}

void turn_on_2(int x1, int y1, int x2, int y2) {
	std::function<int (int)> f = [](int current) { return current+1; };
	turn(x1, y1, x2, y2, f);
}

void toggle_2(int x1, int y1, int x2, int y2) {
	std::function<int (int)> f = [](int current) { return current+2; };
	turn(x1, y1, x2, y2, f);
}


void clear_all() {
	turn_off(0, 0, SIZE-1, SIZE-1);
}

LightOperation str_to_lightoperation(const string& s) { if (s == "turn on") { return TurnOn; }
	else if (s == "turn off") { return TurnOff; }
	else if (s == "toggle") { return Toggle; }
	else assert(false);
}

light_instruction_t extract_instruction(const string& s) {
	regex re("(turn on|turn off|toggle) (\\d+),(\\d+) through (\\d+),(\\d+)");
	smatch m;
	if (regex_match(s, m, re)) {
		int x1 = str_to_int(m[2].str());
		light_instruction_t foo = {1,1,1,1,TurnOn};
		return {
			.x1 = str_to_int(m[2].str()),
			.y1 = str_to_int(m[3].str()),
			.x2 = str_to_int(m[4].str()),
			.y2 = str_to_int(m[5].str()),
			.operation = str_to_lightoperation(m[1].str())
		};
	}
}

int total_brightness() {
	int count = 0;

	for(int i=0; i<SIZE; i++)
		for(int j=0; j<SIZE; j++)
			count += grid[i][j];

	return count;
}

void execute(light_instruction_t i) {
	if (i.operation == TurnOn) turn_on(i.x1, i.y1, i.x2, i.y2);
	else if (i.operation == TurnOff) turn_off(i.x1, i.y1, i.x2, i.y2);
	else if (i.operation == Toggle) toggle(i.x1, i.y1, i.x2, i.y2);
}

void execute_2(light_instruction_t i) {
	if (i.operation == TurnOn) turn_on_2(i.x1, i.y1, i.x2, i.y2);
	else if (i.operation == TurnOff) turn_off_2(i.x1, i.y1, i.x2, i.y2);
	else if (i.operation == Toggle) toggle_2(i.x1, i.y1, i.x2, i.y2);
}

void test_1() {
	clear_all();

	turn_on(0, 0, 999, 999);
	for(int i=0; i<SIZE; i++)
		for(int j=0; j<SIZE; j++)
			assert(grid[i][j] == 1);

	turn_off(0, 0, 999, 999);
	for(int i=0; i<SIZE; i++)
		for(int j=0; j<SIZE; j++)
			assert(grid[i][j] == 0);

	clear_all();
	light_instruction_t i1 = extract_instruction("turn on 0,0 through 999,999");
	assert(i1.x1 == 0);
	assert(i1.y1 == 0);
	assert(i1.x2 == 999);
	assert(i1.y2 == 999);
	assert(i1.operation == TurnOn);
	execute(i1);
	assert(total_brightness() == 1000000);

	clear_all();
	light_instruction_t i2 = extract_instruction("toggle 0,0 through 999,0");
	assert(i2.x1 == 0);
	assert(i2.y1 == 0);
	assert(i2.x2 == 999);
	assert(i2.y2 == 0);
	assert(i2.operation == Toggle);
	execute(i2);
	assert(total_brightness() == 1000);

	turn_on(0, 0, 999, 999);
	light_instruction_t i3 = extract_instruction("turn off 499,499 through 500,500");
	assert(i3.x1 == 499);
	assert(i3.y1 == 499);
	assert(i3.x2 == 500);
	assert(i3.y2 == 500);
	assert(i3.operation == TurnOff);
	execute(i3);
	assert(total_brightness() == 999996);
}

int main() {
	test_1();
	string input = read_file("06_input.txt");
	vector<string> instructions = split_str_by_char(input, '\n');

	// Part 1
	clear_all();
	for(auto i_str: instructions) {
		light_instruction_t i = extract_instruction(i_str);
		execute(i);
	}

	cout << "Part 1: Number of lights lit is: " << total_brightness() << endl;

	// Part 2
	clear_all();
	for(auto i_str: instructions) {
		light_instruction_t i = extract_instruction(i_str);
		execute_2(i);
	}

	cout << "Part 2: Number of lights lit is: " << total_brightness() << endl;
	
	return 0;
}
