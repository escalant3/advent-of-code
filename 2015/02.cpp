#include <assert.h>

#include <algorithm>
#include <iostream>
#include <vector>

#include "utils.h"

using namespace std;

typedef struct {
	int length;
	int width;
	int height;

	int get_area() {
		return 2*this->length*this->width +
			2*this->length*this->height +
			2*this->width*this->height;
	}

	int get_smallest_side_area() {
		return min(
				min(
					this->length*this->width,
					this->length*this->height
				),
				this->width*this->height
			);
	}

	int get_smallest_perimeter() {
		return min(
				min(
					2*this->length+2*this->width,
					2*this->length+2*this->height
				),
				2*this->width+2*this->height
			);
	}

	int cubic_area() {
		return this->length * this->width * this->height;
	}

} dimension_t;


dimension_t extract_dimensions(const string& dimension_str) {
	vector<string> v = split_str_by_char(dimension_str, 'x');
	assert(v.size() == 3);

	return {
		.length = str_to_int(v.at(0)), 
		.width = str_to_int(v.at(1)),
		.height = str_to_int(v.at(2))
	};
}

int get_paper(dimension_t& d) {
	return d.get_area() + d.get_smallest_side_area();
}

int get_ribbon(dimension_t& d) {
	return d.get_smallest_perimeter() + d.cubic_area();
}

void test_1() {
	dimension_t d;
	d = extract_dimensions("2x3x4");
	assert(get_paper(d) == 58);
	d = extract_dimensions("1x1x10");
	assert(get_paper(d) == 43);
	cout << "Test for first part pass!" << endl;
}

int main() {
	test_1();
	string input = read_file("02_input.txt");

	int total_paper = 0;
	int total_ribbon = 0;

	for(auto dimension_str: split_str_by_char(input, '\n')) {
		dimension_t dimension = extract_dimensions(dimension_str);
		int amount_of_paper = get_paper(dimension);
		int amount_of_ribbon = get_ribbon(dimension);
#ifdef DEBUG
		cout << dimension_str << " -> " << amount_of_paper << endl;
		cout << dimension_str << " -> " << amount_of_ribbon << endl;
#endif
		total_paper += amount_of_paper;
		total_ribbon += amount_of_ribbon;
	}

	cout << "Total amount of paper for step 1 is " << total_paper << " square feet" << endl;
	cout << "Total amount of ribbon for step 2 is " << total_ribbon << " feet" << endl;
	return 0;
}
