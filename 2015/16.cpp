#include <iostream>
#include <map>
#include <regex>
#include <vector>

#include "utils.h"

using namespace std;


class AuntSue {
	private:
		int id;
		map<string, int> features;

	public:
		AuntSue(const string& s) {
			regex re("Sue (\\d+): (\\w+): (\\d+), (\\w+): (\\d+), (\\w+): (\\d+)");
			smatch m;
			regex_search(s, m, re);
			id = str_to_int(m[1]);
			features[m[2]] = str_to_int(m[3]);
			features[m[4]] = str_to_int(m[5]);
			features[m[6]] = str_to_int(m[7]);
		}

		bool matches(map<string, int> ticker_tape) {
			for (auto it=ticker_tape.cbegin(); it!=ticker_tape.cend(); ++it) {
				if (features.find(it->first) != features.end() && features[it->first] != it->second) {
					return false;
				}
			}

			return true;
		}

		bool matches_2(map<string, int> ticker_tape) {
			for (auto it=ticker_tape.cbegin(); it!=ticker_tape.cend(); ++it) {
				if (features.find(it->first) != features.end()) {
					if (it->first == "cat" || it->first == "tree" ) {
						if (features[it->first] <= it->second) return false;
					} else if (it->first == "pomeranians" || it->first == "goldfish") {
						if (features[it->first] >= it->second) return false;
					} else {
						if (features[it->first] != it->second) return false;
					}
				}
			}

			return true;
		}

		string str() {
			string s;
			for (auto it=features.cbegin(); it!= features.cend(); ++it) {
				s +=  it->first + "->" + to_string(it->second) + " ";
			}
			return to_string(id) + " " +  s;
		}
};

int main() {
	map<string, int> ticker_tape;
	ticker_tape["children"] = 3;
	ticker_tape["samoyeds"] = 2;
	ticker_tape["cats"] = 7;
	ticker_tape["pomeranians"] = 3;
	ticker_tape["akitas"] = 0;
	ticker_tape["vizslas"] = 0;
	ticker_tape["goldfish"] = 5;
	ticker_tape["trees"] =  3;
	ticker_tape["cars"] =  2;
	ticker_tape["perfumes"] =  1;

	vector<string> input = split_str_by_char(read_file("16_input.txt"), '\n');
	for(auto l: input) {
		AuntSue aunt = AuntSue(l);
		if (aunt.matches(ticker_tape)) {
			cout << "Result: Aunt " << aunt.str() << endl;
			break;
		}
	}

	for(auto l: input) {
		AuntSue aunt = AuntSue(l);
		if (aunt.matches_2(ticker_tape)) {
			cout << "Result 2: Aunt " << aunt.str() << endl;
			break;
		}
	}

	return 0;
}
