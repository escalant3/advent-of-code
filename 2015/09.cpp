#include <algorithm>
#include <climits>
#include <iostream>
#include <regex>
#include <set>

#include "utils.h"

using namespace std;

typedef struct {
	string origin;
	string destination;
	int distance;
} route_detail_t;

route_detail_t parse_route(string s) {
	regex re("(.*) to (.*) = (\\d*)");
	smatch m;
	regex_search(s, m, re);
	return {
		.origin = m[1].str(),
		.destination = m[2].str(),
		.distance = str_to_int(m[3].str()) 
	};
}

vector<vector<string>> get_permutations(const set<string>& s) {
	vector<vector<string>> v;

	vector<string> v_from_set;
	for(auto x: s) {
		v_from_set.push_back(x);
	}

	do {
		vector<string> copy(v_from_set);
		v.push_back(copy);

	} while (next_permutation(v_from_set.begin(), v_from_set.end())); 

	return v;
}

set<string> cities;
map<pair<string, string>, int> distances;

int calculate_cost(vector<string> path) {
	int total = 0;
	for(int i=0; i<path.size()-1; i++) {
		total += distances[make_pair(path[i], path[i+1])];
	}
	return total;
}

void part_1_and_2() {
	string input = read_file("09_input.txt");
	vector<string> lines = split_str_by_char(input, '\n');

	for (auto line: lines) {
		route_detail_t route = parse_route(line);
		distances[make_pair(route.origin, route.destination)] = route.distance;
		distances[make_pair(route.destination, route.origin)] = route.distance;
		cities.insert(route.origin);
		cities.insert(route.destination);
	}

	vector<vector<string>> permutations = get_permutations(cities);

	int min_cost = INT_MAX;
	int max_cost = -1;

	vector<string> min_cost_option;
	vector<string> max_cost_option;

	for(auto p: permutations) {
		int cost = calculate_cost(p);

		if (cost < min_cost) {
			min_cost = cost;
			min_cost_option = p;	
		}

		if (cost > max_cost) {
			max_cost = cost;
			max_cost_option = p;
		}
	}

	cout << "Min cost option: ";
	for(auto e: min_cost_option) {
		cout << e << " -> ";
	}
	cout << min_cost << endl;

	cout << "Max cost option: ";
	for(auto e: max_cost_option) {
		cout << e << " -> ";
	}
	cout << max_cost << endl;
}
int main() {
	part_1_and_2();
	return 0;
}
