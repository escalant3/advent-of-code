'use strict';

const Fs = require('fs');

const run = (instructions, aValue=0, bValue=0) => {
    let r = { a: aValue, b: bValue };
    let pc = 0;

    while (true) {
        const itr = instructions[pc];
        if (!itr) break;
        const op = itr.slice(0, 3);
        const reg = itr[4];
        const offset = Number(itr.split(' ').slice(-1)[0]);
        switch (op) {
            case 'hlf': r[reg] /= 2; break;
            case 'tpl': r[reg] *= 3; break;
            case 'inc': r[reg]++; break;
            case 'jmp': pc += offset; continue;
            case 'jie':
                if (r[reg] % 2 === 0) { pc += offset; continue; }
                break;
            case 'jio':
                if (r[reg] === 1) { pc += offset; continue; }
                break;
            default:
                throw new Error(`Bad instruction ${itr}`);
        }
        pc++;
    }

    return r;
};

const data = Fs.readFileSync('input23.txt', 'utf-8').trim().split('\n');
const r1 = run(data);
console.log(`Part 1: Value of register b: ${r1.b}`);
const r2 = run(data, 1);
console.log(`Part 2 Value of register b: ${r2.b}`);


