#include <assert.h>
#include <iostream>
#include <vector>

using namespace std;

typedef struct {
	int value;
	int repetitions;
} data_t;

string process(string input) {
	string output = "";
	int consecutives = 1;
	char previous_char = input[0];
	vector<data_t> tracker;

	for(char c: input) {
		int value = c - '0';

		// First element
		if (tracker.size() == 0) {
			tracker.push_back({ .value = value, .repetitions = 1});
			continue;
		}

		data_t* last_item = &tracker.at(tracker.size() - 1);
		if (last_item->value == value) {
			last_item->repetitions++;
		} else {
			tracker.push_back({ .value = value, .repetitions = 1});
		}
	}

	for(auto item: tracker) {
		output += to_string(item.repetitions) + to_string(item.value);
	}

	return output;
}

void test() {
	assert(process("1") == "11");
	assert(process("11") == "21");
	assert(process("1211") == "111221");
	assert(process("111221") == "312211");
}

int main() {
	test();
	string input = "3113322113";
	string current_iteration = input;

	// Problem asks the length of doing the process 40 times
	for(int i; i<40; i++) {
		current_iteration = process(current_iteration);
	}

	cout << "Part 1 with input " << input << " has a size of " << current_iteration.size() << endl;

	current_iteration = input;

	// Problem (part 2) asks the length of doing the process 50 times
	for(int i; i<50; i++) {
		current_iteration = process(current_iteration);
	}

	cout << "Part 2 with input " << input << " has a size of " << current_iteration.size() << endl;
}
