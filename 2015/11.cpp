#include <assert.h>
#include <iostream>
#include <set>
#include <vector>

using namespace std;

string increase(string s) {
	for(int i=s.size()-1; i>=0; i--) {
		s[i]++;

		// If we went after z ('{' is next) set to a
		// and continue loop to change next digit
		if (s[i] == '{') {
			s[i] = 'a';
			continue;
		}

		break;
	}

	return s;
}

bool contains_increasing_straight_of_at_least_three(const string& s) {
	for(int i=0; i<s.size()-2; i++) {
		if (s[i]+1 == s[i+1] && s[i]+2 == s[i+2]) {
			return true;
		}
	}

	return false;
}

bool contains_forbidden_letters(const string& s) {
	if (s.find('i') != string::npos) return true;
	if (s.find('l') != string::npos) return true;
	if (s.find('o') != string::npos) return true;
	return false;
}

bool contains_at_least_two_different_pairs(const string& s) {
	set<char> found_letters_with_pair;

	for(int i=0; i<s.size()-1; i++) {
		if (s[i] == s[i+1]) found_letters_with_pair.insert(s[i]);
	}

	return found_letters_with_pair.size() > 1;
}

string get_next_valid_password(string password) {
	do {
		password = increase(password);
	} while (!contains_increasing_straight_of_at_least_three(password) ||
			contains_forbidden_letters(password) ||
			!contains_at_least_two_different_pairs(password));

	return password;
}

void test() {
	assert(increase("aaaaaaaa") == "aaaaaaab");
	assert(increase("aaaaaaaz") == "aaaaaaba");
	assert(increase("dfgehrhq") == "dfgehrhr");
	assert(increase("zzzzzzzz") == "aaaaaaaa");

	assert(contains_increasing_straight_of_at_least_three("abcdefg") == true);
	assert(contains_increasing_straight_of_at_least_three("abdeghl") == false);
	assert(contains_increasing_straight_of_at_least_three("acegikm") == false);
	assert(contains_increasing_straight_of_at_least_three("aaabcee") == true);
	assert(contains_increasing_straight_of_at_least_three("zabgedc") == false);

	assert(contains_forbidden_letters("aeiou") == true);
	assert(contains_forbidden_letters("alaop") == true);
	assert(contains_forbidden_letters("aoeta") == true);
	assert(contains_forbidden_letters("abcde") == false);
	assert(contains_forbidden_letters("abcdefgi") == true);

	assert(contains_at_least_two_different_pairs("aadeggaf") == true);
	assert(contains_at_least_two_different_pairs("aavfaade") == false);

	assert(get_next_valid_password("abcdefgh") == "abcdffaa");
	assert(get_next_valid_password("ghijklmn") == "ghjaabcc");
}

int main() {
	test();
	cout << "The solution to part 1 is " << get_next_valid_password("cqjxjnds") << endl;
	cout << "The solution to part 2 is " << get_next_valid_password("cqjxxyzz") << endl;
	return 0;
}
