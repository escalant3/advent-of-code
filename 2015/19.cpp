#include <iostream>
#include <map>
#include <regex>
#include <vector>
#include <set>

#include "utils.h"

using namespace std;

set<string> process(string input, map<string, vector<string>> replacements) {
	set<string> solutions;

	for(auto it=replacements.cbegin(); it!=replacements.cend(); ++it) {

		string needle = it->first;
		size_t search_pos = 0;
		size_t found_pos = input.find(needle, search_pos);

		while(found_pos != string::npos) {
			for(auto replacement: it->second) {
				string s2 = input;
				s2.replace(found_pos, needle.size(), replacement);
				solutions.insert(s2);
			}

			search_pos = found_pos + 1;
			found_pos = input.find(needle, search_pos);
		}
	}

	return solutions;

}

int process2(string input, map<string, vector<string>> replacements) {
	int steps = 0;
	string s(input);

	cout << s << " (" << to_string(s.size()) + ")" << endl;

	while (s != "e") {
		for(auto it=replacements.cbegin(); it!=replacements.cend(); ++it) {
			for(auto r: it->second) {

				string needle = r;
				size_t search_pos = 0;
				size_t found_pos = s.find(needle, search_pos);

				while(found_pos != string::npos) {
					s.replace(found_pos, needle.size(), it->first);
					++steps;
					search_pos = found_pos + 1;
					found_pos = s.find(needle, search_pos);
				}
			}
		}

		cout << s << " (" << to_string(s.size()) + ")" << endl;
	}

	return steps;
}

void test() {
	map<string, vector<string>> replacements;
	replacements["H"] = {"HO", "OH"};
	replacements["O"] = {"HH"};
	process("HOH", replacements);
}


int main() {
	string input = read_file("19_input.txt");
	input = split_str_by_char(input, '\n')[0]; // Remove trailing '\n'
	vector<string> replacements_input = split_str_by_char(read_file("19_replacements.txt"), '\n');

	map<string, vector<string>> replacements;

	regex re("(\\w+) => (\\w+)");
	smatch m;
	for (auto r: replacements_input) {
		regex_search(r, m, re);
		string needle = m[1].str();
		string replacement = m[2].str();
		if (replacements.find(needle) == replacements.end()) {
			vector<string> v;
			replacements[needle] = v;
		}
		replacements[needle].push_back(replacement);
	}

	auto solutions = process(input, replacements);
	cout << "There are " << solutions.size() << " solutions" << endl;

	cout << endl << endl;

	auto iterations = process2(input, replacements);
	cout << "e transform into the molecule in " << iterations << " iterations" << endl;

	return 0;
}

