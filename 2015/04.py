import hashlib

# Solved in python to avoid implementing MD5 in C++

def get_hash(key, value):
    m = hashlib.md5()
    m.update('%s%d' % (key, value))
    return m.hexdigest()

def find_adventcoin(key, prefix):
    n = 1L
    while True:
        candidate = get_hash(key, n)
        if candidate.startswith(prefix):
            print('The answer for %s is %d with hash %s' % (key, n, candidate))
            break
        n += 1

if __name__ == "__main__":
    find_adventcoin('abcdef', '00000')
    find_adventcoin('pqrstuv', '00000')
    find_adventcoin('yzbqklnj', '00000')
    find_adventcoin('yzbqklnj', '000000')
