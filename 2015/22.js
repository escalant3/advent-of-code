'use strict';

const spells = [
    { name: 'Magic Missile', cost: 53, last: 1, damage: 4 },
    { name: 'Drain', cost: 73, damage: 2, last: 1, heal: 2 },
    { name: 'Shield', cost: 113, last: 6, armor: 7 },
    { name: 'Poison', cost: 173, last: 6, damage: 3 },
    { name: 'Recharge', cost: 229, last: 5, mana: 101 }
];  

const play = (spellList, stats, hard=false) => {
    let playerArmor = 0;

    if (hard && stats.turn % 2 === 0) {
        stats.playerHp -= 1;
        if (stats.playerHp <= 0) return Infinity;
    }

    spellList.filter(x => x.last > 0).forEach(spell => {
        if (spell.damage) stats.bossHp -= spell.damage;
        if (spell.armor) playerArmor = spell.armor;
        if (spell.heal) stats.playerHp += spell.heal;
        if (spell.mana) stats.playerMana += spell.mana;
        spell.last--;
        spell.last = Math.max(spell.last, 0);
    });

    if (stats.turn % 2 !== 0) {
        stats.playerHp -= Math.max(stats.bossDamage - playerArmor, 1);
    }

    if (stats.bossHp <= 0) {
        return spellList.reduce((cost, spell) => spell.cost + cost, 0);
    }

    if (stats.playerHp <= 0 || stats.playerMana <= 0) { 
        return Infinity;
    }

    if (stats.turn % 2 !== 0) {
        stats.turn++;
        return play(spellList, stats, hard);
    }

    const activeSpells = spellList.filter(x => x.last > 0);
    const availableSpells = spells.filter(
        x => !activeSpells.find(y => y.name === x.name) &&
        x.cost <= stats.playerMana
    );

    if (availableSpells.length === 0) return Infinity;

    stats.turn++;
    return availableSpells
        .map(spell => {
            const st = {...stats};
            st.playerMana -= spell.cost;
            const sl = spellList.map(spell => ({...spell}));
            sl.push({...spell});
            return play(sl, st, hard);
        })
        .reduce((cost, game) => Math.min(game, cost), Infinity);
};

// From input
const BOSS_HP = 71;
const BOSS_DAMAGE = 10;

const run = (hard=false) => {
    const stats = {
        playerHp: 50,
        playerMana: 500,
        bossHp: BOSS_HP,
        bossDamage: BOSS_DAMAGE,
        turn: 0
    };
    return play([], stats, hard);
};

console.log(`Part 1: Minimum mana to win is ${run()}`);
console.log(`Part 2: Minimum mana to win is ${run(true)}`);
