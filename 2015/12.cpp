#include <iostream>
#include <regex>

#include "utils.h"

using namespace std;

int main() {
	string input = read_file("12_input.txt");

	regex re("(-?\\d+)");
	smatch m;

	string::const_iterator start_point(input.cbegin());

	int sum = 0;

	while (regex_search(start_point, input.cend(), m, re)) {
		int value_found = str_to_int(m[0].str());
		start_point += m.position() + m.length();
		cout << value_found << endl;
		sum += value_found;
	}

	/*
	string::const_iterator start_point2(input.cbegin());
	regex re2("(\\\"\\d+\\\")");
	while (regex_search(start_point2, input.cend(), m, re2)) {
		start_point2 += m.position() + m.length();
		cout << m[0] << endl;
	}
	*/

	cout << "Part 1: Total sum is " << sum << endl;

	return 0;
}
