#include <iostream>
#include <regex>
#include <vector>

#include "utils.h"

using namespace std;

class Reindeer {
	private:
		string name;
		int top_speed;
		int flying_time;
		int resting_time;

		int lifetime;
		int distance;
		int points;
	public:
		Reindeer (const string& n, int t, int f, int r) {
			name = n;
			top_speed = t;
			flying_time = f;
			resting_time = r;
			distance = 0;
			lifetime = 0;
			points = 0;
		}

		void loop() {
			int cycle = resting_time + flying_time;
			int cycle_position = lifetime % cycle;

			if (cycle_position < flying_time) {
				distance += top_speed;
			}

			lifetime++;
		}

		const string& get_name() {
			return name;
		}

		int get_distance() {
			return distance;
		}

		int get_points() {
			return points;
		}

		void give_leading_point() {
			points++;
		}

		int get_distance_with_math(int seconds) {
			int cycle_time = resting_time + flying_time;
			int whole_cycles = seconds / cycle_time;
			int distance_per_cycle = top_speed * flying_time;
			int distance = distance_per_cycle * whole_cycles;
			int remaing_time = seconds - whole_cycles * cycle_time;
			int remaining_flying_time = min(remaing_time, flying_time);
			distance += remaining_flying_time * top_speed;
			return distance;
		}
};

void test() {
	Reindeer comet = Reindeer("Comet", 14, 10, 127);
	Reindeer dancer = Reindeer("Dancer", 16, 11, 162);

	for(int i=0; i<1000; i++) {
		comet.loop();
		dancer.loop();
	}

	cout << "Comet distance: " << comet.get_distance() << endl;
	cout << "Dancer distance: " << dancer.get_distance() << endl;
	
	cout << "Comet with math: " << comet.get_distance_with_math(1000) << endl;
}

void process() {
	vector<string> input = split_str_by_char(read_file("14_input.txt"), '\n');
	vector<Reindeer> reindeers;

	regex re("(\\w+) can fly (\\d+) km/s for (\\d+) seconds, but then must rest for (\\d+) seconds.");
	smatch m;

	for(auto line: input) {
		regex_search(line, m, re);
		Reindeer r = Reindeer(
			m[1],
			str_to_int(m[2].str()),
			str_to_int(m[3].str()),
			str_to_int(m[4].str()));
		reindeers.push_back(r);
	}

	int seconds = 2503;

	for(int i=0; i<seconds; i++) {
		for(auto &r: reindeers) {
			r.loop();
		}

		int top_distance = 0;
		for(auto r: reindeers) {
			if (r.get_distance() > top_distance) {
				top_distance = r.get_distance();
			}
		}

		for(auto &r: reindeers) {
			if (r.get_distance() == top_distance) {
				r.give_leading_point();
			}
		}
	}

	for(auto r: reindeers) {
		cout << r.get_name() << " flew " << r.get_distance() << 
			" with " << r.get_points() << " points" << endl;
	}
}

int main() {
	//test();
	process();
	return 0;
}
