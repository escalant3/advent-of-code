#include <assert.h>

#include <iostream>
#include <inttypes.h>
#include <regex>

#include "utils.h"

#define PART2 1

using namespace std;

enum Operation { And, Or, Not, Lshift, Rshift, Set };

map<string, uint16_t> wire_system;
map<string, string> direct_open_wires;
map<string, string> unary_open_wires;
map<string, string> binary_open_wires;

uint16_t logic_op(Operation op, uint16_t x) {
	switch(op) {
		case Set:	return x;
		case Not:	return ~x;
		default:	assert(false);
	}
}

uint16_t logic_op(Operation op, uint16_t x, uint16_t y) {
	switch(op) {
		case And: 	return x&y;
		case Or:	return x|y;
		case Lshift:	return x<<y;
		case Rshift:	return x>>y;
		default:	
			cout << "Undefined operation " << op << endl;
			assert(false);
	}
}

Operation str_to_operation(const string& s) {
	if (s == "NOT") return Not;
	else if (s == "AND") return And;
	else if (s == "OR") return Or;
	else if (s == "LSHIFT") return Lshift;
	else if (s == "RSHIFT") return Rshift;

}

bool is_digits(const string& s) {
	return all_of(s.begin(), s.end(), ::isdigit);
}

uint16_t str_to_uint16(const string& s) {
	char *end;
	intmax_t val = strtoimax(s.c_str(), &end, 10);
	return (uint16_t) val;
}

void process_operation(const string& s) {
	regex re("(.+) -> (.+)");
	smatch m;
	if (regex_match(s, m, re)) {
		string destination_wire = m[2].str();
		vector<string> left_side = split_str_by_char(m[1].str(), ' ');

		int left_side_size = left_side.size();

		if (left_side_size == 1) {

			string value = left_side[0];

			if (is_digits(value)) {
#ifdef PART2
				if (destination_wire == "b") {
					wire_system[destination_wire] =
						logic_op(Set, str_to_uint16("3176"));
				} else {
					wire_system[destination_wire] = 
						logic_op(Set, str_to_uint16(value));
				}
#else
				wire_system[destination_wire] = 
					logic_op(Set, str_to_uint16(value));
#endif
			} else {
				direct_open_wires[destination_wire] = value;
			}
		}

		else if (left_side_size == 2) {
			unary_open_wires[destination_wire] = m[1].str();
		}

		else if (left_side_size == 3) {
			binary_open_wires[destination_wire] = m[1].str();
		}
	}
		
}

void test_1() {
	assert(logic_op(And, 123, 456) == 72);
	assert(logic_op(Or, 123, 456) == 507);
	assert(logic_op(Lshift, 123, 2) == 492);
	assert(logic_op(Rshift, 456, 2) == 114);
	assert(logic_op(Set, 123) == 123);
	assert(logic_op(Not, 123) == 65412);
	assert(logic_op(Not, 456) == 65079);
	
	process_operation("123 -> x");
	process_operation("456 -> y");
	process_operation("x AND y -> d");
	process_operation("x OR y -> e");
	process_operation("x LSHIFT 2 -> f");
	process_operation("x RSHIFT 2 -> fg");
	process_operation("NOT x -> h");
	process_operation("NOT y ->ih");

	assert(wire_system["d"] = 72);
	assert(wire_system["e"] = 507);
	assert(wire_system["f"] = 492);
	assert(wire_system["g"] = 114);
	assert(wire_system["h"] = 65412);
	assert(wire_system["i"] = 65079);
	assert(wire_system["x"] = 123);
	assert(wire_system["y"] = 456);

	cout << "All tests for part 1 passed!" << endl;
}

int main() {
	//test_1();
	
	string input = read_file("07_input.txt");
	vector<string> instructions = split_str_by_char(input, '\n');
	for(auto instruction: instructions) {
		process_operation(instruction);
	}

	
	bool changes = true;

	while(changes) {
		changes = false;

		for(auto it=direct_open_wires.begin(); it!=direct_open_wires.end(); ++it) {
			string value = it->second;
			if (wire_system.find(value) != wire_system.end()) {
				wire_system[it->first] = logic_op(Set, wire_system[value]);
				direct_open_wires.erase(it);
				changes = true;
			}
		}

		for(auto it=unary_open_wires.begin(); it!=unary_open_wires.end(); ++it) {
			string value = split_str_by_char(it->second, ' ')[1];
			if (wire_system.find(value) != wire_system.end()) {
				uint16_t result = logic_op(Not, wire_system[value]);
				wire_system[it->first] = result;
				unary_open_wires.erase(it);
				changes = true;
			}
		}
		
		for(auto it=binary_open_wires.begin(); it!=binary_open_wires.end(); ++it) {
			string value1 = split_str_by_char(it->second, ' ')[0];
			string value2 = split_str_by_char(it->second, ' ')[2];
			string operator_str = split_str_by_char(it->second, ' ')[1];

			if (wire_system.find(value1) != wire_system.end() && is_digits(value2)) {
				uint16_t result = logic_op(
						str_to_operation(operator_str),
						wire_system[value1],
						str_to_uint16(value2));
				wire_system[it->first] = result;
				binary_open_wires.erase(it);
				changes = true;
			}
		}

		for(auto it=binary_open_wires.begin(); it!=binary_open_wires.end(); ++it) {
			string value1 = split_str_by_char(it->second, ' ')[0];
			string value2 = split_str_by_char(it->second, ' ')[2];
			string operator_str = split_str_by_char(it->second, ' ')[1];

			if (wire_system.find(value2) != wire_system.end() && is_digits(value1)) {
				uint16_t result = logic_op(
						str_to_operation(operator_str),
						str_to_uint16(value1),
						wire_system[value2]);
				wire_system[it->first] = result;
				binary_open_wires.erase(it);
				changes = true;
			}
		}

		for(auto it=binary_open_wires.begin(); it!=binary_open_wires.end(); ++it) {
			string value1 = split_str_by_char(it->second, ' ')[0];
			string value2 = split_str_by_char(it->second, ' ')[2];
			string operator_str = split_str_by_char(it->second, ' ')[1];

			if (wire_system.find(value1) != wire_system.end() && wire_system.find(value2) != wire_system.end()) {
				uint16_t result = logic_op(
						str_to_operation(operator_str),
						wire_system[value1],
						wire_system[value2]);
				wire_system[it->first] = result;
				binary_open_wires.erase(it);
				changes = true;
			}
		}
	}

	cout << "Value of a: " << wire_system["a"] << endl;

	return 0;
}
