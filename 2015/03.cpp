#include <iostream>
#include <map>

#include "utils.h"

using namespace std;

enum Turn { Santa, Robosanta };

void add_visit(map<string, int>& m, int x, int y) {
	string key = to_string(x) + "-" + to_string(y);
	
	if (m.find(key) == m.end()) {
		m[key] = 0;
	}

	m[key] += 1; 
}

int part_1() {

	// Consider starting point (0,0)
	int x = 0;
	int y = 0;

	map<string, int> path_track;
	string input = read_file("03_input.txt");
	add_visit(path_track, x, y);

	for(char step: input) {
		if (step == 'v') y+= 1;
		else if (step == '^') y-= 1;
		else if (step == '<') x-= 1;
		else if (step == '>') x+= 1;

		add_visit(path_track, x, y);
	}

	return path_track.size();
}

int part_2() {

	// Consider starting point (0,0)
	int santa_x = 0;
	int santa_y = 0;
	int robosanta_x = 0;
	int robosanta_y = 0;
	Turn turn = Santa;

	map<string, int> path_track;
	string input = read_file("03_input.txt");
	add_visit(path_track, 0, 0);

	for(char step: input) {
		if (turn == Santa) {
			if (step == 'v') santa_y += 1;
			else if (step == '^') santa_y -= 1;
			else if (step == '<') santa_x -= 1;
			else if (step == '>') santa_x += 1;

			add_visit(path_track, santa_x, santa_y);
			turn = Robosanta;

		} else {
			if (step == 'v') robosanta_y += 1;
			else if (step == '^') robosanta_y -= 1;
			else if (step == '<') robosanta_x -= 1;
			else if (step == '>') robosanta_x += 1;

			turn = Santa;
			add_visit(path_track, robosanta_x, robosanta_y);
		}

	}

	return path_track.size();

}



int main() {

	int nodes_visited_part_1 = part_1();
	cout << "Number of nodes visited: " << nodes_visited_part_1 << endl;
	int nodes_visited_part_2 = part_2();
	cout << "Number of nodes visited 2: " << nodes_visited_part_2 << endl;
	return 0;
}
