#include "utils.h"

std::string read_file(const std::string& filename) {
	std::ifstream f(filename);
	std::stringstream buffer;
	buffer << f.rdbuf();
	return buffer.str();
}

std::vector<std::string> split_str_by_char(const std::string& input, char separator) {

	std::string buffer = "";
	std::vector<std::string> v = {};

	for(char c: input) {
		if (c == separator) {
			v.push_back(buffer);
			buffer = "";
			continue;
		}

		buffer += c;
	}

	if (buffer != "") {
		v.push_back(buffer);
	}

	return v;
}

int str_to_int(const std::string& input) {
	return atoi(input.c_str());
}

