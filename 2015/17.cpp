#include <bitset>
#include <iostream>
#include <map>
#include <vector>

using namespace std;

struct result_t {
	int good_combinations;
	int good_combinations_with_minimum_number_of_containers;
	int minimum_number_of_containers;
};

struct result_t process(int liters, vector<int> input) {
	size_t input_size = input.size();

	if (input_size > 32) {
		cout << "Input is too big for 32 bit mask" << endl;
		return {-1, -1 -1};
	}

	size_t max_mask = 2 << (input_size - 1);
	int good_combinations = 0;
	map<int, int> combinations_per_number_of_containers;

	for(int i=0; i<max_mask; ++i) {
		bitset<32> b(i);
		int sum = 0;
		for(size_t j=0; j<input_size; ++j) {
			sum += b[j]*input[j];
		}

		if (sum == liters) {
			++good_combinations;
			int number_of_bits_set = b.count();
			if (combinations_per_number_of_containers.find(number_of_bits_set) ==
					combinations_per_number_of_containers.end()) {
				combinations_per_number_of_containers[number_of_bits_set] = 0;
			}
			combinations_per_number_of_containers[number_of_bits_set] += 1;
		}

	}

	int minimum_number_of_containers = input_size;
	int good_combinations_with_minimum_number_of_containers = 0;

	for(auto it=combinations_per_number_of_containers.begin();
			it!=combinations_per_number_of_containers.end(); ++it) {
		if (it->first < minimum_number_of_containers) {
			minimum_number_of_containers = it->first;
			good_combinations_with_minimum_number_of_containers = it->second;
		}
	}
	return {
		good_combinations,
		good_combinations_with_minimum_number_of_containers,
		minimum_number_of_containers
	};
}

void test() {
	vector<int> input = {20, 15, 10, 5, 5};
	int liters = 25;
	struct result_t result = process(liters, input);
	cout << "Example good combinations: " <<
		result.good_combinations << endl;

	cout << "Example good combinations with " <<
		result.minimum_number_of_containers <<
		" containers: " <<
		result.good_combinations_with_minimum_number_of_containers << endl;
}

int main() {
	test();

	vector<int> input = {50, 44, 11, 49, 42, 46, 18, 32, 26, 40, 21, 7, 18, 43, 10, 47, 36, 24, 22, 40};
	int liters = 150;
	struct result_t result = process(liters, input);
	cout << "Input good combinations: " <<
		result.good_combinations << endl;

	cout << "Input good combinations with " <<
		result.minimum_number_of_containers <<
		" containers: " <<
		result.good_combinations_with_minimum_number_of_containers << endl;
	return 0;
}
