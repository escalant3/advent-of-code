#include <iostream>
#include <regex>

#include "utils.h"

using namespace std;

typedef struct {
	int num_in_string_code;
	int num_in_memory;
} char_counter_t;

typedef struct {
	int num_in_string_code_before;
	int num_in_string_code_after;
} char_counter_2_t;

void replace_all(string& haystack, const string& needle, const string& replacement) {
	size_t start = 0;
	while ((start = haystack.find(needle)) != string::npos) {
		size_t end = start + replacement.length();

	}
}

char_counter_t get_count(string& s) {

	int total_code_characters = (int) s.size();
	
#ifdef DEBUG
	cout << s << endl;
#endif

	// Remove enclosing quoutes
	s = s.substr(1, s.size() - 2);

	// Escaped \" count as one
	regex re1("\\\\\"");
	s = regex_replace(s, re1, "\"");

	// Escaped \\ count as one
	regex re2("\\\\\\\\");
	s = regex_replace(s, re2, "\\");

	// Escaped \xab count as one
	regex re3("\\\\x[0-9a-f]{2}");
	s = regex_replace(s, re3, "X");

#ifdef DEBUG
	cout << s << endl;
#endif

	int in_memory_characters = (int) s.size();
	
	return {
		.num_in_string_code = total_code_characters,
		.num_in_memory = in_memory_characters
	};
}

char_counter_2_t get_count_2(string& s) {

	int size_before = (int) s.size();
	
#ifdef DEBUG
	cout << s << endl;
#endif

	// Encode \\ into \\\\ ->
	regex r2("\\\\");
	s = regex_replace(s, r2, "\\\\");

	// Endode \" into \\\" ->
	regex r1("\"");
	s = regex_replace(s, r1, "\\\"");

	// Encode enclosing quoutes
	s = "\"" + s + "\"";


#ifdef DEBUG
	cout << s << endl;
#endif

	int size_after = (int) s.size();
	
	return {
		.num_in_string_code_before = size_before,
		.num_in_string_code_after = size_after
	};
}

void part_1() {
	string input = read_file("08_input.txt");
	vector<string> lines = split_str_by_char(input, '\n');
	int sum_code = 0;
	int sum_memory = 0;
	for(auto line: lines) {
		char_counter_t c1 =get_count(line);
		sum_code += c1.num_in_string_code;
		sum_memory += c1.num_in_memory;
	}
	cout << "Input has " << sum_code <<
		" characters in code and " << sum_memory <<
		" characters in memory" << endl;

	cout << "Solution " << sum_code - sum_memory << endl;
}


void part_2() {
	//string input = read_file("temp.txt");
	string input = read_file("08_input.txt");
	vector<string> lines = split_str_by_char(input, '\n');
	int sum_before = 0;
	int sum_after = 0;
	for(auto line: lines) {
		char_counter_2_t c1 =get_count_2(line);
		sum_before += c1.num_in_string_code_before;
		sum_after += c1.num_in_string_code_after;
	}
	cout << "Input has " << sum_before <<
		" characters before and " << sum_after <<
		" after" << endl;

	cout << "Solution " << sum_after - sum_before << endl;
}

int main() {
	part_1();
	part_2();
	return 0;
}
