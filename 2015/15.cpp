#include <iostream>

using namespace std;

struct ingredient_t {
	string name;
	int capacity;
	int durability;
	int flavor;
	int texture;
	int calories;
};

struct result_t {
	int max;
	int max_with_500_calories;
};

struct result_t get_max_score(struct ingredient_t ingredients[]) {
	int max = 0;
	int max_with_500_calories = 0;

	for(int i=0; i<100; ++i) {
		for (int j=0; j<100; ++j) {
			if (i+j > 100) continue;
			for (int k=0; k<100; ++k) {
				if (i+j+k > 100) continue;
				for (int l=0; l<100; ++l) {
					if (i+j+k+l != 100) continue;
						int c = i*ingredients[0].capacity +
									j*ingredients[1].capacity +
									k*ingredients[2].capacity +
									l*ingredients[3].capacity;
						int d = i*ingredients[0].durability +
									j*ingredients[1].durability +
									k*ingredients[2].durability +
									l*ingredients[3].durability;
						int f = i*ingredients[0].flavor +
									j*ingredients[1].flavor +
									k*ingredients[2].flavor +
									l*ingredients[3].flavor;
						int t = i*ingredients[0].texture +
									j*ingredients[1].texture +
									k*ingredients[2].texture +
									l*ingredients[3].texture;

						int cal = i*ingredients[0].calories +
									j*ingredients[1].calories +
									k*ingredients[2].calories +
									l*ingredients[3].calories;

						int score = c*d*f*t;

						if (c<0 || d<0 || f<0 || t<0) score = 0;

						if (score>max) {
							cout << score << endl;
							max = score;
						}

						if (cal == 500 && score > max_with_500_calories) {
							max_with_500_calories = score;
						}
				}
			}
		}
	}

	return { max, max_with_500_calories };;
}

int main() {
	struct ingredient_t frosting =		{ "Frosting", 4, -2, 0, 0, 5 };
	struct ingredient_t candy =			{ "Candy", 0, 5, -1, 0, 8 };
	struct ingredient_t butterscotch =	{ "Butterscotch", -1, 0, 5, 0, 6 };
	struct ingredient_t sugar =			{ "Sugar", 0, 0, -2, 2, 1 };

	struct ingredient_t ingredients[] = { frosting, candy, butterscotch, sugar };
	result_t result = get_max_score(ingredients);

	cout << "Max score: " << result.max << endl;
	cout << "Max score with 500 calories: " << result.max_with_500_calories << endl;

	return 0;
}
