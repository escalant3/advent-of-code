#include <algorithm>
#include <iostream>
#include <map>
#include <regex>
#include <set>
#include <vector>

#include "utils.h"

using namespace std;

class DinnerTable {
	private:
		set<string> people; 
		map<pair<string, string>, int> rules;
	public:
		int get_happiness(const string& p1, const string& p2) {
			pair<string, string> key = make_pair(p1, p2);
			return rules[key];
		}

		void add_rule(const string& p1, const string& p2, int happiness) {
			pair<string, string> key = make_pair(p1, p2);
			people.insert(p1);
			rules[key] = happiness;
		}

		int get_optimal_change_in_happiness() {
			int best = 0;
			
			vector<string> people_list;
			for(auto p: people) people_list.push_back(p);
			sort(people_list.begin(), people_list.end());

			do {
				int current_happiness = 0;

				for(size_t i=0; i<people_list.size(); i++) {
					string p1 = people_list[i];
					string p2 = people_list[(i+1)%people_list.size()];
					string p3 = people_list[(i+people_list.size()-1)%people_list.size()];
					pair<string, string> key;
				       	key = make_pair(p1, p2);
					current_happiness += rules[key];
					key = make_pair(p1, p3);
					current_happiness += rules[key];
				}

				if (current_happiness > best) {
#ifdef DEBUG
					for(auto p: people_list) {
						cout << p << "-";
					}
					cout << "->" << current_happiness << endl;
#endif
					best = current_happiness;
				}

			} while (next_permutation(people_list.begin(), people_list.end()));

			return best;
		}

		void add_zero_happiness_person() {
			vector<string> people_list;
			for(auto p: people) people_list.push_back(p);

			for(auto person: people_list) {
				add_rule("Diego", person, 0);
				add_rule(person, "Diego", 0);
			}
		}

} dinner_table;

regex re("(\\w+) would (\\w+) (\\d+) happiness units by sitting next to (\\w+)");

void str_to_rule(const string& s) {
	smatch m;
	regex_search(s, m, re);
	int value = str_to_int(m[3]);
	if (m[2].str() == "lose") {
		value *= -1;
	}	       
	dinner_table.add_rule(m[1].str(), m[4].str(), value);
}

void process(const string& filename) {
	vector<string> input = split_str_by_char(read_file(filename), '\n');

	// Extract rules
	for(auto line: input) {
		str_to_rule(line);
	}

	cout << "P1: Best happiness score is " << dinner_table.get_optimal_change_in_happiness() << endl;

	// Add myself to the table
	dinner_table.add_zero_happiness_person();
	cout << "P2: Best happiness score is " << dinner_table.get_optimal_change_in_happiness() << endl;
}

void test() {
	process("13_test.txt");
}

int main() {
	//test();
	process("13_input.txt");
	return 0;
}
