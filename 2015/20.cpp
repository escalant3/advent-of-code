#include <iostream>

using namespace std;

long get_house_present(int n) {
	long sum = 0;
	for(int i=1; i<=n; ++i) {
		if (n%i == 0) {
			sum += i;
		}
	}
	return sum*10;
}

long get_house_present_2(int n) {
	long sum = 0;
	for(int i=1; i<=n; ++i) {
		if (n%i == 0 && n/i <= 50) {
			sum += i;
		}
	}
	return sum*11;
}


void part1() {
	long input = 33100000;
	long max = 0;
	for(long i=700000; ; ++i) {
		long value = get_house_present(i);
		if (value > max) {
			max = value;
			cout << i << " " << value << endl;
		}
		if (value >= input) {
			cout << "House " << i << " got " << value << " presents" << endl;
			break;
		}
	}
}

void part2() {
	long input = 33100000;
	long max = 0;
	for(long i=1; ; ++i) {
		long value = get_house_present_2(i);
		if (value > max) {
			max = value;
			cout << i << " " << value << endl;
		}
		if (value >= input) {
			cout << "House " << i << " got " << value << " presents" << endl;
			break;
		}
	}
}

int main() {
	part1();
	part2();
	return 0;
}
