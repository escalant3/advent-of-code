'use strict';

// TODO - Too slow. It should be optimized without array allocations

const Fs = require('fs');

const sum = x => x.reduce((t, i) => t + i, 0);
const prod = x => x.reduce((t, i) => t * i, 1);
const intersect = (a, b) => a.some(x => b.includes(x));

const run = (splits) => {
    const data = Fs.readFileSync('input24.txt', 'utf-8').trim().split('\n').map(Number);
    const total = sum(data);
    const bucketGoal = total / splits;

    let validBuckets = [];
    let options = data.map(x => [x]);
    while (true) {
        const newOptions = options.map(option => {
            const largest = option[option.length-1];
            const newCandidates = data.filter(x => x > largest).map(x => option.concat(x)).filter(nc => {
                const s = sum(nc);
                if (s === bucketGoal) validBuckets.push(nc);
                return s < bucketGoal;
            });
            return newCandidates;
        });
        options = newOptions.flat();
        if (options.length === 0) break;
    }
    return validBuckets.reduce((min, item) => Math.min(min, prod(item)), Infinity);
};

console.log(`Part 1: ${run(3)}`);
console.log(`Part 2: ${run(4)}`);


    /*
    const bucketsWithSmallestQE = validBuckets.filter(x => prod(x) === smallestQE);
    const smallestBucketSize = bucketsWithSmallestQE.reduce((min, item) => Math.min(item.length, min), Infinity);
    const passengerBucket = bucketsWithSmallestQE.find(x => x.length === smallestBucketSize)
    console.log('Passenger', passengerBucket, smallestBucketSize, smallestQE);
    validBuckets = validBuckets.filter(x => !intersect(x, passengerBucket));

    const arrangements = [];
    for(let i=0; i<validBuckets.length-1; i++) {
        for(let j=i+1; j<validBuckets.length; j++) {
            if (!intersect(validBuckets[i], validBuckets[j])) {
                arrangements.push([passengerBucket, validBuckets[i], validBuckets[j]]);
            }
        }
    }
    console.log(validBuckets);
    console.log(arrangements);
    */
