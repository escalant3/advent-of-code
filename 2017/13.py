def process(layers, initial_position, return_early=False):
    sum = 0
    for (layer_depth, layer_range) in layers:
        if layer_range is None:
            continue

#        print initial_position, layer_depth, layer_range, initial_position + layer_depth, (initial_position + layer_depth) % (2 * (layer_range-1)) == 0
        if (initial_position + layer_depth) % (2 * (layer_range-1)) == 0:
            sum += layer_depth * layer_range
            if return_early:
                return -1

    return sum

def part_1(layers):
    print 'Result part_1:', process(layers, 0)

def part_2(layers):
    for i in range(0, -15, -1):
        print i, process(layers,i)

    initial_position = 0
    cost = process(layers, initial_position, True)
    while (cost != 0):
        initial_position -= 1
        cost = process(layers, initial_position, True)
        if (initial_position %  1000 == 0):
            print initial_position * -1

    print 'Result part_2:', initial_position * - 1

def is_impact(layer_depth, layer_range, delay=0):
    #print  (layer_range * 2 - 1 - layer_depth)
    return (delay + layer_depth) % ((layer_range-1) * 2) == 0

def part_22(layers, delay):
    for l in layers:
        layer_depth, layer_range = l
        if layer_range is None:
            continue
        if is_impact(layer_depth, layer_range, delay):
            print 'CRASH at', layer_depth, 'with', delay, 'delay'
            return False
    return True

def test():
    layers = ((0,3), (1,2), (2,None), (3,None), (4, 4), (5,None), (6,4))
    #cost = process(layers, 0)
    #part_1(layers)
    print is_impact(0, 3, 0) == True
    print is_impact(0, 8, 0) == True
    print is_impact(0, 2, 6) == True
    print is_impact(1, 3, 0) == False
    print is_impact(1, 2, 1) == True
    print is_impact(1, 2, 2) == False
    #print is_impact(1, 2, 2) == False
    #print is_impact(1, 2, 3) == True

    delay = 0
    while not part_22(layers, delay):
        delay += 1

    print 'Passed with delay', delay

if __name__ == '__main__':
    #test()

    with open('13_input.txt') as f:
        data = f.read()
        lines = data.split('\n')
        layers = []
        for line in lines:
            if line:
                layer_depth, layer_range = line.split(': ')
                layers.append((int(layer_depth), int(layer_range)))
#        part_1(layers)


        delay = 0
        while not part_22(layers, delay):
            delay += 1

    print 'Passed with delay', delay

