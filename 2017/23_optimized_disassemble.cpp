#include <iostream>

int a = 1;
int b = 0;
int c = 0;
int d = 0;
int e = 0;
int f = 0;
int g = 0;
int h = 0;

void print_registers() {

    std::cout << "a: " << a <<
    "\t b: " << b <<
    "\t c: " << c <<
    "\t d: " << d <<
    "\t e: " << e <<
    "\t f: " << f <<
    "\t g: " << g <<
    "\t h: " << h <<
    std::endl;
}

int main() {

    b = 79;
    c = b;

    if (a != 0) {
        b *= 100;
        b -= -100000;
        c = b;
        c -= -17000;
    }

    while (true) {
        f = 1;
        d = 2;
        
        do {
            e = 2;

            do {
                g = d;
                g *= e;
                g -= b;

                if (g == 0)
                    f = 0;

                e -= -1;
                g = e;
                g -= b;
            } while (g != 0 && f != 0);

            d -= -1;
            g = d;
            g -= b;
        } while (g!=0 && f != 0);

        if (f == 0) {
            h -= -1;
            print_registers();
        }

        g = b;
        g -= c;

        if (g == 0) {
            print_registers();
            return 0; // DONE
        }

        b -= -17;
    }
}

