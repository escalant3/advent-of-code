#include <iostream>
#include <cctype>
#include <map>
#include <vector>

#include "utils.h"

using namespace std;

class Computer {
	private:
		map<char, long> registers;
		vector<string> instructions;
		long program_counter = 0;
		long get_value(const string& operand) {
			if (isalpha(operand[0])) {
				return registers[operand[0]];
			}

			return str_to_int(operand);
		}
	public:
        int mul_calls = 0;
        Computer() {
            cout << "Initializing registers" << endl;
            registers['a'] = 0; // 0 for debug mode (part 1)
            registers['b'] = 0;
            registers['c'] = 0;
            registers['d'] = 0;
            registers['e'] = 0;
            registers['f'] = 0;
            registers['g'] = 0;
            registers['h'] = 0;
        }
		void add_instruction(const string& i) {
			instructions.push_back(i);
		}
		bool is_program_complete() {
			return program_counter >= instructions.size();
		}
		void execute() {
			string i = instructions[program_counter];
            /*
			cout << "Executing: " << i << endl;
            */
            cout << "a: " << registers['a'] <<
            "\t b: " << registers['b'] <<
            "\t c: " << registers['c'] <<
            "\t d: " << registers['d'] <<
            "\t e: " << registers['e'] <<
            "\t f: " << registers['f'] <<
            "\t g: " << registers['g'] <<
            "\t h: " << registers['h'] <<
            endl;
//            cin.get();
			vector<string> instruction = split_str_by_char(i, ' ');
			string operation = instruction[0];
			string operand1 = instruction[1];
			string operand2;
			if (instruction.size() == 3) {
				operand2 = instruction[2];
			}

			if (operation == "set") {
				long value = get_value(operand2);
				registers[operand1[0]] = value;
			} else if (operation == "sub") {
                long value = get_value(operand2);
                registers[operand1[0]] -= value;
			} else if (operation == "mul") {
                mul_calls++;
				long value2 = get_value(operand2);
				registers[operand1[0]] *= value2;;
			} else if (operation == "jnz") {
				long value = get_value(operand1);
				long value2 = get_value(operand2);
				if (value != 0) {
					program_counter += value2;
					return;
				}
			}

			++program_counter;

		}
};

void process(vector<string> instructions) {
	Computer c;

	for (auto i: instructions) {
		if (i != "") {
			c.add_instruction(i);
		}
	}

	while (!c.is_program_complete()) {
		c.execute();
	}

    cout << "Calls to mul: " << c.mul_calls << endl;
}

int main() {
	string input = read_file("23_input.txt");
	vector<string> instructions = split_str_by_char(input, '\n');
	process(instructions);
	return 0;
}
