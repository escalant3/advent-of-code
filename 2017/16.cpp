#include <assert.h>

#include <iostream>
#include <regex>

#include "utils.h"

using namespace std;

class ProgramDance {
	private:
		string programs;
	public:
		ProgramDance(const string& s) {
			programs = s;
		}

		void spin(int x) {
			size_t i = programs.size() - x;
			string chunk_to_spin = programs.substr(i, x);
			programs =chunk_to_spin + programs.substr(0, programs.size() -x);
		}

		void exchange(int a, int b) {
			char aux = programs[a];
			programs[a] = programs[b];
			programs[b] = aux;
		}

		void partner(char a, char b) {
			size_t pos_a = programs.find(a, 0);
			size_t pos_b = programs.find(b, 0);
			exchange(pos_a, pos_b);
		}

		void set(const string& s) {
			programs = s;
		}

		string str() {
			return programs;
		}
};

int char_to_int(char c) {
	return c - '0';
}

string process(const string& input, vector<string> dance_moves, long iterations=1) {
	ProgramDance p(input);

	regex re_split("s(\\d+)");
	regex re_exchange("x(\\d+)/(\\d+)");
	regex re_partner("p([a-p])/([a-p])");
	smatch m;

	for(long i=0; i<iterations; ++i) {

		// If we get the input string again the cycle will 
		// continuosly repeat. Skip forward all the useless
		// work
		if (i != 0 && p.str() == input) {
			i = iterations - (iterations % i);
		}
		
		for(auto move: dance_moves) {
			if (regex_search(move, m, re_split)) {
				p.spin(str_to_int(m[1].str()));
			} else if (regex_search(move, m, re_exchange)) {
				p.exchange(str_to_int(m[1]), str_to_int(m[2]));
			} else if (regex_search(move, m, re_partner)) {
				p.partner((m[1].str())[0], (m[2].str())[0]);
			} else {
				return "Uknown command: " + move;
			}
		}
	}

	return p.str();
}

void test() {
	ProgramDance p ("abcde");
	p.spin(3);
	assert("cdeab" == p.str());

	ProgramDance p2 ("abcde");
	p2.spin(1);
	assert("eabcd" == p2.str());

	p2.exchange(3, 4);
	assert("eabdc" == p2.str());

	p2.partner('e', 'b');
	assert("baedc" == p2.str());

	vector<string> moves = {"s1", "x3/4", "pe/b"};
	assert("baedc" == process("abcde", moves));
}

int main() {
	//test();
	string input = read_file("16_input.txt");
	long iterations = 1;
	cout << "Result after " << iterations << " iterations:" <<
		process("abcdefghijklmnop", split_str_by_char(input, ','), iterations) <<
		endl;

	iterations = 1000000000;
	cout << "Result after " << iterations << " iterations:" <<
		process("abcdefghijklmnop", split_str_by_char(input, ','), iterations) <<
		endl;

	return 0;
}
