#include <iostream>
#include <set>

#include "utils.h"

bool is_valid(std::string s) {
	std::vector<std::string> full_list = split_str_by_char(s, ' ');
	std::set<std::string> non_repeated_list(full_list.begin(), full_list.end());
	return full_list.size() == non_repeated_list.size();
}

bool is_valid_2(std::string s) {
	std::vector<std::string> full_list = split_str_by_char(s, ' ');
	// Sort the string for part 2 to easy anagram detection
	for(int i=0; i<full_list.size(); i++)
		std::sort(full_list[i].begin(), full_list[i].end());
	std::set<std::string> non_repeated_list(full_list.begin(), full_list.end());
	return full_list.size() == non_repeated_list.size();
}



int main() {
	std::vector<std::string> phrases = split_str_by_char(read_file("04_input.txt"), '\n');

	int valid_phrases = 0;
	int valid_phrases_2 = 0;
	for (auto phrase: phrases) {
		if (is_valid(phrase)) valid_phrases++;
		if (is_valid_2(phrase)) valid_phrases_2++;
	}

	std::cout << valid_phrases << " of " << phrases.size() << " are valid in part 1" <<std::endl;
	std::cout << valid_phrases_2 << " of " << phrases.size() << " are valid in part 2" <<std::endl;

	return 0;
}
