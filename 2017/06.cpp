#include <iostream>
#include <set>
#include <vector>

#include "utils.h"

using namespace std;

int get_highest_index(vector<int> blocks) {
	int max_index = 0;
	int max_value = 0;

	for(int i=0; i<blocks.size(); i++) {
		if (blocks[i] > max_value) {
			max_index = i;
			max_value = blocks[i];
		}
	}	

	return max_index;
}

string get_blocks_as_string(vector<int> v) {
	string response = "";
	for(auto e: v) response += to_string(e) + " ";
	return response;
}

void process_1(string input_str) {
	vector<string> input = split_str_by_char(input_str, ' ');
	vector<int> blocks;
	set<string> combos;
	int appearances = 0;
	int cycles = 0;

	for(auto i: input) {
		blocks.push_back(str_to_int(i));
	}
	
	while (appearances < 2) {
		cycles++;

		// Find highest
		int max_index = get_highest_index(blocks);

		// Redistribute
		int current_value = blocks[max_index];
		blocks[max_index] = 0;
		int next_index = (max_index + 1) % blocks.size();
		for(int i=0; i<current_value; i++) {
			blocks[next_index]++;
			next_index = (next_index + 1) % blocks.size();

			string combo = get_blocks_as_string(blocks);
		}
		
		// Check if combo has happened
		string combo = get_blocks_as_string(blocks);

		if (combos.find(combo) != combos.end()) {
			cout << "The #" << appearances+1 << " repeated combo is " << combo << " after " << cycles << " cycles " << endl;
			appearances++;

			// Reset for next iteration
			cycles = 0;
			combos.clear();
			combos.insert(combo);
		}

		combos.insert(combo);
	}
}

void test_1() {
	process_1("0 2 7 0");
}

int main() {
	test_1();
	process_1("11 11 13 7 0 15 5 5 4 4 1 1 7 1 15 11");
	return 0;
}
