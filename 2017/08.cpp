#include <assert.h>
#include <iostream>
#include <regex>

#include "utils.h"

using namespace std;

enum operator_t {Increase, Decrease};
enum condition_t {Greater, Lower, GreaterOrEqual, LowerOrEqual, Equal, Different};

typedef struct {
	string destination; 
	operator_t op;
	int operand;
	string condition_register;
	condition_t condition;
	int condition_value;

	void to_string() {
		cout << destination << " " << operand << " " << 
			op <<  " " <<
			condition_register << " " << condition << " "  << condition_value << endl;
	}
} instruction;

class RegisterArray {
	private:
		map<string, int> regs;
		int highest_value_ever = 0;

		void set_if_new(const string& register_name) {
			if (regs.find(register_name) == regs.end()) {
				regs[register_name] = 0;
			}
		}

		void mark_highest(int value) {
			if (value > highest_value_ever)
				highest_value_ever = value;
		}
	public:
		int get_value(const string& register_name) {
			set_if_new(register_name);
			return regs[register_name];
		}
		int increase_value_by(const string& register_name, int value) {
			set_if_new(register_name);
			regs[register_name] += value;
			mark_highest(regs[register_name]);
		}
		int decrease_value_by(const string& register_name, int value) {
			set_if_new(register_name);
			regs[register_name] -= value;
			mark_highest(regs[register_name]);
		}
		int get_largest_value() {
			int max = 0;
			for(auto it=regs.begin(); it!=regs.end(); it++) {
				if (it->second > max) {
					max = it->second;
				}
			}
			return max;
		}
		int get_highest_value_ever() {
			return highest_value_ever;
		}
} registers;

condition_t get_condition(const string& s) {
	if (s == ">") return Greater;
	else if (s == "<") return Lower;
	else if (s == ">=") return GreaterOrEqual;
	else if (s == "<=") return LowerOrEqual;
	else if (s == "==") return Equal;
	else if (s == "!=") return Different;
	else assert(false);
}

instruction str_to_instruction(const string& s) {
	vector<string> s1 = split_str_by_char(s, ' ');

	instruction i = {
		.destination = s1[0],
		.op = (s1[1] == "inc") ? Increase : Decrease,
		.operand = str_to_int(s1[2]),
	       	.condition_register = s1[4],
		.condition = get_condition(s1[5]),
		.condition_value = str_to_int(s1[6])	
	};

	//i.to_string();

	return i;
}

void process(instruction i) {
	// Return if condition is not met
	switch (i.condition) {
		case Greater:
			if (registers.get_value(i.condition_register) <= i.condition_value) return;
			break;
		case Lower:
			if (registers.get_value(i.condition_register) >= i.condition_value) return;
			break;
		case GreaterOrEqual:
			if (registers.get_value(i.condition_register) < i.condition_value) return;
			break;
		case LowerOrEqual:
			if (registers.get_value(i.condition_register) > i.condition_value) return;
			break;
		case Equal:
			if (registers.get_value(i.condition_register) != i.condition_value) return;
			break;
		case Different:
			if (registers.get_value(i.condition_register) == i.condition_value) return;
			break;
	}

	if (i.op == Decrease) {
		registers.decrease_value_by(i.destination, i.operand);
	} else if (i.op == Increase) {
		registers.increase_value_by(i.destination, i.operand);
	} else {
		assert(false);
	}
}

void test_1() {
	instruction i1 = str_to_instruction("b inc 5 if a > 1");
	process(i1);
	instruction i2 = str_to_instruction("a inc 1 if b < 5");
	process(i2);
	instruction i3 = str_to_instruction("c dev -10 if a >= 1");
	process(i3);
	instruction i4 = str_to_instruction("c inc -20 if c == 10");
	process(i4);
	cout << registers.get_largest_value() << endl;
}

int main() {
	//test_1();
	string input = read_file("08_input.txt");
	for(auto line: split_str_by_char(input, '\n')) {
		process(str_to_instruction(line));
	}
	cout << "Current maximum value " << registers.get_largest_value() << endl;
	cout << "Maximum ever " << registers.get_highest_value_ever() << endl;
	return 0;
}

