#include <iostream>
#include <map>

using namespace std;

// PART 1

int get_group_size(int radius) {
	if (radius == 0) {
		return 1;
	}

	return radius * 8;
}

int get_max_distance(int radius) {
	if (radius == 0) return 0;
	return get_max_distance(radius-1) + 2;
}

int get_steps(int n) {
	int covered_numbers = 0;
	int radius = -1;

	while(true) {
		radius++;
		covered_numbers += get_group_size(radius);

		if (covered_numbers > n) {
			int ring_start = covered_numbers - get_group_size(radius) + 1;
			int ring_end = covered_numbers;

			int max_ring_distance = get_max_distance(radius);

			// Lowest value starts in the lower-right corner (above the corner)
			// Distance is the max - 1 (max would be the actual corner)
			int current_distance = max_ring_distance - 1;
			int distance_modifier = -1;

			map<int, int> ring_distances;

			for(int i=ring_start; i<ring_end; i++) {
				ring_distances[i] = current_distance;

				// Reached left, right, top, or bottom centers.
				// That's the minimum distance so start increasing
				if (current_distance == radius) {
					distance_modifier = 1;
				}

				// Reached a corner. That's the maximum distance so decrease
				if (current_distance == max_ring_distance) {
					distance_modifier = -1;
				}

				current_distance += distance_modifier;
			}

			return ring_distances[n];
		}
	}
}

void process_part_1(int input) {
	int steps = get_steps(input);
	cout << "The steps for " << input << " are " << steps << endl;
}


// PART 2
map<pair<int, int>, int> grid;

int get_next_value(int x, int y) {
	int sum = 0;
	for(int i=x-1; i<=x+1; i++) {
		for(int j=y-1; j<=y+1; j++) {
			pair<int, int> p = make_pair(i, j);
			sum += grid.find(p) == grid.end() ? 0 : grid[p];
		}
	}
	return sum;
}

void draw_grid(int size) {
	for(int j=size; j>=-size; j--) {
		for(int i=-size; i<=size; i++) {
			cout << grid[make_pair(i, j)] << '\t';
		}

		cout << endl;
	}
}

void process_part_2(int input) {
	int x = 0;
	int y = 0;

	int current_value = 0;

	grid[make_pair(x, y)] = 1;

	int max_radius = 4;
	int radius = 1;

	int answer = 0;

	for(int radius=1; radius<=max_radius; radius++) {
		while (x<radius && answer == 0) {
			x += 1;
			current_value = get_next_value(x, y);
			if (current_value > input) answer = current_value;
			grid[make_pair(x, y)] = current_value;
		}

		while (y<radius && answer == 0) {
			y += 1;
			current_value = get_next_value(x, y);
			if (current_value > input) answer = current_value;
			grid[make_pair(x, y)] = current_value;
		}

		while (x>-radius && answer == 0) {
			x += -1;
			current_value = get_next_value(x, y);
			if (current_value > input) answer = current_value;
			grid[make_pair(x, y)] = current_value;
		}

		while (y>-radius && answer == 0) {
			y += -1;
			current_value = get_next_value(x, y);
			if (current_value > input) answer = current_value;
			grid[make_pair(x, y)] = current_value;
		}

		while (x<radius && answer == 0) {
			x += 1;
			current_value = get_next_value(x, y);
			if (current_value > input) answer = current_value;
			grid[make_pair(x, y)] = current_value;
		}
	}

	draw_grid(max_radius);
	cout << "The first number larger than " << input << " is " << answer << endl;
}

void test_1() {
	process_part_1(8);
	process_part_1(12);
	process_part_1(33);
	process_part_1(1024);
}

int main() {
	//test_1();
	process_part_1(368078);
	process_part_2(368078);
	return 0;
}
