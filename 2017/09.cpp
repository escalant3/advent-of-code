#include <iostream>

#include "utils.h"

using namespace std;

typedef struct {
	int score;
	int garbage_chars;

	string str() {
		return to_string(score) + "-" + to_string(garbage_chars);
	}
} result_t;

result_t process(const string& s) {
	int sum = 0;
	int garbage_sum = 0;
	int current_group_score = 0;
	bool garbage = false;
	bool ignore_next = false;

	for(auto c: s) {
		if (ignore_next) {
			ignore_next = false;
			continue;
		} else if (!garbage && c == '{') {
			++current_group_score;
		} else if (!garbage && c == '}') {
			sum += current_group_score;
			current_group_score--;
		} else if (!garbage && c == '<') {
			garbage = true;
		} else if (c == '>') {
			garbage = false;
		} else if (c == '!') {
			ignore_next = true;
		} else if (garbage == true) {
			garbage_sum++;
		}
	}

	result_t result = { .score = sum, .garbage_chars = garbage_sum };

	return result;
}

void test() {
	cout << process("{}").str() << endl;
	cout << process("{{{}}}").str() << endl;
	cout << process("{{},{}}").str() << endl;
	cout << process("{{{},{},{{}}}}").str() << endl;
	cout << process("{<a>,<a>,<a>,<a>}").str() << endl;
	cout << process("{{<ab>},{<ab>},{<ab>},{<ab>}}").str() << endl;
	cout << process("{{<!!>},{<!!>},{<!!>},{<!!>}}").str() << endl;
	cout << process("{{<a!>},{<a!>},{<a!>},{<ab>}}").str() << endl;

	cout << process("<random characters>").str() << endl;
	cout << process("<<<<>").str() << endl;
	cout << process("<{!>}>").str() << endl;
	cout << process("<!!>").str() << endl;
	cout << process("<!!!>>").str() << endl;
	cout << process("<{o\"i!a,<{i<a>").str() << endl;
}

int main() {
	//test();
	string input = read_file("09_input.txt");
	cout << "Sum of input is " << process(input).str() << endl;
	return 0;
}
