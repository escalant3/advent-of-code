#include <iostream>

#define MAX_16_BITS 65535	// 1111 1111 1111 1111

#define PART_2 1

using namespace std;

long get_next(long n, long factor) {
	return (n*factor) % 2147483647;	
}

long get_next_part_2(long n, long factor, long multiple_of) {
	long candidate = n;
       
	do {	
		candidate = get_next(candidate, factor);
	} while (candidate % multiple_of != 0);

	return candidate;
}

void process(int a, int b, int pairs) {

	long a_factor = 16807;
	long b_factor = 48271;

	int total = 0;

	for(int i=0; i<pairs; ++i) {
#ifdef PART_2
		a = get_next_part_2(a, a_factor, 4);
		b = get_next_part_2(b, b_factor, 8);
#else
		a = get_next(a, a_factor);
		b = get_next(b, b_factor);
#endif

		if ((a & MAX_16_BITS) == (b & MAX_16_BITS)) {
			total += 1;
		}
	}

	cout << "Total score is: " << total << endl;
}

void test() {
#ifdef PART_2
	process(65, 8921, 5000000);
#else
	process(65, 8921, 5);
#endif
}

int main() {
//	test();
#ifdef PART_2
	process(679, 771, 5000000); // Puzzle input
#else
	process(679, 771, 40000000); // Puzzle input
#endif
	return 0;
}
