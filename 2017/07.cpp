#include<algorithm>
#include <iostream>
#include <map>
#include <regex>
#include <vector>

#include "utils.h"

using namespace std;

struct node_t;

class NodeRegistry {
	private:
		map<string, struct node_t> registry;
	public:
		bool contains(const string& s);
		struct node_t get(const string& s); 
		void set(const string& s, struct node_t n);
		void update_node_weight(const string& s, int weight);
} registry;

struct node_t {
	string name;
	int weight;
	vector<string> children;

	bool is_leaf() {
		return children.size() == 0;
	}

	int get_total_weight() {
		int sum = 0;
		for(auto child: children) {
			sum += registry.get(child).get_total_weight();
		}
		return weight + sum;
	}

	void adjust_children_weight() {
		map<int, vector<struct node_t>> level;

		for(auto child: children) {
			struct node_t child_node = registry.get(child);
			int child_total_weight = child_node.get_total_weight();

			if (level.find(child_total_weight) == level.end()) {
				vector<struct node_t> v;
				level[child_total_weight] = v;
			}

			level[child_total_weight].push_back(child_node);
		}

		// Not all the nodes have the same size
		if (level.size() > 1) {

			int different_weight;
			int common_weight;

			struct node_t unbalanced_node;

			for(auto it=level.begin(); it!=level.end(); it++) {
				struct node_t node;

				if (it->second.size() == 1) {
					unbalanced_node = it->second[0];
				} else {
					common_weight = it->first;
				}
			}

			unbalanced_node.adjust_children_weight();
			int new_total_weight = unbalanced_node.get_total_weight();
			int diff = common_weight - new_total_weight;

			if (diff!=0) {
				cout << "Adjusting "  << unbalanced_node.name << "(" <<
					unbalanced_node.weight <<
			      		")" << " with " << diff << endl;	
				registry.update_node_weight(unbalanced_node.name, unbalanced_node.weight + diff);
			}
		}
	}

};

bool NodeRegistry::contains(const string& s) {
	return (registry.find(s) != registry.end());
}

struct node_t NodeRegistry::get(const string& s) {
	return registry[s];
}

void NodeRegistry::set(const string& s, struct node_t n) {
	registry[s] = n;
}

void NodeRegistry::update_node_weight(const string& s, int weight) {
	registry[s].weight = weight;
}

map<string, struct node_t> queued_nodes;

struct node_t str_to_node(const string& s) {
	regex re("^(\\w+) \\((\\d+)\\)( -> (.*))?$");
	smatch m;
	regex_search(s, m, re);

	struct node_t node;
	node.name = m[1].str();
	node.weight = str_to_int(m[2].str());

	// has children?
	string children_str = m[4].str();
	if (children_str != "") {
		children_str.erase(remove_if(children_str.begin(), children_str.end(), ::isspace), children_str.end());
		node.children = split_str_by_char(children_str, ',');
	}

	return node;
}

bool all_children_are_registered(struct node_t n) {
	for(auto child: n.children) {
		if (!registry.contains(child)) {
			return false;
		}
	}

	return true;
}

void process(string filename) {
	string input = read_file(filename);
	vector<string> lines = split_str_by_char(input, '\n');

	for(auto line: lines) {
		struct node_t node = str_to_node(line);
		queued_nodes[node.name] = node;
	}

	// Place leafs - Last placed node is the root
	struct node_t last_node_processed;
	while (queued_nodes.size() > 1) {
		for(auto it=queued_nodes.begin(); it!=queued_nodes.end(); /* no-inc */) {
			if (it->second.is_leaf() || all_children_are_registered(it->second)) {
				registry.set(it->first, it->second);
				last_node_processed = it->second;
				queued_nodes.erase(it++);
				continue;
			}
			it++;
		}
	}

	cout << "Last node processed (root) is " << last_node_processed.name << endl;

	last_node_processed.adjust_children_weight();
}

void test() {
	process("07_test.txt");
}

int main() {
	//test();
	process("07_input.txt");
	return 0;
}
