#include <climits>
#include <iostream>
#include <vector>

using namespace std;

#define SPREADSHEET_ROWS 16
#define SPREADSHEET_COLUMNS 16

string spreadsheet[SPREADSHEET_ROWS] = { 
	"1364	461	1438	1456	818	999	105	1065	314	99	1353	148	837	590	404	123",
	"204	99	235	2281	2848	3307	1447	3848	3681	963	3525	525	288	278	3059	821",
	"280	311	100	287	265	383	204	380	90	377	398	99	194	297	399	87",
	"7698	2334	7693	218	7344	3887	3423	7287	7700	2447	7412	6147	231	1066	248	208",
	"3740	837	4144	123	155	2494	1706	4150	183	4198	1221	4061	95	148	3460	550",
	"1376	1462	73	968	95	1721	544	982	829	1868	1683	618	82	1660	83	1778",
	"197	2295	5475	2886	2646	186	5925	237	3034	5897	1477	196	1778	3496	5041	3314",
	"179	2949	3197	2745	1341	3128	1580	184	1026	147	2692	212	2487	2947	3547	1120",
	"460	73	52	373	41	133	671	61	634	62	715	644	182	524	648	320",
	"169	207	5529	4820	248	6210	255	6342	4366	5775	5472	3954	3791	1311	7074	5729",
	"5965	7445	2317	196	1886	3638	266	6068	6179	6333	229	230	1791	6900	3108	5827",
	"212	249	226	129	196	245	187	332	111	126	184	99	276	93	222	56",
	"51	592	426	66	594	406	577	25	265	578	522	57	547	65	564	622",
	"215	2092	1603	1001	940	2054	245	2685	206	1043	2808	208	194	2339	2028	2580",
	"378	171	155	1100	184	937	792	1436	1734	179	1611	1349	647	1778	1723	1709",
	"4463	4757	201	186	3812	2413	2085	4685	5294	5755	2898	200	5536	5226	1028	180"
};

vector<int> split_string_by_character(const string& input, char separator) {
	
	vector<int> v;

	string buffer = "";

	for(char s: input) {
		if (s == separator) {
			v.push_back(atoi(buffer.c_str()));
			buffer = "";
			continue;	
		}

		buffer += s;
	}

	if (buffer != "") v.push_back(atoi(buffer.c_str()));

	return v;
}

int part_one() {

	vector<int> differences = {};

	for(string row: spreadsheet) {
		vector<int> cells = split_string_by_character(row, '\t');
		int row_min = INT_MAX;
		int row_max = 0;
		for (int cell: cells) {
			if (cell < row_min) row_min = cell;
			if (cell > row_max) row_max = cell;
		}

		int difference = row_max - row_min;
		differences.push_back(difference);
	}

	int sum = 0;
	for(int d: differences) {
		sum += d;
	}

	return sum;
}

int get_difference_part_2(vector<int> v) {
	for(int i=0; i < v.size(); i++) {
		for (int j=i+1; j < v.size(); j++) {
			int a = v.at(i);
			int b = v.at(j);
			if (b>a) {
				int aux = b;
				b = a;
				a = aux;
			}

			if (a%b == 0) return a/b;
		}
	}

	cout << "ERROR: Numbers not found" << endl;
}

int part_two() {

	vector<int> differences = {};

	for(string row: spreadsheet) {
		vector<int> cells = split_string_by_character(row, '\t');
		int difference = get_difference_part_2(cells);
		differences.push_back(difference);
	}

	int sum = 0;
	for(int d: differences) {
		sum += d;
	}

	return sum;
}



int main() {
	
	int part_one_result = part_one();
	cout << "Result: " << part_one_result << endl;

	int part_two_result = part_two();
	cout << "Result 2: " << part_two_result << endl;

	return 0;
}
