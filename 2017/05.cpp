#include <iostream>
#include <vector>

#include "utils.h"

using namespace std;

int increase_current(int current_value, int part) {
	if (part == 1) {
		return 1;
	}

	// Part 2
	return current_value >= 3 ? -1 : 1;
}

int process(vector<int> instructions, int part) {
	int current_position = 0;
	int steps = 0;

	while (current_position >= 0 && current_position < instructions.size()) {
		int next_position = current_position + instructions[current_position];
		instructions[current_position] += increase_current(instructions[current_position], part);
		steps++;
		current_position = next_position;
	}

	return steps;
}

void test() {
	vector<int> example = {0, 3, 0, 1, -3};
	cout << "Test 1 done in " << process(example, 1) << " steps" << endl;
	cout << "Test 2 done in " << process(example, 2) << " steps" << endl;
}

int main() {
	test();
	string file_data = read_file("05_input.txt");
	vector<string> lines = split_str_by_char(file_data, '\n');
	vector<int> instructions;
	for(auto line: lines) instructions.push_back(str_to_int(line));
	cout << "Part 1 done in " << process(instructions, 1) << " steps" << endl;
	cout << "Part 2 done in " << process(instructions, 2) << " steps" << endl;

}
