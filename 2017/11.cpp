#include <iostream>
#include <map>
#include <vector>

#include "utils.h"

using namespace std;

int adjust_distance(map<string, int> d) {
	while (d["se"] > 0 and d["sw"] > 0) {
		--d["se"];
		--d["sw"];
		++d["s"];
	}

	while (d["ne"] > 0 and d["nw"] > 0) {
		--d["ne"];
		--d["nw"];
		++d["n"];
	}

	while (d["ne"] > 0 and d["sw"] > 0) {
		--d["ne"];
		--d["sw"];
	}

	while (d["nw"] > 0 and d["se"] > 0) {
		--d["nw"];
		--d["se"];
	}

	while (d["n"] > 0 and d["s"] > 0) {
		--d["n"];
		--d["s"];
	}

	while (d["ne"] > 0 and d["s"] > 0) {
		--d["ne"];
		--d["s"];
		++d["se"];
	}

	while (d["nw"] > 0 and d["s"] > 0) {
		--d["nw"];
		--d["s"];
		++d["sw"];
	}

	int sum = 0;
	for (auto it=d.begin(); it!=d.end(); ++it) sum+= it->second;
	return sum;
}

bool valid_step(string step) {
	return (step == "se" || step == "sw" || step == "s" ||
			step == "ne" || step == "nw" || step == "n");
}

void process(vector<string> steps) {
	cout << steps.size() << " steps" << endl;

	map<string, int> d;
	d["se"] = 0;
	d["sw"] = 0;
	d["nw"] = 0;
	d["ne"] = 0;
	d["n"] = 0;
	d["s"] = 0;

	int max_distance = 0;

	for (auto step: steps) {
		if (!valid_step(step)) continue;
		d[step] += 1;
		int iteration_distance = adjust_distance(d);
		if (iteration_distance > max_distance)
			max_distance = iteration_distance;
	}

	cout << "Max distance is " << max_distance << endl;

	cout << "Final distance is " << adjust_distance(d) << endl;
}

void test() {
	vector<string> s1 = {"ne", "ne", "ne"};
	process(s1);
	vector<string> s2 = {"ne", "ne", "sw", "sw"};
	process(s2);
	vector<string> s3 = {"ne", "ne", "s", "s"};
	process(s3);
	vector<string> s4 = {"se", "sw", "se", "sw", "sw"};
	process(s4);
}

int main() {
	string input = read_file("11_input.txt");
	input.replace(input.size()-1, 1, ""); // Remove \n
	vector<string> steps = split_str_by_char(input, ',');

	process(steps);

	return 0;
}
