#!/usr/bin/env python


def spin(step, cycles):

    buf = [0]

    current_position = 0

    for i in range(1, cycles+1):

        buffer_length = len(buf)
        new_position = (current_position + step) % (buffer_length)

        if (new_position == buffer_length):
            buf.append(i)
        else:
            buf.insert(new_position+1, i)

        current_position = new_position + 1

    print buf[max(current_position-3, 0):min(current_position+3, len(buf))]

def spin_2(step, cycles):

    buf = [0, -1]

    current_position = 0

    for i in range(1, cycles+1):

        buffer_length = i
        new_position = (current_position + step) % (buffer_length)

        if (new_position == 0):
            print 'Element after 0 changed:', i
            buf[1] = i

        current_position = new_position + 1

if __name__ == '__main__':

    # Part 1
    # spin(step=3, cycles=2018)  # Test
    spin(step=355, cycles=2017)

    # Part 2
    spin_2(step=355, cycles=50000000)

