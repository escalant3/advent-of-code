#include <assert.h>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdint.h>
#include <vector>

// #define INVAL
// #define PRINT

using namespace std;

typedef uint32_t u32;

string get_hash(const string& input_string) {
	vector<int> list;
	vector<int> input_lengths;

	for(int i=0; i<256; ++i) list.push_back(i);
	for(auto c: input_string) input_lengths.push_back(c); // Converts char to int
	input_lengths.push_back(17);
	input_lengths.push_back(31);
	input_lengths.push_back(73);
	input_lengths.push_back(47);
	input_lengths.push_back(23);

	int skip_size = 0;
	int pos = 0;
	int size = list.size();
	for(int round=0; round<64; ++round) {
		for(auto l: input_lengths) {
			int i = pos;
			int j = pos+l-1;
			while (i<j) {
				int aux = list[i%size];
				list[i%size] = list[j%size];
				list[j%size] = aux;
				++i;
				--j;
			}
			pos += l + skip_size++;
		}
	}

	vector<int> dense_harsh;
	for(size_t i=0; i<list.size(); i+=16) {
		int dense_value = list[i];
		for(int j=i+1; j<i+16; ++j) {
			dense_value = dense_value ^ list[j];
		}
		dense_harsh.push_back(dense_value);
	}

	stringstream ss;
	for(auto i: dense_harsh) ss << setfill('0') << setw(2) << hex << i;
	return ss.str();
}

int get_one_bits(const string& s) {
	int sum = 0;
	for(auto ascii_c: s) {
		int c = strtoul(string(1, ascii_c).c_str(), NULL, 16);
		if (c & 1) ++sum;
		if ((c >> 1) & 1) ++sum;
		if ((c >> 2) & 1) ++sum;
		if ((c >> 3) & 1) ++sum;
	}
	return sum;
}

int get_used_squares(const string& key) {
	int sum = 0;
	for(int i=0; i<128; ++i) {
		string hash = get_hash(key + "-" + to_string(i));
		sum += get_one_bits(hash);
	}

	return sum;
}

void invalidate_section_cells(bool grid[128][128], int i, int j) {
#ifdef INVAL
	cout << "Invalidating (" << i << "," << j << ")" << endl;
#endif
	grid[i][j] = false;

	if ((i-1)>=0 && grid[i-1][j]) 	invalidate_section_cells(grid, i-1, j);
	if ((i+1)<128 && grid[i+1][j]) 	invalidate_section_cells(grid, i+1, j);
	if ((j-1)>=0 && grid[i][j-1]) 	invalidate_section_cells(grid, i, j-1);
	if ((j+1)<128 && grid[i][j+1]) 	invalidate_section_cells(grid, i, j+1);
}

int get_regions(const string& key) {

	bool grid[128][128];

	for(int i=0; i<128; ++i) {
		string hash = get_hash(key + "-" + to_string(i));
		for(int segment=0; segment<32; segment += 8) {
			u32 c = strtoul(hash.substr(segment, 8).c_str(), NULL, 16);
			for(int j=0; j<32; ++j) {
				grid[i][j+(segment*4)] = ((c >> j) & 1) ? true : false;
			}
		}

	}

	// Flip array
	for(int i=0; i<128; ++i) {
		for(int k=1; k<5; ++k) {
			for(int j=0; j<16; ++j) {
				int front_index = j + (k-1)*(32);
				int back_index = k*31 + (k-1) - j;
				bool aux = grid[i][back_index];
				grid[i][back_index] = grid[i][front_index];
				grid[i][front_index] = aux;
			}
		}
	}

#ifdef PRINT
	int dim_start = 0;
	int dim = 128;
	for (int i=dim_start; i<dim; ++i) {
		for (int j=dim_start; j<dim; ++j) {
			cout << grid[i][j]  << " ";
		}
		cout << endl;
	}
#endif

	int sum = 0;
	for(int i=0; i<128; ++i)
		for (int j=0; j<128; ++j)
			sum += grid[i][j] ? 1 : 0;

	cout << "Sum is " << sum << endl;

	int sections = 0;
	for(int i=0; i<128; ++i) {
		for (int j=0; j<128; ++j) {
			if (grid[i][j]) {
				sections += 1;
				#ifdef INVAL
				cout << "SECTION " << sections << endl;
				#endif
				invalidate_section_cells(grid, i, j);
			}

		}
	}

	cout << "Number of sections is " << sections << endl;
}

void test() {
	cout << get_hash("flqrgnkx-0") << endl;
	cout << get_hash("flqrgnkx-1") << endl;
	cout << get_hash("flqrgnkx-2") << endl;
	cout << get_hash("flqrgnkx-3") << endl;
	cout << get_hash("flqrgnkx-4") << endl;
	cout << get_hash("flqrgnkx-5") << endl;
	cout << get_hash("flqrgnkx-6") << endl;
	cout << get_hash("flqrgnkx-7") << endl;

	cout << get_one_bits("0") << endl;
	cout << get_one_bits("1") << endl;
	cout << get_one_bits("a") << endl;

	string key = "flqrgnkx";

	cout << "Used squares: " << get_used_squares(key) << endl;

	get_regions(key);
}


int main() {
	//test();
	string input = "stpzcrnm";

	// Part 1
	cout << "Used squares: " << get_used_squares(input) << endl;
	bool grid[128][128];
	for(int i=0;i<128;++i) {
		for(int j=0;j<128;++j) {
			grid[i][j] = true;
		}
	}

	// Part 2
	get_regions(input);
	return 0;
}
