#ifndef UTILS_H
#define UTILS_H

#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

std::string read_file(const std::string& filename);
std::vector<std::string> split_str_by_char(const std::string& input, char separator);
int str_to_int(const std::string& input);
#endif
