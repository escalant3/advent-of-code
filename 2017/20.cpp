#include <iostream>
#include <map>
#include <regex>

#include "utils.h"

using namespace std;

struct position {
  long x;
  long y;
  long z;
};

struct velocity {
  long x;
  long y;
  long z;
};

struct acceleration {
  long x;
  long y;
  long z;
};

struct particle {
  struct position p;
  struct velocity v;
  struct acceleration a;
  bool removed = false;
  void update() {
    v.x += a.x;
    v.y += a.y;
    v.z += a.z;
    p.x += v.x;
    p.y += v.y;
    p.z += v.z;
  }
  string str() {
    return "<" + to_string(p.x) + "," + to_string(p.y) + "," + to_string(p.z) + ">";
  }
  long distance() {
    return abs(p.x) + abs(p.y) + abs(p.z);
  }
};

void test() {
  struct position o0 = {3, 0, 0};
  struct velocity v0 = {2, 0, 0};
  struct acceleration a0 = {-1, 0, 0};
  struct particle p0 = {o0, v0, a0};

  struct position o1 = {4, 0, 0};
  struct velocity v1 = {0, 0, 0};
  struct acceleration a1 = {-2, 0, 0};
  struct particle p1 = {o1, v1, a1};

  for(int i=0; i<3; ++i) {
    p0.update();
    p1.update();
    cout << p0.str() << " Distance: " << p0.distance() << endl;
    cout << p1.str() << " Distance: " << p1.distance() << endl;
    cout << endl;
  }

}

int main() {
  //test();

  string input = read_file("20_input.txt");
  vector<string> lines = split_str_by_char(input, '\n');
  regex re("p=<(-?\\d+),(-?\\d+),(-?\\d+)>, v=<(-?\\d+),(-?\\d+),(-?\\d+)>, a=<(-?\\d+),(-?\\d+),(-?\\d+)>");
  smatch m;
  vector<struct particle> particles;
  for (auto l: lines) {
    regex_search(l, m, re);
    struct position o = {str_to_int(m[1].str()), str_to_int(m[2].str()), str_to_int(m[3].str())};
    struct velocity v = {str_to_int(m[4].str()), str_to_int(m[5].str()), str_to_int(m[6].str())};
    struct acceleration a = {str_to_int(m[7].str()), str_to_int(m[8].str()), str_to_int(m[9].str())};
    struct particle p = {o, v, a};
    particles.push_back(p);
  }

  for(int i=0; i<10000; ++i) {
    long distance = 99999999999;
    int closest_particle = 0;
    for(size_t j=0; j<particles.size(); ++j) {
      particles.at(j).update();
      if (particles.at(j).distance() < distance) {
        distance = particles.at(j).distance();
        closest_particle = j;
      }
    }


    cout << "Closest particle in iteration " << i << " is particle in position " <<
      closest_particle << " -> " << particles.at(closest_particle).str() << endl;
  }


  // Part 2
  particles.clear();

  map<string, vector<int>> collisions;
  for (auto l: lines) {
    regex_search(l, m, re);
    struct position o = {str_to_int(m[1].str()), str_to_int(m[2].str()), str_to_int(m[3].str())};
    struct velocity v = {str_to_int(m[4].str()), str_to_int(m[5].str()), str_to_int(m[6].str())};
    struct acceleration a = {str_to_int(m[7].str()), str_to_int(m[8].str()), str_to_int(m[9].str())};
    struct particle p = {o, v, a};
    particles.push_back(p);
  }

  for(int i=0; i<500; ++i) {
    long distance = 99999999999;
    int closest_particle = 0;
    for(size_t j=0; j<particles.size(); ++j) {
      particles.at(j).update();
      collisions[particles.at(j).str()].push_back(j);
      if (particles.at(j).distance() < distance) {
        distance = particles.at(j).distance();
        closest_particle = j;
      }
    }

    for (auto it=collisions.begin(); it!=collisions.end(); ++it) {
      if (it->second.size() > 1) {
        for(auto pos: it->second) {
          cout << "Removing particle " << pos << " at " << particles.at(pos).str() << endl;
          particles.at(pos).removed = true;
        }
      }
    }

    collisions.clear();

    int particles_left = 0;
    for (auto p: particles) {
      if (!p.removed) ++particles_left;
    }

    cout << "There are " << particles_left << " particles left" << endl;
  }


  return 0;
}