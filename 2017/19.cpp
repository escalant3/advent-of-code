#include <algorithm>
#include <iostream>
#include <cctype>
#include <map>
#include <vector>

#include "assert.h"

#include "utils.h"

using namespace std;

typedef struct Position {
	int x;
	int y; 
} Position;

enum Direction { north, west, south, east, none };

Direction resolve_next_position(vector<vector<char>>& matrix, Position& current_position, Direction current_direction, vector<char>& path_tracker) {

	cout << "Accessing element: (" << current_position.x << ", " << current_position.y << ")" << endl;
	char current_tile = matrix[current_position.y][current_position.x];
	if (current_tile == '|' || current_tile == '-') { 
		return current_direction;
	}
	else if (current_tile >= 'A' && current_tile <= 'Z') {
		path_tracker.push_back(current_tile);
	} else if (current_tile == '+') {
		if (current_direction == north || current_direction == south) {
			if (current_position.x > 0) {
				auto left_tile = matrix[current_position.y][current_position.x-1];
				if (left_tile != ' ') {
					return west;
				}
			}

			if (current_position.x < matrix[0].size() - 1) {
				auto right_tile = matrix[current_position.y][current_position.x+1];
				if (right_tile != ' ') {
					return east;
				}
			}

			return none;
		}

		if (current_direction == west || current_direction == east) {
			if (current_position.y > 0) {
				auto north_tile = matrix[current_position.y-1][current_position.x];
				if (north_tile != ' ') {
					return north;
				}
			}

			if (current_position.y < matrix.size() - 1) {
				auto south_tile = matrix[current_position.y+1][current_position.x];
				if (south_tile != ' ') {
					return south;
				}
			}

			return none;
		}


	} else {
		return none;
	}
	return current_direction;
}
	
void execute_1(vector<string> rows) {
	// Create matrix
	vector<vector<char>> matrix;
	for (auto row: rows) {
		vector<char> new_row;
		for (auto cell: row) {
			new_row.push_back(cell);
		}
		matrix.push_back(new_row);
	}

	// Find initial element
	Position current_position;
	Direction current_direction = south;

	current_position.y = 0;

	vector<char>::iterator it;
	vector<char> first_row = matrix[0];

	it = std::find(first_row.begin(), first_row.end(), '|');
	if (it == first_row.end()) {
		cerr << "Start element not found!" << endl;
		assert(false); // Could not find start element
	}

	auto index = std::distance(first_row.begin(), it);
	current_position.x = index;

	vector<char> path_tracker;
	unsigned int number_of_steps = 0;

	// Iterate until end
	while (current_direction != none) {
		number_of_steps++;
		switch (current_direction) {
			case south:
				current_position.y += 1;
				current_direction = resolve_next_position(matrix, current_position, current_direction, path_tracker);
				break;
			case north:
				current_position.y -= 1;
				current_direction = resolve_next_position(matrix, current_position, current_direction, path_tracker);
				break;
			case west:
				current_position.x -= 1;
				current_direction = resolve_next_position(matrix, current_position, current_direction, path_tracker);
				break;
			case east:
				current_position.x += 1;
				current_direction = resolve_next_position(matrix, current_position, current_direction, path_tracker);
				break;
		}		
	}

	cout << "Path: ";
	for (auto c: path_tracker) {
		cout << c << " ";
	}
	cout <<  " in " << number_of_steps << " steps." << endl;


}

void test() {
	string input = read_file("19_test.txt");
	vector<string> rows = split_str_by_char(input, '\n');
	for(auto row: rows) {
		cout << row << endl;
	}
	execute_1(rows);
}

int main() {
	// test();
	string input = read_file("19_input.txt");
	vector<string> rows = split_str_by_char(input, '\n');
	execute_1(rows);
	return 0;
}
