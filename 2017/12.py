def process(programs, target_program='0'):

    connected_programs = []
    # Add the 0 program
    connected_programs.append(target_program)
    programs[target_program] = []

    # Add all programs that are directly connected with 0
    for (program_id, connections) in programs.iteritems():
        if target_program in connections:
            connected_programs.append(program_id)
            programs[program_id] = []

    # Add all the rest until no more are added
    changing = True
    while changing:
        changing = False
        for (program_id, connections) in programs.iteritems():
            for c in connections:
                if c in connected_programs:
                    connected_programs.append(program_id)
                    programs[program_id] = []
                    changing = True
                    break


    print len(connected_programs)

    # Return disconnected programs
    disconnected = {}
    for (program_id, connections) in programs.iteritems():
        if connections:
            disconnected[program_id] = connections

    return disconnected

def test():
    programs = {
        '0': ['2'],
        '1': ['1'],
        '2': ['0', '3', '4'],
        '3': ['2', '4'],
        '4': ['2', '3', '6'],
        '5': ['6'],
        '6': ['4', '5']
    }

    disconnected = process(programs)

    number_of_groups = 1

    while len(disconnected) > 0:
        new_target = disconnected.keys()[0]
        disconnected = process(programs, new_target)
        number_of_groups += 1

    print 'Number_of_groups', number_of_groups

#test()

with open('12_input.txt') as f:
    data = f.read()
    data = data.replace(' ', '')
    lines = data.split('\n')
    programs = {}
    for line in lines:
        if not line:
            continue
        program_id, program_pipes = line.split('<->')
        programs[program_id] = program_pipes.split(',')

    disconnected = process(programs)

    number_of_groups = 1

    while len(disconnected) > 0:
        new_target = disconnected.keys()[0]
        disconnected = process(programs, new_target)
        number_of_groups += 1

    print 'Number_of_groups', number_of_groups


