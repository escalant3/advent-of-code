#include <iomanip>
#include <iostream>
#include <vector>

using namespace std;

void process(vector<int> list, vector<int> input_lengths) {
	int skip_size = 0;
	int pos = 0;
	int size = list.size();
	for(auto l: input_lengths) {
		int i = pos;
		int j = pos+l-1;
		while (i<j) {
			int aux = list[i%size];
			list[i%size] = list[j%size];
			list[j%size] = aux;
			++i;
			--j;
		}
		pos += l + skip_size++;
	}

	for(auto i: list) cout << i << " ";
	cout << endl;
	cout << "Result: " << (list[0]*list[1]) << endl;
}

void process_2(vector<int> list, vector<int> input_lengths) {
	int skip_size = 0;
	int pos = 0;
	int size = list.size();
	for(int round=0; round<64; ++round) {
		for(auto l: input_lengths) {
			int i = pos;
			int j = pos+l-1;
			while (i<j) {
				int aux = list[i%size];
				list[i%size] = list[j%size];
				list[j%size] = aux;
				++i;
				--j;
			}
			pos += l + skip_size++;
		}
	}

	vector<int> dense_harsh;
	for(size_t i=0; i<list.size(); i+=16) {
		int dense_value = list[i];
		for(int j=i+1; j<i+16; ++j) {
			dense_value = dense_value ^ list[j];
		}
		dense_harsh.push_back(dense_value);
	}

	cout << "Result 2: ";
	for(auto i: dense_harsh) cout << setfill('0') << setw(2)  << hex << i;
	cout << endl;
}

void test() {
	vector<int> list = {0, 1, 2, 3, 4};
	vector<int> input_lengths = {3, 4, 1, 5};
	process(list, input_lengths);
}
int main() {
	//test();

	// Part 1
	vector<int> list;
	for(int i=0; i<256; ++i) list.push_back(i);
	vector<int> input_lengths = {14,58,0,116,179,16,1,104,2,254,167,86,255,55,122,244};
	process(list, input_lengths);

	// Part 2
	list.clear();
	for(int i=0; i<256; ++i) list.push_back(i);
	string input_string = "14,58,0,116,179,16,1,104,2,254,167,86,255,55,122,244";
	input_lengths.clear();
	for(auto c: input_string) input_lengths.push_back(c); // Converts char to int
	input_lengths.push_back(17);
	input_lengths.push_back(31);
	input_lengths.push_back(73);
	input_lengths.push_back(47);
	input_lengths.push_back(23);
	process_2(list, input_lengths);

	return 0;
}
