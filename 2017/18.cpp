#include <iostream>
#include <cctype>
#include <map>
#include <vector>

#include "utils.h"

using namespace std;

class Computer {
	private:
		map<char, long> registers;
		vector<string> instructions;
		long last_sound_played = 0;
		long program_counter = 0;
		bool sound_retrieved = false;
		long get_value(const string& operand) {
			if (isalpha(operand[0])) {
				return registers[operand[0]];
			}

			return str_to_int(operand);
		}
	public:
		void add_instruction(const string& i) {
			instructions.push_back(i);
		}
		bool is_program_complete() {
			return (program_counter >= instructions.size() || sound_retrieved);
		}
		long get_last_sound_played() {
			return last_sound_played;
		}
		void execute() {
			string i = instructions[program_counter];
			//cout << "Executing: " << i << endl;
			vector<string> instruction = split_str_by_char(i, ' ');
			string operation = instruction[0];
			string operand1 = instruction[1];
			string operand2;
			if (instruction.size() == 3) {
				operand2 = instruction[2];
			}
			if (operation == "snd") {
				last_sound_played = get_value(operand1);
			} else if (operation == "set") {
				long value = get_value(operand2);
				registers[operand1[0]] = value;
			} else if (operation == "add") {
				long value1 = get_value(operand1);
				long value2 = get_value(operand2);
				registers[operand1[0]] = value1 + value2;;
			} else if (operation == "mul") {
				long value1 = get_value(operand1);
				long value2 = get_value(operand2);
				registers[operand1[0]] = value1 * value2;;
			} else if (operation == "mod") {
				long value1 = get_value(operand1);
				long value2 = get_value(operand2);
				registers[operand1[0]] = value1 % value2;;
			} else if (operation == "rcv") {
				long value = get_value(operand1);
				if (value != 0) {
					sound_retrieved = true;
					cout << "Value sent to RCV " << value << " from operand " << operand1 << endl;
					cout << "Value recovered " << last_sound_played << endl;
				}
			} else if (operation == "jgz") {
				long value = get_value(operand1);
				long value2 = get_value(operand2);
				if (value != 0) {
					program_counter += value2;
					return;
				}
			}

			++program_counter;

		}
};

void process(vector<string> instructions) {
	Computer c;

	for (auto i: instructions) {
		if (i != "") {
			c.add_instruction(i);
		}
	}

	while (!c.is_program_complete()) {
		c.execute();
	}

	cout << "Last sound played is " << c.get_last_sound_played() << endl;
}

void test() {
	string input = read_file("18_test.txt");
	vector<string> instructions = split_str_by_char(input, '\n');
	process(instructions);
}

int main() {
	//test();

	string input = read_file("18_input.txt");
	vector<string> instructions = split_str_by_char(input, '\n');
	process(instructions);
	return 0;
}
