from dataclasses import dataclass


@dataclass
class Node2:
    value: int
    id: int


def move_value(node_list, node):
    if node.value == 0:
        return

    index = node_list.index(node)
    node = node_list.pop(index)
    if node.value > 0:
        index = (index + node.value) % len(node_list)
    elif node.value < 0:
        index = (index + node.value) % len(node_list)
    node_list.insert(index, node)


def part1():
    with open('20_input.txt') as f:
        data = f.read()
        data = map(int, data.strip().split('\n'))

    node_list = []
    zero_node = None
    for i, value in enumerate(data):
        n = Node2(id=i, value=value)
        node_list.append(n)
        if value == 0:
            zero_node = n

    original_node_list = node_list[:]
    for node in original_node_list:
        move_value(node_list, node)

    zero_node_index = node_list.index(zero_node)
    result = 0
    for position in [1000, 2000, 3000]:
        result +=  node_list[(zero_node_index + position) % len(node_list)].value
    print(f'Part 1: {result}')


def part2():
    decryption_key = 811589153
    with open('20_input.txt') as f:
        data = f.read()
        data = map(int, data.strip().split('\n'))

    node_list = []
    zero_node = None
    for i, value in enumerate(data):
        n = Node2(id=i, value=value*decryption_key)
        node_list.append(n)
        if value == 0:
            zero_node = n

    original_node_list = node_list[:]
    for step in range(10):
        for node in original_node_list:
            move_value(node_list, node)

    zero_node_index = node_list.index(zero_node)
    result = 0
    for position in [1000, 2000, 3000]:
        result +=  node_list[(zero_node_index + position) % len(node_list)].value
    print(f'Part 2: {result}')


if __name__ == '__main__':
    part1()
    part2()

