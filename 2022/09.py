#!/usr/bin/env python3

import math

def main(file_name, number_of_knots=1):
    with open(file_name) as f:
        knots = [[0, 0]]
        for i in range(number_of_knots):
            knots.append([0, 0])
        tail_visits = set()
        tail_visits.add(tuple(knots[-1]))
        for line in f.readlines():
            line = line.strip()
            if not line:
                continue

            direction, amount = line.split(' ')
            amount = int(amount)
            
            for i in range(amount):
                if direction == 'R':
                    knots[0][0] += 1
                elif direction == 'L' :
                    knots[0][0] -= 1
                elif direction == 'U':
                    knots[0][1] += 1
                elif direction == 'D':
                    knots[0][1] -= 1
                else:
                    raise ValueError(f'Invalid direction {direction}')

                for k, knot in enumerate(knots):
                    if k == 0:
                        continue

                    distance = math.sqrt(math.pow(knots[k-1][0] - knot[0], 2) + math.pow(knots[k-1][1] - knot[1], 2))
                    max_distance = 1
                    # Diagonal case
                    if knots[k-1][0] != knot[0] and knots[k-1][1] != knot[1]:
                        max_distance = 2
                    if distance > max_distance:
                        x_move = 0
                        if knots[k-1][0] > knot[0]:
                            x_move = 1
                        elif knots[k-1][0] < knot[0]:
                            x_move = -1
                        y_move = 0
                        if knots[k-1][1] > knot[1]:
                            y_move = 1
                        elif knots[k-1][1] < knot[1]:
                            y_move = -1

                        knot[0] += x_move
                        knot[1] += y_move
                        if k == len(knots) - 1:
                            tail_visits.add(tuple(knot))

    return len(tail_visits)


if __name__ == '__main__':
    r = main('09_input.txt')
    print(f'Part 1: {r}')
    r = main('09_input.txt', 9)
    print(f'Part 2: {r}')
