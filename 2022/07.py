#!/usr/bin/env python3

class FileItem:
    def __init__(self, name, size):
        self.name = name
        self.size = size

class DirItem:
    def __init__(self, name):
        self.name = name
        self.children = []

    @property
    def size(self):
        return sum([x.size for x in self.children])

    def add_child(self, child):
        self.children.append(child)
    
    def change_dir(self, name):
        for c in self.children:
            if isinstance(c, DirItem) and c.name == name:
                return c
        raise ValueError(f'Directory {name} not found in {self.name}')

def main(file_name):
    with open(file_name) as f:
        data = f.read()
    lines = data.split('\n')

    stack = []
    dir_list = []
    root = DirItem('/')
    pwd = root

    for line in lines[1:]:
        if not line or line == '$ ls':
            continue

        if line == '$ cd ..':
            pwd = stack.pop()
            continue

        if line.startswith('$ cd'):
            stack.append(pwd)
            pwd = pwd.change_dir(line[5:])
            continue

        if line.startswith('dir'):
            dir_name = line[4:]
            d = DirItem(dir_name)
            pwd.add_child(d)
            dir_list.append(d)
        else:
            size, name = line.split(' ')
            pwd.add_child(FileItem(name, int(size)))

    total_size = root.size # Recursion calculates sizes

    total_disk_space = 70000000
    required_free_space = 30000000
    used_space = total_size

    free_space = total_disk_space - used_space
    space_to_be_freed = required_free_space - free_space 

    part1 = 0
    part2 = total_disk_space
    for d in dir_list:

        # Part 1
        if d.size < 100000:
            part1 += d.size

        # Part 2
        if d.size > space_to_be_freed and d.size < part2:
            part2 = d.size 

    return part1, part2

if __name__ == '__main__':
    r1, r2 = main('07_input.txt')
    print(f'Part 1: {r1}\nPart 2: {r2}')
