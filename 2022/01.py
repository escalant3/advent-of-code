#!/usr/bin/env python3

def get_calories_list(input_file):
    with open(input_file) as f:
        elf_items = []
        sums = []

        for line in f.readlines():
            line = line.strip()
            if line:
                elf_items.append(int(line))
                continue

            sums.append(sum(elf_items))
            elf_items = []

        if elf_items:
            sums.append(sum(elf_items))

    return sums

def part1(input_file):
    calories_list = get_calories_list(input_file)

    max_sum = 0
    for i,sum_value in enumerate(calories_list):
        if sum_value > max_sum:
            max_sum = sum_value
    return max_sum

def part2(input_file):
    calories_list = get_calories_list(input_file)
    calories_list = sorted(calories_list)
    calories_list.reverse()
    return sum(calories_list[:3])


if __name__ == '__main__':
    #r = part1('01_test.txt')
    r = part1('01_input.txt')
    print(f'Max calories carried by an elf: {r}')
    r = part2('01_input.txt')
    print(f'Calories carried by top-3 elves: {r}')
