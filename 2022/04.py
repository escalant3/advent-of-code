#!/usr/bin/env python3

def main(file_name):
    with open(file_name) as f:
        overlap = 0
        full_overlap_count = 0
        for line in f.readlines():
            line = line.strip()
            if not line:
                continue
            elves = line.split(',')
            start1, end1 = list(map(int, elves[0].split('-')))
            start2, end2 = list(map(int, elves[1].split('-')))
            size1 = end1 - start1
            size2 = end2 - start2

            # Part 1
            if size1 > size2 and start1 <= start2 and end1 >= end2:
                full_overlap_count += 1
            elif start2 <= start1 and end2 >= end1:
                full_overlap_count += 1

            # Part 2
            if (start1 <= start2 and end1 >= start2) or \
                (start2 <= start1 and end2 >= start1):
                overlap += 1

    return full_overlap_count, overlap

if __name__ == '__main__':
    r1, r2 = main('04_input.txt')
    print(f'Part 1: {r1}')
    print(f'Part 2: {r2}')
