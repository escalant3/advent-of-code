#!/usr/bin/env python3

class Monkey:
    def __init__(self, data, worry_control):
        self.items = [int(x) for x in data[0].split(': ')[1].split(',')]
        self.operation = data[1].split('new = ')[1].split(' ')
        if self.operation[2] != 'old':
            self.operation[2] = int(self.operation[2])
        self.test = int(data[2].split('divisible by ')[1])
        self.true_destination = int(data[3].split('monkey ')[1])
        self.false_destination = int(data[4].split('monkey ')[1])
        self.inspections = 0
        self.worry_control = worry_control

    def evaluate(self):
        result = []
        for item in self.items:
            self.inspections += 1
            operand = self.operation[2]
            if operand == 'old':
                operand = item
            if self.operation[1] == '+':
                item += operand
            elif self.operation[1] == '*':
                item *= operand
            else:
                raise ValueError(f'bad operation {self.operation}')
            if self.worry_control:
                item = item // 3

            destination = self.true_destination if item % self.test == 0 else self.false_destination

            result.append({
                'destination': destination,
                'value': item,
            })

        self.items = []
        return result

    def add_item(self, value):
        self.items.append(value)

def main(file_name, worry_control=True):
    with open(file_name) as f:
        data = f.read().split('\n')
    
    monkeys = []
    for i, line in enumerate(data):
        line = line.strip()
        if not line.startswith('Monkey'):
            continue
        monkeys.append(Monkey(data[i+1:i+6], worry_control=worry_control))

    rounds = 20 if worry_control else 10000
    max_worry = 1
    for monkey in monkeys:
        max_worry *= monkey.test
    for i in range(rounds):
        for monkey in monkeys:
            results = monkey.evaluate()
            for r in results:
                v = r['value'] % max_worry
                monkeys[r['destination']].add_item(v)

    inspections = sorted([x.inspections for x in monkeys])
    inspections.reverse()
    return inspections[0] * inspections[1]

if __name__ == '__main__':
    r = main('11_input.txt')
    print(f'Part 1: {r}')
    r = main('11_input.txt', False)
    print(f'Part 2: {r}')

