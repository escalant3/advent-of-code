from collections import defaultdict

class Cube:
    def __init__(self, x, y, z):
        self.x, self.y, self.z = x, y, z
        self.sides_exposed = 6
    
    def __repr__(self):
        return (f'({self.x}, {self.y}, {self.z})')

    @classmethod
    def from_comma_string(Cube, cub_str):
        x, y, z = map(int, cub_str.split(','))
        return Cube(x,y,z)

    def is_adjacent(self, c):
        #return c.x == self.x or c.y == self.y or c.z == self.z
        return abs(c.x-self.x) == 1 and c.y == self.y and c.z == self.z or \
            c.x == self.x and abs(c.y - self.y) == 1 and c.z == self.z or \
            c.x == self.x and c.y == self.y and abs(c.z - self.z) == 1

    def get_adjacent_points(self, minX, minY, minZ, maxX, maxY, maxZ):
        x = self.x
        y = self.y
        z = self.z
        return [
            [(i, y , z) for i in range(x-1, minX-1, -1)],
            [(i, y , z) for i in range(x+1, maxX + 1)],
            [(x, i , z) for i in range(y-1, minY-1, -1)],
            [(x, i , z) for i in range(y+1, maxY + 1)],
            [(x, y , i) for i in range(z-1, minZ-1, -1)],
            [(x, y , i) for i in range(z+1, maxZ + 1)],
        ]


def part1(cubes):
    for i in range(len(cubes)-1):
        for j in range(i+1, len(cubes)):
            if cubes[i].is_adjacent(cubes[j]):
                cubes[i].sides_exposed -= 1
                cubes[j].sides_exposed -= 1

    return sum([c.sides_exposed for c in cubes])


def part2(cubes):
    minX = 1000
    minY = 1000
    minZ = 1000
    maxX = 0
    maxY = 0
    maxZ = 0
    space = {}
    for c in cubes:
        space[(c.x,c.y,c.z)] = 1
        if c.x < minX: minX = c.x
        if c.y < minY: minY = c.y
        if c.z < minZ: minZ = c.z
        if c.x > maxX: maxX = c.x
        if c.y > maxY: maxY = c.y
        if c.z > maxZ: maxZ = c.z
    
    air_points = set()
    for x,y,z in space:
        adjacent_points = Cube(x,y,z).get_adjacent_points(minX, minY, minZ, maxX, maxY, maxZ)

        # Discard external facing sides
        any_adjacent_occupied = lambda points: sum([space.get(coords, 0) for coords in points])
        adjacent_points = [ap for ap in adjacent_points if any_adjacent_occupied(ap) != 0]
        
        # Discard unlocked cubes
        flat = [p for row in adjacent_points for p in row if space.get((p[0], p[1], p[2]), 0) == 0]
        for p in flat:
            adjacent_points = Cube(p[0],p[1],p[2]).get_adjacent_points(minX, minY, minZ, maxX, maxY, maxZ)
            for i in range(len(adjacent_points)):
                adjacent_points[i] = [p for p in adjacent_points[i] if space.get((p[0], p[1], p[2])) == 1]
            surrounded_sides = len([p for p in adjacent_points if p])

            if surrounded_sides == 6:
                air_points.add(p)

    # Filter cubes with a path to the exterior
    filtered_air_points = set()
    for p in air_points:
        space[(p[0], p[1], p[2])] = 2
    for p in air_points:
        if space.get((p[0]-1, p[1], p[2]), 0) == 0 or space.get((p[0]+1, p[1], p[2]), 0) == 0 or \
            space.get((p[0], p[1]-1, p[2]), 0) == 0 or space.get((p[0], p[1]+1, p[2])) == 0 or \
            space.get((p[0], p[1], p[2]-1), 0) == 0 or space.get((p[0], p[1], p[2]+1), 0) == 0:
                space[(p[0], p[1], p[2])] = 0
        else:
            filtered_air_points.add(p)
    return filtered_air_points


def main(data_input):
    cubes = []
    for line in data_input.split('\n'):
        line = line.strip()
        if not line:
            continue
        cubes.append(Cube.from_comma_string(line))

    surface_area = part1(cubes)
    print(f'Part 1: {surface_area}')

    air_cubes = part2(cubes)
    air_cubes_2 = [Cube(p[0],p[1],p[2]) for p in list(air_cubes)]
    air_surface = part1(air_cubes_2)
    print(f'Part 2: {surface_area-air_surface}')
    

if __name__ == '__main__':
    data_input = '''
        2,2,2
        1,2,2
        3,2,2
        2,1,2
        2,3,2
        2,2,1
        2,2,3
        2,2,4
        2,2,6
        1,2,5
        3,2,5
        2,1,5
        2,3,5
'''

    with open('18_input.txt') as f:
        data_input = f.read()

    main(data_input)
