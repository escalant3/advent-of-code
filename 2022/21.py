def part1():
    with open('21_input.txt') as f:
        data = [m for m in f.read().split('\n') if m]
    
    solved = {}
    todo = {}
    for line in data:
        name, task = line.split(': ')
        if task.isdigit():
            solved[name] = int(task)
        else:
            todo[name] = task

    while len(todo):
        for key, value in todo.items():
            if '+' in value:
                op1, op2 = value.split(' + ')
                if op1 in solved and op2 in solved:
                    solved[key] = solved[op1] + solved[op2]
                    todo.pop(key)
                    break

            if '-' in value:
                op1, op2 = value.split(' - ')
                if op1 in solved and op2 in solved:
                    solved[key] = solved[op1] - solved[op2]
                    todo.pop(key)
                    break
                    
            if '*' in value:
                op1, op2 = value.split(' * ')
                if op1 in solved and op2 in solved:
                    solved[key] = solved[op1] * solved[op2]
                    todo.pop(key)
                    break
        
            if '/' in value:
                op1, op2 = value.split(' / ')
                if op1 in solved and op2 in solved:
                    solved[key] = int(solved[op1] / solved[op2])
                    todo.pop(key)
                    break
        

    print('Part 1:', solved['root'])


def replace_root_vars(line, r1, r2):
    line = line.replace(r1, 'YYYY')
    line = line.replace(r2, 'YYYY')
    if line.startswith('humn:'):
        line = 'humn: XXXX'
    return line

def part2():
    with open('21_input.txt') as f:
        data = [m for m in f.read().split('\n') if m]

    for line in data:
        if line.startswith('root:'):
            line = line.split(': ')[1]
            r1, r2 = line.split(' + ')
            break

    data = [replace_root_vars(line, r1, r2) for line in data if not line.startswith('root:')]


    expanded_data = []
    for line in data:
        if '+' in line:
            res, operation = line.split(': ')
            op1, op2 = operation.split(' + ')

            expanded_data.append(line)
            expanded_data.append(f'{op1}: {res} - {op2}')
            expanded_data.append(f'{op2}: {res} - {op1}')

        elif '-' in line:
            res, operation = line.split(': ')
            op1, op2 = operation.split(' - ')

            expanded_data.append(line)
            expanded_data.append(f'{op1}: {res} + {op2}')
            expanded_data.append(f'{op2}: {op1} - {res}')
    
        elif '*' in line:
            res, operation = line.split(': ')
            op1, op2 = operation.split(' * ')

            expanded_data.append(line)
            expanded_data.append(f'{op1}: {res} / {op2}')
            expanded_data.append(f'{op2}: {res} / {op1}')

        elif '/' in line:
            res, operation = line.split(': ')
            op1, op2 = operation.split(' / ')

            expanded_data.append(line)
            expanded_data.append(f'{op1}: {res} * {op2}')
            expanded_data.append(f'{op2}: {op1} / {res}')

        else:
            expanded_data.append(line)

    solved = {}
    todos = []
    for line in expanded_data:
        name, task = line.split(': ')

        if task.isdigit():
            solved[name] = int(task)
        else:
            todos.append(f'{name}: {task}')

    while len(todos):
        for todo in todos:
            key, value = todo.split(': ')

            if '+' in value:
                op1, op2 = value.split(' + ')
                if op1 in solved and op2 in solved:
                    solved[key] = solved[op1] + solved[op2]
                    todos = [t for t in todos if not t.startswith(key)]
                    break

            if '-' in value:
                op1, op2 = value.split(' - ')
                if op1 in solved and op2 in solved:
                    solved[key] = solved[op1] - solved[op2]
                    todos = [t for t in todos if not t.startswith(key)]
                    break
                    
            if '*' in value:
                op1, op2 = value.split(' * ')
                if op1 in solved and op2 in solved:
                    solved[key] = solved[op1] * solved[op2]
                    todos = [t for t in todos if not t.startswith(key)]
                    break
        
            if '/' in value:
                op1, op2 = value.split(' / ')
                if op1 in solved and op2 in solved:
                    solved[key] = int(solved[op1] / solved[op2])
                    todos = [t for t in todos if not t.startswith(key)]
                    break

    print('Part 2', solved['humn'])


if __name__ == '__main__':
    part1()
    part2()
