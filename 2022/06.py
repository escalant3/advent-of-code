#!/usr/bin/env python3

def len_to_packet_start(char_sequence, unique_len=4):
    for i in range(len(char_sequence)-unique_len):
        section = char_sequence[i:i+unique_len]
        if sum([section.count(x) for x in section]) == unique_len:
            return i + unique_len
    raise ValueError('start sequence not found')

def len_to_message_start(char_sequence):
    return len_to_packet_start(char_sequence, 14)

def test():
    assert len_to_packet_start('mjqjpqmgbljsphdztnvjfqwrcgsmlb') == 7
    assert len_to_packet_start('bvwbjplbgvbhsrlpgdmjqwftvncz') == 5
    assert len_to_packet_start('nppdvjthqldpwncqszvftbrmjlhg') == 6
    assert len_to_packet_start('nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg') == 10
    assert len_to_packet_start('zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw') == 11

    assert len_to_message_start('mjqjpqmgbljsphdztnvjfqwrcgsmlb') == 19
    assert len_to_message_start('bvwbjplbgvbhsrlpgdmjqwftvncz') == 23
    assert len_to_message_start('nppdvjthqldpwncqszvftbrmjlhg') == 23
    assert len_to_message_start('nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg') == 29
    assert len_to_message_start('zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw') == 26

if __name__ == '__main__':
    test()
    with open('06_input.txt') as f:
        data = f.read()
        data = data.strip()
    print(f'Part 1: {len_to_packet_start(data)}')
    print(f'Part 2: {len_to_message_start(data)}')
