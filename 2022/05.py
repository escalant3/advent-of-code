#!/usr/bin/env python3

import re

def main(input_file):
    move_re = re.compile(r'move (\d+) from (\d) to (\d)')

    with open(input_file) as f:
        stacks = []
        crates_read = False
        lines = f.read().split('\n')

    for line in lines:
        if not line.strip():
            continue

        if not stacks:
            stacks = []
            stacks_2 = []
            num_items = len(line)//4 + 1
            for i in range(num_items):
                stacks.append([])
                stacks_2.append([])

        if not crates_read:
            if '[' not in line:
                crates_read = True
                continue
            num_items = len(line)//4+1
            for i in range(num_items):
                crate = line[i*4:(i+1)*4].strip()
                if crate:
                    stacks[i].insert(0, crate[1]) # We only add the character 
                    stacks_2[i].insert(0, crate[1])
            continue

        quantity, source, destination = move_re.search(line).groups()
        for i in range(int(quantity)):
            crate = stacks[int(source)-1].pop()
            stacks[int(destination)-1].append(crate)

        stacks_2[int(destination)-1].extend(stacks_2[int(source)-1][-int(quantity):])
        stacks_2[int(source)-1] = stacks_2[int(source)-1][0:-int(quantity)]

    return ''.join([x.pop() for x in stacks]), ''.join([x.pop() for x in stacks_2])

if __name__ == '__main__':
    r1, r2 = main('05_input.txt')
    print(f'Part 1: {r1}\nPart 2: {r2}')
