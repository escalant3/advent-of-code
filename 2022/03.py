#!/usr/bin/env python

import math

def get_priority(item):
    if item >= 'a' and item <= 'z':
        return ord(item) - ord('a') + 1
    elif item >= 'A' and item <= 'Z':
        return ord(item) - ord('A') + 27
    else:
        raise ValueError(item)

def part1(input_file):
    rucksack_priorities = []

    with open(input_file) as f:
        for line in f.readlines():
            line = line.strip()
            if not line:
                continue
            half  = len(line)/2
            first = line[:math.ceil(half)]
            second = line[-math.floor(half):]
            for i, item in enumerate(first):
                if item in second:
                    priority = get_priority(item)
                    rucksack_priorities.append(priority)
                    break
    return sum(rucksack_priorities)

def part2(input_file):
    with open(input_file) as f:
        data = [line.strip() for line in f.readlines() if line.strip()]
    rucksack_priorities = []
    for i in range(0, len(data), 3):
        group = data[i:i+3]
        for item in group[0]:
            if item in group[1] and item in group[2]:
                rucksack_priorities.append(get_priority(item))
                break
    return sum(rucksack_priorities)


if __name__ == '__main__':
    r = part1('03_input.txt')
    print(f'Part 1: {r}')
    r = part2('03_input.txt')
    print(f'Part 2: {r}')

