#!/usr/bin/env python3

def load_data(file_name):
    points = {}
    with open(file_name) as f:
        for line in f.readlines():
            line = line.strip()
            if not line: continue
            coords = line.split(' -> ')
            for i, coord in enumerate(coords):
                if i == 0: continue
                ax, ay = map(int, coords[i-1].split(','))
                bx, by = map(int, coord.split(','))
                if ax != bx and ay != by:
                    raise ValueError('not a straight line')
                xstart, xend = sorted([ax, bx])
                ystart, yend = sorted([ay, by])
                for x in range(xstart, xend+1):
                    for y in range(ystart, yend+1):
                        points[(x,y)] = 1
    return points

def simulate_sand(points, abyss):
    sand_position = (500, 0)
    while True:
        if sand_position[1] >= abyss: return True
        current_position = sand_position
        sand_position = (current_position[0], current_position[1]+1)
        if sand_position not in points: continue
        sand_position = (current_position[0]-1, current_position[1]+1) 
        if sand_position not in points: continue
        sand_position = (current_position[0]+1, current_position[1]+1) 
        if sand_position not in points: continue
        points[current_position] = 1
        return False

    
def part1(points):
    abyss = max([p[1] for p in points.keys()])
    rounds = 0
    while True:
        into_abyss = simulate_sand(points, abyss)
        if into_abyss: return rounds
        rounds += 1


def simulate_sand_with_floor(points, floor):
    sand_position = (500, 0)
    while True:
        current_position = sand_position
        if sand_position[1] == floor-1:
            points[current_position] = 1
            return False

        sand_position = (current_position[0], current_position[1]+1)
        if sand_position not in points: continue
        sand_position = (current_position[0]-1, current_position[1]+1) 
        if sand_position not in points: continue
        sand_position = (current_position[0]+1, current_position[1]+1) 
        if sand_position not in points: continue

        if current_position == (500, 0):
            return True

        points[current_position] = 1
        return False

def part2(points):
    floor = max([p[1] for p in points.keys()]) + 2
    rounds = 0
    while True:
        into_spawn = simulate_sand_with_floor(points, floor)
        if into_spawn: return rounds + 1
        rounds += 1


if __name__ == '__main__':
    points = load_data('14_input.txt')
    rounds = part1(points)
    print(f'Part 1: {rounds}')
    points = load_data('14_input.txt')
    rounds = part2(points)
    print(f'Part 2: {rounds}')
