#!/usr/bin/env python3

def main(file_name):
    data = []
    with open(file_name) as f:
        for line in f.readlines():
            line = line.strip()
            if not line:
                continue
            data.append(list(line))
    width = len(data[0])
    height = len(data)
    visible_trees = 0
    max_scenic_score = 0
    for i in range(height):
        if i == 0 or i == height - 1:
            visible_trees += width
            continue

        for j in range(width):
            if j == 0 or j == width - 1:
                visible_trees += 1
                continue

            scenic_score = 0
            vertical = [x[j] for x in data]
            horizontal = data[i]
            tree_value = data[i][j]

            north = [x < tree_value for x in vertical[:i]]
            south = [x < tree_value for x in vertical[i+1:]]
            west = [x < tree_value for x in horizontal[:j]]
            east = [x < tree_value for x in horizontal[j+1:]]
            north.reverse()
            west.reverse()
            scenic_score = [0] * 4
            for d, direction in enumerate([north, south, west, east]):
                for x in direction:
                    scenic_score[d] += 1
                    if not x:
                        break

            if all(north) or all(south) or all(west) or all(east):
                visible_trees += 1

            scenic_score = scenic_score[0] * scenic_score[1] * scenic_score[2] * scenic_score[3]
            if scenic_score > max_scenic_score:
                max_scenic_score = scenic_score

    return visible_trees, max_scenic_score


if __name__ == '__main__':
    r1, r2 = main('08_input.txt')
    print(f'Part 1: {r1}\nPart 2: {r2}')

