#!/usr/bin/env python3

class Machine:
    def __init__(self):
        self.x = 1
        self.cycle = 0
        self.beam_position = 0
        self.marks = (20, 60, 100, 140, 180, 220)
        self.signal_strength = []

    def increase_cycles(self):
        self.cycle += 1
        if self.cycle in self.marks:
            self.signal_strength.append(self.cycle * self.x)
        self.draw_pixel()

    def execute_noop(self):
        self.increase_cycles()

    def execute_addx(self, value):
        self.increase_cycles()
        self.increase_cycles()
        self.x += value

    def draw_pixel(self):
        if self.beam_position == 40:
            print('')
            self.beam_position = 0

        if self.beam_position >= (self.x - 1) and \
            self.beam_position <= (self.x + 1):
            print('#', end='')
        else:
            print('.', end='')

        self.beam_position += 1
        

def main(file_name):
    with open(file_name) as f:
        m = Machine()
        for line in f.readlines():
            line = line.strip()
            if line == 'noop':
                m.execute_noop()
            elif line.startswith('addx'):
                m.execute_addx(int(line.split(' ')[1]))
            elif not line:
                continue
            else:
                raise ValueError(line)

    return sum(m.signal_strength)


if __name__ == '__main__':
    r = main('10_input.txt')
    print(f'Part 1: {r}')
