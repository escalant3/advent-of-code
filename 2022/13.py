#!/usr/bin/env python3

import json
from functools import cmp_to_key

def load_data(file_name):
    pairs = []
    with open(file_name) as f:
        pair = []
        for line in f.readlines():
            line = line.strip()
            if not line:
                pairs.append(pair)
                pair = []
                continue
            pair.append(json.loads(line))
    return pairs


def compare_pair(a, b):
    type_a = type(a)
    type_b = type(b)
    if type_a == int and type_b == int:
        if a != b: return a < b
        else: return None
    elif type_a == list and type_b == int:
        return compare_pair(a, [b])
    elif type_a == int and type_b == list:
        return compare_pair([a], b)
    elif type_a == list and type_b == list:
        for i, item_a in enumerate(a):
            if i >= len(b): return False
            item_b = b[i]
            r = compare_pair(item_a, item_b)
            if r is not None:
                return r
        if len(a) == len(b):
            return None
        return True
    else:
        raise ValueError(f'Unexpected input: {a} {b}')


def compare_pairs(data):
    result = []
    for pair in data:
        a, b = pair
        result.append(compare_pair(a, b))
    return result 


def part1(file_name):
    data = load_data(file_name)
    result = compare_pairs(data)
    return sum([(i+1) for i, x in enumerate(result) if x])

def sort_pair(a, b):
    r = compare_pair(a, b)
    return -1 if r else 1

def part2(file_name):
    marker1 = [[2]]
    marker2 = [[6]] 
    data = load_data(file_name)
    flat_data = []
    for pair in data:
        flat_data.extend(pair)
    flat_data.extend([marker1, marker2])
    sorted_data = sorted(flat_data, key=cmp_to_key(sort_pair))
    m1 = sorted_data.index(marker1) + 1
    m2 = sorted_data.index(marker2) + 1
    return m1 * m2

if __name__ == '__main__':
    r = part1('13_input.txt')
    print(f'Part 1: {r}')
    r = part2('13_input.txt')
    print(f'Part 2: {r}')

            



