#!/usr/bin/env python3

MAX_COST = 9999

class Tile:
    def __init__(self, tile_id, name):
        self.tile_id = tile_id
        self.name = name
        self.neighbors = set()
        self.cost = MAX_COST

    def add_neighbor(self, tile):
        if ord(tile.name) - ord(self.name) <= 1:
            self.neighbors.add(tile)

    def __str__(self):
        neighbors = ','.join([n.name for n in self.neighbors])
        return f'({self.tile_id}) {self.name} -> {neighbors}'# (visited: {self.visited})'
    
    __repr__ = __str__


def load_tiles(file_name):
    tile_list = []
    width = None
    height = None
    start = None
    end = None
    with open(file_name) as f:
        id_counter = 0
        for line in f.readlines():
            line = line.strip()
            if not line:
                continue
            for c in line:
                if c == 'S':
                    start = id_counter
                    c = 'a'
                if c == 'E':
                    end = id_counter
                    c = 'z'
                tile_list.append(Tile(id_counter, c))
                id_counter += 1
            if width is None:
                width = id_counter
        height = id_counter // width

    for ti, tile in enumerate(tile_list):
        left = tile.tile_id -1
        up = tile.tile_id - width
        down = tile.tile_id + width
        right = tile.tile_id + 1
        if left % width == width - 1:
            left = -1
        if right % width == 0:
            right = -1
        for i in [right, down, left, up]:
            if i < 0 or i >= len(tile_list):
                continue
            tile.add_neighbor(tile_list[i])

    return tile_list, start, end

def shortest_path(tile_list, start, end):
    visit_list = set()
    visit_list.add(tile_list[start])
    cost = 0
    while len(visit_list) > 0:
        next_visit_list = set()
        for tile in visit_list:
            if tile.cost == MAX_COST:
                tile.cost = cost
            for n in tile.neighbors:
                if n.cost == MAX_COST:
                    next_visit_list.add(n)
        cost += 1
        visit_list = next_visit_list

    return tile_list[end].cost

def part1(file_name):
    tile_list, start, end = load_tiles(file_name)
    return shortest_path(tile_list, start, end)

def part2(file_name, min_path):
    tile_list, start, end = load_tiles(file_name)
    for i, tile in enumerate(tile_list):
        if tile.name == 'a':
            cost = shortest_path(tile_list, i, end)
            if cost < min_path:
                min_path = cost
        for tile in tile_list:
            tile.cost = MAX_COST
    return min_path
    

if __name__ == '__main__':
    r = part1('12_input.txt')
    print(f'Part 1: {r}')
    r = part2('12_input.txt', r)
    print(f'Part 2: {r}')
