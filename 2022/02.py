#!/usr/bin/env python3

count_by_play = {
    'X': 1,
    'Y': 2,
    'Z': 3,
}

count_by_rule = {
    'A': {
        'X': 3,
        'Y': 6,
        'Z': 0,
    },
    'B': {
        'X': 0,
        'Y': 3,
        'Z': 6,
    },
    'C': {
        'X': 6,
        'Y': 0,
        'Z': 3
    },
}

count_by_end = {
    'X': {
        'A': 'Z',
        'B': 'X',
        'C': 'Y',
    },
    'Y': {
        'A': 'X',
        'B': 'Y',
        'C': 'Z',
    },
    'Z': {
        'A': 'Y',
        'B': 'Z',
        'C': 'X',
    }
}

def main(file_name):
    with open(file_name) as f:
        data = f.read()
        score_part1 = 0
        score_part2 = 0
        for line in data.split('\n'):
            line = line.strip()
            if not line:
                continue
            opponent, me = line.split(' ')
            score_part1 += count_by_rule[opponent][me]
            score_part1 += count_by_play[me]

            end = me
            me = count_by_end[end][opponent]
            score_part2 += count_by_rule[opponent][me]
            score_part2 += count_by_play[me]
    return score_part1, score_part2


if __name__ == '__main__':
    r1, r2 = main('02_input.txt')
    print(f'Part 1: {r1}\nPart 2: {r2}')
