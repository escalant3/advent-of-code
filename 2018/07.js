'use strict';

const Fs = require('fs');

const run = (fileName, workers=1, delay=0) => {
    const dependencyList = {};
    const data = Fs.readFileSync(fileName, 'utf-8').trim().split('\n').map(line => {
        const [_, dependency, step] = line.match(/Step (\w) must be finished before step (\w) can begin/);
        if (!dependencyList[step]) {
            dependencyList[step] = [];
        }
        if (!dependencyList[dependency]) {
            dependencyList[dependency] = [];
        }
        dependencyList[step].push(dependency);
    });

    let result = '';
    let seconds = -1;

    let workersLoad = Array(workers).fill(-1);
    let activeJobs = Array(workers).fill(null);

    while (Object.keys(dependencyList).length > 0 || workersLoad.some(x => x>0)) {

        seconds++;
        console.log(`Tic ${seconds}. Workers load is ${workersLoad} on ${activeJobs}`);

        // There are not workers available to pick new tasks
        if (workersLoad.every(x => x>0)) {
            workersLoad = workersLoad.map(x => x-1);
            continue;
        }

        workersLoad = workersLoad.map((load, i) => {
            if (load < 1) {

                // Mark job finished
                const finishedJob = activeJobs[i];
                if (finishedJob) {
                    result += finishedJob;
                    delete dependencyList[finishedJob];
                    for(let step in dependencyList) {
                        dependencyList[step] = dependencyList[step].filter(x => x !== finishedJob);
                    }
                    activeJobs[i] = null;
                }

                // Grab another one if available
                const readySteps = Object.keys(dependencyList).filter(x => dependencyList[x].length === 0 && !activeJobs.includes(x));
                readySteps.sort();

                if (readySteps.length > 0) {
                    const currentStep = readySteps[0];
                    readySteps.splice(0, 1);
                    activeJobs[i] = currentStep;
                    console.log(`Worker ${i} grabs job ${currentStep} at ${seconds}`);
                    return currentStep.charCodeAt() + delay - 64 - 1;
                }

                return 0;
            }

            return load-1;
        });
    }

    return { result, seconds };
};

console.log(`Part 1: ${run('07input.txt').result}`);
console.log(`Part 2: ${run('07input.txt', 5, 60).seconds}`);

/*
const Assert = require('assert');
Assert.strictEqual(run('07test.txt').result, 'CABDFE');
Assert.strictEqual(run('07test.txt', 2, 0).result, 'CABFDE');
Assert.strictEqual(run('07test.txt', 2, 0).seconds, 15);
*/
