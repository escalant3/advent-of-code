'use strict';

const Assert = require('assert');
const Fs = require('fs');

const test = ['abcdef', 'bababc', 'abbcde', 'abcccd', 'aabcdd', 'abcdee', 'ababab'];

const doChecksum = (listOfIDs) => {
    let exactlyTwice = 0;
    let exactlyThrice = 0;

    listOfIDs.forEach(item => {
        let hasExactlyTwice = false;
        let hasExactlyThrice = false;

        const sortedItem = item.split('').sort().join('');

        let itemBeingChecked = sortedItem[0];
        let twiceCounter = 1;
        let thriceCounter = 1;
        for (let i=1; i<sortedItem.length; i++) {
            if (sortedItem[i] === itemBeingChecked) {
                twiceCounter++;
                thriceCounter++;

                if (i !== sortedItem.length-1) {
                    continue;
                }
            }

            if (twiceCounter === 2) { hasExactlyTwice = true };
            if (thriceCounter === 3) { hasExactlyThrice = true };

            itemBeingChecked = sortedItem[i];
            twiceCounter = 1;
            thriceCounter = 1;
        }

        if (hasExactlyTwice) { exactlyTwice++; }
        if (hasExactlyThrice) { exactlyThrice++; }
    });

    console.log(`${exactlyTwice} * ${exactlyThrice} = ${exactlyTwice*exactlyThrice}`);
};

//doChecksum(test);


const data = Fs.readFileSync('input02.txt', 'utf-8')
    .trim()
    .split('\n');

doChecksum(data);

// Part 2
const distanceIsOne = (w1, w2) => {
    Assert.equal(w1.length, w2.length);
    let distance = 0;
    for (let i=0; i<w1.length; i++) {
        if (w1[i] !== w2[i]) distance++;
        if (distance > 1) {
            return false;
        }
    }

    return distance === 1;
};

data.sort();

outer:
for (let i=0; i<data.length-1; i++) {
    for (let j=i+1; j<data.length; j++) {
        if (distanceIsOne(data[i], data[j])) {
            console.log(`Only one letter differs between ${data[i]} and ${data[j]}. Key is:`);
            console.log(data[i].split('').filter((letter, idx) => letter === data[j][idx]).join(''));
            break outer;
        }
    }
}

