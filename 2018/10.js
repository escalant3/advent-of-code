'use strict';

const Fs = require('fs');

const lineToPoint = line => {
    const [x, y, vx, vy] = line.match(/(-?\d+)/g).map(Number);
    return { x, y, vx, vy };
};

const findEdgesAndArea = points => {
    const edges = points.reduce((acc, point) => {
        return {
            minX: Math.min(acc.minX, point.x),
            maxX: Math.max(acc.maxX, point.x),
            minY: Math.min(acc.minY, point.y),
            maxY: Math.max(acc.maxY, point.y)
        };
    }, { minX: Infinity, maxX: -Infinity, minY: Infinity, maxY: -Infinity });

    const { minX, maxX, minY, maxY } = edges;
    edges.area =  Math.abs(maxX-minX) * Math.abs(maxY-minY);
    return edges;
};

const draw = points => {
    const { minX, maxX, minY, maxY } = findEdgesAndArea(points);
    const grid = Array(maxY-minY+1);
    for(let j=0; j<grid.length; j++) {
        grid[j] = new Array(maxX-minX+1).fill(' ');
    }
    points.forEach(point => {
        grid[point.y - minY][point.x - minX] = '#';
    });

    grid.forEach(row => console.log(row.join('')));
};

let points = Fs.readFileSync('input10.txt', 'utf-8').trim().split('\n').map(lineToPoint);

for(let i=0; i<Infinity; i++) {
    const nextPoints = points.map(point => {
        return {
            x: point.x + point.vx,
            y: point.y + point.vy,
            vx: point.vx,
            vy: point.vy
        };
    });

    const { area: currentArea } = findEdgesAndArea(points)
    const { area: nextArea } = findEdgesAndArea(nextPoints);

    if (currentArea < nextArea) {
        console.log(`Tic: ${i}`);
        draw(points);
        break;
    }

    points = nextPoints;
}
