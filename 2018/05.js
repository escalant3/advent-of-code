'use strict';

const Fs = require('fs');

const alphabetSize = 26;
const regexString = [...Array(alphabetSize).keys()].map(i => {
    const upper = String.fromCharCode(65 + i);
    const lower = String.fromCharCode(65 + i + alphabetSize + 6);
    return `${upper}${lower}|${lower}${upper}`;
}).join('|');
const regex = new RegExp(regexString);

const reactPolymer = data => {
    let currentLength = data.length;
    while (true) {
        data = data.replace(regex, '');
        if (data.length === currentLength) {
            return currentLength;
        }
        currentLength = data.length;
    }
};

const part1 = fileName => {
    let data = Fs.readFileSync(fileName, 'utf-8').trim();
    return reactPolymer(data);
};

const part2 = fileName => {
    let data = Fs.readFileSync(fileName, 'utf-8').trim();
    const alphabet = [...Array(alphabetSize).keys()].map(i => String.fromCharCode(65 + i));
    let bestLength = Infinity;
    alphabet.forEach(letter => {
        const modifiedData = data.replace(new RegExp(letter, 'gi'), '');
        const length = reactPolymer(modifiedData);
        if (length < bestLength) {
            bestLength = length;
        }
    });
    return bestLength;
};

console.log(`Part 1: ${part1('input05.txt')}`);
console.log(`Part 2: ${part2('input05.txt')}`);

const Assert = require('assert');
Assert.strictEqual(part1('test05.txt'), 10);
Assert.strictEqual(part2('test05.txt'), 4);
