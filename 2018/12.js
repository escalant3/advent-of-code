'use strict';

const Fs = require('fs');

const parseInput = fileName => {
    const data = Fs.readFileSync(fileName, 'utf-8').trim().split('\n\n');
    const initialState = data[0].split(': ')[1].trim();
    const rules = data[1].split('\n').reduce((rules, rule) => {
        const [input, output] = rule.split(' => ');
        rules[input] = output;
        return rules;
    }, {});
    return { initialState, rules };
};

const run = generations => {
    const { initialState, rules } = parseInput('input12.txt');
    const leftPadding = Array(10).fill('.').join('');
    const rightPadding = '....'; //Array(5).fill('.').join('');
    let state = `${leftPadding}${initialState}`;

    const log = [];

    for(let i=1; i<=generations; i++) {
        const lastPlant = state.lastIndexOf('#');
        if (lastPlant >= state.length - 3) {
            state = `${state}.....`;
        }

        state = state.split('')
            .map((x, i) => rules[state.slice(i-2, i+3)] || '.')
            .join('');
        const firstPlant = state.indexOf('#');
        const aux = state.slice(firstPlant, lastPlant);
        if (log.includes(aux)) {

            let sum = 0;
            for(let i=0; i< state.length; i++) {
                const index = i - leftPadding.length;
                if (state[i] === '#') {
                    sum += index;
                }
                else {
                }
            };
            if (false) {
                console.log(`REPEATED SEQUENCE ${i}, ${log.indexOf(aux)} Sum: ${sum}`);
            }
        } else {
            log.push(aux);
        }
    }

    let sum = 0;
    for(let i=0; i< state.length; i++) {
        const index = i - leftPadding.length;
        if (state[i] === '#') {
            sum += index;
        }
        else {
        }
    };
    console.log(`${generations} generations: ${sum}`);
};

run(20);
// run(1000);
// After sequence 93 the same state keeps propagating but moving to the right
// Analyzing the numbers the linear function to represent the sum is:
// geneneration * 80 + 866
// so for generation 50,000,000,000 the value is:
console.log(`Part 2: ${50000000000 * 80 + 866}`);


