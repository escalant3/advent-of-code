'use strict';

const Fs = require('fs');

let seek = 0;
const data = Fs.readFileSync('input08.txt', 'utf-8').trim().split(' ').map(x => Number(x));
const tree = [];

const processNode = () => {

    const [numChildNodes, numMetadata] = data.slice(seek);
    seek += 2;

    const node = { children: [], metadata: [] };
    if (seek === 2) {
        tree.push(node);
    }

    for(let i=0; i<numChildNodes; i++) {
        node.children.push(processNode(node));
    }

    node.metadata = data.slice(seek, seek + numMetadata);
    seek += numMetadata;

    return node;
};

processNode();

const part1 = (node) => {
    const nodeSum = node.metadata.reduce((acc, item) => acc + item, 0);
    return nodeSum + node.children.map(node => part1(node)).reduce((acc, item) => acc + item, 0);
};

const part2 = (node) => {
    if (!node.children.length) {
        return node.metadata.reduce((acc, item) => acc + item, 0);
    }

    let nodeSum = 0;
    for(let metadata of node.metadata) {
        const childNode = node.children[metadata-1];
        if (!childNode) continue;
        nodeSum += part2(childNode);
    }
    return nodeSum;
};

console.log(`Part 1: ${part1(tree[0])}`);
console.log(`Part 1: ${part2(tree[0])}`);
