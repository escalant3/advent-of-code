'use strict';

class Node {
    constructor(value) {
        this.value = value;
        this.next = null;
    }
}

class NodeList {
    constructor() {
        this.list = [];
        this.sequenceTracker = '';
    }

    start(value) {
        const node = new Node(value);
        this.list.push(node);
        this.sequenceTracker += node;
        node.next = node;
        return node;;
    }

    append(value) {
        const node = new Node(value);
        const previousNode = this.list[this.list.length-1];
        if (previousNode) {
            previousNode.next = node;
        }
        this.list.push(node);
        this.sequenceTracker = (this.sequenceTracker + value).slice(-10);
        node.next = this.list[0];
        return node;
    }

    get length() {
        return this.list.length;
    }

    slice(start, end) {
        return this.list.slice(start, end);
    }

    findSequence(input) {
        return this.sequenceTracker.includes(input);
    }

    nodesBeforeSequence(input) {
        return this.list.map(x => x.value).join('').indexOf(input);
    }
}

const part1 = (input) => {
    const nodeList = new NodeList();
    let firstElf = nodeList.start(3);
    let secondElf = nodeList.append(7);

    while(nodeList.length < input + 10) {
        const newRecipes = `${firstElf.value + secondElf.value}`;
        newRecipes.split('').forEach(score => nodeList.append(Number(score)));

        let firstElfSteps = firstElf.value + 1;
        let secondElfSteps = secondElf.value + 1;
        while(firstElfSteps>0) { firstElf = firstElf.next; firstElfSteps--; };
        while(secondElfSteps>0) { secondElf = secondElf.next; secondElfSteps--; };
    }

    return nodeList.slice(input, input+10).map(x => x.value).join('');
};

const part2 = (input) => {
    const nodeList = new NodeList();
    let firstElf = nodeList.start(3);
    let secondElf = nodeList.append(7);

    while(true) {
        const newRecipes = `${firstElf.value + secondElf.value}`;
        newRecipes.split('').forEach(score => nodeList.append(Number(score)));

        let firstElfSteps = firstElf.value + 1;
        let secondElfSteps = secondElf.value + 1;
        while(firstElfSteps>0) { firstElf = firstElf.next; firstElfSteps--; };
        while(secondElfSteps>0) { secondElf = secondElf.next; secondElfSteps--; };

        if (nodeList.findSequence(input)) {
            return nodeList.nodesBeforeSequence(input);
        }
    }
};


console.log(`Part 1: ${part1(793031)}`);
console.log(`Part 2: ${part2('793031')}`);

const Assert = require('assert');
Assert.strictEqual(part1(9), '5158916779');
Assert.strictEqual(part1(5), '0124515891');
Assert.strictEqual(part1(18), '9251071085');
Assert.strictEqual(part1(2018), '5941429882');
Assert.strictEqual(part2('51589'), 9);
Assert.strictEqual(part2('01245'), 5);
Assert.strictEqual(part2('92510'), 18);
Assert.strictEqual(part2('59414'), 2018);
