'use strict';

class Node {
    constructor(value) {
        this.value = value;
        this.previous = this.next = this;
    }
}

class DoubleLinkedList {

    constructor(sizeAllocated) {
        this.list = new Array(sizeAllocated);
        this.list[0] = new Node(0);
        this.activeNode = this.list[0];
        this.size = 1;

    }

    addBetweenTwoNextNodes(value) {
        const node = new Node(value);

        if (value % 23 === 0) {
            this.activeNode = this.activeNode.previous.previous.previous.previous.previous.previous.previous;
            const score = this.activeNode.value + value;
            this.activeNode.previous.next = this.activeNode.next;
            this.activeNode.next.previous = this.activeNode.previous;
            this.activeNode = this.activeNode.next;
            return score;
        }


        const aux = this.activeNode.next.next;
        node.next = this.activeNode.next.next;
        node.previous = this.activeNode.next;
        this.activeNode.next.next = node;
        aux.previous = node;
        this.list[this.size] = node;
        this.size++;
        this.activeNode = node;
        return 0;
    }
}

const runGame = (lastMarble, numPlayers) => {

    const players = Array(numPlayers).fill(0);

    const list = new DoubleLinkedList(lastMarble);
    let sum = 0;
    for(let i=1; i<=lastMarble; i++) {
        players[(i-1) % numPlayers] += list.addBetweenTwoNextNodes(i);
    }

    return players.reduce((acc, item) => Math.max(acc, item), 0);
};

console.log(`Part 1: ${runGame(70901, 429)}`);
console.log(`Part 2: ${runGame(70901*100, 429)}`);


const Assert = require('assert');
Assert.strictEqual(runGame(25, 9), 32);
Assert.strictEqual(runGame(1618, 10), 8317);
Assert.strictEqual(runGame(7999, 13), 146373);
Assert.strictEqual(runGame(1104, 17), 2764);
Assert.strictEqual(runGame(6111, 21), 54718);
Assert.strictEqual(runGame(5807, 30), 37305);
