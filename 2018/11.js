'use strict';

const powerLevel = (x, y, serialNumber) => {
    const rackId = x + 10;
    let powerLevel = rackId * y;
    powerLevel += serialNumber;
    powerLevel *= rackId;
    const digit = Math.floor(powerLevel / 100) % 10;
    return digit - 5;
};

const findLargestSquare = (serialNumber, minSize=3, maxSize=3) =>{
    const powerGrid = new Array(300*300).fill(0);
    for(let i=0; i<300; i++) {
        for(let j=0; j<300; j++) {
            powerGrid[j * 300 + i] = powerLevel(i+1, j+1, serialNumber);
        }
    }

    let maxPower = 0;
    let coordinate = null;
    let sizeImproved = true;

    for(let size=minSize; size<=maxSize; size++) {
        if (!sizeImproved) {
            break;
        }
        sizeImproved = false;

        for(let i=0; i<=300-size; i++) {
            for(let j=0; j<=300-size; j++) {
                let sum = 0;
                for(let k=i; k<i+size; k++) {
                    for(let l=j; l<j+size; l++) {
                        sum += powerGrid[l * 300 + k];
                    }
                }
                if (sum > maxPower) {
                    maxPower = sum;
                    coordinate = `${i+1},${j+1},${size}`;
                    sizeImproved = true;
                }
            }
        }
    }

    return coordinate;
};

console.log(`Part 1: ${findLargestSquare(6042).split(',').slice(0, 2).join(',')}`);
console.log(`Part 2: ${findLargestSquare(6042, 1, 300)}`);

const Assert = require('assert');
Assert.strictEqual(powerLevel(3, 5, 8), 4);
Assert.strictEqual(powerLevel(122, 79, 57), -5);
Assert.strictEqual(powerLevel(217, 196, 39), 0);
Assert.strictEqual(powerLevel(101, 153, 71), 4);
Assert.strictEqual(findLargestSquare(18), '33,45,3');
Assert.strictEqual(findLargestSquare(42), '21,61,3');
