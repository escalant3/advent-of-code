'use strict';

const Fs = require('fs');

const getAccumulatedNaps = fileName => {
    const data = Fs.readFileSync(fileName, 'utf-8').trim().split('\n')
        .sort();

    const naps = [];
    let currentGuard = null;
    for(let line of data) {
        const minute = Number(line.match(/\d{2}:(\d{2})/)[1]);

        const beginShiftMatch = line.match(/Guard #(\d+)/);
        if (beginShiftMatch) {
            const [_, guardId] = beginShiftMatch;
            currentGuard = Number(guardId);
            continue;
        }

        if (line.includes('falls asleep')) {
            naps.push({ guard: currentGuard, start: minute, end: null });
            continue;
        }

        if (line.includes('wakes up')) {
            naps[naps.length-1].end = minute;
            continue;
        }

        throw new Error(`Invalid line ${line}`);
    }

    return naps.reduce((acc, nap) => {
        if (!acc[nap.guard]) {
            acc[nap.guard] = Array(60).fill(0);
        }
        for(let i=nap.start; i<nap.end; i++) {
            acc[nap.guard][i] += 1;
        }
        return acc;
    }, {});
};

const part1 = fileName => {
    const accumulatedNaps = getAccumulatedNaps(fileName);
    let maxMinutesSlept = 0;
    let sleepiestGuard = null;
    for(let guardId in accumulatedNaps) {
        const minutesSlept = accumulatedNaps[guardId].reduce((acc, i) => acc + i, 0);
        if (minutesSlept > maxMinutesSlept) {
            maxMinutesSlept = minutesSlept;
            sleepiestGuard = guardId;
        }
    }

    const bestMinute = accumulatedNaps[sleepiestGuard].indexOf(
        accumulatedNaps[sleepiestGuard].reduce((acc, minute) => Math.max(acc, minute), 0)
    );

    return sleepiestGuard * bestMinute;
};

console.log(`Part 1: ${part1('input04.txt')}`);

const part2 = fileName => {
    const accumulatedNaps = getAccumulatedNaps(fileName);
    let maxSleepiestMinute = 0;
    let sleepiestGuard = null;
    for(let guardId in accumulatedNaps) {
        const sleepiestMinute = accumulatedNaps[guardId].reduce((acc, i) => Math.max(acc, i), 0);
        if (sleepiestMinute > maxSleepiestMinute) {
            maxSleepiestMinute = sleepiestMinute;
            sleepiestGuard = guardId;
        }
    }

    const bestMinute = accumulatedNaps[sleepiestGuard].indexOf(maxSleepiestMinute);
    return sleepiestGuard * bestMinute;
};

console.log(`Part 2: ${part2('input04.txt')}`);

// Test
const Assert = require('assert');
Assert.strictEqual(part1('test04.txt'), 240);
Assert.strictEqual(part2('test04.txt'), 4455);
