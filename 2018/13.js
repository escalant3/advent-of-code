'use strict'

const Fs = require('fs');


const directions = {
    '>': { x: 1, y: 0 },
    '<': { x: -1, y: 0 },
    '^': { x: 0, y: -1 },
    'v': { x: 0, y: 1 }
};

let trainId = 0;

class Train {
    constructor(x, y, speed) {
        this.id = trainId;
        trainId++;
        this.x = x;
        this.y = y;
        this.speed = Object.assign({}, speed);
        this.lastTurn = 'r';
    }

    moveStraight() {
        this.x += this.speed.x;
        this.y += this.speed.y;
    }

    turn(symbol) {
        if (symbol === '/' && this.speed.x === 0) {
            this.speed.x = this.speed.y === 1 ? -1 : 1;
            this.speed.y = 0;
        }
        else if (symbol === '/' && this.speed.y === 0) {
            this.speed.y = this.speed.x === 1 ? -1 : 1;
            this.speed.x = 0;
        }
        else if (symbol === '\\' && this.speed.x === 0) {
            this.speed.x = this.speed.y === 1 ? 1 : -1;
            this.speed.y = 0;
        }
        else if (symbol === '\\' && this.speed.y === 0) {
            this.speed.y = this.speed.x === 1 ? 1 : -1;
            this.speed.x = 0;
        }
    }

    turnLeft() {
        if (this.speed.x === 0) {
            this.speed.x = this.speed.y === 1 ? 1 : -1;
            this.speed.y = 0;
        }
        else if (this.speed.y === 0) {
            this.speed.y = this.speed.x === 1 ? -1 : 1;
            this.speed.x = 0;
        }
    }

    turnRight() {
        if (this.speed.x === 0) {
            this.speed.x = this.speed.y === 1 ? -1 : 1;
            this.speed.y = 0;
        }
        else if (this.speed.y === 0) {
            this.speed.y = this.speed.x === 1 ? 1 : -1;
            this.speed.x = 0;
        }
    }

    manageIntersection() {
        if (this.lastTurn === 'r') {
            this.turnLeft();
            this.lastTurn = 'l';
        } else if (this.lastTurn === 'l') {
            this.lastTurn = 's';
        } else if (this.lastTurn === 's') {
            this.turnRight();
            this.lastTurn = 'r';
        }
    }

    move(map) {
        this.moveStraight();
        const newPosition = map[this.y][this.x];

        if (newPosition === '/' || newPosition === '\\') {
            this.turn(newPosition);
        }
        else if (newPosition === '+') {
            this.manageIntersection();
        }

    }
}

const findTrains = map => {
    const trains = [];
    for(let i=0; i<map.length; i++) {
        ['>', '<', '^', 'v'].forEach(trainSymbol => {
            const row = map[i];
            let trainIndex = row.indexOf(trainSymbol);
            while (trainIndex !== -1) {
                trains.push(new Train(trainIndex, i, directions[trainSymbol]));
                if (trainSymbol === '>' || trainSymbol === '<') {
                    map[i][trainIndex] = '-';
                } else {
                    map[i][trainIndex] = '|';
                }

                trainIndex = row.indexOf(trainSymbol, trainIndex + 1);
            }
        });
    }
    return trains;
};


const part1 = () => {
    const map = Fs.readFileSync('input13.txt', 'utf-8').trimRight().split('\n').map(row => row.split(''));
    const trains = findTrains(map);

    outer:
    while (true) {
        for(let train of trains) {
            train.move(map);
            if (trains.filter(otherTrain => train.x === otherTrain.x && train.y === otherTrain.y).length > 1) {
                console.log(`First rain crash at ${train.x}, ${train.y}`);
                break outer;
            }
        };
    }
};

const part2 = () => {
    const map = Fs.readFileSync('input13.txt', 'utf-8').trimRight().split('\n').map(row => row.split(''));
    let trains = findTrains(map);

    const sortTrains = () => trains.sort((a,b) => b.y !== a.y ? a.y - b.y : a.x - b.x);

    while (trains.length > 1) {
        sortTrains();
        for(let train of trains) {
            train.move(map);
            trains.forEach(otherTrain => {
                if (train.id !== otherTrain.id && train.x === otherTrain.x && train.y === otherTrain.y && !otherTrain.crashed) {
                    train.crashed = true;
                    otherTrain.crashed = true;
                }
            });
        };
        trains = trains.filter(train => !train.crashed);
    }

    console.log(`Last train position ${trains[0].x}, ${trains[0].y}`);
};


part1();
part2();
