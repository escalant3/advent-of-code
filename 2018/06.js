'use strict';

const Fs = require('fs');

const getClosestPoint = (points, x, y) => {
    const distances = points.map(point => {
        return Math.abs(x - point.x) + Math.abs(y - point.y);
    });

    let closestPoint = null;
    let minDistance = Infinity;

    for(let i=0; i<distances.length; i++) {
        if (distances[i] < minDistance) {
            minDistance = distances[i];
            closestPoint = i;
            continue;
        }
        if (distances[i] === minDistance) {
            closestPoint = null;
        }
    }
    return closestPoint;
};

const part1 = fileName => {
    const data = Fs.readFileSync(fileName, 'utf-8').trim().split('\n').map(line => {
        const [x, y] = line.split(',');
        return { x: Number(x), y: Number(y) };
    });

    const { width, height } = data.reduce((acc, item) => {
        return {
            width: Math.max(acc.width, item.x),
            height: Math.max(acc.height, item.y)
        };
    }, { width: 0, height: 0});

    const grid = [...Array(height+1)].map(x => ([...Array(width+1)].fill('.')));

    data.forEach(({ x, y }, i) => {
        grid[y][x] = i;
    });

    for(let x=0; x<grid[0].length; x++) {
        for(let y=0; y<grid.length; y++) {
            const closestPoint = getClosestPoint(data, x, y);
            grid[y][x] = closestPoint;
        }
    }

    let maxNonInfinityArea = 0;
    for(let i=0; i<data.length; i++) {
        if (grid[0].includes(i) || grid[grid.length-1].includes(i) ||
            grid.map(x => x[0]).includes(i) || grid.map(x => x[x.length-1]).includes(i)) {
            continue;
        }

        const area = grid.reduce((acc, row) => acc + row.reduce((rowAcc, item) => item === i ? rowAcc + 1 : rowAcc, 0), 0);
        if (area > maxNonInfinityArea) {
            maxNonInfinityArea = area;
        }
    }

    return maxNonInfinityArea;

};

console.log(`Part 1: ${part1('06input.txt')}`);

const part2 = (fileName, safeDistance) => {
    const data = Fs.readFileSync(fileName, 'utf-8').trim().split('\n').map(line => {
        const [x, y] = line.split(',');
        return { x: Number(x), y: Number(y) };
    });

    const { width, height } = data.reduce((acc, item) => {
        return {
            width: Math.max(acc.width, item.x),
            height: Math.max(acc.height, item.y)
        };
    }, { width: 0, height: 0});

    const grid = [...Array(height+1)].map(x => ([...Array(width+1)].fill(0)));

    for(let x=0; x<grid[0].length; x++) {
        for(let y=0; y<grid.length; y++) {
            data.forEach(point => {
                grid[y][x] += Math.abs(point.x - x) + Math.abs(point.y - y);
            });
        }
    }

    return grid.flat().filter(x => x<safeDistance).length;
};

console.log(`Part 2: ${part2('06input.txt', 10000)}`);

const Assert = require('assert');
Assert.strictEqual(part1('06test.txt'), 17);
Assert.strictEqual(part2('06test.txt', 32), 16);

