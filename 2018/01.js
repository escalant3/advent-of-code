'use strict';

const Fs = require('fs');

const data = Fs.readFileSync('input01.txt', 'utf-8')
    .trim()
    .split('\n');

// Part 1
const result = data.reduce((acc, value) => {
    if (value[0] === '+') {
        return acc + Number(value.slice(1));
    } else if (value[0] === '-') {
        return acc - Number(value.slice(1));
    } else {
        throw new Error(`Bad input ${value}`);
    }
}, 0);

console.log(result);

// Part 2
let acc = 0;
const frequencies = [acc];
outer:
while (true) {
    for (let value of data) {
        if (value[0] === '+') {
            acc += Number(value.slice(1));
        } else if (value[0] === '-') {
            acc -= Number(value.slice(1));
        } else {
            throw new Error(`Bad input ${value}`);
        }

        if (frequencies.includes(acc)) {
            console.log(`Frequency ${acc} reached twice`);
            break outer;
        }

        frequencies.push(acc);
    }
}

