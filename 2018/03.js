'use strict';

const Assert = require('assert');
const Fs = require('fs');

const test = [
    '#1 @ 1,3: 4x4',
    '#2 @ 3,1: 4x4',
    '#3 @ 5,5: 2x2'
];

const CLAIM_REGEX = /#(\d+) @ (\d+),(\d+): (\d+)x(\d+)/;

const parseClaim = (claimText) => {
    const [_, id, left, top, width, height] = claimText.match(CLAIM_REGEX);
    return {
        id,
        left: +left,
        top: +top,
        width: +width,
        height: +height
    };
};

/**
 * Only inches returned with 1 do not have overlap
 */
const markFabricWithOverlaps = (claims) => {
    const { maxWidth, maxHeight } = claims.reduce((acc, item) => {
        const { maxWidth, maxHeight } = acc;
        return {
            maxWidth: Math.max(maxWidth, item.left + item.width + 1),
            maxHeight: Math.max(maxHeight, item.top + item.height + 1)
        };
    }, { maxWidth: 0, maxHeight: 0 });

    const fabric = Array(maxWidth*maxHeight).fill(0);
    claims.forEach(claim => {
        for(let i=claim.left; i<claim.left+claim.width; i++)
            for(let j=claim.top; j<claim.top+claim.height; j++)
                fabric[i + maxWidth * j] += 1;
    });

    return { fabric, maxWidth, maxHeight };
};

const findIntersections = (claims) => {
    const { fabric } = markFabricWithOverlaps(claims);
    return fabric.filter(x => x>1).length;
};

const findClaimWithoutOverlaps = (claims) => {
    const { fabric, maxWidth } = markFabricWithOverlaps(claims);
    claimStart:
    for(let claim of claims) {
        for(let i=claim.left; i<claim.left+claim.width; i++) {
            for(let j=claim.top; j<claim.top+claim.height; j++) {
                if (fabric[i + maxWidth * j] !== 1) continue claimStart;
            }
        }
        return claim.id;
    }
};

const data = Fs.readFileSync('input03.txt', 'utf-8')
    .trim()
    .split('\n');

//const claims = test.map(parseClaim);
const claims = data.map(parseClaim);
const result = findIntersections(claims);
console.log(`Total intersections: ${result}`);

const result2 = findClaimWithoutOverlaps(claims);
console.log(`Id without overlaps: ${result2}`);

