'use strict';

const Fs = require('fs');

const decodeRoom = (roomStr) => {

    const components = roomStr.split('-');
    const roomName = components.slice(0, -1);
    const shortName = roomName.join('').split('');
    let [_, roomId, checksum] = components[components.length-1].match(/(\d+)\[(\w+)/);
    const histogram = shortName.reduce((hist, item) => {

        if (!hist[item]) hist[item] = 0;
        hist[item]++;
        return hist;
    }, {});

    let hash = '';
    for(let i=0; i<5; i++) {
        let max = 0;
        let maxLetter = null;
        Object.keys(histogram).sort().forEach(letter => {
            if (max < histogram[letter]) {
                maxLetter = letter;
                max = histogram[letter];
            }
        });

        hash += maxLetter;
        delete histogram[maxLetter];
    }

    roomId = Number(roomId);

    const aCode = 97;
    const decodedName = roomName.map(word => {

        return word.split('').map(letter => {

            let code = letter.charCodeAt() - aCode;
            code += roomId;
            code %= 26;
            return String.fromCharCode(code + 97);
        }).join('');
    }).join('-');

    return {
        valid: hash === checksum,
        roomId: roomId,
        decodedName: decodedName
    }
};

const data = Fs.readFileSync('04input.txt', 'utf-8').split('\n').filter(Boolean);

const validRooms = data
    .map(item => decodeRoom(item))
    .filter(room => room.valid);

const part1 = validRooms.reduce((result, room) => result + room.roomId, 0);
console.log(`Part 1: ${part1}`);

const part2 = validRooms.filter(x => x.decodedName.includes('north') && x.decodedName.includes('pole'));
console.log(`Part 2: ${JSON.stringify(part2)}`);

const test = () => {

    const Assert = require('assert');
    Assert.strictEqual(decodeRoom('aaaaa-bbb-z-y-x-123[abxyz]').valid, true);
    Assert.strictEqual(decodeRoom('a-b-c-d-e-f-g-h-987[abcde]').valid, true);
    Assert.strictEqual(decodeRoom('not-a-real-room-404[oarel]').valid, true);
    Assert.strictEqual(decodeRoom('totally-real-room-200[decoy]').valid, false);
};

test();
