'use strict';

const Fs = require('fs');

const isTriangle = (sides) => {

    sides = sides.slice().sort((a,b) => Number(a) - Number(b));
    return sides[0] + sides[1] > sides[2];
};

const test = () => {

    const Assert = require('assert');
    Assert.strictEqual(isTriangle([5, 10, 25]), false);
    Assert.strictEqual(isTriangle([118,331,413]), true);
};

test();

const rawData = Fs.readFileSync('03input.txt', 'utf-8').split('\n')
    .map(line => line.match(/\W*(\d+)\W*(\d+)\W*(\d+)/))
    .filter(match => match)
    .map(groups => [groups[1], groups[2], groups[3]].map(Number));

console.log(`Part 1: ${rawData.filter(isTriangle).length}`);

let part2Counter = 0;
for(let i=0; i<3; i++) {
    for(let j=0; j<rawData.length; j+=3) {
        if (isTriangle([rawData[j][i], rawData[j+1][i], rawData[j+2][i]])) part2Counter++;
    }
}
console.log(`Part 2: ${part2Counter}`);
