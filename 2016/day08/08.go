package main

import (
	_ "embed"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

const HEIGHT = 6
const WIDTH = 50

type screen [HEIGHT][WIDTH]bool

func (s *screen) print() {
	for _, row := range s {
		for _, column := range row {
			if column {
				fmt.Print("#")
			} else {
				fmt.Print(".")
			}
		}
		fmt.Println()
	}
	fmt.Println()
	fmt.Println()
}

func (s *screen) rect(a, b int) {
	for j := 0; j < a; j++ {
		for i := 0; i < b; i++ {
			s[i][j] = true
		}
	}
}

func (s *screen) rotateColumn(column, amount int) {
	var buffer [HEIGHT]bool
	for row := 0; row < HEIGHT; row++ {
		buffer[row] = s[row][column]
	}
	for row := 0; row < HEIGHT; row++ {
		s[(row+amount)%HEIGHT][column] = buffer[row]
	}
}

func (s *screen) rotateRow(row, amount int) {
	var buffer [WIDTH]bool
	for column := 0; column < WIDTH; column++ {
		buffer[column] = s[row][column]
	}
	for column := 0; column < WIDTH; column++ {
		s[row][(column+amount)%WIDTH] = buffer[column]
	}
}

func (s *screen) litNumber() int {
	sum := 0
	for _, row := range s {
		for _, cell := range row {
			if cell {
				sum++
			}
		}
	}
	return sum
}

//go:embed input_08.txt
var input string

func main() {
	var s screen
	rectRegex := regexp.MustCompile(`rect (\d+)x(\d+)`)
	rotateRowRegex := regexp.MustCompile(`rotate row y=(\d+) by (\d+)`)
	rotateColumnRegex := regexp.MustCompile(`rotate column x=(\d+) by (\d+)`)
	for _, instruction := range strings.Split(input, "\n") {
		if r := rectRegex.FindStringSubmatch(instruction); len(r) > 0 {
			a, _ := strconv.Atoi(r[1])
			b, _ := strconv.Atoi(r[2])
			s.rect(a, b)
		} else if r := rotateRowRegex.FindStringSubmatch(instruction); len(r) > 0 {
			a, _ := strconv.Atoi(r[1])
			b, _ := strconv.Atoi(r[2])
			s.rotateRow(a, b)
		} else if r := rotateColumnRegex.FindStringSubmatch(instruction); len(r) > 0 {
			a, _ := strconv.Atoi(r[1])
			b, _ := strconv.Atoi(r[2])
			s.rotateColumn(a, b)
		}
	}
	fmt.Println("Part 1:", s.litNumber())
	fmt.Println("Part 2:")
	s.print()
}
