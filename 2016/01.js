'use strict';

const Fs = require('fs');

const Directions = {
    N: ['W', 'E'],
    W: ['S', 'N'],
    S: ['E', 'W'],
    E: ['N', 'S']
};

const distance = (pathString) => {
    const steps = pathString.split(', ');
    const position = [0, 0];
    let direction = 'N';
    for(let step of steps) {
        if (step[0] === 'L') {
            direction = Directions[direction][0]
        } else {
            direction = Directions[direction][1]
        }
        const distance = Number(step.slice(1));
        switch (direction) {
            case 'N': position[1] -= distance; break;
            case 'S': position[1] += distance; break;
            case 'W': position[0] -= distance; break;
            case 'E': position[0] += distance; break;
        }
    }

    return Math.abs(position[0]) + Math.abs(position[1]);
}


const distance2 = (pathString) => {
    const steps = pathString.split(', ');
    const visitedPositions = ['0:0'];
    let direction = 'N';
    for(let step of steps) {
        if (step[0] === 'L') {
            direction = Directions[direction][0]
        } else {
            direction = Directions[direction][1]
        }
        const distance = Number(step.slice(1));
        let position = visitedPositions.slice(-1)[0].split(':').map(Number);
        for(let i=0; i<distance; i++) {
            switch (direction) {
                case 'N': position[1] -= 1; break;
                case 'S': position[1] += 1; break;
                case 'W': position[0] -= 1; break;
                case 'E': position[0] += 1; break;
            }
            const positionString = `${position[0]}:${position[1]}`;
            if (visitedPositions.includes(positionString)) {
                return Math.abs(position[0]) + Math.abs(position[1]);
            }
            visitedPositions.push(positionString);
        }
    }
}

const test = () => {
    const Assert = require('assert');
    Assert.strictEqual(distance('R2, L3'), 5);
    Assert.strictEqual(distance('R2, R2, R2'), 2);
    Assert.strictEqual(distance('R5, L5, R5, R3'), 12);
}

const data = Fs.readFileSync('input01.txt', 'utf-8');
console.log(`Part 1: ${distance(data)}`);

console.log(`Part 2: ${distance2(data)}`);
