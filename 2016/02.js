'use strict';

const Fs = require('fs');

const getCode = (input) => {

    const steps = input.split('\n').filter(x => x);
    let currentButton = 5;
    let code = '';
    for(let step of steps) {
        for(let move of step) {
            if (move === 'U' && currentButton > 3) currentButton -= 3;
            else if (move === 'D' && currentButton < 7) currentButton += 3;
            else if (move === 'L' && ![1,4,7].includes(currentButton)) currentButton--;
            else if (move === 'R' && ![3,6,9].includes(currentButton)) currentButton++;
        }
        code += `${currentButton}`;
    }
    return code;
};

const getCode2 = (input) => {

    const pad = {
        '1': {D: '3'},
        '2': {D: '6', R: '3'},
        '3': {L: '2', U: '1', R: '4', D: '7'},
        '4': {L: '3', D: '8'},
        '5': {R: '6'},
        '6': {L: '5', U: '2', R: '7', D: 'A'},
        '7': {L: '6', U: '3', R: '8', D: 'B'},
        '8': {L: '7', U: '4', R: '9', D: 'C'},
        '9': {L: '8'},
        'A': {U: '6', R: 'B'},
        'B': {U: '7', R: 'C', D: 'D', L: 'A'},
        'C': {L: 'B', U: '8'},
        'D': {U: 'B'}
    };

    const steps = input.split('\n').filter(x => x);
    let currentButton = '5';
    let code = '';
    for(let step of steps) {
        for(let move of step) {
            currentButton = pad[currentButton][move] || currentButton;
        }
        code += `${currentButton}`;
    }
    return code;
};

const test = () => {

    const Assert = require('assert');
    Assert.strictEqual(getCode('ULL\nRRDDD\nLURDL\nUUUUD'), '1985');
    Assert.strictEqual(getCode2('ULL\nRRDDD\nLURDL\nUUUUD'), '5DB3');
};

test();

const data = Fs.readFileSync('02input.txt', 'utf-8');
console.log(`Part 1: ${getCode(data)}`);
console.log(`Part 2: ${getCode2(data)}`);
