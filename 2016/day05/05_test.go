package main

import "testing"

func TestPassword(t *testing.T) {
	expected := "18f47a30"
	r := getPassword("abc")
	if r != expected {
		t.Errorf("expected %s; got %s", expected, r)
	}
}
func TestPassword2(t *testing.T) {
	expected := "05ace8e3"
	r := getPassword2("abc")
	if r != expected {
		t.Errorf("expected %s; got %s", expected, r)
	}
}
