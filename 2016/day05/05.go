package main

import (
	"crypto/md5"
	"fmt"
	"io"
	"strconv"
)

func getPassword(input string) string {
	var password []byte
	for i := 0; len(password) < 8; i++ {
		h := md5.New()
		io.WriteString(h, input+strconv.Itoa(i))
		hash := fmt.Sprintf("%x", h.Sum(nil))
		if hash[:5] == "00000" {
			password = append(password, hash[5])
		}
	}
	return string(password)
}

func isValid(p []byte) bool {
	for _, c := range p {
		if c == 'x' {
			return false
		}
	}
	return true
}

func getPassword2(input string) string {
	password := []byte{'x', 'x', 'x', 'x', 'x', 'x', 'x', 'x'}
	fmt.Printf("%s\n", password)
	for i := 0; !isValid(password); i++ {
		h := md5.New()
		io.WriteString(h, input+strconv.Itoa(i))
		hash := fmt.Sprintf("%x", h.Sum(nil))
		if hash[:5] == "00000" {
			position := int(hash[5]) - 48 // Position of '0' in ascii
			if position < 8 && password[position] == 'x' {
				password[position] = hash[6]
				fmt.Printf("%s\n", password)
			}
		}
	}
	return string(password)
}

func main() {
	r := getPassword("ojvtpuvg")
	fmt.Println("Part 1:", r)
	r = getPassword2("ojvtpuvg")
	fmt.Println("Part 2:", r)
	// wrong 1550c4bd

}
