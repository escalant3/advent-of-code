#!/usr/bin/env python3

import re

def main(file_name, chip1, chip2):
    with open(file_name) as f:
        lines = [x.strip() for x in f.read().split('\n') if x.strip()]

    value_re = re.compile(r'value (\d+) goes to bot (\d+)')
    gives_re = re.compile(r'bot (\d+) gives low to (\w+) (\d+) and high to (\w+) (\d+)')

    bots = {}
    outputs = {}

    def give_to_bot(bot, value):
        if bot not in bots:
            bots[bot] = []
        bots[bot].append(value)

    for line in lines:

        v_srh = value_re.search(line)
        if v_srh is None:
            continue

        value, bot = v_srh.groups()
        give_to_bot(bot, int(value))

    target_bot_id = None

    while True:
        pending = []
        for line in lines:
            if target_bot_id is None:
                for key,value in bots.items():
                    if chip1 in value and chip2 in value:
                        target_bot_id = key

            g_srh = gives_re.search(line)
            if g_srh is None:
                continue

            source, dest1_type, dest1_id, dest2_type, dest2_id = g_srh.groups()
            if not source in bots or len(bots[source]) != 2:
                pending.append(line)
                continue

            high = max(bots[source])
            low = min(bots[source])
            if dest1_type == 'output':
                outputs[dest1_id] = low
            else:
                give_to_bot(dest1_id, low)

            if dest2_type == 'output':
                outputs[dest2_id] = high
            else:
                give_to_bot(dest2_id, high)
            bots[source] = []

        if len(pending) == 0:
            return target_bot_id, outputs['0'] * outputs['1'] * outputs['2']
        
        lines = pending
        pending = []
 

if __name__ == '__main__':
    bot_id, result = main('10_input.txt', 17, 61)
    print(f'Part1 - Bot processing configuration: {bot_id}')
    print(f'Part2 - Result: {result}')

