package main

import (
	_ "embed"
	"testing"
)

//go:embed input_06_test.txt
var testInput string

func TestPart1(t *testing.T) {
	r := correct(testInput, extractCharacterPart1)
	if r != "easter" {
		t.Errorf("expected: easter, got :%s", r)
	}
}

func TestPart2(t *testing.T) {
	r := correct(testInput, extractCharacterPart2)
	if r != "advent" {
		t.Errorf("expected: advent, got :%s", r)
	}
}
