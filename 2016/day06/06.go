package main

import (
	_ "embed"
	"fmt"
	"math"
	"strings"
)

//go:embed input_06.txt
var input string

func extractCharacterPart1(histogram map[byte]int) byte {
	max := 0
	var letter byte
	for key, value := range histogram {
		if value > max {
			max = value
			letter = key
		}
	}
	return letter
}

func extractCharacterPart2(histogram map[byte]int) byte {
	min := math.MaxInt
	var letter byte
	for key, value := range histogram {
		if value < min {
			min = value
			letter = key
		}
	}
	return letter
}

func correct(input string, extract func(map[byte]int) byte) string {
	messages := strings.Fields(input)
	var correctedMessage []byte
	for position := 0; position < len(messages[0]); position++ {
		count := make(map[byte]int)
		for _, message := range messages {
			count[message[position]]++
		}
		correctedMessage = append(correctedMessage, extract(count))
	}
	return string(correctedMessage)
}

func main() {
	r := correct(input, extractCharacterPart1)
	fmt.Println("Part 1:", r)
	r = correct(input, extractCharacterPart2)
	fmt.Println("Part 2:", r)
}
