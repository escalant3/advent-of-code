package main

import (
	_ "embed"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

//go:embed input_09.txt
var input string

func decompress(input string) string {
	position := 0
	result := ""
	markerRegexp := regexp.MustCompile(`\((\d+)x(\d+)\)`)
	markerResult := markerRegexp.FindStringSubmatchIndex(input[position:])
	for len(markerResult) > 0 {

		fmt.Println(position, markerResult)
		result += input[position : position+markerResult[0]]
		numberOfCharacters, err := strconv.Atoi(input[position+markerResult[2] : position+markerResult[3]])
		if err != nil {
			panic(err)
		}
		repeats, err := strconv.Atoi(input[position+markerResult[4] : position+markerResult[5]])
		if err != nil {
			panic(err)
		}
		substring := input[position+markerResult[1] : position+markerResult[1]+numberOfCharacters]
		for i := 0; i < repeats; i++ {
			result += substring
		}
		position += markerResult[1] + numberOfCharacters
		markerResult = markerRegexp.FindStringSubmatchIndex(input[position:])
	}

	return result + input[position:]
}

func decompressV2(input string) int {
	position := 0
	length := 0
	result := ""
	markerRegexp := regexp.MustCompile(`\((\d+)x(\d+)\)`)
	markerResult := markerRegexp.FindStringSubmatchIndex(input[position:])
	for len(markerResult) > 0 {

		length += markerResult[0]
		result += input[position : position+markerResult[0]]
		numberOfCharacters, err := strconv.Atoi(input[position+markerResult[2] : position+markerResult[3]])
		if err != nil {
			panic(err)
		}
		repeats, err := strconv.Atoi(input[position+markerResult[4] : position+markerResult[5]])
		if err != nil {
			panic(err)
		}
		output := ""
		substring := input[position+markerResult[1] : position+markerResult[1]+numberOfCharacters]
		for i := 0; i < repeats; i++ {
			output += substring
		}
		length += decompressV2(output)
		position += markerResult[1] + numberOfCharacters
		markerResult = markerRegexp.FindStringSubmatchIndex(input[position:])
	}

	return length + len(input[position:])
}

func main() {
	r := decompress(input)
	fmt.Println("Part 1:", len(strings.TrimSpace(r)))
	fmt.Println("Part 2:", decompressV2(input)-1)
}
