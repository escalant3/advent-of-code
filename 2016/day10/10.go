package main

import (
	_ "embed"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

//go:embed input_10.txt
var input string

type Bot struct {
	low  int
	high int
}

func NewBot() *Bot {
	return &Bot{-1, -1}
}

func (b *Bot) giveValue(value int) {
	if b.low == -1 {
		b.low = value
	} else if b.high == -1 {
		if value < b.low {
			b.high = b.low
			b.low = value
		} else {
			b.high = value
		}
	} else {
		panic("Three values given to a robot")
	}
}

func main() {
	//values := make(map[int]int)
	bots := make(map[int]*Bot)
	giveRegexp := regexp.MustCompile(`bot (\d+) gives low to bot (\d+) and high to bot (\d+)`)
	valueRegexp := regexp.MustCompile(`value (\d+) goes to bot (\d+)`)

	for _, instruction := range strings.Split(input, "\n") {
		if r := valueRegexp.FindStringSubmatch(instruction); len(r) > 0 {
			value, _ := strconv.Atoi(r[1])
			number, _ := strconv.Atoi(r[2])
			if _, ok := bots[number]; !ok {
				bot := NewBot()
				bots[number] = bot
			}
			bots[number].giveValue(value)
		}
	}
	for _, instruction := range strings.Split(input, "\n") {
		if r := giveRegexp.FindStringSubmatch(instruction); len(r) > 0 {
			giver, _ := strconv.Atoi(r[1])
			low, _ := strconv.Atoi(r[2])
			high, _ := strconv.Atoi(r[3])
			fmt.Println(giver, low, high)
			// givers[giver] = give{low, high}
		}
	}

	for botId, bot := range bots {
		fmt.Println(botId, ": ", bot)
	}
	//	fmt.Println(bots)
	//	fmt.Println(givers)
}
