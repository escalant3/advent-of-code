package main

import "testing"

func TestSupportTLS(t *testing.T) {
	cases := map[string]bool{
		"abba[mnop]qrst":       true,
		"abcd[bddb]xyyx":       false,
		"aaaa[qwer]tyui":       false,
		"ioxxoj[asdfgh]zxcvbn": true,
	}
	for input, expectation := range cases {
		if r := supportTLS(input); r != expectation {
			t.Errorf("input %s -> expected %v; got %v", input, expectation, r)
		}
	}
}

func TestSupportSSL(t *testing.T) {
	cases := map[string]bool{
		"aba[bab]xyz":   true,
		"xyx[xyx]xyx":   false,
		"aaa[kek]eke":   true,
		"zazbz[bzb]cdb": true,
	}
	for input, expectation := range cases {
		if r := supportSSL(input); r != expectation {
			t.Errorf("input %s -> expected %v; got %v", input, expectation, r)
		}
	}
}
