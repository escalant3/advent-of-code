package main

import (
	_ "embed"
	"fmt"
	"strings"
)

//go:embed input_07.txt
var input string

func supportTLS(ip string) bool {
	inBrackets := false
	validSoFar := false
	for i := 0; i < len(ip)-3; i++ {
		if ip[i] == '[' {
			inBrackets = true
		}
		if ip[i] == ']' {
			inBrackets = false
		}
		if ip[i] == ip[i+3] && ip[i+1] == ip[i+2] && ip[i] != ip[i+1] {
			if inBrackets {
				return false
			}
			validSoFar = true
		}
	}
	return validSoFar
}

func supportSSL(ip string) bool {
	inBrackets := false
	var abas []string
	var babs []string
	for i := 0; i < len(ip)-2; i++ {
		if ip[i] == '[' {
			inBrackets = true
		}
		if ip[i] == ']' {
			inBrackets = false
		}
		if ip[i] == ip[i+2] && ip[i] != ip[i+1] {
			if inBrackets {
				babs = append(babs, ip[i:i+3])
			} else {
				abas = append(abas, ip[i:i+3])
			}
		}
	}

	for _, aba := range abas {
		for _, bab := range babs {
			if aba[0] == bab[1] && aba[1] == bab[0] {
				return true
			}
		}
	}
	return false
}

func main() {
	sum := 0
	sum2 := 0
	for _, ip := range strings.Fields(input) {
		if supportTLS(ip) {
			sum++
		}
		if supportSSL(ip) {
			sum2++
		}
	}
	fmt.Println("Part 1:", sum)
	fmt.Println("Part 2:", sum2)
}
