package main

import (
	_ "embed"
	"testing"
)

//go:embed 06_test.txt
var testInput string

//go:embed 06_test2.txt
var testInput2 string

func TestPart1(t *testing.T) {
	r := part1(testInput)
	if r != 42 {
		t.Errorf("Expected %d got %d\n", 42, r)
	}
}

func TestPart2(t *testing.T) {
	r := part2(testInput2)
	if r != 4 {
		t.Errorf("Expected %d got %d\n", 4, r)
	}
}
