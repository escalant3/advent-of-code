package main

import (
	_ "embed"
	"fmt"
	"strings"
)

type Node struct {
	value    string
	parent   *Node
	children []*Node
}

func getOrbitsFromNode(dict map[string]*Node, nodeName string) map[string]int {
	orbits := map[string]int{nodeName: 0}
	objects := dict[nodeName].children
	count := 1
	for len(objects) > 0 {
		newObjects := []*Node{}
		for _, obj := range objects {
			orbits[obj.value] = count
			newObjects = append(newObjects, obj.children...)
		}
		count++
		objects = newObjects
	}

	return orbits
}

func parse(s string) map[string]*Node {
	lines := strings.Split(s, "\n")
	dict := make(map[string]*Node)
	for _, line := range lines {
		components := strings.Split(line, ")")
		a, b := components[0], components[1]
		v := dict[a]
		if v == nil {
			n := &Node{a, nil, []*Node{}}
			dict[a] = n
		}
		v = dict[b]
		if v == nil {
			n := &Node{b, nil, []*Node{}}
			dict[b] = n
		}

		dict[a].children = append(dict[a].children, dict[b])
		dict[b].parent = dict[a]
	}

	return dict
}

func part1(s string) int {
	dict := parse(s)
	orbits := getOrbitsFromNode(dict, "COM")
	sum := 0
	for _, n := range orbits {
		sum += n
	}
	return sum
}

func part2(s string) int {
	dict := parse(s)
	var commonParent *Node

	youParents := map[*Node]int{dict["YOU"].parent: 0}
	sanParents := map[*Node]int{dict["SAN"].parent: 0}

	for {
		for yp := range youParents {
			for sp := range sanParents {
				if yp.value == sp.value {
					commonParent = yp
				}
			}
		}
		if commonParent != nil {
			break
		}
		for yp := range youParents {
			if yp.parent != nil {
				youParents[yp.parent] = 0
			}
		}
		for sp := range sanParents {
			if sp.parent != nil {
				sanParents[sp.parent] = 0
			}
		}
	}

	orbits := getOrbitsFromNode(dict, commonParent.value)
	return orbits[dict["YOU"].parent.value] + orbits[dict["SAN"].parent.value]
}

//go:embed 06_input.txt
var input string

func main() {
	r := part1(input)
	fmt.Printf("Part 1: %d\n", r)
	r = part2(input)
	fmt.Printf("Part 2: %d\n", r)
}
