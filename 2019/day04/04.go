package main

import (
	"fmt"
	"strconv"
)

func toInt(s string) int {
	v, _ := strconv.Atoi(s)
	return v
}

func valid(s string) (p1, p2 bool) {
	if len(s) != 6 {
		return
	}

	hasRepeated := false
	hasPair := false

	for i := 1; i < 6; i++ {
		if s[i-1] > s[i] {
			return
		}

		if s[i-1] == s[i] {
			hasRepeated = true

			if i != 5 && i != 1 && s[i+1] != s[i] && s[i-2] != s[i] {
				hasPair = true
			} else if i == 1 && s[i+1] != s[i] {
				hasPair = true
			} else if i == 5 && s[i-2] != s[i] {
				hasPair = true
			}
		}

	}

	return hasRepeated, hasPair
}

func solve(min, max int) (int, int) {
	candidatesP1 := []int{}
	candidatesP2 := []int{}

	for i := min; i <= max; i++ {
		candidate := strconv.Itoa(i)
		validP1, validP2 := valid(candidate)
		if validP1 {
			candidatesP1 = append(candidatesP1, toInt(candidate))
		}
		if validP2 {
			candidatesP2 = append(candidatesP2, toInt(candidate))
		}
	}

	return len(candidatesP1), len(candidatesP2)
}

func main() {
	r1, r2 := solve(347312, 805915)
	fmt.Printf("Part 1: %d\n", r1)
	fmt.Printf("Part 2: %d\n", r2)
}
