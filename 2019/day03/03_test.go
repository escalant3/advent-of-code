package main

import (
	"testing"
)

func TestManhattanDistance(t *testing.T) {
	p1 := &Point{1, 1}
	p2 := &Point{3, 3}
	if r := p1.manhattanDistance(p2); r != 4 {
		t.Errorf("Expected 4 got %d", r)
	}
}

func TestSolve(t *testing.T) {
	expected1 := 6
	expected2 := 30
	if r1, r2 := solve("R8,U5,L5,D3", "U7,R6,D4,L4"); r1 != expected1 || r2 != expected2 {
		t.Errorf("Expected part 1 to be %d got %d", expected1, r1)
		t.Errorf("Expected part 2 to be %d got %d", expected2, r2)
	}

}
