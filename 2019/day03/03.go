package main

import (
	_ "embed"
	"fmt"
	"log"
	"math"
	"sort"
	"strconv"
	"strings"
)

//go:embed 03_input.txt
var input string

type Point struct {
	x int
	y int
}

func abs(v int) int {
	if v < 0 {
		return -v
	}
	return v
}

func stringToInt(s string) int {
	v, err := strconv.Atoi(s)
	if err != nil {
		log.Panicf("could not parse int %s", s)
	}
	return v
}

func (p *Point) manhattanDistance(p2 *Point) int {
	return abs(p.x-p2.x) + abs(p.y-p2.y)
}

func getPointsFromWire(wire string) ([]Point, map[Point]int) {
	var newPos Point
	costs := make(map[Point]int)
	cost := 1
	pos := Point{}

	points := []Point{}

	handlePoint := func(i, j int) {
		p := Point{i, j}
		points = append(points, Point{i, j})
		if costs[p] == 0 {
			costs[p] = cost
		}
		cost++
	}

	for _, step := range strings.Split(wire, ",") {
		v := stringToInt(step[1:])
		if step[0] == 'U' {
			newPos = Point{pos.x, pos.y - v}
			for i := pos.y - 1; i >= newPos.y; i-- {
				handlePoint(newPos.x, i)
			}
		} else if step[0] == 'D' {
			newPos = Point{pos.x, pos.y + v}
			for i := pos.y + 1; i <= newPos.y; i++ {
				handlePoint(newPos.x, i)
			}
		} else if step[0] == 'L' {
			newPos = Point{pos.x - v, pos.y}
			for i := pos.x - 1; i >= newPos.x; i-- {
				handlePoint(i, newPos.y)
			}
		} else {
			newPos = Point{pos.x + v, pos.y}
			for i := pos.x + 1; i <= newPos.x; i++ {
				handlePoint(i, newPos.y)
			}
		}

		pos = newPos
	}

	return points, costs
}

func sortPoints(points []Point) {
	sort.Slice(points, func(i, j int) bool {
		if points[i].x == points[j].x {
			return points[i].y < points[j].y
		}
		return points[i].x < points[j].x
	})
}

func getIntersectionPoints(points1, points2 []Point) []Point {

	sortPoints(points1)
	sortPoints(points2)

	points := []Point{}

	for _, p1 := range points1 {
		for _, p2 := range points2 {
			if p2.x > p1.x {
				break
			}
			if p2.x == p1.x && p2.y > p1.y {
				break
			}
			if p1 == p2 {
				points = append(points, p1)
			}
		}
	}

	return points
}

func solve(wire1, wire2 string) (int, int) {

	points1, costs1 := getPointsFromWire(wire1)
	points2, costs2 := getPointsFromWire(wire2)

	intersectionPoints := getIntersectionPoints(points1, points2)

	minDistance := math.MaxInt
	minCost := math.MaxInt
	for _, point := range intersectionPoints {
		d := point.manhattanDistance(&Point{0, 0})
		if d < minDistance {
			minDistance = d
		}

		c := costs1[point] + costs2[point]
		if c < minCost {
			minCost = c
		}
	}
	return minDistance, minCost
}

func main() {
	lines := strings.Split(input, "\n")
	r1, r2 := solve(lines[0], lines[1])
	fmt.Printf("Part 1: %d\n", r1)
	fmt.Printf("Part 2: %d\n", r2)
}
