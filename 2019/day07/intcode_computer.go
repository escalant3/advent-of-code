package main

import (
	_ "embed"
	"log"
	"strconv"
	"strings"
)

type Operation int
type ParamMode int

const (
	Add         Operation = 1
	Mul                   = 2
	Input                 = 3
	Output                = 4
	JumpIfTrue            = 5
	JumpIfFalse           = 6
	LessThan              = 7
	Equals                = 8
	End                   = 99
)

const (
	Position  ParamMode = 0
	Immediate           = 1
)

type Instruction struct {
	operation Operation
	p1mod     ParamMode
	p2mod     ParamMode
}

type IntCodeComputer struct {
	mem          []int
	pc           int
	inputCounter int
	output       int
}

func (i *Instruction) isTwoParameterOperation() bool {
	return i.operation == JumpIfFalse || i.operation == JumpIfTrue
}

func (i *Instruction) isThreeParameterOperation() bool {
	return i.operation == Add || i.operation == Mul || i.operation == LessThan || i.operation == Equals
}

func toInt(s string) int {
	v, _ := strconv.Atoi(s)
	return v
}

func (icc *IntCodeComputer) fetchInstruction() *Instruction {
	raw := icc.mem[icc.pc]
	i := &Instruction{}
	i.operation = Operation(raw % 100)
	i.p1mod = ParamMode(raw % 1000 / 100)
	i.p2mod = ParamMode(raw % 10000 / 1000)
	return i
}

func (icc *IntCodeComputer) process(userInputs []int) {

	//icc.pc = 0
	//icc.halted = false
	//icc.inputCounter = 0
	i := icc.fetchInstruction()

	for i.operation != End {
		var p1, p2 int
		if i.p1mod == Immediate {
			p1 = icc.mem[icc.pc+1]
		} else {
			p1 = icc.mem[icc.mem[icc.pc+1]]
		}
		if i.isTwoParameterOperation() || i.isThreeParameterOperation() {
			if i.p2mod == Immediate {
				p2 = icc.mem[icc.pc+2]
			} else {
				p2 = icc.mem[icc.mem[icc.pc+2]]
			}
		}
		if i.operation == Add {
			icc.mem[icc.mem[icc.pc+3]] = p1 + p2
			icc.pc += 4
		} else if i.operation == Mul {
			icc.mem[icc.mem[icc.pc+3]] = p1 * p2
			icc.pc += 4
		} else if i.operation == Input {
			if icc.inputCounter >= len(userInputs) {
				break
			}
			icc.mem[icc.mem[icc.pc+1]] = userInputs[icc.inputCounter]
			icc.inputCounter++
			icc.pc += 2
		} else if i.operation == Output {
			icc.pc += 2
			icc.output = p1
			//fmt.Printf("Output %d\n", p1)
			break
		} else if i.operation == JumpIfTrue {
			if p1 != 0 {
				icc.pc = p2
			} else {
				icc.pc += 3
			}
		} else if i.operation == JumpIfFalse {
			if p1 == 0 {
				icc.pc = p2
			} else {
				icc.pc += 3
			}
		} else if i.operation == LessThan {
			if p1 < p2 {
				icc.mem[icc.mem[icc.pc+3]] = 1
			} else {
				icc.mem[icc.mem[icc.pc+3]] = 0
			}
			icc.pc += 4
		} else if i.operation == Equals {
			if p1 == p2 {
				icc.mem[icc.mem[icc.pc+3]] = 1
			} else {
				icc.mem[icc.mem[icc.pc+3]] = 0
			}
			icc.pc += 4
		} else {
			log.Panicf("Bad instruction %d\n", icc.mem[icc.pc])
		}

		i = icc.fetchInstruction()
	}
}

func NewComputer(input string) *IntCodeComputer {
	raw_instructions := strings.Split(input, ",")
	icc := &IntCodeComputer{}
	icc.pc = 0
	for _, ins := range raw_instructions {
		v, _ := strconv.Atoi(ins)
		icc.mem = append(icc.mem, v)
	}
	return icc
}
