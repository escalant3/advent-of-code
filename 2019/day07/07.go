package main

import (
	_ "embed"
	"fmt"
)

//go:embed 07_input.txt
var input string

type Amplifier struct {
	icc    *IntCodeComputer
	inputs []int
}

func thruster(program string, phaseSetting []int) int {
	amplifiers := make([]Amplifier, 5)
	for i := 0; i < 5; i++ {
		a := &amplifiers[i]
		if i == 0 {
			a.inputs = []int{phaseSetting[i], 0}
		} else {
			a.inputs = []int{phaseSetting[i], amplifiers[i-1].icc.output}
		}

		a.icc = NewComputer(program)
		a.icc.process(a.inputs)
	}
	return amplifiers[4].icc.output
}

func thrusterWithFeedback(program string, phaseSetting []int) int {
	amplifiers := make([]Amplifier, 5)
	for i := 0; i < 5; i++ {
		a := &amplifiers[i]
		if i == 0 {
			a.inputs = []int{phaseSetting[i], 0}
		} else {
			a.inputs = []int{phaseSetting[i]}
		}
		a.icc = NewComputer(program)
	}

	lastOutput := -1

	for {
		for i := 0; i < 5; i++ {
			a := &amplifiers[i]

			a.icc.process(a.inputs)
			if a.icc.output != amplifiers[(i+1)%5].inputs[len(amplifiers[(i+1)%5].inputs)-1] {
				amplifiers[(i+1)%5].inputs = append(amplifiers[(i+1)%5].inputs, a.icc.output)
			}
		}

		if lastOutput == amplifiers[4].icc.output {
			return amplifiers[4].icc.output
		}

		lastOutput = amplifiers[4].icc.output
	}
}

func part1(input string) int {
	maxSignal := 0
	combinations := getPossibleInputs([]int{0, 1, 2, 3, 4})
	for _, c := range combinations {
		output := thruster(input, c)
		if output > maxSignal {
			maxSignal = output
		}
	}
	return maxSignal
}

func part2(input string) int {
	maxSignal := 0
	combinations := getPossibleInputs([]int{5, 6, 7, 8, 9})
	for _, c := range combinations {
		output := thrusterWithFeedback(input, c)
		if output > maxSignal {
			maxSignal = output
		}
	}
	return maxSignal
}

func contains(s []int, e int) bool {
	for _, i := range s {
		if i == e {
			return true
		}
	}
	return false
}

func getPossibleInputs(elements []int) [][]int {
	pools := make([][]int, 0)
	for i := 0; i < len(elements); i++ {
		pools = append(pools, []int{elements[i]})
	}

	for i := 0; i < len(elements)-1; i++ {
		newPools := make([][]int, 0)
		for j := 0; j < len(elements); j++ {
			for k := 0; k < len(pools); k++ {
				if contains(pools[k], elements[j]) {
					continue
				}
				newPools = append(newPools, append(pools[k], elements[j]))
			}
		}
		pools = newPools
	}
	return pools
}

func main() {
	r := part1(input)
	fmt.Printf("Part 1: %d\n", r)
	r = part2(input)
	fmt.Printf("Part 2: %d\n", r)
}
