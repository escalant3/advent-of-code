package main

import (
	"fmt"
	"strings"
)

type SIF struct {
	width, height int
	layers        [][]string
}

func newImage(width, height int, data string) *SIF {
	sif := &SIF{width, height, make([][]string, len(data)/width/height)}
	for i := 0; i < len(sif.layers); i++ {
		for j := 0; j < height; j++ {
			offset := i*width*height + j*width
			sif.layers[i] = append(sif.layers[i], data[offset:offset+width])
		}
	}
	return sif
}

func (sif *SIF) digitInLayerCount(layerId int, digit string) int {
	digitsInLayer := 0
	for _, r := range sif.layers[layerId] {
		digitsInLayer += strings.Count(r, digit)
	}
	return digitsInLayer
}

func (sif *SIF) decode() []string {
	output := sif.layers[len(sif.layers)-1]
	for i := len(sif.layers) - 2; i >= 0; i-- {
		for row := 0; row < len(sif.layers[i]); row++ {
			for column := 0; column < len(sif.layers[i][row]); column++ {
				v := sif.layers[i][row][column]
				if v == '2' {
					continue
				}
				output[row] = output[row][0:column] + string(v) + output[row][column+1:]
			}
		}
	}

	return output
}

func (sif *SIF) print() {
	o := sif.decode()
	for _, l := range o {
		l = strings.ReplaceAll(l, "0", " ")
		l = strings.ReplaceAll(l, "1", "#")
		fmt.Println(l)
	}
}
