package main

import (
	_ "embed"
	"fmt"
	"math"
)

func part1(sif *SIF) int {
	v := math.MaxInt
	layerId := -1
	for i := range sif.layers {
		zeroesInLayer := sif.digitInLayerCount(i, "0")
		if zeroesInLayer < v {
			v = zeroesInLayer
			layerId = i
		}
	}

	return sif.digitInLayerCount(layerId, "1") * sif.digitInLayerCount(layerId, "2")
}

//go:embed 08_input.txt
var input string

func main() {
	//sif := newImage(3, 2, "123456789012")
	//sif := newImage(2, 2, "0222112222120000")
	sif := newImage(25, 6, input)
	r := part1(sif)
	fmt.Printf("Part 1: %d\n", r)
	fmt.Println("Part 2:")
	sif.print()
}
