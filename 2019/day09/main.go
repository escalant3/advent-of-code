package main

import (
	_ "embed"
	"fmt"
)

//go:embed 09_input.txt
var input string

func main() {
	icc := NewComputer(input)
	icc.process([]int{1})
	fmt.Printf("Part 1: %d\n", icc.output)

	icc = NewComputer(input)
	icc.process([]int{2})
	fmt.Printf("Part 2: %d\n", icc.output)
}
