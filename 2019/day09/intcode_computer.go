package main

import (
	_ "embed"
	"log"
	"strconv"
	"strings"
)

type Operation int
type ParamMode int

const (
	Add             Operation = 1
	Mul                       = 2
	Input                     = 3
	Output                    = 4
	JumpIfTrue                = 5
	JumpIfFalse               = 6
	LessThan                  = 7
	Equals                    = 8
	SetRelativeBase           = 9
	End                       = 99
)

const (
	Position  ParamMode = 0
	Immediate           = 1
	Relative            = 2
)

type Instruction struct {
	operation Operation
	p1mod     ParamMode
	p2mod     ParamMode
	p3mod     ParamMode
}

type Memory struct {
	internal     map[int]int
	relativeBase int
}

func NewMemory() *Memory {
	m := &Memory{}
	m.internal = make(map[int]int)
	return m
}

func (m *Memory) getValue(mode ParamMode, param int) int {
	param = m.internal[param]

	if mode == Immediate {
		return param
	}

	addr := param
	if mode == Relative {
		addr = m.relativeBase + param
	}
	if addr < 0 {
		panic("Trying to access negative memory")
	}
	return m.internal[addr]
}

func (m *Memory) initValue(addr, value int) {
	m.internal[addr] = value
}

type IntCodeComputer struct {
	mem          *Memory
	pc           int
	inputCounter int
	output       int
}

func (icc *IntCodeComputer) ReadValue(m ParamMode, n int) int {
	return icc.mem.getValue(m, n)
}

func (icc *IntCodeComputer) WriteValue(m ParamMode, n int, v int) {
	if m == Position {
		icc.mem.internal[icc.mem.internal[n]] = v
	} else if m == Relative {
		icc.mem.internal[icc.mem.internal[n]+icc.mem.relativeBase] = v
	}
}

func (icc *IntCodeComputer) fetchInstruction() *Instruction {
	raw := icc.mem.getValue(Immediate, icc.pc)
	i := &Instruction{}
	i.operation = Operation(raw % 100)
	i.p1mod = ParamMode(raw % 1000 / 100)
	i.p2mod = ParamMode(raw % 10000 / 1000)
	i.p3mod = ParamMode(raw % 100000 / 10000)
	return i
}

func (icc *IntCodeComputer) process(userInputs []int) {

	i := icc.fetchInstruction()

	for i.operation != End {
		if i.operation == Add {
			icc.WriteValue(i.p3mod, icc.pc+3, icc.ReadValue(i.p1mod, icc.pc+1)+icc.ReadValue(i.p2mod, icc.pc+2))
			icc.pc += 4
		} else if i.operation == Mul {
			icc.WriteValue(i.p3mod, icc.pc+3, icc.ReadValue(i.p1mod, icc.pc+1)*icc.ReadValue(i.p2mod, icc.pc+2))
			icc.pc += 4
		} else if i.operation == Input {
			if icc.inputCounter >= len(userInputs) {
				break
			}
			icc.WriteValue(i.p1mod, icc.pc+1, userInputs[icc.inputCounter])
			icc.inputCounter++
			icc.pc += 2
		} else if i.operation == Output {
			icc.output = icc.ReadValue(i.p1mod, icc.pc+1)
			icc.pc += 2
			//fmt.Printf("Output %d\n", icc.output)
		} else if i.operation == JumpIfTrue {
			p1 := icc.ReadValue(i.p1mod, icc.pc+1)
			p2 := icc.ReadValue(i.p2mod, icc.pc+2)
			if p1 != 0 {
				icc.pc = p2
			} else {
				icc.pc += 3
			}
		} else if i.operation == JumpIfFalse {
			p1 := icc.ReadValue(i.p1mod, icc.pc+1)
			p2 := icc.ReadValue(i.p2mod, icc.pc+2)
			if p1 == 0 {
				icc.pc = p2
			} else {
				icc.pc += 3
			}
		} else if i.operation == LessThan {
			p1 := icc.ReadValue(i.p1mod, icc.pc+1)
			p2 := icc.ReadValue(i.p2mod, icc.pc+2)
			if p1 < p2 {
				icc.WriteValue(i.p3mod, icc.pc+3, 1)
			} else {
				icc.WriteValue(i.p3mod, icc.pc+3, 0)
			}
			icc.pc += 4
		} else if i.operation == Equals {
			p1 := icc.ReadValue(i.p1mod, icc.pc+1)
			p2 := icc.ReadValue(i.p2mod, icc.pc+2)
			if p1 == p2 {
				icc.WriteValue(i.p3mod, icc.pc+3, 1)
			} else {
				icc.WriteValue(i.p3mod, icc.pc+3, 0)
			}
			icc.pc += 4
		} else if i.operation == SetRelativeBase {
			icc.mem.relativeBase += icc.ReadValue(i.p1mod, icc.pc+1)
			icc.pc += 2
		} else {
			log.Panicf("Bad instruction %d\n", icc.mem.getValue(Immediate, icc.pc))
		}

		i = icc.fetchInstruction()
	}
}

func NewComputer(input string) *IntCodeComputer {
	raw_instructions := strings.Split(input, ",")
	icc := &IntCodeComputer{}
	icc.pc = 0
	icc.mem = NewMemory()
	for i, ins := range raw_instructions {
		v, _ := strconv.Atoi(ins)
		icc.mem.initValue(i, v)
	}
	return icc
}
