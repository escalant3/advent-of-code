#!/usr/bin/env python3

def process(input):
    instructions = [int(x) for x in input.split(',')]
    instruction_pointer = 0

    while instructions[instruction_pointer] != 99:
        if instructions[instruction_pointer] == 1:
            instructions[instructions[instruction_pointer+3]] = instructions[instructions[instruction_pointer+1]] + instructions[instructions[instruction_pointer+2]]
        elif instructions[instruction_pointer] == 2:
            instructions[instructions[instruction_pointer+3]] = instructions[instructions[instruction_pointer+1]] * instructions[instructions[instruction_pointer+2]]
        else:
            raise ValueError('Unknown instruction %d' % instructions[instruction_pointer])

        instruction_pointer += 4

    return ','.join(map(str, instructions))

def set_program_input(program, noun, verb):
    instructions = program.split(',')
    instructions[1] = noun
    instructions[2] = verb
    return ','.join(instructions)

def reset_program():
    with open('02_input.txt') as f:
        return f.readline()

def part1():
    input = reset_program()
    program = set_program_input(input, '12', '2')
    print('Program output is %s' % process(program))

def part2():
    for noun in range(100):
        for verb in range(100):
            input = reset_program()
            program = set_program_input(input, str(noun), str(verb))
            output = process(program).split(',')[0]
            if output == '19690720':
                print('The code is %d' % (100 * noun + verb))
                return

def test():
    assert process('1,0,0,0,99') == '2,0,0,0,99'
    assert process('2,3,0,3,99') == '2,3,0,6,99'
    assert process('2,4,4,5,99,0') == '2,4,4,5,99,9801'
    assert process('1,1,1,4,99,5,6,0,99') == '30,1,1,4,2,5,6,0,99'

if __name__ == '__main__':
    test()
    part1()
    part2()