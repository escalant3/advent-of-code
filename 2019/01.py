#!/usr/bin/env python3

def get_fuel_required(mass):
    return mass // 3 - 2

def get_fuel_required_including_fuel(mass):
    total_fuel = mass // 3 - 2
    if total_fuel <= 0:
        return 0
    return total_fuel + get_fuel_required_including_fuel(total_fuel)

def part1():
    with open('01_input.txt') as f:
        per_module_fuel = [get_fuel_required(int(ship_module_mass)) for ship_module_mass in f]
    total_fuel = sum(per_module_fuel)
    print('Required fuel is %d' % total_fuel)
    return total_fuel

def part2():
    with open('01_input.txt') as f:
        per_module_fuel = [get_fuel_required_including_fuel(int(ship_module_mass)) for ship_module_mass in f]
    total_fuel = sum(per_module_fuel)
    print('Required fuel including fuel is %d' % total_fuel)
    return total_fuel

def test():
    assert get_fuel_required(12) == 2
    assert get_fuel_required(14) == 2
    assert get_fuel_required(1969) == 654
    assert get_fuel_required(100756) == 33583


if __name__ == '__main__':
    test()
    part1()
    part2()