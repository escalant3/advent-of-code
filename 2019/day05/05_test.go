package main

import "testing"

func TestInstructionParse(t *testing.T) {
	var i *Instruction
	i = NewInstruction(1002)
	if i.operation != Mul || i.p1mod != Position {
		t.Errorf("Parsing error for %d", 1002)
	}
}

func TestMachine(t *testing.T) {
	memory := set_program_input("3,9,8,9,10,9,4,9,99,-1,8")
	r := process(memory, 8)
	if r != 1 {
		t.Error("Unexpected output")
	}

	memory = set_program_input("3,9,8,9,10,9,4,9,99,-1,8")
	r = process(memory, 9)
	if r != 0 {
		t.Error("Unexpected output")
	}

	memory = set_program_input("3,9,7,9,10,9,4,9,99,-1,8")
	r = process(memory, 1)
	if r != 1 {
		t.Error("Unexpected output")
	}

	memory = set_program_input("3,9,7,9,10,9,4,9,99,-1,8")
	r = process(memory, 9)
	if r != 0 {
		t.Error("Unexpected output")
	}

	memory = set_program_input("3,3,1108,-1,8,3,4,3,99")
	r = process(memory, 9)
	if r != 0 {
		t.Error("Unexpected output")
	}

	memory = set_program_input("3,3,1108,-1,8,3,4,3,99")
	r = process(memory, 8)
	if r != 1 {
		t.Error("Unexpected output")
	}

	memory = set_program_input("3,3,1107,-1,8,3,4,3,99")
	r = process(memory, 9)
	if r != 0 {
		t.Error("Unexpected output")
	}

	memory = set_program_input("3,3,1107,-1,8,3,4,3,99")
	r = process(memory, 5)
	if r != 1 {
		t.Error("Unexpected output")
	}

	memory = set_program_input("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9")
	r = process(memory, 5)
	if r != 1 {
		t.Error("Unexpected output")
	}

	memory = set_program_input("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9")
	r = process(memory, 0)
	if r != 0 {
		t.Error("Unexpected output")
	}

	memory = set_program_input("3,3,1105,-1,9,1101,0,0,12,4,12,99,1")
	r = process(memory, 5)
	if r != 1 {
		t.Error("Unexpected output")
	}

	memory = set_program_input("3,3,1105,-1,9,1101,0,0,12,4,12,99,1")
	r = process(memory, 0)
	if r != 0 {
		t.Error("Unexpected output")
	}

	memory = set_program_input("3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99")
	r = process(memory, 8)
	if r != 1000 {
		t.Error("Unexpected output")
	}
	memory = set_program_input("3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99")
	r = process(memory, 7)
	if r != 999 {
		t.Error("Unexpected output")
	}
	memory = set_program_input("3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99")
	r = process(memory, 9)
	if r != 1001 {
		t.Error("Unexpected output")
	}
}
