package main

import (
	_ "embed"
	"fmt"
	"log"
	"strconv"
	"strings"
)

type Operation int
type ParamMode int

const (
	Add         Operation = 1
	Mul                   = 2
	Input                 = 3
	Output                = 4
	JumpIfTrue            = 5
	JumpIfFalse           = 6
	LessThan              = 7
	Equals                = 8
	End                   = 99
)

const (
	Position  ParamMode = 0
	Immediate           = 1
)

type Instruction struct {
	operation Operation
	p1mod     ParamMode
	p2mod     ParamMode
}

func (i *Instruction) IsTwoParameterOperation() bool {
	return i.operation == JumpIfFalse || i.operation == JumpIfTrue
}

func (i *Instruction) IsThreeParameterOperation() bool {
	return i.operation == Add || i.operation == Mul || i.operation == LessThan || i.operation == Equals
}

func toInt(s string) int {
	v, _ := strconv.Atoi(s)
	return v
}

func NewInstruction(raw int) *Instruction {
	i := Instruction{}
	i.operation = Operation(raw % 100)
	i.p1mod = ParamMode(raw % 1000 / 100)
	i.p2mod = ParamMode(raw % 10000 / 1000)
	return &i
}

func process(mem []int, userInput int) int {
	pc := 0
	var lastOutput int

	i := NewInstruction(mem[pc])

	for i.operation != End {
		var p1, p2 int
		if i.p1mod == Immediate {
			p1 = mem[pc+1]
		} else {
			p1 = mem[mem[pc+1]]
		}
		if i.IsTwoParameterOperation() || i.IsThreeParameterOperation() {
			if i.p2mod == Immediate {
				p2 = mem[pc+2]
			} else {
				p2 = mem[mem[pc+2]]
			}
		}
		if i.operation == Add {
			mem[mem[pc+3]] = p1 + p2
			pc += 4
		} else if i.operation == Mul {
			mem[mem[pc+3]] = p1 * p2
			pc += 4
		} else if i.operation == Input {
			mem[mem[pc+1]] = userInput
			pc += 2
		} else if i.operation == Output {
			fmt.Printf("Program Output: %d\n", p1)
			lastOutput = p1
			pc += 2
		} else if i.operation == JumpIfTrue {
			if p1 != 0 {
				pc = p2
			} else {
				pc += 3
			}
		} else if i.operation == JumpIfFalse {
			if p1 == 0 {
				pc = p2
			} else {
				pc += 3
			}
		} else if i.operation == LessThan {
			if p1 < p2 {
				mem[mem[pc+3]] = 1
			} else {
				mem[mem[pc+3]] = 0
			}
			pc += 4
		} else if i.operation == Equals {
			if p1 == p2 {
				mem[mem[pc+3]] = 1
			} else {
				mem[mem[pc+3]] = 0
			}
			pc += 4
		} else {
			log.Panicf("Bad instruction %d\n", mem[pc])
		}

		i = NewInstruction(mem[pc])
	}

	return lastOutput
}

func set_program_input(input string) []int {
	raw_instructions := strings.Split(input, ",")
	instructions := []int{}
	for _, ins := range raw_instructions {
		v, _ := strconv.Atoi(ins)
		instructions = append(instructions, v)
	}

	return instructions
}

//go:embed 05_input.txt
var input string

func main() {
	memory := set_program_input(input)
	diagnosticCode := process(memory, 1)
	fmt.Printf("Part 1: %d\n", diagnosticCode)
	memory = set_program_input(input)
	diagnosticCode = process(memory, 5)
	fmt.Printf("Part 2: %d\n", diagnosticCode)
	// Wrong 10428552
}
