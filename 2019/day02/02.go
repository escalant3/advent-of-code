package main

import (
	_ "embed"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func process(instructions []int) {
	instruction_pointer := 0
	const ADD = 1
	const MUL = 2
	const END = 99

	for instructions[instruction_pointer] != END {
		if instructions[instruction_pointer] == 1 {
			instructions[instructions[instruction_pointer+3]] = instructions[instructions[instruction_pointer+1]] + instructions[instructions[instruction_pointer+2]]
		} else if instructions[instruction_pointer] == 2 {
			instructions[instructions[instruction_pointer+3]] = instructions[instructions[instruction_pointer+1]] * instructions[instructions[instruction_pointer+2]]
		} else {
			log.Panicf("Bad instruction %d\n", instructions[instruction_pointer])
		}

		instruction_pointer += 4
	}
}

func set_program_input(input string, noun, verb int) []int {
	raw_instructions := strings.Split(input, ",")
	instructions := []int{}
	for _, ins := range raw_instructions {
		v, _ := strconv.Atoi(ins)
		instructions = append(instructions, v)
	}

	instructions[1] = noun
	instructions[2] = verb

	return instructions
}

//go:embed 02_input.txt
var input string

func main() {
	memory := set_program_input(input, 12, 2)
	process(memory)
	fmt.Printf("Part 1: %d\n", memory[0])

	const PART_2_GOAL = 19690720
	for noun := 0; noun <= 99; noun++ {
		for verb := 0; verb <= 99; verb++ {
			memory = set_program_input(input, noun, verb)
			process(memory)
			if memory[0] == PART_2_GOAL {
				fmt.Printf("Part 2 %d\n", 100*noun+verb)
				os.Exit(0)
			}
		}
	}
	log.Panic("Solution for Part 2 not found!")
}
